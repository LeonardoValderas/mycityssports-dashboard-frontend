import FuseAnimate from '@fuse/core/FuseAnimate';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
import { darken } from '@material-ui/core/styles/colorManipulator';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import React, { useState } from 'react';
import JWTLogin from './JWTLogin';
import Constants from '../apps/utils/Constants';
import { getCountry } from '../../main/apps/utils/AuthUtils' 

const useStyles = makeStyles(theme => ({
	root: {
		background: `linear-gradient(to right, ${theme.palette.primary.dark} 0%, ${darken(
			theme.palette.primary.dark,
			0.5
		)} 100%)`,
		color: theme.palette.primary.contrastText
	}
}));


function Login() {
	const classes = useStyles();
	const country = getCountry()
	const [title, setTitle] = useState((country === Constants.BR) ? Constants.TITLE_PT : Constants.TITLE_ES);
	const [doLogin, setDoLogin] = useState((country === Constants.BR) ? Constants.DO_LOGIN_PT : Constants.DO_LOGIN_ES);
	const languageChanged = (lang) =>  { 
		if (lang === Constants.PT){
		 setTitle(Constants.TITLE_PT)
		 setDoLogin(Constants.DO_LOGIN_PT)   
		} else {
		 setTitle(Constants.TITLE_ES)   
		 setDoLogin(Constants.DO_LOGIN_ES)   
		}
	 }

	return (
		<div className={clsx(classes.root, 'flex flex-col flex-1 flex-shrink-0 p-24 md:flex-row md:p-0')}>
			<div className="flex flex-col flex-grow-0 items-center text-white text-center md:p-128 md:flex-shrink-0 md:flex-1  md:text-left">
			{/* <div className="flex flex-col flex-grow-0 items-center text-white p-16 text-center md:p-128 md:items-start md:flex-shrink-0 md:flex-1 md:text-left"> */}
				<FuseAnimate animation="transition.expandIn">
					<img className="w-1/3 mb-32" src="assets/images/logos/MCS_W.svg" alt="logo" />
				</FuseAnimate>

				{/* <FuseAnimate animation="transition.slideUpIn" delay={300}>
					<Typography variant="h3" color="inherit" className="font-light">
						My City's Sport 
					</Typography>
				</FuseAnimate> */}

				<FuseAnimate delay={400}>
					<Typography variant="h5" color="inherit" className="max-w-412 mt-16">
						{title}
					</Typography>
				</FuseAnimate>
			</div>

			<FuseAnimate animation={{ translateX: [0, '100%'] }}>
				<Card className="w-full max-w-512 mx-auto m-16 md:m-0" square>
					<CardContent className="flex flex-col items-center justify-center p-32 md:p-48 md:pt-128 ">
						<Typography variant="h6" className="text-center md:w-full mb-48">
							{doLogin}
						</Typography>
						<JWTLogin 
						languageChanged={languageChanged}/>
					</CardContent>
				</Card>
			</FuseAnimate>
		</div>
	);
}

export default Login;
