
const docData = [
	{
		id: '1',
		title: 'Fixture',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
				'<p><b>Fecha</b></p>\n<p>Es el número de fecha del fixture a ingresar. Este dato es obligatorio.\n</p>\n' + 
				'<p><b>Dia y Hora</b></p>\n<p>Es el dia y la hora del fixture a ingresar. Este dato es obligatorio.\n</p>\n' + 
				'<p><b>Torneo</b></p>\n<p>Es el torneo del fixture a ingresar. Este dato es obligatorio.\n</p>\n' + 
				'<p><b>Deporte</b></p>\n<p>Es el deporte del fixture a ingresar. Este dato es obligatorio.\n</p>\n' + 
				'<p><b>División</b></p>\n<p>Es la división del fixture a ingresar. Este dato es obligatorio.\n</p>\n' +
				'<p><b>Zona/Grupo</b></p>\n<p>Es zona/grupo del fixture a ingresar. Este dato es obligatorio solo para torneos de tipo Grupo/Zona.\n</p>\n' +
				'<p><b>Instancia</b></p>\n<p>Es la instancia final del fixture a ingresar. Este dato no es obligatorio.\n</p>\n' +
				'<p><b>Sub Divisiones</b></p>\n<p>Es la sub división del fixture a ingresar. Este dato es obligatorio si el torneo posee sub divisiones.\n</p>\n' +
				'<p><b>Equipo Local</b></p>\n<p>Es el equipo local del fixture a ingresar. Este dato es obligatorio y no puede ser igual al equipo visitante.\n</p>\n' +
				'<p><b>Equipo Visitante</b></p>\n<p>Es el equipo visitante del fixture a ingresar. Este dato es obligatorio y no puede ser igual al equipo local.\n</p>\n' + 
				'<p><b>Campo de Juego</b></p>\n<p>Es el lugar donde se jugará el fixture a ingresar. Este dato es obligatorio.\n</p>\n' + 
				'<p><b>Comentario</b></p>\n<p>Es el comentario en caso de existir alguna aclaración. Este dato no es obligatorio y no puede superar los 100 caracteres.\n</p>\n' +
				'<p><b>Fuente</b></p>\n<p>Es la indicación del proveedor de la información, ya sea, sitio web, nombre de la institución, etc. Este dato no es obligatorio y no puede superar los 50 caracteres.\n</p>\n' + 
				'<p><b>Juego Finalizado</b></p>\n<p>Es el estado del fixture. Este dato es obligatorio y las opciones disponibles son: finalizado/no finalizado. Por default este campo es no finalizado y solo sera editable cuando se ingresen los resultos.\n</p>\n' +
				'<p><b>Árbitros</b></p>\n<p>Es/Son el/los árbitro/s y función/es del fixture. Este dato no es obligatorio.\n</p>\n' +
				'<p><b>Resultado Local</b></p>\n<p>Es el resultado del equipo local del fixture. Este dato no es obligatorio.\n</p>\n' +
				'<p><b>Resultado Visitante</b></p>\n<p>Es el resultado del equipo visitante del fixture. Este dato no es obligatorio.\n</p>\n' +
				'<p><b>Resultado Penales Local</b></p>\n<p>Es el resultado penales del equipo local del fixture. Este dato no es obligatorio y sera editable cuando el resultado local y visitante son iguales.\n</p>\n' +
				'<p><b>Resultado Penales Visitante</b></p>\n<p>Es el resultado penales del equipo visitante del fixture. Este dato no es obligatorio y sera editable cuando el resultado local y visitante son iguales.\n</p>\n' +
				'<p><b>Goleadores</b></p>\n<p>Es/Son el/los goleador/es del fixture. Puede adicionar el minuto de cada gol. Estos datos no son obligatorios y estarán disponibles si los resultados están ingresados. \n</p>\n' +
				'<p><b>Tarjetas</b></p>\n<p>Es/Son la/las tarjeta/s del fixture. Puede adicionar el jugador que recibio la tarjeta y el minuto del evento. Estos datos no son obligatorios. \n</p>\n'
			},
			{
				id: '2',
				title: 'Como cargar un fixture?',
				content:
				'<p><b>Opción Fixtures</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Fixtures" y presionar el botón "Adicionar nuevo fixture".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Seleccionar la fecha, dia y hora, torneo, deporte, división, equipo local, equipo visitante, campo de juego. Estos campos son obligatorios. Selecionar zona/grupo, instancia final, sub division. Ingresar comentario, fuente, arbitro/s, resultado/s, goleador/es y tarjeta/s . Estos dados son opcionales, salvo zona/grupo que es obligatorio si el torneo es de tipo Grupo/Zona y sub division que es obligatorio si existen sub divisiones en el torneo seleccionado. Si al querer seleccionar un torneo no existen opciones por favor ver la sección "Torneo". Si al querer seleccionar un deporte no existen opciones por favor ver la sección "Mis Deportes" ó "Torneo". Si al querer seleccionar una división no existen opciones por favor ver la sección "División" ó "Torneo". Si al querer seleccionar una zona no existen opciones por favor ver la sección "Torneo - Zonas". Si al querer seleccionar una instance no existen opciones por favor ver la sección "Torneo". Si al querer seleccionar una sub división no existen opciones por favor ver la sección "Torneo". Si al querer seleccionar un equipo no existen opciones por favor ver la sección "Equipo". Si al querer seleccionar un campo de juego no existen opciones por favor ver la sección "Campo de Juego". Si al querer seleccionar un árbitro no existen opciones por favor ver la sección "Árbitro". Si al querer seleccionar una función no existen opciones por favor ver la sección "Árbitro - Función". Si al querer seleccionar un jugador no existen opciones por favor ver la sección "Juagdor". Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar un fixture?',
				content:
				'<p><b>Opción Fixtures</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Fixtures".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Seleccionar el torneo y click sobre el fixture a ser editado. Editar los campos deseados y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar fixture?',
				content:
				'<p><b>Opción Fixtures</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Fixtures".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de el/los fixture/s a ser eliminada/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			},
			{
				id: '5',
				title: 'Descripción de los datos solicitado "Fixtures CSV"',
				content:
					'<p><b>Desde</b></p>\n<p>Es la fecha de inicio de la descarga. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Hasta</b></p>\n<p>Es la fecha final de la descarga. Este dato es obligatorio.\n</p>\n' +	
					'<p><b>Torneo</b></p>\n<p>Es el torneo a descargar. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Deporte</b></p>\n<p>Es el deporte a descargar. Este dato es obligatorio.\n</p>\n' +
					'<p><b>División</b></p>\n<p>Es la división a descargar. Este dato es obligatorio.\n</p>\n'
			},
			{
				id: '6',
				title: 'Como descargar Fixtures CSV?',
				content:
				'<p><b>Opción Fixtures - Fixtures CSV</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Fixtures - Fixtures CSV".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Seleccionar fecha de inicio, fecha final, torneo, deporte y división. Estos campos son obligatorios. Si al querer seleccionar un torneo no existen opciones por favor ver la sección "Torneo". Si al querer seleccionar un deporte no existen opciones por favor ver la sección "Mis Deportes" y/ó "Torneo". Si al querer seleccionar una división no existen opciones por favor ver la sección "Division" y/ó "Torneo". Por último presionar el botón "Procesar" y luego "Bajar"\n</p>\n'
			}
		]
	},
	{
		id: '2',
		title: 'Tabla de Posiciones',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
					'<p><b>Pts Ajustes</b></p>\n<p>Es la modificación de los puntos de un equipo. Este dato no es obligatorio y el valor ingresado sera restado de los puntos totales.\n</p>\n' + 
					'<p><b>Comentario</b></p>\n<p>Es el comentario en caso de existir alguna aclaración. Este dato no es obligatorio.\n</p>\n' +
					'<p><b>Fuente</b></p>\n<p>Es la indicación del proveedor de la información, ya sea, sitio web, nombre de la institución, etc. Este dato no es obligatorio y no puede superar los 50 caracteres.\n</p>\n' 
			},
			{
				id: '2',
				title: 'Como cargar una tabla de posiciones?',
				content:
				'<p>La tabla de posiciones se carga automaticamente con los datos de los partidos finalizados.\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar un tabla de posiciones?',
				content:
				'<p><b>Opción Tabla de Posiciones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Tabla de Posiciones".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre la tabla de posiciones a ser editada. Editar los campos deseados y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar un tabla de posiciones?',
				content:
				'<p><b>Opción Tabla de Posiciones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Tabla de Posiciones".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de la/las tabla/s de posiciones a ser eliminada/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			},
			{
				id: '5',
				title: 'Descripción de los datos solicitado "Tabla de Posiciones CSV"',
				content:
					'<p><b>Torneo</b></p>\n<p>Es el torneo a descargar. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Deporte</b></p>\n<p>Es el deporte a descargar. Este dato es obligatorio.\n</p>\n' +
					'<p><b>División</b></p>\n<p>Es la división a descargar. Este dato es obligatorio.\n</p>\n'
			},
			{
				id: '6',
				title: 'Como descargar una tabla de posiciones CSV?',
				content:
				'<p><b>Opción Tabla de Posiciones - Tabla de Posiciones CSV</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Tabla de Posiciones - Tabla de Posiciones CSV".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Seleccionar el torneo, deporte y división. Estos campos son obligatorios. Si al querer seleccionar un torneo no existen opciones por favor ver la sección "Torneo". Si al querer seleccionar un deporte no existen opciones por favor ver la sección "Mis Deportes" y/ó "Torneo". Si al querer seleccionar una división no existen opciones por favor ver la sección "Division" y/ó "Torneo". Por último presionar el botón "Procesar" y luego "Bajar"\n</p>\n'
			}
		]
	},
	{
		id: '3',
		title: 'Torneo',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
					'<p><b>Tipo de torneo</b></p>\n<p>Es el tipo de torneo a ingresar. Este dato es obligatorio y las opciones son: 1) Puntos, cuando el torneo se disputa "todos contra todos", por ejemplo: Apertura, Clausura. 2) Grupo/Zona: Cuando el torneo posee zonas ó grupos, por ejemplo: Mundial.\n</p>\n' + 
					'<p><b>Torneo</b></p>\n<p>Es el nombre del torneo a ingresar. Este dato es obligatorio y no puede superar los 50 caracteres.\n</p>\n' + 
					'<p><b>Año</b></p>\n<p>Es el año en el que el torneo sera inicializado. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Puntos para el ganador</b></p>\n<p>Es la cantidad de puntos que el ganador de un juego recibirá. Este dato es obligatorio y por default es 3.\n</p>\n' + 
					'<p><b>Deportes</b></p>\n<p>Es/Son el/los deporte/s que el torneo incluira. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Divisiones</b></p>\n<p>Es/Son la/s división/es que el torneo incluira. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Sub Divisiones</b></p>\n<p>Es/Son la/s sub división/es que el torneo incluira. Este dato no es obligatorio.\n</p>\n' +
					'<p><b>Zonas del torneo</b></p>\n<p>Es/Son la/s zona/s que el torneo incluira. Este dato es obligatorio solo para torneos de tipo Grupo/Zona.\n</p>\n' +
					'<p><b>Instancias del torneo</b></p>\n<p>Es/Son la/s instancias/s finales que el torneo incluira. Este dato no es obligatorio.\n</p>\n'
			},
			{
				id: '2',
				title: 'Como cargar un torneo?',
				content:
				'<p><b>Opción Torneos</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Torneos" y presionar el botón "Adicionar nuevo torneo".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Selecionar el tipo de torneo, ingresar nombre, año y puntos para el ganador. Seleccionar deporte/s, división/es. Estos campos son obligatorios. Ingresar sub division/es, seleccionar zonas/grupos e instancias finales. Estos dados son opcionales, salvo zonas/grupos que es obligatorio si el torneo es de tipo Grupo/Zona. Si al querer seleccionar un deporte no existen opciones por favor ver la sección "Mis Deportes". Si al querer seleccionar una división no existen opciones por favor ver la sección "División". Si al querer seleccionar una zona no existen opciones por favor ver la sección Torneo - Zonas". Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar un torneo?',
				content:
				'<p><b>Opción Torneos</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Torneos".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre el torneo a ser editada. Editar los campos deseados y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar un torneo?',
				content:
				'<p><b>Opción Torneos</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Torneos".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de la/las torneo/s a ser eliminada/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			},
			{
				id: '5',
				title: 'Descripción de los datos solicitado "Zonas/Grupos"',
				content:
					'<p><b>Zona/Grupo</b></p>\n<p>Es el nombre de la zona ó grupo a ingresar. Este dato es obligatorio y no puede superar los 80 caracteres.\n</p>\n'
			},
			{
				id: '6',
				title: 'Como cargar una zona/grupo?',
				content:
				'<p><b>Opción Torneos - Zonas/Grupos</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Torneos - Zonas/Grupos" y presionar el botón "Adicionar nueva zona/grupo".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar el nombre de la zona ó grupo. Este campo es obligatorio. Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '7',
				title: 'Como actualizar una zona/grupo?',
				content:
				'<p><b>Opción Torneos - Zonas/Grupos</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Torneos - Zonas/Grupos".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre la zona/grupo a ser editado. Editar el nombre y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '8',
				title: 'Como eliminar una zona/grupo?',
				content:
				'<p><b>Opción Funciones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Arbitros-Funciones".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de la/las zona/s ó grupo/s a ser eliminada/s ó /eliminado/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			}
		]
	},
	{
		id: '4',
		title: 'Equipo',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
					'<p><b>Nombre</b></p>\n<p>Es el nombre del equipo a ingresar. Este dato es obligatorio y no puede superar los 60 caracteres.\n</p>\n'+ 
					'<p><b>Deporte</b></p>\n<p>Es el deporte donde ese equipo desarrolla sus actividades. Si bien el equipo puede existir en varios tipos de deportes no siempre es administrado por la misma comisión/grupo y algunas caracteristicas pueden cambiar, por ejemplo: el escudo. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Fuente</b></p>\n<p>Es la indicación del proveedor de la información, ya sea, sitio web, nombre de la institución, etc. Este dato no es obligatorio y no puede superar los 50 caracteres.\n</p>\n' + 
					'<p><b>Escudo</b></p>\n<p>Es el emblema que caracteriza el equipo. Se recomienda imagen de cuadrada de 128x128. Este dato no es obligatorio. \n</p>\n'
			},
			{
				id: '2',
				title: 'Como cargar un equipo?',
				content:
				'<p><b>Opción Equipos</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Equipos" y presionar el botón "Adicionar nuevo equipo".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar el nombre y seleccionar el tipo de deporte. Estos dos campos son obligatorios. Si al querer seleccionar un deporte no existen opciones por favor ver la sección "Mis Deportes". La Fuente y el Escudo son opcionales. Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar un equipo?',
				content:
				'<p><b>Opción Equipos</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Equipos".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre el equipo a ser editado. Editar ó adicionar los campos deseados, luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar un equipo?',
				content:
				'<p><b>Opción Equipos</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Equipos".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de el/los equipo/s a ser eliminado/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			}
		]
	},
	{
		id: '5',
		title: 'División',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
					'<p><b>División</b></p>\n<p>Es el nombre del la división a ingresar. Este dato es obligatorio y no puede superar los 50 caracteres.\n</p>\n'+ 
					'<p><b>Orden en la Lista</b></p>\n<p>Es la posición que tendrá esa división la lista de divisiones a ser exhibidas en el sistema y en las aplicaciones. Por ejemplo, el torneo "xyz" tiene 3 divisiones, Primera octava y cuarta. Si quiero que la división "Primera" sea exhibido en primero, cuarta en segundo y octava en tercer lugar, el valor de orden de "Primera" debe ser 1, "Cuarta", un número mayor a 1 y "Octava" un valor mayor a "Cuarta", " Este dato es obligatorio.\n</p>\n'
			},
			{
				id: '2',
				title: 'Como cargar una división?',
				content:
				'<p><b>Opción Divisiones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Divisiones" y presionar el botón "Adicionar nueva división".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar la división y orden en la lista. Estos campos son obligatorios. Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar un división?',
				content:
				'<p><b>Opción Divisiones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Divisiones".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre la división a ser editado. Editar ó adicionar los campos deseados, luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar un división?',
				content:
				'<p><b>Opción Divisiones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Divisiones".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de la/las división/es a ser eliminada/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			}
		]
	},
	{
		id: '6',
		title: 'Jugador',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
					'<p><b>Nombre</b></p>\n<p>Es el nombre del jugador a ingresar. Este dato es obligatorio y no puede superar los 100 caracteres.\n</p>\n'+ 
					'<p><b>No Documento</b></p>\n<p>Es el ID del jugador. Este dato es obligatorio y no puede superar los 20 caracteres.\n</p>\n' +
					'<p><b>No Socio</b></p>\n<p>Es número de socio del jugador. Este dato es obligatorio y no puede superar los 20 caracteres.\n</p>\n' + 
					'<p><b>Fecha de nacimiento</b></p>\n<p>Es la fecha de nacimiento del jugador. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Deporte</b></p>\n<p>Es el deporte donde el equipo del jugador desarrolla sus actividades. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Equipo</b></p>\n<p>Es el equipo donde el jugador desarrolla sus actividades. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>División</b></p>\n<p>Es la división donde el jugador desarrolla sus actividades. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Fuente</b></p>\n<p>Es la indicación del proveedor de la información, ya sea, sitio web, nombre de la institución, etc. Este dato no es obligatorio y no puede superar los 50 caracteres.\n</p>\n' + 
					'<p><b>Foto</b></p>\n<p>Es la foto del jugador. Se recomienda imagen de cuadrada de 128x128. Este dato no es obligatorio. \n</p>\n'
				},
			{
				id: '2',
				title: 'Como cargar un jugador?',
				content:
				'<p><b>Opción Jugadores</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Jugadores" y presionar el botón "Adicionar nuevo jugador".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar el nombre, documento, número de socio, seleccionar la fecha de nacimiento, deporte, equipo y división. Estos campos son obligatorios. Si al querer seleccionar un deporte no existen opciones por favor ver la sección "Mis Deportes". Si al querer seleccionar un equipo no existen opciones por favor ver la sección "Equipo". Si al querer seleccionar una división no existen opciones por favor ver la sección "División". Fuente y foto son opcionales, Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar un jugador?',
				content:
				'<p><b>Opción Jugadores</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Jugadores".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre el jugador a ser editado. Editar ó adicionar los campos deseados, luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar un jugador',
				content:
				'<p><b>Opción Juagadores</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Jugadores".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de el/los juagdor/es a ser eliminado/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			}
		]
	},
	{
		id: '7',
		title: 'Arbitro',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado "Arbitro"',
				content:
					'<p><b>Nombre</b></p>\n<p>Es el nombre de árbitro a ingresar. Este dato es obligatorio y no puede superar los 80 caracteres.\n</p>\n'
			},
			{
				id: '2',
				title: 'Como cargar un árbitro?',
				content:
				'<p><b>Opción Arbitros</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Arbitros" y presionar el botón "Adicionar nuevo árbitro".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar el nombre del árbitro. Este campo es obligatorio. Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar un árbitro?',
				content:
				'<p><b>Opción Arbitros</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Arbitros".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre el árbitro a ser editado. Editar el nombre y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar un árbitro?',
				content:
				'<p><b>Opción Arbitros</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Arbitros".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de el/los árbitro/s a ser eliminado/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			},
			{
				id: '5',
				title: 'Descripción de los datos solicitado "Función"',
				content:
					'<p><b>Función</b></p>\n<p>Es el nombre de la función a ingresar. Este dato es obligatorio y no puede superar los 80 caracteres.\n</p>\n'
			},
			{
				id: '6',
				title: 'Como cargar una función?',
				content:
				'<p><b>Opción Funciones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Arbitros-Funciones" y presionar el botón "Adicionar nueva función".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar el nombre de la función. Este campo es obligatorio. Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '7',
				title: 'Como actualizar una función?',
				content:
				'<p><b>Opción Funciones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Arbitros-Funciones".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre la función a ser editado. Editar el nombre y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '8',
				title: 'Como eliminar una función?',
				content:
				'<p><b>Opción Funciones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Arbitros-Funciones".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de la/las función/es a ser eliminada/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			}
		]
	},
	{
		id: '8',
		title: 'Noticia',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
					'<p><b>Titulo</b></p>\n<p>Es el titulo de la noticia a ingresar. Este dato es obligatorio y no puede superar los 50 caracteres.\n</p>\n' + 
					'<p><b>Noticia</b></p>\n<p>Es el cuerpo de la noticia a ingresar. Este dato es obligatorio y no puede superar los 200 caracteres.\n</p>\n' + 
					'<p><b>Fecha de inicio</b></p>\n<p>Es la fecha en la que comienza a correr la noticia. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Fecha de expiración</b></p>\n<p>Es la fecha en la que termina la exhibición de la noticia. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Torneo</b></p>\n<p>Es el torneo donde la noticia será exhibida . Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Deportes</b></p>\n<p>Es/Son el/los deporte/s donde la noticia será exhibida. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Divisiones</b></p>\n<p>Es/Son la/s división/es donde la noticia será exhibida. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Sub Divisiones</b></p>\n<p>Es/Son la/s sub división/es donde la noticia será exhibida. Este dato es obligatorio si existen sub divisiones en el torneo.\n</p>\n' +
					'<p><b>Fuente</b></p>\n<p>Es la indicación del proveedor de la información, ya sea, sitio web, nombre de la institución, etc. Este dato no es obligatorio y no puede superar los 50 caracteres.\n</p>\n' + 
					'<p><b>Imagen</b></p>\n<p>Es la imagen de la noticia. Se recomienda imagen de cuadrada de 280x280. Este dato no es obligatorio. \n</p>\n'
			},
			{
				id: '2',
				title: 'Como cargar una noticia?',
				content:
				'<p><b>Opción Noticias</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Noticias" y presionar el botón "Adicionar nueva noticia".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar titulo, el cuerpo de la noticia, fecha de inicio, fecha de expiración, seleccionar un torneo, deporte/s, división/es, sub division/es.  Estos campos son obligatorios. Si al querer seleccionar un torneo no existen opciones por favor ver la sección "Torneo". Si al querer seleccionar un deporte no existen opciones por favor ver la sección "Mis Deportes" y/ó "Torneo". Si al querer seleccionar una división no existen opciones por favor ver la sección "Division" y/ó "Torneo". Si al querer seleccionar una sub división no existen opciones por favor ver la sección "Torneo". Fuente e imagen son opcionales, Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar una noticia?',
				content:
				'<p><b>Opción Noticias</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Noticias".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre la noticia a ser editada. Editar los campos deseados y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar una noticia?',
				content:
				'<p><b>Opción Noticias</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Noticias".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de la/las noticia/s a ser eliminada/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			}
		]
	},
	{
		id: '9',
		title: 'Campo de Juego',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
					'<p><b>Nombre</b></p>\n<p>Es el nombre del campo de juego a ingresar. Este dato es obligatorio y no puede superar los 150 caracteres.\n</p>\n'+ 
					'<p><b>Calle</b></p>\n<p>Es la calle donde se encuentra localizado el campo de juego. Este dato es obligatorio y no puede superar los 200 caracteres.\n</p>\n' +
					'<p><b>Número</b></p>\n<p>Es la altura de la calle donde se encuentra localizado el campo de juego. Este dato es obligatorio y no puede superar los 10 caracteres.\n</p>\n' + 
					'<p><b>Barrio</b></p>\n<p>Es el barrio donde se encuentra localizado el campo de juego. Este dato es obligatorio y no puede superar los 200 caracteres.\n</p>\n' + 
					'<p><b>Fuente</b></p>\n<p>Es la indicación del proveedor de la información, ya sea, sitio web, nombre de la institución, etc. Este dato no es obligatorio y no puede superar los 50 caracteres.\n</p>\n' + 
					'<p><b>Punto en el Mapa</b></p>\n<p>Es la localización del campo de juego. Este dato es obligatorio. \n</p>\n'
				},
			{
				id: '2',
				title: 'Como cargar un campo de juego?',
				content:
				'<p><b>Opción Campo de Juego</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Campos de Juego" y presionar el botón "Adicionar nuevo Campo de Juego".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar el nombre, calle, número, barrio y posicionar un punto en el mapa. Estos campos son obligatorios. La fuente es opcional, Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar un campo de juego?',
				content:
				'<p><b>Opción Campo de Juego</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Campos de Juego".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre el campo de juego a ser editado. Editar ó adicionar los datos deseados, luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar un campo de juego?',
				content:
				'<p><b>Opción Campo de Juego</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Campos de Juego".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de el/los campo/s de juego a ser eliminado/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			}
		]
	},
	{
		id: '10',
		title: 'Sanción',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
					'<p><b>Torneo</b></p>\n<p>Es el torneo donde la sanción será exhibida . Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Deporte</b></p>\n<p>Es el deporte donde la sanción será exhibida. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Equipo</b></p>\n<p>Es el equipo donde el jugador sancionado desarrolla sus actividades. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>División</b></p>\n<p>Es la división donde el jugador sancionado desarrolla sus actividades. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Jugador</b></p>\n<p>Es el jugador que recibió la sanción. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Sub División</b></p>\n<p>Es la sub división deonde el jugador sancionado desarrolla sus actividades. Este dato es obligatorio si existen sub divisiones en el torneo.\n</p>\n' +	
					'<p><b>Movio/Aclaración</b></p>\n<p>Es la causa de la sanción a ingresar. Este dato es obligatorio y no puede superar los 100 caracteres.\n</p>\n' + 
					'<p><b>Sanción</b></p>\n<p>Es la sanción en sí a ingresar. Este dato es obligatorio y no puede superar los 50 caracteres.\n</p>\n' + 
					'<p><b>Expiración de la sanción</b></p>\n<p>Es la fecha en la que termina la exhibición de la sanción. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Fuente</b></p>\n<p>Es la indicación del proveedor de la información, ya sea, sitio web, nombre de la institución, etc. Este dato no es obligatorio y no puede superar los 50 caracteres.\n</p>\n' 
			},
			{
				id: '2',
				title: 'Como cargar una sanción?',
				content:
				'<p><b>Opción Sanciones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Sanciones" y presionar el botón "Adicionar nueva sanción".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Seleccionar torneo, deporte, equipo del jugador, división, jugador, y una sub division si existe. Ingresar el motivo, la sanción y seleccionar una fecha de expiración. Estos campos son obligatorios. Si al querer seleccionar un torneo no existen opciones por favor ver la sección "Torneo". Si al querer seleccionar un deporte no existen opciones por favor ver la sección "Mis Deportes" y/ó "Torneo". Si al querer seleccionar una división no existen opciones por favor ver la sección "Division" y/ó "Torneo". Si al querer seleccionar un jugador no existen opciones por favor ver la sección "Jugador". Si al querer seleccionar una sub división no existen opciones por favor ver la sección "Torneo". La fuente es opcional, Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar una sanción?',
				content:
				'<p><b>Opción Sanciones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Sanciones".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre la sanción a ser editada. Editar los campos deseados y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar una sanción?',
				content:
				'<p><b>Opción Sanciones</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Sanciones".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de la/las sanción/s a ser eliminada/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			},
			
		]
	},
	{
		id: '11',
		title: 'Ranking',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado',
				content:
					'<p><b>Torneo</b></p>\n<p>Es el torneo donde el ranking será exhibida. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Deporte</b></p>\n<p>Es el deporte donde el ranking será exhibida. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Equipo</b></p>\n<p>Es el equipo donde el jugador rankeando desarrolla sus actividades. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>División</b></p>\n<p>Es la división donde el jugador rankeando desarrolla sus actividades. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Jugador</b></p>\n<p>Es el jugador rankeando. Este dato es obligatorio.\n</p>\n' +
					'<p><b>Sub División</b></p>\n<p>Es la sub división donde el jugador rankeado desarrolla sus actividades. Este dato es obligatorio si existen sub divisiones en el torneo.\n</p>\n' +	
					'<p><b>Tipo de Ranking</b></p>\n<p>Es el tipo de ranking a ingresar. Este dato es obligatorio.\n</p>\n' + 
					'<p><b>Puntos</b></p>\n<p>Es la unidad de medida del ranking a ingresar. Este dato es obligatorio y no puede superar los 4 caracteres.\n</p>\n' + 
					'<p><b>Es Descendente</b></p>\n<p>Es el orden del ranking. Este dato es obligatorio y es descendente por default.\n</p>\n' +
					'<p><b>Comentario</b></p>\n<p>Es el comentario en caso de existir alguna aclaración. Este dato no es obligatorio.\n</p>\n' +
					'<p><b>Fuente</b></p>\n<p>Es la indicación del proveedor de la información, ya sea, sitio web, nombre de la institución, etc. Este dato no es obligatorio y no puede superar los 50 caracteres.\n</p>\n' 
			},
			{
				id: '2',
				title: 'Como cargar un ranking?',
				content:
				'<p><b>Opción Rankings</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Rankings" y presionar el botón "Adicionar nuevo ranking".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Seleccionar torneo, deporte, equipo del jugador, división, jugador, sub division si existe y el tipo de ranking. Ingresar los puntos y validar si el ranking es descendente ó no. Estos campos son obligatorios. Si al querer seleccionar un torneo no existen opciones por favor ver la sección "Torneo". Si al querer seleccionar un deporte no existen opciones por favor ver la sección "Mis Deportes" y/ó "Torneo". Si al querer seleccionar una división no existen opciones por favor ver la sección "Division" y/ó "Torneo". Si al querer seleccionar un jugador no existen opciones por favor ver la sección "Jugador". Si al querer seleccionar una sub división no existen opciones por favor ver la sección "Torneo". Si al querer seleccionar un tipo de ranking no existen opciones por favor ver la sección "Rankings-Tipo de Ranking". El comentario y la fuente son opcionales, Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '3',
				title: 'Como actualizar un ranking?',
				content:
				'<p><b>Opción Rankings</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Rankings".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre el ranking a ser editado. Editar los campos deseados y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '4',
				title: 'Como eliminar un ranking?',
				content:
				'<p><b>Opción Rankings</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Rankings".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de el/los ranking/s a ser eliminado/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			},
			{
				id: '5',
				title: 'Descripción de los datos solicitado "Tipo de Ranking"',
				content:
					'<p><b>Tipo de Ranking</b></p>\n<p>Es el tipo de ranking que engloba a los rankeados. Este dato es obligatorio y no puede superar los 80 caracteres.\n</p>\n'
			},
			{
				id: '6',
				title: 'Como cargar un tipo de ranking?',
				content:
				'<p><b>Opción Tipo de Ranking</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Tipo de Ranking" y presionar el botón "Adicionar nuevo tipo de ranking".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar el tipo de ranking. Este campo es obligatorio. Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '7',
				title: 'Como actualizar un tipo de ranking?',
				content:
				'<p><b>Opción Tipo de Ranking</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Tipo de Ranking".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre el tipo de ranking a ser editado. Editar el tipo de ranking y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '8',
				title: 'Como eliminar un tipo de ranking?',
				content:
				'<p><b>Opción Tipo de Ranking</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Tipo de Ranking".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de el/los tipo/s de ranking a ser eliminado/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			},
		]
	},
	{
		id: '12',
		title: 'Institucional (disponible para usuario administrador)',
		path: '/pages/knowledge-base',
		featuredSection: [
			{
				id: '1',
				title: 'Descripción de los datos solicitado "Mis Datos"',
				content:
					'<p><b>Nombre</b></p>\n<p>Es el nombre de la institutción. Este dato no es editable.\n</p>\n' + 
					'<p><b>Descripción</b></p>\n<p>Es un resumen acerca de la institución. Este dato no es obligatorio y no puede superar los 400 caracteres.\n</p>\n' + 
					'<p><b>Punto en el Mapa</b></p>\n<p>Es la localización de la institución. Este dato no es obligatorio. \n</p>\n' +
					'<p><b>Calle</b></p>\n<p>Es la calle donde se encuentra localizada la institución. Este dato no es obligatorio y no puede superar los 200 caracteres.\n</p>\n' +
					'<p><b>Número</b></p>\n<p>Es la altura de la calle donde se encuentra localizada la institución. Este dato no es obligatorio y no puede superar los 10 caracteres.\n</p>\n' + 
					'<p><b>Barrio</b></p>\n<p>Es el barrio donde se encuentra localizada la institución. Este dato no es obligatorio y no puede superar los 200 caracteres.\n</p>\n' + 
					'<p><b>Complemento</b></p>\n<p>Es una información adicional referido a la localización de la institución. Este dato no es obligatorio y no puede superar los 200 caracteres.\n</p>\n' + 
					'<p><b>Telefone</b></p>\n<p>Es el número de contacto fijo de la institución. Este dato no es obligatorio y no puede superar los 20 caracteres.\n</p>\n' + 
					'<p><b>WhatsApp</b></p>\n<p>Es el número de contacto via WhatsApp de la institución. Este dato no es obligatorio y no puede superar los 20 caracteres.\n</p>\n' + 
					'<p><b>Email</b></p>\n<p>Es la dirección de correo de la institución. Este dato no es obligatorio y no puede superar los 100 caracteres.\n</p>\n' + 
					'<p><b>Web</b></p>\n<p>Es es sitio web de la institución. Este dato no es obligatorio y no puede superar los 300 caracteres.\n</p>\n' + 
					'<p><b>Facebook</b></p>\n<p>Es la dirección de la red social Facebook de la institución. Este dato no es obligatorio y no puede superar los 300 caracteres.\n</p>\n' + 
					'<p><b>Instagram</b></p>\n<p>Es la dirección de la red social Instagram de la institución. Este dato no es obligatorio y no puede superar los 300 caracteres.\n</p>\n' + 
					'<p><b>Fuente</b></p>\n<p>Es la indicación del proveedor de la información, ya sea, sitio web, nombre de la institución, etc. Este dato no es obligatorio y no puede superar los 50 caracteres.\n</p>\n' +
					'<p><b>Logo</b></p>\n<p>Es la imagen que representa la institución. Se recomienda imagen de cuadrada de 280x280. Este dato no es obligatorio. \n</p>\n'
			},
			{
				id: '2',
				title: 'Como actualizar mis datos?',
				content:
				'<p><b>Opción Institucional - Mis Datos</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Institutcional - Mis Datos".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Editar los campos deseados y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '3',
				title: '"Mis deportes"',
				content:
					'<p><b>Deporte</b></p>\n<p>Es la lista de deportes disponibles.\n</p>\n'
			},
			{
				id: '4',
				title: 'Como seleccionar/deseleccionar mis deportes?',
				content:
				'<p><b>Opción Institucional - Mis Deportes</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Institucional - Mis Deportes".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Seleccionar ou deseleccionar el/los deporte/s. Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '5',
				title: 'Descripción de los datos solicitado "Mis Sponsors"',
				content:
				'<p><b>Nombre</b></p>\n<p>Es el nombre del sponsor a ingresar. Este dato es obligatorio y no puede superar los 200 caracteres.\n</p>\n' + 
				'<p><b>Punto en el Mapa</b></p>\n<p>Es la localización del sponsor a ingresar. Este dato es obligatorio. \n</p>\n' +
				'<p><b>Calle</b></p>\n<p>Es la calle donde se encuentra localizado el sponsor a ingresar. Este dato es obligatorio y no puede superar los 200 caracteres.\n</p>\n' +
				'<p><b>Número</b></p>\n<p>Es la altura de la calle donde se encuentra localizado el sponsor a ingresar. Este dato es obligatorio y no puede superar los 10 caracteres.\n</p>\n' + 
				'<p><b>Barrio</b></p>\n<p>Es el barrio donde se encuentra localizado el sponsor a ingresar. Este dato es obligatorio y no puede superar los 200 caracteres.\n</p>\n' + 
				'<p><b>Complemento</b></p>\n<p>Es una información adicional referido a la localización del sponsor a ingresar. Este dato no es obligatorio y no puede superar los 200 caracteres.\n</p>\n' + 
				'<p><b>Telefone</b></p>\n<p>Es el número de contacto fijo del sponsor a ingresar. Este dato no es obligatorio y no puede superar los 20 caracteres.\n</p>\n' + 
				'<p><b>WhatsApp</b></p>\n<p>Es el número de contacto via WhatsApp del sponsor a ingresar. Este dato no es obligatorio y no puede superar los 20 caracteres.\n</p>\n' + 
				'<p><b>Email</b></p>\n<p>Es la dirección de correo del sponsor a ingresar. Este dato no es obligatorio y no puede superar los 100 caracteres.\n</p>\n' + 
				'<p><b>Web</b></p>\n<p>Es es sitio web del sponsor a ingresar. Este dato no es obligatorio y no puede superar los 300 caracteres.\n</p>\n' + 
				'<p><b>Facebook</b></p>\n<p>Es la dirección de la red social Facebook del sponsor a ingresar. Este dato no es obligatorio y no puede superar los 300 caracteres.\n</p>\n' + 
				'<p><b>Instagram</b></p>\n<p>Es la dirección de la red social Instagram del sponsor a ingresar. Este dato no es obligatorio y no puede superar los 300 caracteres.\n</p>\n' + 
				'<p><b>Mensaje de apoyo</b></p>\n<p>Es el mensaje que el sponsor desea dar al publico en general. Este dato es no obligatorio y no puede superar los 400 caracteres.\n</p>\n' +
				'<p><b>Sponsor Activo</b></p>\n<p>Es el estado del sponsor a ingresar. Este dato es obligatorio y las opciones disponibles son: activo/no activo.\n</p>\n' +
				'<p><b>Logo</b></p>\n<p>Es la imagen que representa al sponsor a ingresar. Se recomienda imagen de cuadrada de 280x280. Este dato no es obligatorio. \n</p>\n'
			},
			{
				id: '6',
				title: 'Como cargar un sponsor?',
				content:
				'<p><b>Opción Institucional - Mis Sponsors</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Institucional - Mis Sponsors" y presionar el botón "Adicionar nuevo sponsor".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar el nombre, punto en el mapa, calle, número, barrio. Estos campos son obligatorios. Ingresar complemento, telefono, whatsapp, email, web, facebook instagram, mensaje de apoyo y logo. Estos datos son opcionales. Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '7',
				title: 'Como actualizar un sponsor?',
				content:
				'<p><b>Opción Institucional - Mis Sponsors</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Institucional - Mis Sponsors".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre el sponsor a ser editado. Editar los campos deseados y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '8',
				title: 'Como eliminar un sponsor?',
				content:
				'<p><b>Opción Institucional - Mis Sponsors</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Institucional - Mis Sponsors".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de el/los sponsor/s a ser eliminado/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			},
			{
				id: '9',
				title: 'Descripción de los datos solicitado "Mis Usuarios"',
				content:
				'<p><b>Nombre</b></p>\n<p>Es el nombre del usuario a ingresar. Este dato es obligatorio y no puede superar los 200 caracteres.\n</p>\n' + 
				'<p><b>Email</b></p>\n<p>Es la dirección de correo del usuario a ingresar. Este dato es obligatorio y no puede superar los 200 caracteres.\n</p>\n' + 
				'<p><b>Contraseña</b></p>\n<p>Es la clave del usuario a ingresar. Este dato es obligatorio y no puede superar los 20 caracteres. \n</p>\n' +
				'<p><b>Confirmar Contraseña</b></p>\n<p>Es la confirmación de la clave del usuario a ingresar. Este dato es obligatorio y debe ser igual a la contraseña. \n</p>\n' +
				'<p><b>Usuario Activo</b></p>\n<p>Es el estado del usuario a ingresar. Este dato es obligatorio y las opciones disponibles son: activo/no activo.\n</p>\n' +
				'<p><b>Foto</b></p>\n<p>Es la imagen del usuario a ingresar. Se recomienda imagen de cuadrada de 128x128. Este dato no es obligatorio. \n</p>\n'
			},
			{
				id: '10',
				title: 'Como cargar un usuario?',
				content:
				'<p><b>Opción Institucional - Mis Usuarios</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Institucional - Mis Usuarios" y presionar el botón "Adicionar nuevo usuario".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Ingresar el nombre, email, contraseña y confirmar contraseña. Estos campos son obligatorios. Ingresar una imagen. Este dato es opcional. Por último presionar el botón "Salvar"\n</p>\n'
			},
			{
				id: '11',
				title: 'Como actualizar un usuario?',
				content:
				'<p><b>Opción Institucional - Mis Usuarios</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Institucional - Mis Usuarios".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Click sobre el usuario a ser editado. Editar los campos deseados y luego presionar el botón "Actualizar". \n</p>\n'
			},
			{
				id: '12',
				title: 'Como eliminar un usuario?',
				content:
				'<p><b>Opción Institucional - Mis Usuarios</b></p>\n<p>Abrir el "Menu Lateral", seleccionar la opción "Institucional - Mis Usuarios".\n</p>\n'+ 
				'<p><b>Proceso</b></p>\n<p>Tildar el/los casillero/s de el/los usuario/s a ser eliminado/s, luego seleccionar la opción (...) en la parte superior de la lista y luego "Remover"\n</p>\n'
			},
		]
	},
];


export default docData;