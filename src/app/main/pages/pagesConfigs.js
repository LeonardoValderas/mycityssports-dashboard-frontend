import Error404PageConfig from './errors/404/Error404PageConfig';
import Error500PageConfig from './errors/500/Error500PageConfig';
import KnowledgeBasePageConfig from './knowledge-base/KnowledgeBaseConfig';


const pagesConfigs = [
	Error404PageConfig,
	Error500PageConfig,
	KnowledgeBasePageConfig
];

export default pagesConfigs;
