import FuseSplashScreen from '@fuse/core/FuseSplashScreen';
import React from 'react';

function Callback(props) {
	return <FuseSplashScreen />;
}

export default Callback;
