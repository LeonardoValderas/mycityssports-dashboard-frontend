import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FusePageCarded from '@fuse/core/FusePageCarded';
import IconButton from '@material-ui/core/IconButton';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import moment from 'moment/moment';
import _ from '@lodash';
import CircularProgress from '@material-ui/core/CircularProgress';
//import constants from '../../utils/Constants'
import Button from '@material-ui/core/Button';
import { isMinorOrSameDateNow } from '../../utils/DateValidator'
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../sanction-i18n/es';
import pt from '../sanction-i18n/pt';
i18next.addResourceBundle('es', 'sanction', es);
i18next.addResourceBundle('pt', 'sanction', pt);

function Sanction(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('sanction');
	const data = useSelector(({ sanctionApp }) => sanctionApp.sanction.data);
	const savedModel = useSelector(({ sanctionApp }) => sanctionApp.sanction.savedModel);
	const tournaments = useSelector(({ sanctionApp }) => sanctionApp.sanction.tournaments);
	const teamModels = useSelector(({ sanctionApp }) => sanctionApp.sanction.teams);
	const playerModels = useSelector(({ sanctionApp }) => sanctionApp.sanction.players);

	const [teams, setTeams] = useState(teamModels);
	const [sports, setSports] = useState([]);
	const [divisions, setDivisions] = useState([]);
	const [subDivisions, setSubDivisions] = useState([]);
	const [players, setPlayers] = useState(playerModels);
	const loading = useSelector(({ sanctionApp }) => sanctionApp.sanction.loading);
	const progress = useSelector(({ sanctionApp }) => sanctionApp.sanction.progress);
	const theme = useTheme();
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();

	useDeepCompareEffect(() => {
		dispatch(Actions.getTournaments());
		//dispatch(Actions.getSports());
		function updateDivisionState() {
			const { sanctionId } = routeParams;
			if (sanctionId === 'new') {
				dispatch(Actions.newSanction());
			} else {
				dispatch(Actions.getSanction(sanctionId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (teamModels) {
			setTeams(teamModels);
		}
	}, [teamModels]);

	// useEffect(() => {
	// 	if (divisionModels) {
	// 		setDivisions(divisionModels);
	// 	}
	// }, [divisionModels]);

	// useEffect(() => {
	// 	if (tournamentModels) {
	// 		setTournaments(tournamentModels);
	// 	}
	// }, [tournamentModels]);

	useEffect(() => {
		if (playerModels) {
			setPlayers(playerModels);
		}
	}, [playerModels]);

	useEffect(() => {
		if (savedModel) {
			setForm(data);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((data && !form) || (data && form && data._id !== form._id)) {
			if (routeParams.sanctionId !== 'new') {
				getDataFromFormData(data);
			}
			setForm(data);
		}
	}, [form, data, setForm]);

	function getDataFromFormData(data) {
		if (data.tournament && data.tournament !== undefined && data.tournament._id) {
			const id = data.tournament._id
			setTournamentSports(id)
			setTournamentDivisions(id)
			setTournamentSubDivisions(id)
		}
	}

	function cleanTournamentSubDivision() {
		setForm(
			_.set(
				{ ...form },
				'subDivision', ''
			)
		);
	}

	// function getDataFromFormData(data) {
	// 	dispatch(Actions.getTeamsForSportId(data.sport && data.sport._id));
	// 	dispatch(Actions.getDivisions());
	// 	dispatch(Actions.getPlayersForTeamIdAndDivisionId(data.team && data.team._id, data.division && data.division._id));
	// 	dispatch(Actions.getTournamentForDivisionId(data.division && data.division._id));
	// }

	function setTournamentDivisions(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setDivisions(tournament.divisions)
			return
		}
		setDivisions([])
	}

	function setTournamentSports(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setSports(tournament.sports)
			return
		}
		setSports([])
	}

	function setTournamentSubDivisions(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setSubDivisions(tournament.subDivisions)
			return
		}
		setSubDivisions([])
	}

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return form
			&& form.motive.length > 0
			&& form.sanction.length > 0
			&& form.sport
			&& form.team
			&& form.division
			&& form.tournament
			&& form.player
			&& subDivisionValidation()
			&& !isMinorOrSameDateNow(moment(form.expiration).format('MM/DD/YYYY'))
			&& !_.isEqual(data, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}


	function subDivisionValidation() {
		return _.isEmpty(subDivisions) ? true : form.subDivision
	}


	// function handleChipChangeTournament(value) {
	// 	if (!form.tournament || form.tournament._id !== value._id) {
	// 		setSports(value.sports)
	// 		setDivisions(value.divisions)
	// 		setSubDivisions(value.subDivisions)
	// 		setFormValue('tournament', value)
	// 	}
	// }

	function handleChipChange(value, name) {
		var isSubdivision = false

		if (_.isEqual(name, 'tournament') && (!form.tournament || form.tournament._id !== value._id)) {
			setSports(value.sports)
			setDivisions(value.divisions)
			setSubDivisions(value.subDivisions)
		} else if (_.isEqual(name, 'sport') && (!form.sport || form.sport._id !== value._id)) {
			form.team = null
			//form.division = null
			form.player = null
			setTeams([])
			//setDivisions([])
			setPlayers([])
			dispatch(Actions.getTeamsForSportId(value._id));
		} else if (_.isEqual(name, 'team') && (!form.team || form.team._id !== value._id)) {
			//	form.division = null
			form.player = null
			//setDivisions([])
			setPlayers([])
			if (form.division)
				dispatch(Actions.getPlayersForTeamIdAndDivisionId(value._id, form.division._id));
			//dispatch(Actions.getDivisions());
		} else if (_.isEqual(name, 'division') && (!form.division || form.division._id !== value._id)) {
			form.player = null
			setPlayers([])
			//form.tournament = null
			//setTournaments([])
			if (form.team)
				dispatch(Actions.getPlayersForTeamIdAndDivisionId(form.team._id, value._id));
			//dispatch(Actions.getTournamentForDivisionId(value._id));			
		} else if (_.isEqual(name, 'subDivision')){
			isSubdivision = true
		}

		setForm(
			_.set(
				{ ...form },
				name, isSubdivision ? value.value : value
			)
		);
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/sanction/sanctions"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">{t('SANCTIONS')}</span>
								</Typography>
							</FuseAnimate>
						</div>
						<CircularProgress color="secondary" style={{ display: progress ? '' : 'none' }} />
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.saveSanction(form, routeParams.sanctionId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.sanctionId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label={t('SANCTION')} />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<FuseChipSelect
										className="mt-16 md:mr-8 md:w-3/6 w-full"
										value={form.tournament}
										onChange={value => handleChipChange(value, 'tournament')}
										placeholder={t('TOURNAMENT_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => `${option.name} ${option.year}`}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('TOURNAMENT'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={tournaments || []}
									//	isMulti
									/>
									<FuseChipSelect
										className="mt-16 md:mr-8 md:w-3/6 w-full"
										value={form.sport}
										onChange={value => handleChipChange(value, 'sport')}
										placeholder={t('SPORT_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => `${option.name} (${option.gender.name})`}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('SPORT'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={sports || []}
									//	isMulti
									/>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<FuseChipSelect
										className="mt-16 md:w-3/6 w-full"
										value={form.team}
										onChange={value => handleChipChange(value, 'team')}
										placeholder={t('TEAM_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => option.name}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('TEAM'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={teams || []}
									//	isMulti
									/>
									<FuseChipSelect
										className="mt-16 md:ml-8 md:w-3/6 w-full"
										value={form.division}
										onChange={value => handleChipChange(value, 'division')}
										placeholder={t('DIVISION_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => option.name}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('DIVISION'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={divisions || []}
									//	isMulti
									/>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<FuseChipSelect
										className="mt-16 md:w-3/6 w-full"
										value={form.player}
										onChange={value => handleChipChange(value, 'player')}
										placeholder={t('PLAYER_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => option.name}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('PLAYER'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={players || []}
									//	isMulti
									/>
									<div className="flex mx-0 mt-16 md:ml-8 w-full md:w-3/6">
										<FuseChipSelect
											className="w-full"
											value={
												form.subDivision ?
													{
														value: form.subDivision,
														label: form.subDivision
													} : form.subDivision
											}
											onChange={value => handleChipChange(value, 'subDivision')}
											placeholder={t('SUB_DIVISION_SELECTION')}
											variant='fixed'
											required={!_.isEmpty(subDivisions)}
											//getOptionLabel={option => option}
											//getOptionValue={option => option}
											textFieldProps={{
												label: t('SUB_DIVISION'),
												InputLabelProps: {
													shrink: true
												},
												variant: 'outlined'
											}}
											options={subDivisions.map(t => ({ value: t, label: t })) || []}
											//options={subDivisions || []}
											isDisabled={progress}
										/>
										<IconButton
											onClick={() =>
												cleanTournamentSubDivision()
											}
											disabled={progress}
											className="p-2 h-24 mx-2 md:mx-8 mt-8"
										>
											<Icon>autorenew</Icon>
										</IconButton>
									</div>
								</div>

								<TextField
									className="mt-16"
									error={form.motive === ''}
									required
									label={t('MOTIVE')}
									autoFocus
									id="motive"
									name="motive"
									value={form.motive}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 100,
									}}
								/>

								<TextField
									className="mt-16"
									error={form.sanction === ''}
									required
									label={t('SANCTION')}
									//autoFocus
									id="sanction"
									name="sanction"
									value={form.sanction}
									onChange={handleChange}
									variant="outlined"
									fullWidth
									inputProps={{
										maxLength: 50,
									}}
								/>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										id="expiration"
										name="expiration"
										label={t('DATE_END')}
										type="date"
										className="mt-16 md:mr-8 md:w-2/6 w-full"
										InputLabelProps={{
											shrink: true
										}}
										inputProps={{
											min: moment(new Date()).format('YYYY-MM-DD')
										}}
										value={form.expiration}
										onChange={handleChange}
										variant="outlined"
									/>

									<TextField
										className="mt-16 md:w-4/6 w-full"
										label={t('SOURCE')}
										id="source"
										name="source"
										value={form.source}
										onChange={handleChange}
										variant="outlined"
										cols={8}
										fullWidth
										inputProps={{
											maxLength: 50,
										}}
										disabled={progress}
									/>
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('sanctionApp', reducer)(Sanction);
