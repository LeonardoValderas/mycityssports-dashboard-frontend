import Checkbox from '@material-ui/core/Checkbox';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';
import clsx from 'clsx';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../sanction-i18n/es';
import pt from '../sanction-i18n/pt';
i18next.addResourceBundle('es', 'sanction', es);
i18next.addResourceBundle('pt', 'sanction', pt);



const useStyles = makeStyles(theme => ({
	actionsButtonWrapper: {
		background: theme.palette.background.paper
	}
}));

function SanctionsTableHead(props) {
	const classes = useStyles(props);
	const { t } = useTranslation('sanction');
	const [selectedDivisionsMenu, setSelectedDivisionsMenu] = useState(null);

	const rows = [
		{
			id: 'image',
			align: 'left',
			disablePadding: true,
			label: '',
			colSpan: 1,
			sort: false
		},
		{
			id: 'player.name',
			align: 'left',
			disablePadding: false,
			label: t('NAME'),
			colSpan: 1,
			sort: true
		},
		{
			id: 'team.name',
			align: 'left',
			disablePadding: false,
			label: t('TEAM'),
			colSpan: 1,
			sort: true
		},
		{
			id: 'sport.name',
			align: 'left',
			disablePadding: false,
			label: t('SPORT'),
			colSpan: 1,
			sort: true
		},
		{
			id: 'tournament.name',
			align: 'left',
			disablePadding: false,
			label: t('TOURNAMENT'),
			colSpan: 3,
			sort: true
		},
		{
			id: 'division.name',
			align: 'left',
			disablePadding: false,
			label: t('DIVISION'),
			colSpan: 1,
			sort: true
		},
		{
			id: 'subDivision',
			align: 'left',
			disablePadding: false,
			label: t('SUB_DIVISION'),
			sort: true
		},
		{
			id: 'motive',
			align: 'left',
			disablePadding: false,
			label: t('MOTIVE'),
			sort: true
		},
		{
			id: 'sanction',
			align: 'left',
			disablePadding: false,
			label: t('SANCTION'),
			sort: true
		},
		{
			id: 'expiration',
			align: 'left',
			disablePadding: false,
			label: t('END'),
			sort: true
		}
	];

	const createSortHandler = property => event => {
		props.onRequestSort(event, property);
	};

	function openSelectedDivisionsMenu(event) {
		setSelectedDivisionsMenu(event.currentTarget);
	}

	function closeSelectedDivisionsMenu() {
		setSelectedDivisionsMenu(null);
	}

	function deleteAndCloseMenu(){
		closeSelectedDivisionsMenu()
        props.deleteSelectedItems()
	}
	return (
		<TableHead>
			<TableRow className="h-64">
				<TableCell padding="none" className="relative w-64 text-center">
					<Checkbox
						indeterminate={props.numSelected > 0 && props.numSelected < props.rowCount}
						checked={props.numSelected === props.rowCount}
						onChange={props.onSelectAllClick}
					/>
					
					{(props.numSelected > 0  && !props.cleanSelected) && (
						<div
							className={clsx(
								'flex items-center justify-center absolute w-64 top-0 ltr:left-0 rtl:right-0 mx-56 h-64 z-10',
								classes.actionsButtonWrapper
							)}
						>
							<IconButton
								aria-owns={selectedDivisionsMenu ? 'selectedDivisionsMenu' : null}
								aria-haspopup="true"
								onClick={openSelectedDivisionsMenu}
							>
								<Icon>more_horiz</Icon>
							</IconButton>
							<Menu
								id="selectedDivisionsMenu"
								anchorEl={selectedDivisionsMenu}
								open={Boolean(selectedDivisionsMenu)}
								onClose={closeSelectedDivisionsMenu}
							>
								<MenuList>
									<MenuItem
										onClick={() => {
											deleteAndCloseMenu();
										}}
									>
										<ListItemIcon className="min-w-40">
											<Icon>delete</Icon>
										</ListItemIcon>
										<ListItemText primary="Eliminar" />
									</MenuItem>
								</MenuList>
							</Menu>
						</div>
					)}
				</TableCell>
				{rows.map(row => {
					return (
						<TableCell
							key={row.id}
							align={row.align}
							padding={row.disablePadding ? 'none' : 'default'}
							colSpan={row.colSpan}
							sortDirection={props.order.id === row.id ? props.order.direction : false}
						>
							{row.sort && (
								<Tooltip
									title="Sort"
									placement={row.align === 'right' ? 'bottom-end' : 'bottom-start'}
									enterDelay={300}
								>
									<TableSortLabel
										active={props.order.id === row.id}
										direction={props.order.direction}
										onClick={createSortHandler(row.id)}
									>
										{row.label}
									</TableSortLabel>
								</Tooltip>
							)}
						</TableCell>
					);
				}, this)}
			</TableRow>
		</TableHead>
	);
}

export default SanctionsTableHead;
