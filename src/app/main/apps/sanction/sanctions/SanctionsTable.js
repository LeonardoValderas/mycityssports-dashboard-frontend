import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../store/actions';
import SanctionsTableHead from './SanctionsTableHead';
import FuseLoading from '@fuse/core/FuseLoading';
import moment from 'moment/moment';
import constants from '../../utils/Constants'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../sanction-i18n/es';
import pt from '../sanction-i18n/pt';
i18next.addResourceBundle('es', 'sanction', es);
i18next.addResourceBundle('pt', 'sanction', pt);

function SanctionsTable(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('sanction');
	const sanctions = useSelector(({ sanctionsApp }) => sanctionsApp.sanctions.data);
	const searchText = useSelector(({ sanctionsApp }) => sanctionsApp.sanctions.searchText);
	const pageRow = useSelector(({ sanctionsApp }) => sanctionsApp.sanctions.pageRow);
	const loading = useSelector(({ sanctionsApp }) => sanctionsApp.sanctions.loading);
	const cleanSelected = useSelector(({ sanctionsApp }) => sanctionsApp.sanctions.cleanSelected);
	const [clean, setClean] = useState(cleanSelected);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(sanctions);
	const [page, setPage] = useState(pageRow.page);
	const [rowsPerPage, setRowsPerPage] = useState(pageRow.row);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useEffect(() => {
		dispatch(Actions.getSanctions());
	}, [dispatch]);

	useEffect(() => {
		setClean(cleanSelected)
	}, [cleanSelected]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(sanctions, item => item.player.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(sanctions);
		}
	}, [sanctions, searchText]);

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setClean(false)
			setSelected(data.map(n => n._id));
			return;
		}
		setSelected([]);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function deleteSanctions() {
		handleClose()
		dispatch(Actions.deleteSanctions({ ids: selected }));
	}

	function deleteSelectedItems() {
		handleClickOpen()
	}

	function handleClick(item) {
		dispatch(Actions.setPageAndRow(page, rowsPerPage))
		props.history.push(`/apps/sanction/sanctions/${item._id}`);
	}

	function handleCheck(event, id) {
		setClean(false)
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	return (
		<div className="w-full flex flex-col">
			<div>
				<Dialog
					open={open}
					onClose={handleClose}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title">Atención</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							{`${t('DELETE_ITEM')} ${selected.length === 1 ? t('SINGLE_SANCTION') : t('PLURAL_SANCTION')}?`}
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose} color="primary">
							Cancelar
          				</Button>
						<Button onClick={deleteSanctions} color="primary" autoFocus>
							Eliminar
          				</Button>
					</DialogActions>
				</Dialog>
			</div>
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table className="min-w-xl" aria-labelledby="tableTitle">
					<SanctionsTableHead
						numSelected={selected.length}
						order={order}
						cleanSelected={clean}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={data.length}
						deleteSelectedItems={deleteSelectedItems}
					/>

					<TableBody>
						{_.orderBy(
							data,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											const orderId = order.id
											if (orderId && o !== null) {
												let filter = o
												const split = orderId.split('.')
												split.map(s => {
													const f = filter[s]
													if (f) {
														filter = f
													}
												})
												return filter;
											}
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n._id) !== -1;
								return (
									<TableRow
										className="h-64 cursor-pointer"
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n._id}
										selected={isSelected}
										onClick={event => handleClick(n)}
									>
										<TableCell className="w-64 text-center" padding="none">
											<Checkbox
												checked={isSelected}
												onClick={event => event.stopPropagation()}
												onChange={event => handleCheck(event, n._id)}
											/>
										</TableCell>

										<TableCell className="w-52" component="th" scope="row" padding="none">
											{(
												<img
													className="w-full block rounded"
													src={n.player.urlImage || constants.DEFAULT_URL_IMAGE}
													alt={'Imagen'}
												/>
											)}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.player.name}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.team.name}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.sport.name} {n.sport.gender.name}
										</TableCell>
										<TableCell colSpan={3} component="th" scope="row" >
											<Typography noWrap>
												{`${n.tournament.name} ${n.tournament.year}`}
											</Typography>

										</TableCell>
										<TableCell component="th" scope="row">
											{n.division.name}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.subDivision}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.motive}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.sanction}
										</TableCell>
										<TableCell component="th" scope="row">
											{moment(n.expiration).format('DD-MM-YYYY')}
										</TableCell>

									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>
			<div>
				<TablePagination
					className="overflow-hidden"
					component="div"
					count={data.length}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
}

export default withRouter(SanctionsTable);
