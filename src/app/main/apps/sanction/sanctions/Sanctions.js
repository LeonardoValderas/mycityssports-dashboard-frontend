import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import SanctionsHeader from './SanctionsHeader';
import SanctionsTable from './SanctionsTable';

function Sanctions() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<SanctionsHeader />}
			content={<SanctionsTable />}
			innerScroll
		/>
	);
}

export default withReducer('sanctionsApp', reducer)(Sanctions);
