
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import moment from 'moment/moment';
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_SANCTION = '[SANCTION APP] GET SANCTION';
export const GET_SANCTION_SPORTS = '[SANCTION APP] GET SANCTION SPORTS';
export const GET_SANCTION_TEAMS = '[SANCTION APP] GET SANCTION TEAMS';
export const GET_SANCTION_DIVISIONS = '[SANCTION APP] GET SANCTION DIVISIONS';
export const GET_SANCTION_TOURNAMENTS = '[SANCTION APP] GET SANCTION TOURNAMENTS';
export const GET_SANCTION_PLAYERS = '[SANCTION APP] GET SANCTION PLAYERS';
export const SAVE_SANCTION = '[SANCTION APP] SAVE SANCTION';
export const SET_LOADING_SANCTION = '[SANCTION APP] SET LOADING';
export const SET_PROGRESS_SANCTION = '[SANCTION APP] SET PROGRESS SANCTION';

const lang = i18next.language
const translate = translateInfo(lang)

export function getTournaments() {
	return dispatch => {
		const institutionId = getInstitutionId()
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/tournaments/institutions/${institutionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const dataForm = resp.data.models
			dispatch(setTournaments(dataForm))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function getTeamsForSportId(sportId) {
	return dispatch => {
		dispatch(setProgress(true))
		//dispatch(setLoading({ show: true, label: 'Cargando...' }))
		const institutionId = getInstitutionId()
		axios.get(`${translate.baseUrl}/teams/institutions/${institutionId}/sports/${sportId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
			const models = resp.data.models
			dispatch(setTeams(models || []))
		}).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setProgress(false))
        })
	}
}

export function getPlayersForTeamIdAndDivisionId(teamId, divisionId) {
	return dispatch => {
		dispatch(setProgress(true))
		//dispatch(setLoading({ show: true, label: 'Cargando...' }))
		const institutionId = getInstitutionId()
		axios.get(`${translate.baseUrl}/players/institutions/${institutionId}/teams/${teamId}/divisions/${divisionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
			const models = resp.data.models
			dispatch(setPlayers(models || []))
		}).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
			dispatch(setProgress(false))
			//dispatch(setLoading({ show: false, label: '' }))
        })
	}
}

export function getSanction(sanctionId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/sanctions/${sanctionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function setModel(model) {
	return {
		type: GET_SANCTION,
		payload: model
	}
}

export function setTeams(teams) {
	return {
		type: GET_SANCTION_TEAMS,
		payload: teams
	}
}

export function setTournaments(tournaments) {
	return {
		type: GET_SANCTION_TOURNAMENTS,
		payload: tournaments
	}
}

export function setPlayers(players) {
	return {
		type: GET_SANCTION_PLAYERS,
		payload: players
	}
}

export function saveSanction(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/sanctions/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newSanction() {
	const data = {
		sport: null,
		team: null,
		division: null,
		subDivision: null,
		tournament: null,
		player: null,
		motive:'',
		sanction:'',
		source:'',
		expiration: moment(new Date()).format('YYYY-MM-DD'),
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_SANCTION,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_SANCTION,
		payload: model
	};
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_SANCTION,
        payload: loading
    }
}

export function setProgress(loading) {
	return {
		type: SET_PROGRESS_SANCTION,
		payload: loading
	}
}