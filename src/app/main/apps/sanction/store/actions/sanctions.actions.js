import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_SANCTIONS = '[SANCTIONS APP] GET SANCTIONS';
export const SET_SANCTIONS_SEARCH_TEXT = '[SANCTIONS APP] SET SANCTIONS SEARCH TEXT';
export const SET_SANCTIONS_PAGE_NUMBER_ROW = '[SANCTIONS APP] SET SANCTIONS PAGE NUMBER ROW';
export const SET_LOADING = '[SANCTIONS APP] SET LOADING';
export const CLEAN_SELELCTED_SANCTION = '[SANCTIONS APP] CLEAN SELELCTED SANCTION';

const lang = i18next.language
const translate = translateInfo(lang)

export function getSanctions() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/sanctions/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_SANCTIONS,
        payload: models
    }
}

export function setPageAndRow(page, row) {
    return {
        type: SET_SANCTIONS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setSanctionsSearchText(event) {
    return {
        type: SET_SANCTIONS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteSanctions(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/sanctions/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getSanctions())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_SANCTION,
        payload: clean
    }
}
