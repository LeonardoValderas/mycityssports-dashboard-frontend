import { combineReducers } from 'redux';
import sanction from './sanction.reducer';
import sanctions from './sanctions.reducer';

const reducer = combineReducers({
	sanctions,
	sanction
});

export default reducer;
