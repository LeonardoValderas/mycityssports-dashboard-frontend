import * as Actions from '../actions';

const initialState = {
	data: null,
	//sports: [],
	teams: [],
	//divisions: [],
	tournaments: [],
	players: [],
	returnPage: false,
	loading: {
		show: false,
		label: ''
	},
	progress: false,
	savedModel: false
};

const sanctionReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_SANCTION: {
			return {
				...state,
				data: action.payload,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				returnPage: false,
				savedModel: false
			};
		}
		case Actions.GET_SANCTION_TOURNAMENTS: {
			return {
				...state,
				tournaments: action.payload,
				progress: false,
				savedModel: false
			};
		}
		// case Actions.GET_SANCTION_FORM: {
		// 	return {
		// 		...state,
		// 		sports: action.payload && action.payload.sports && action.payload.sports.sports || [],
		// 		divisions: action.payload && action.payload.divisions || [],
		// 		progress: false,
		// 		savedModel: false
		// 	};
		// }
		// case Actions.GET_SANCTION_SPORTS: {
		// 	return {
		// 		...state,
		// 		sports: action.payload,
		// 		progress: false,
		// 		savedModel: false
		// 	};
		// }
		case Actions.GET_SANCTION_TEAMS: {
			return {
				...state,
				teams: action.payload,
				savedModel: false,
				loading: {
					show: false,
					label: '',
				},
				progress: false,
			};
		}
		// case Actions.GET_SANCTION_DIVISIONS: {
		// 	return {
		// 		...state,
		// 		divisions: action.payload,
		// 		savedModel: false,
		// 		loading: {
		// 			show: false,
		// 			label: ''
		// 		},
		// 		progress: false,
		// 	};
		// }
		case Actions.GET_SANCTION_TOURNAMENTS: {
			return {
				...state,
				tournaments: action.payload,
				savedModel: false,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
			};
		}
		case Actions.GET_SANCTION_PLAYERS: {
			return {
				...state,
				players: action.payload,
				savedModel: false,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
			};
		}
		case Actions.SAVE_SANCTION: {
			return {
				...state,
				data: action.payload,
				returnPage: true,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				savedModel: true
			};
		}
		case Actions.SET_LOADING_SANCTION: {
			return {
				...state,
				loading: action.payload,
				savedModel: false,
				progress: false,
			};
		}
		case Actions.SET_PROGRESS_SANCTION: {
			return {
				...state,
				progress: action.payload,
				loading: {
					show: false,
					label: ''
				},
				savedModel: false
			};
		}
		default: {
			return state;
		}
	}
};

export default sanctionReducer;
