import React from 'react';
import { Redirect } from 'react-router-dom';

const SanctionsAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/sanction/sanctions/:sanctionId',
			component: React.lazy(() => import('./sanction/Sanction'))
		},
		{
			path: '/apps/sanction/sanctions',
			component: React.lazy(() => import('./sanctions/Sanctions'))
		},
		{
			path: '/apps/sanction',
			component: () => <Redirect to="/apps/sanction/sanctions" />
		}
	]
};

export default SanctionsAppConfig;
