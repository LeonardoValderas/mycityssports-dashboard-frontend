import React from 'react';
//import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
//import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
//import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import MaterialTable from 'material-table';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

function Row(props) {
  const { row, titles, tableDataTitle } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root} onClick={() => setOpen(!open)}>
      <TableCell style={{ width: 5 }} size='small' component="td" scope="row" align="left">
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
				<TableCell align="left">
          {row.tournamentInstance.name}
        </TableCell>
      </TableRow>
  
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <MaterialTable
              title={`${row.tournamentInstance.name} - ${tableDataTitle}`}
              columns={titles}
              data={row.tableDataInstance || []}
              localization={{
                header: {
                  actions: 'Acciones'
                },
                toolbar: {
                  searchPlaceholder: 'Buscar'
                },
                pagination: {
                  labelRowsSelect: 'filas'
                },
                body: {
                  editRow: {
                    saveTooltip: 'Salvar',
                    cancelTooltip: 'Cancelar'
                  },
                  editTooltip: 'Editar'
                }
              }}
              options={{
                actionsColumnIndex: titles && titles.length
              }}

              // editable={{
              //   onRowUpdate: (newData, oldData) =>
              //     new Promise((resolve) => {
              //       setTimeout(() => {
              //         resolve();
              //         if (oldData) {
              //            setTable((prevState) => {
              //             const tableData = [...prevState.tableData];
              //             let newItem = newData
              //             newItem.points.difference = parseInt(newItem.points.difference)
              //             newItem.points.total = newItem.points.value + newItem.points.difference
              //             tableData[tableData.indexOf(oldData)] = newItem;
              //             return { ...prevState, tableData };
              //           });
              //         }
              //       }, 600);
              //     })
              // }}
            />
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

export default function CollapsibleInstanceTable(props) {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
          <TableCell align="left" colSpan={2}>Instancias Finales</TableCell>
            <TableCell />
          </TableRow>
        </TableHead>
        
        <TableBody>
          {props.tournamentInstances.map((row) => (
            <Row key={row._id}
              row={row}
              tableDataTitle={props.tableDataTitle}
              titles={props.titles}/>
          ))}
        </TableBody>
      
      </Table>
    </TableContainer>
  );
}