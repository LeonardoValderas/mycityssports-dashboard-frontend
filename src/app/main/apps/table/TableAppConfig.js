import React from 'react';
import { Redirect } from 'react-router-dom';

const TablesAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/table/tables/:tableId',
			component: React.lazy(() => import('./table/Table'))
		},
		{
			path: '/apps/table/tables',
			component: React.lazy(() => import('./tables/Tables'))
		},
		{
			path: '/apps/table/table-csv',
			component: React.lazy(() => import('./table-csv/TableCsv'))
		},
		{
			path: '/apps/table',
			component: () => <Redirect to="/apps/table/Tables" />
		}
	]
};

export default TablesAppConfig;
