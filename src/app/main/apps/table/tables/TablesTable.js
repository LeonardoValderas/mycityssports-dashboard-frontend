import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../store/actions';
import TablesTableHead from './TablesTableHead';
import FuseLoading from '@fuse/core/FuseLoading';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../table-i18n/es';
import pt from '../table-i18n/pt';
i18next.addResourceBundle('es', 'table', es);
i18next.addResourceBundle('pt', 'table', pt);

function TablesTable(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('table');
	const tables = useSelector(({ tablesApp }) => tablesApp.tables.data);
	const searchText = useSelector(({ tablesApp }) => tablesApp.tables.searchText);
	const loading = useSelector(({ tablesApp }) => tablesApp.tables.loading);
	const cleanSelected = useSelector(({ tablesApp }) => tablesApp.tables.cleanSelected);
	const [clean, setClean] = useState(cleanSelected);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(tables);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useEffect(() => {
		dispatch(Actions.getTables());
	}, [dispatch]);

	useEffect(() => {
		setClean(cleanSelected)
	}, [cleanSelected]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(tables, item => item.tournament.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(tables);
		}
	}, [tables, searchText]);

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setClean(false)
			setSelected(data.map(n => n._id));
			return;
		}
		setSelected([]);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function deleteTables() {
		handleClose()
		dispatch(Actions.deleteTables({ ids: selected }));
	}

	function deleteSelectedItems() {
		handleClickOpen()
	}

	function handleClick(item) {
		props.history.push(`/apps/table/tables/${item._id}`);
	}

	function handleCheck(event, id) {
		setClean(false)
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	return (
		<div className="w-full flex flex-col">
			<div>
				<Dialog
					open={open}
					onClose={handleClose}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title">Atención</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							{`${t('DELETE_ITEM')} ${selected.length === 1 ? t('SINGLE_TABLE') : t('PLURAL_TABLE')}?`}
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose} color="primary">
							Cancelar
          				</Button>
						<Button onClick={deleteTables} color="primary" autoFocus>
							Eliminar
          				</Button>
					</DialogActions>
				</Dialog>
			</div>
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table className="min-w-xl" aria-labelledby="tableTitle">
					<TablesTableHead
						numSelected={selected.length}
						order={order}
						cleanSelected={clean}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={data.length}
						deleteSelectedItems={deleteSelectedItems}
					/>

					<TableBody>
						{_.orderBy(
							data,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											const orderId = order.id
											if (orderId && o !== null) {
												let filter = o
												const split = orderId.split('.')
												split.map(s => {
													const f = filter[s]
													if (f) {
														filter = f
													}
												})
												return filter;
											}
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n._id) !== -1;
								return (
									<TableRow
										className="h-64 cursor-pointer"
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n._id}
										selected={isSelected}
										onClick={event => handleClick(n)}
									>
										<TableCell className="w-64 text-center" padding="none">
											<Checkbox
											    disabled = {true}
												checked={isSelected}
												onClick={event => event.stopPropagation()}
												onChange={event => handleCheck(event, n._id)}
											/>
										</TableCell>

										<TableCell component="th" scope="row">
											{n.sport.name} {n.sport.gender.name}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.tournament.name} {n.tournament.year}
										</TableCell>
										{/* <TableCell component="th" scope="row">
											{n.tournamentZone && n.tournamentZone.name}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.tournamentInstance && n.tournamentInstance.name}
										</TableCell> */}
										<TableCell component="th" scope="row">
											{n.division.name}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.subDivision}
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>
			<div>
				<TablePagination
					className="overflow-hidden"
					component="div"
					count={data.length}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Prev Pag'
					}}
					nextIconButtonProps={{
						'aria-label': 'Sig Pag'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
}

export default withRouter(TablesTable);
