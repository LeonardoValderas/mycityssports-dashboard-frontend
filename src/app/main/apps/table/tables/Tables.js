import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import TablesHeader from './TablesHeader';
import TablesTable from './TablesTable';

function Tables() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<TablesHeader />}
			content={<TablesTable />}
			innerScroll
		/>
	);
}

export default withReducer('tablesApp', reducer)(Tables);
