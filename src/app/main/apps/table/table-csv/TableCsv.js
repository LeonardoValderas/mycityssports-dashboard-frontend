import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FusePageCarded from '@fuse/core/FusePageCarded';
import CircularProgress from '@material-ui/core/CircularProgress';
// import IconButton from '@material-ui/core/IconButton';
// import Icon from '@material-ui/core/Icon';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import moment from 'moment/moment';
import _ from '@lodash';
// import MomentUtils from "@date-io/moment";
import "moment/locale/es";
import Button from '@material-ui/core/Button';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { CSVLink } from "react-csv";
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../table-i18n/es';
import pt from '../table-i18n/pt';
i18next.addResourceBundle('es', 'table', es);
i18next.addResourceBundle('pt', 'table', pt);

function TableCsv(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('table');
	const dataInit = useSelector(({ tableCsvApp }) => tableCsvApp.tableCsv.dataInit);
	const data = useSelector(({ tableCsvApp }) => tableCsvApp.tableCsv.data);
	const loading = useSelector(({ tableCsvApp }) => tableCsvApp.tableCsv.loading);
	const progress = useSelector(({ tableCsvApp }) => tableCsvApp.tableCsv.progress);
	const tournaments = useSelector(({ tableCsvApp }) => tableCsvApp.tableCsv.tournaments);
	const [sports, setSports] = useState([]);
	const [divisions, setDivisions] = useState([]);
	const [columns, setColumns] = useState([]);
	const [showCSV, setShowCSV] = useState(false);
	const [tabValue, setTabValue] = useState(0);
	const { form, setForm } = useForm(null);
	const routeParams = useParams();


	const [tournament, setTournament] = useState('');
	const [sport, setSport] = useState('');
	const [division, setDivision] = useState('');

	const headers = [
		{ label: t('TOURNAMENT'), key: 'tournament' },
		{ label: t('SPORT'), key: 'sport' },
		{ label: t('DIVISION'), key: 'division' },
		{ label: t('SUB_DIVISION'), key: 'subDivision' },

		{ label: t('FINAL_INSTANCE'), key: 'instance' },
		{ label: t('LOCAL_TEAM'), key: 'homeTeam' },
		{ label: 'Resultado Local', key: 'homeResult' },
		{ label: t('RL_PENALTIES'), key: 'homeResultPenalty' },
		{ label: t('VISITORS_TEAM'), key: 'visitorsTeam' },
		{ label: 'Resultado Visita', key: 'visitorsResult' },
		{ label: t('RV_PENALTIES'), key: 'visitorsResultPenalty' },

		{ label: 'Zona', key: 'zone' },
		{ label: t('POSITION'), key: 'position' },
		{ label: t('TEAM'), key: 'team' },
		{ label: t('POINTS'), key: 'points' },
		{ label: 'PJ', key: 'pj' },
		{ label: 'PG', key: 'pg' },
		{ label: 'PE', key: 'pe' },
		{ label: 'PP', key: 'pp' },
		{ label: 'GF', key: 'gf' },
		{ label: 'GE', key: 'ge' },
		{ label: 'DG', key: 'dg' },
		{ label: 'Comentario', key: 'comment' }
	];

	useDeepCompareEffect(() => {
		dispatch(Actions.getTournamentsCsv());
		function initForm() {
			dispatch(Actions.initTableCsv());
		}

		initForm();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (dataInit && !form) {
			setForm(dataInit);
		}
	}, [form, dataInit, setForm]);

	useEffect(() => {
		if (data) {
			setShowCSV(false)
			setColumnsData()
		}
	}, [data]);

	function setColumnsData() {
		setColumns([])
		var newColumns = []
		data.map((item) => {
			const t = item.tournament.name + " - " + item.tournament.year
			setTournament(t)
			const s = item.sport.name + " - " + item.sport.gender.name
			setSport(s)
			const d = item.division.name
			setDivision(d)
			const subDivision = item.subDivision

			item.tournamentInstances.map((i) => {
				const instance = i.tournamentInstance.name
				i.tableDataInstance.map((f) => {
					const row = getRowFromFixture(tournament, sport, division, subDivision, instance, '', f)
					newColumns.push(row)
				})
			})

			item.tournamentZones.map((i) => {
				const zone = i.tournamentZone.name
				i.tableDataZone.map((f) => {
					const row = getRowFromFixture(tournament, sport, division, subDivision, '', zone, f)
					newColumns.push(row)
				})
			})

			item.tournamentPoints.map((f) => {
				const row = getRowFromFixture(tournament, sport, division, subDivision, '', '', f)
				newColumns.push(row)
			})
		})
		setColumns(newColumns)
		setShowCSV(true)
	}

	function getRowFromFixture(tournament, sport, division, subDivision, instance, zone, f) {
		return {
			tournament: tournament,
			sport: sport,
			division: division,
			subDivision: subDivision,
			instance: instance,
			zone: zone,
			homeTeam: f.homeTeam ? f.homeTeam.name : '',
			homeResult: f.homeResult,
			homeResultPenalty: f.homeResultPenalty,
			visitorsTeam: f.visitorsTeam ? f.visitorsTeam.name : '',
			visitorsResult: f.visitorsResult,
			visitorsResultPenalty: f.visitorsResultPenalty,
			position: f.position,
			team: f.team ? f.team.name : '',
			points: f.points ? f.points.total : '',
			pj: f.pj ? f.pj.total : '',
			pg: f.pg ? f.pg.total : '',
			pe: f.pe ? f.pe.total : '',
			pp: f.pp ? f.pp.total : '',
			gf: f.gf ? f.gf.total : '',
			ge: f.ge ? f.ge.total : '',
			dg: f.dg ? f.dg.total : '',
			comment: f.comment
		}
	}
	function dateTimeFormat(dateTime) {
		let newDateTime = moment(new Date(dateTime)).format('YYYY-MM-DD HH:mm')
		return newDateTime
	}

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return SubmittedFormValidation()
			&& !_.isEqual(data, form);
	}

	function handleChipChange(value, name) {
		//const isSubdivision = name === 'subDivision'
		setForm(
			_.set(
				{ ...form },
				name, value
			)
		);
		//name, isSubdivision ? value.value : value
	}

	function SubmittedFormValidation() {
		return form
			// && form.dateTimeStart !== ''
			// && form.dateTimeEnd !== ''
			// && validateDate()
			&& form.sport
			&& form.tournament
			&& form.division
		// && subDivisionValidation()
	}

	// function validateDate() {
	// 	return !isDateStartMinorOrSameDateEnd(form.dateTimeStart, form.dateTimeEnd)
	// }

	// function subDivisionValidation() {
	// 	return _.isEmpty(subDivisions) ? true : form.subDivision
	// }

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function setFormValue(name, value) {
		setForm(
			_.set(
				{ ...form },
				name, value
			)
		);
	}

	function handleChipChangeSport(value) {
		setFormValue('sport', value)
	}

	function handleChipChangeTournament(value) {
		if (!form.tournament || form.tournament._id !== value._id) {
			setSports(value.sports)
			setDivisions(value.divisions)
			//setSubDivisions(value.subDivisions)
			setFormValue('tournament', value)
		}
	}

	// function cleanTournamentSubDivision() {
	// 	setForm(
	// 		_.set(
	// 			{ ...form },
	// 			'subDivision', ''
	// 		)
	// 	);
	// }

	function getCSVFileName() {
		return `${tournament}_${sport}_${division}_tables.csv`
	}

	function dateTimeFormat(dateTime) {
		let newDateTime = moment(new Date(dateTime)).format('YYYY-MM-DD hh:mm')
		return newDateTime
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">

						<CircularProgress color="secondary" style={{ display: progress ? '' : 'none' }} />

					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label={t('TABLE_CSV')} />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								{/* <div className="flex mx-0">
									<MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={"es"}>
										<DatePicker
											id='dateTimeStart'
											className="mt-8 w-3/6"
											label="Desde"
											format="DD-MM-YYYY"
											name='dateTimeStart'
											disabled={progress}
											inputVariant="outlined"
											value={dateTimeFormat(form.dateTimeStart)}
											onChange={date => setInForm('dateTimeStart', moment.parseZone(date).format('YYYY-MM-DD HH:mm'))}
										/>

										<DatePicker
											id='dateTimeEnd'
											className="mt-8 ml-16 w-3/6"
											label="Hasta"
											format="DD-MM-YYYY"
											name='dateTimeEnd'
											disabled={progress}
											inputVariant="outlined"
											value={dateTimeFormat(form.dateTimeEnd)}
											onChange={date => setInForm('dateTimeEnd', moment.parseZone(date).format('YYYY-MM-DD HH:mm'))}
										/>
									</MuiPickersUtilsProvider>

								</div> */}
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<FuseChipSelect
										className="mt-24 w-full md:w-3/6"
										value={form.tournament}
										onChange={value => handleChipChangeTournament(value)}
										placeholder={t('TOURNAMENT_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => `${option.name} ${option.year}`}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('TOURNAMENT'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										isDisabled={progress}
										options={tournaments || []}
									/>

									<FuseChipSelect
										className="mt-24 md:ml-24 w-full md:w-3/6"
										value={form.sport}
										onChange={value => handleChipChangeSport(value)}
										placeholder={t('SPORT_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => `${option.name} (${option.gender.name})`}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('SPORT'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										isDisabled={progress}
										options={sports || []}
									//	isMulti
									/>
								</div>
								<div className="flex mx-0">
									<FuseChipSelect
										className="mt-24 w-full md:w-3/6"
										value={form.division}
										onChange={value => handleChipChange(value, 'division')}
										placeholder={t('DIVISION_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => option.name}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('DIVISION'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={divisions || []}
										isDisabled={progress}
									/>
									{/* <FuseChipSelect
										className="mt-24 ml-24 w-3/6"
										value={
											form.subDivision ?
												{
													value: form.subDivision,
													label: form.subDivision
												} : form.subDivision
										}
										onChange={value => handleChipChange(value, 'subDivision')}
										placeholder="Seleccione la sub división"
										variant='fixed'
										required={!_.isEmpty(subDivisions)}
										textFieldProps={{
											label: 'Sub División',
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={subDivisions.map(t => ({ value: t, label: t })) || []}
										isDisabled={progress}
									/>
									<IconButton
										onClick={() =>
											cleanTournamentSubDivision()
										}
										disabled={progress}
										className="mt-12"
									>
										<Icon>autorenew</Icon>
									</IconButton> */}
								</div>
								<div className="w-full mt-48" style={{ textAlign: 'center' }}>

									<FuseAnimate animation="transition.slideRightIn" delay={300}>
										<Button
											className="whitespace-no-wrap normal-case"
											variant="contained"
											color="secondary"
											disabled={!canBeSubmitted()}
											onClick={() => {
												dispatch(Actions.getTablesToCSV(form))
											}
											}
										>
											{t('PROCESS')}
										</Button>
									</FuseAnimate>
								</div>
								<div className="w-full mt-32" style={{ textAlign: 'center' }}>
									{showCSV && (<CSVLink
										data={columns}
										headers={headers}
										filename={getCSVFileName()}
										className="btn btn-primary"
										target="_blank">
										{t('DOWNLOAD')}
								        </CSVLink>)
									}
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('tableCsvApp', reducer)(TableCsv);
