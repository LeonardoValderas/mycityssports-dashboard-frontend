import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useDeepCompareEffect } from '@fuse/hooks';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import { isIntegerTwoDigits } from '../../utils/NumberValidator'
import CollapsibleInstanceTable from '../CollapsibleInstanceTable';
import CollapsibleZoneTable from '../CollapsibleZoneTable';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../table-i18n/es';
import pt from '../table-i18n/pt';
i18next.addResourceBundle('es', 'table', es);
i18next.addResourceBundle('pt', 'table', pt);

function Table(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('table');
	const data = useSelector(({ tableApp }) => tableApp.table.data);
	const savedModel = useSelector(({ tableApp }) => tableApp.table.savedModel);
	const loading = useSelector(({ tableApp }) => tableApp.table.loading);
	const [table, setTable] = useState(null);
	const [tableDataTitle, setTableDataTitle] = useState('');
	const [tabValue, setTabValue] = useState(0);
	const theme = useTheme();
	const routeParams = useParams();

	const titles = [
		{ title: t('POSITION'), field: 'position', editable: 'never' },
		{ title: t('TEAM'), field: 'team.name', editable: 'never' },
		{ title: 'PJ', field: 'pj.total', editable: 'never' },
		{ title: 'PG', field: 'pg.total', editable: 'never' },
		{ title: 'PE', field: 'pe.total', editable: 'never' },
		{ title: 'PP', field: 'pp.total', editable: 'never' },
		{ title: 'GF', field: 'gf.total', editable: 'never' },
		{ title: 'GE', field: 'ge.total', editable: 'never' },
		{ title: 'DG', field: 'dg.total', editable: 'never' },
		{ title: 'Pts', field: 'points.total', editable: 'never' },
		{ title: 'Pts Ajuste', field: 'points.difference' },
	]

	const instanceTitles = [
		{ title: t('TEAM'), field: 'homeTeam.name', editable: 'never' },
		{ title: 'Resultado', field: 'homeResult', editable: 'never' },
		{ title: t('RESULT_PENALTY'), field: 'homeResultPenalty', editable: 'never' },
		{ title: t('TEAM'), field: 'visitorsTeam.name', editable: 'never' },
		{ title: 'Resultado', field: 'visitorsResult', editable: 'never' },
		{ title: t('RESULT_PENALTY'), field: 'visitorsResultPenalty', editable: 'never' },
	]

	useDeepCompareEffect(() => {
		function updateDivisionState() {
			const { tableId } = routeParams;
			dispatch(Actions.getTable(tableId));
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel && data) {
			setTable(data)
		}
	}, [savedModel]);

	useEffect(() => {
		if (data) {
			setTable(data)
			const title = `${data.division.name} ${data.subDivision || ""} - ${data.tournament.name} ${data.tournament.year} ${data.tournamentZone ? data.tournamentZone.name : ''} ${data.tournamentInstance ? data.tournamentInstance.name : ''} - ${data.sport.name} ${data.sport.gender.name}`
			setTableDataTitle(title)
		}
	}, [data]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function handleChangeComment(event) {
		event.persist();
		setTableValue('comment', event.target.value)
	}

	function handleChangeSource(event) {
		event.persist();
		setTableValue('source', event.target.value)
	}

	function setTableValue(name, value) {
		setTable(_table =>
			_.setIn(
				{ ..._table },
				name,
				value
			)
		)
	}

	function updateTournamentZones(tournamentZoneId, newData, oldData) {
		if (isIntegerTwoDigits(newData.points.difference)) {
			let state = { ...table }
			let tournamentZones = [...state.tournamentZones]
			let index = tournamentZones.findIndex(tz => tz.tournamentZone._id === tournamentZoneId)
			let tournamentZone = { ...tournamentZones[index] }
			let tableDataZone = [...tournamentZone.tableDataZone]
			let indexZone = tableDataZone.indexOf(oldData)

			let newItem = newData
			newItem.points.difference = parseInt(newItem.points.difference)
			newItem.points.total = newItem.points.value + newItem.points.difference

			tableDataZone[indexZone] = newItem
			tournamentZone.tableDataZone = tableDataZone
			tournamentZones[index] = tournamentZone
			state.tournamentZones = tournamentZones

			setTable(state)
		}
	}

	function canBeSubmitted() {
		return !_.isEqual(data, table);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				table && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/table/tables"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">{t('TABLES')}</span>
								</Typography>
							</FuseAnimate>
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => {
									dispatch(Actions.saveTable(table, 'put'))
								}
								}
							>
								Actualizar
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label={t('TABLE')} />
					<Tab className="h-64 normal-case" label={t('OTHER')} />
				</Tabs>
			}
			content={
				table && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<React.Fragment>
									<div >
										{table.tournamentInstances && table.tournamentInstances.length > 0 && <CollapsibleInstanceTable
											tournamentInstances={table.tournamentInstances}
											tableDataTitle={tableDataTitle}
											titles={instanceTitles} />
										}
									</div>
									<div className="mt-16">
										{table.tournamentZones && table.tournamentZones.length > 0 && <CollapsibleZoneTable
											tournamentZones={table.tournamentZones}
											tableDataTitle={tableDataTitle}
											titles={titles}
											updateTournamentZones={updateTournamentZones}
										/>
										}
									</div>
									<div className="mt-16">
										{table.tournamentPoints && table.tournamentPoints.length > 0 && <MaterialTable
											title={tableDataTitle}
											columns={titles}
											data={table.tournamentPoints || []}
											localization={{
												header: {
													actions: 'Acciones'
												},
												toolbar: {
													searchPlaceholder: 'Buscar'
												},
												pagination: {
													labelRowsSelect: 'filas'
												},
												body: {
													editRow: {
														saveTooltip: 'Salvar',
														cancelTooltip: 'Cancelar'
													},
													editTooltip: 'Editar'
												}
											}}
											options={{
												actionsColumnIndex: titles.length
											}}

											editable={{
												onRowUpdate: (newData, oldData) =>
													new Promise((resolve) => {
														setTimeout(() => {
															resolve();
															if (oldData) {
																setTable((prevState) => {
																	const tournamentPoints = [...prevState.tournamentPoints];
																	let newItem = newData
																	if (isIntegerTwoDigits(newItem.points.difference)) {
																		newItem.points.difference = parseInt(newItem.points.difference)
																		newItem.points.total = newItem.points.value + newItem.points.difference
																		tournamentPoints[tournamentPoints.indexOf(oldData)] = newItem;
																	}
																	return { ...prevState, tournamentPoints };
																});
															}
														}, 600);
													})
											}}
										/>
										}
									</div>
								</React.Fragment>
							</div>
						)}
						{tabValue === 1 && (
							<div>

								<TextField
									className="mt-16"
									label="Comentario"
									autoFocus
									id="comment"
									name="comment"
									value={table.comment}
									onChange={handleChangeComment}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 100,
									}}
								/>

								<TextField
									className="mt-16"
									label={t('SOURCE')}
									id="source"
									name="source"
									value={table.source}
									onChange={handleChangeSource}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 50,
									}}
								/>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('tableApp', reducer)(Table);
