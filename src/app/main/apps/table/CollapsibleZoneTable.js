import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import MaterialTable from 'material-table';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

function Row(props) {
  const { row, titles, tableDataTitle, updateTournamentZones} = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();
  const tournamentZoneId = row.tournamentZone._id
  return (
    <React.Fragment>
      <TableRow className={classes.root} onClick={() => setOpen(!open)}>
      <TableCell style={{ width: 5 }} size='small' component="td" scope="row" align="left">
					<IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
						{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
					</IconButton>
				</TableCell>
      <TableCell align="left">
          {row.tournamentZone.name}
        </TableCell>
      </TableRow>
  
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <MaterialTable
              title={`${row.tournamentZone.name} - ${tableDataTitle}`}
              columns={titles}
              data={row.tableDataZone || []}
              localization={{
                header: {
                  actions: 'Acciones'
                },
                toolbar: {
                  searchPlaceholder: 'Buscar'
                },
                pagination: {
                  labelRowsSelect: 'filas'
                },
                body: {
                  editRow: {
                    saveTooltip: 'Salvar',
                    cancelTooltip: 'Cancelar'
                  },
                  editTooltip: 'Editar'
                }
              }}
              options={{
                actionsColumnIndex: titles && titles.length
              }}

              editable={{
                onRowUpdate: (newData, oldData) =>
                new Promise((resolve) => {
                    setTimeout(() => {
                      resolve();
                      updateTournamentZones(tournamentZoneId, newData, oldData)
                    }, 600);
                  })
                
              }}
            />
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

export default function CollapsibleZoneTable(props) {
  const { tournamentZones, tableDataTitle, titles, updateTournamentZones} = props;

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell align="left">Grupos</TableCell>
            <TableCell />
          </TableRow>
        </TableHead>
        
        <TableBody>
          {tournamentZones.map((row) => (
            <Row key={row._id}
              row={row}
              tableDataTitle={tableDataTitle}
              titles={titles}
              updateTournamentZones={updateTournamentZones} />
          ))}
        </TableBody>
      
      </Table>
    </TableContainer>
  );
}