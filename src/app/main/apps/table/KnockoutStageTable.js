import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import MuiTableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const TableCell = withStyles({
  root: {
    borderBottom: "none"
  }
})(MuiTableCell);

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  }
}));

function createData(homeTeam, visitorsTeam, homeResult, visitorsResult){
  return { homeTeam, visitorsTeam, homeResult, visitorsResult };
}

const eighth = [ 
  createData("Argentina", "Brasil", 1, 2),
  createData("Peru", "chile", 3, 2),
  createData("Ecuador", "Paraguay", 3, 5),
  createData("Colombia", "Uruguay", 1, 2),
  createData("Usa", "Canada", 5, 2),
  createData("Mexico", "Costa Rica", 6, 2),
  createData("Republica Dominicana", "Arabia", 1, 0),
  createData("Alemania", "Suecia", 1, 3)
]

const fourth = [ 
  createData("Argentina", "Brasil", 1, 2),
  createData("Peru", "chile", 3, 2),
  createData("Ecuador", "Paraguay", 3, 5),
  createData("Colombia", "Uruguay", 1, 2)
]

const semi = [ 
  createData("Argentina", "Brasil", 1, 2),
  createData("Peru", "chile", 3, 2),
]

const final = [ 
  createData("Argentina", "Brasil", 1, 2)
]

const rows = [ { 
  eighth: eighth,
  fourth: fourth,
  semi: semi,
  final: final
}];
 

export default function SimpleTable() {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table} size="small">
        <TableHead>
          <TableRow>
            <TableCell align="center" colSpan={2}>Octavos de final</TableCell>
            <TableCell align="colSpan={2}" colSpan={2} ></TableCell>
            <TableCell align="center" colSpan={2} >Cuartos de final</TableCell>
            <TableCell align="right">Fat&nbsp;(g)</TableCell>
            <TableCell align="right">Carbs&nbsp;(g)</TableCell>
            <TableCell align="right">Protein&nbsp;(g)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            
            <React.Fragment key={2+1}>
              <TableRow key={8+1}>
                <TableCell width="18%" align="left" style={{ borderWidth: 1 }} >
                  {row.eighth[0].homeTeam}
                </TableCell>
                <TableCell width="8%" align="center" style={{ borderWidth: 1 }}>
                    {row.eighth[0].homeResult}
                </TableCell>
              </TableRow>
              <TableRow key={8-1}>
                <TableCell align="left" style={{ borderWidth: 1 }} >
                  {row.eighth[0].visitorsTeam}
                </TableCell>
                <TableCell width="8%" align="center" style={{ borderWidth: 1 }}>
                   {row.eighth[0].visitorsResult}
                </TableCell>
                <TableCell width="1%" style={{ borderTopWidth: 3, borderRightWidth: 3, borderLeftWidth: -1 }}/>
              </TableRow>

              <TableRow >
                <TableCell  colSpan={2} style={{ borderTopWidth: 3 }}>
                  &nbsp;
               </TableCell>
               <TableCell width="1%" style={{ borderRightWidth: 3, borderLeftWidth: 0 }}>
                  &nbsp;
               </TableCell>
              </TableRow>

              <TableRow key={7}>
                <TableCell width="18%" align="left" style={{ borderWidth: 1 }} >
                 {row.eighth[1].homeTeam}
                </TableCell>
                <TableCell width="8%" align="center" style={{ borderWidth: 1 }}>
                 {row.eighth[1].homeResult}
                </TableCell>
                <TableCell width="1%" style={{ borderRightWidth: 3 }}/>
              </TableRow>
              <TableRow key={row.calories}>
                <TableCell align="left" style={{ borderWidth: 1 }} >
                  {row.eighth[1].visitorsTeam}
                </TableCell>
                <TableCell align="center" style={{ borderWidth: 1 }}>
                  {row.eighth[1].visitorsResult}
                </TableCell>
                <TableCell width="1%" style={{ borderTopWidth: 3 }}/>
              </TableRow>
              
              <TableRow style={{ borderTopWidth: 3 }}>
                <TableCell align="left" colSpan={2} >
                  &nbsp;
               </TableCell>
              </TableRow>

            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}
