import { combineReducers } from 'redux';
import table from './table.reducer';
import tables from './tables.reducer';
import tableCsv from './table-csv.reducer';
const reducer = combineReducers({
	tables,
	table,
	tableCsv
});

export default reducer;
