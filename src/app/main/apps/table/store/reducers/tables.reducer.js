import * as Actions from '../actions';

const initialState = {
	data: [],
	searchText: '',
	loading: {
	  show: false,
	  label: ''
	},
	cleanSelected: false
};

const tablesReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_TABLES: {
			return {
				...state,
				data: action.payload, 
				loading: {
					show: false,
					label: ''
				  }
			};
		}
		case Actions.SET_LOADING: {
			return {
				...state,
				loading: action.payload
			};
		}
		case Actions.SET_TABLES_SEARCH_TEXT: {
			return {
				...state,
				searchText: action.searchText
			};
		}
		case Actions.CLEAN_SELELCTED_TABLE: {
			return {
				...state,
				cleanSelected: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default tablesReducer;
