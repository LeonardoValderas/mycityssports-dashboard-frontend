
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_TABLE_CSV = '[TABLE CSV APP] GET TABLE CSV';
export const GET_TABLE_CSV_TOURNAMENT = '[TABLE CSV APP] GET TABLE CSV TOURNAMENT';
export const INIT_TABLE_CSV = '[TABLE CSV APP] INIT TABLE CSV';
export const SET_LOADING_TABLE_CSV = '[TABLE CSV APP] SET LOADING';
export const SET_PROGRESS_TABLE_CSV = '[TABLE CSV APP] SET PROGRESS';

const lang = i18next.language
const translate = translateInfo(lang)

export function getTournamentsCsv() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/tournaments/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setTournaments(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function getTablesToCSV(form) {
	const tournamentId = form.tournament._id
	const sportId = form.sport._id
    const divisionId = form.division._id
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading}))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/tables/institutions/${institutionId}/tournaments/${tournamentId}/sports/${sportId}/divisions/${divisionId}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModel(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setTournaments(models) {
	return {
		type: GET_TABLE_CSV_TOURNAMENT,
		payload: models
	}
}


export function setModel(model) {
	return {
		type: GET_TABLE_CSV,
		payload: model
	}
}


export function initTableCsv() {
	const data = {
        // dateTimeStart: moment(new Date()).format('YYYY-MM-DD'),
        // dateTimeEnd: moment(new Date()).format('YYYY-MM-DD'),
		sport: null,
		tournament: null,
		division: null,
		subDivision: null
	};

	return {
		type: INIT_TABLE_CSV,
		payload: data
	};
}

export function setLoading(loading) {
	return {
		type: SET_LOADING_TABLE_CSV,
		payload: loading
	}
}

export function setProgress(loading) {
	return {
		type: SET_PROGRESS_TABLE_CSV,
		payload: loading
	}
}

