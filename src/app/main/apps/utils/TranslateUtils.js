import constants from './Constants'

export function translateInfo(lang) {
	const data = {
		baseUrl: (lang === constants.PT) ? constants.API_URL_BR : constants.API_URL,
		loading: (lang === constants.PT) ? constants.LOADING_PT : constants.LOADING_ES,
		processing: (lang === constants.PT) ? constants.PROCESSING_PT : constants.PROCESSING_ES,	
		success: (lang === constants.PT) ? constants.SUCCESS_MESSAGE_SUBMIT_PT : constants.SUCCESS_MESSAGE_SUBMIT_ES,	
	};

	return data
}