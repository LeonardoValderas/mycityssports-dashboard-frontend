export function isIntegerTwoDigits(value) {
  return /^-?\d{1,2}$/.test(value);
}

export function isIntegerAndPositive(value) {
  return /^\+?(0|[1-9]\d*)$/.test(value);
}

export function isMinorEqualsToOneHundred(value) {
  return parseInt(value) <= 100;
}

export function isMinorEqualsToOneFityHundred(value) {
  return parseInt(value) <= 150;
}

export function isIntegerAndMinorEqualsToOneHundred(value) {
  return isIntegerAndPositive(value) && isMinorEqualsToOneHundred(value)
}

export function isIntegerMaiorZeroAndMinorEqualsToOneHundred(value) {
  return isIntegerAndPositive(value) && isMinorEqualsToOneHundred(value) && parseInt(value) !== 0
}

export function isIntegerMaiorZeroAndMinorEqualsToOneFityHundred(value) {
  return isIntegerAndPositive(value) && isMinorEqualsToOneFityHundred(value) && parseInt(value) !== 0
}