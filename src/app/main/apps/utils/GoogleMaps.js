import React, { Component } from 'react'
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';
import {getCityPosition} from './AuthUtils'
export class GoogleMap extends Component {
    render() {
        const localPosition = getCityPosition()
        const localLatitude = localPosition.latitude ? parseFloat(localPosition.latitude) : 0.0
        const localLongitude = localPosition.longitude ? parseFloat(localPosition.longitude) : 0.0
       
        const latitude = this.props.latitude
        const longitude = this.props.longitude

        return (
            <div className="w-full h-350">

                <Map google={this.props.google}
                    // width={"100%"}
                    style={{ width: 980, height: 350, position: 'absolute' }}
                    zoom={14}
                    initialCenter={{
                        lat: latitude || localLatitude,
                        lng: longitude || localLongitude
                    }}
                    center={{
                        lat: latitude || localLatitude,
                        lng: longitude || localLongitude
                    }}
                    visible={true}
                    onClick={this.props.mapClicked}
                    error={this.props.error}>

                    {(latitude && longitude) && <Marker
                        position={{ lat: latitude, lng: longitude }}
                        name={'Current location'} />}
                </Map>
            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyB4UQnF9Gro3x7ygNke_yn62m_5iYNocCw')
})(GoogleMap)