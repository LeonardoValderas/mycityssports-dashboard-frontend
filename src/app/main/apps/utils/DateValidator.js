export function isMinorDateNow(dateStn) {
  if (!dateStn) {
    return true
  }
  //   const currentDate = new Date(dateStn).toLocaleDateString('en-US', {
  //     year: "numeric",
  //     month: "2-digit",
  //     day: "2-digit",
  //   })
  //   const now = new Date().toLocaleDateString('en-US', {
  //     year: "numeric",
  //     month: "2-digit",
  //     day: "2-digit",
  //   })
  // }
  const currentDate = new Date(dateStn)
  const now = new Date()
  let result = currentDate.getTime() < now.getTime();
  return result
}

export function isMinorOrSameDateNow(dateStn) {
  if (!dateStn) {
    return true
  }
  const currentDate = new Date(dateStn)
  const now = new Date()

  // const currentDate = new Date(dateStn).toLocaleDateString('en-US', {
  //   year: "numeric",
  //   month: "2-digit",
  //   day: "2-digit",
  // })
  // const now = new Date().toLocaleDateString('en-US', {
  //   year: "numeric",
  //   month: "2-digit",
  //   day: "2-digit",
  // })
  let result = currentDate.getTime() <= now.getTime();
  return result
}

export function isDateStartMinorOrSameDateEnd(dateStart, dateEnd) {
  if (!dateStart || !dateEnd) {
    return true
  }
  // const startDate = new Date(dateStart).toLocaleDateString('en-US', {
  //   year: "numeric",
  //   month: "2-digit",
  //   day: "2-digit",
  // })
  // const endDate = new Date(dateEnd).toLocaleDateString('en-US', {
  //   year: "numeric",
  //   month: "2-digit",
  //   day: "2-digit",
  // })

  const startDate = new Date(dateStart)
  const endDate = new Date(dateEnd)
  let result = endDate.getTime() <= startDate.getTime();
  return result
}

export function isMaiorOrSameDateNow(dateStn) {
  if (!dateStn) {
    return true
  }

  const currentDate = new Date(dateStn)
  const now = new Date()
  // const currentDate = new Date(dateStn).toLocaleDateString('en-US', {
  //   year: "numeric",
  //   month: "2-digit",
  //   day: "2-digit",
  // })
  // const now = new Date().toLocaleDateString('en-US', {
  //   year: "numeric",
  //   month: "2-digit",
  //   day: "2-digit",
  // })
  const isTrue = currentDate.getTime() >= now.getTime()
  return isTrue;
}