export default {
    //API_URL:'https://mcs-dash-be-staging.herokuapp.com/api',
    
    API_URL:'https://mcs-dash-be-prod.herokuapp.com/api',
    API_URL_BR:'https://mcs-dash-br-be-prod.herokuapp.com/api',
    OAPI_URL:'https://mcs-dash-be-prod.herokuapp.com/oapi',
    OAPI_URL_BR:'https://mcs-dash-br-be-prod.herokuapp.com/oapi',
    // API_URL: 'http://localhost:3003/api',
    // API_URL_BR: 'http://localhost:3004/api',
    // OAPI_URL: 'http://localhost:3003/oapi',
    // OAPI_URL_BR: 'http://localhost:3004/oapi',
    
    
    //OAPI_URL:'https://mcs-dash-be-staging.herokuapp.com/oapi',
    
    

    CITY_LOCATION: {
        lat: -41.1335,
        lng: -71.3103
    },
    CITY_LOCATION_BR: {
        lat: -22.908333,
        lng: -43.196388
    },
    //USER_ID: '5ea0715d7c7511e507fa8225',
    //INSTITUTION_ID: "5e9f59288ca2fee05eff0d3f",
    DEFAULT_URL_IMAGE_128_128: "https://res.cloudinary.com/dkzhvtsml/image/upload/c_limit,h_128,w_128/v1588030335/image_default_upload.jpg",
    DEFAULT_URL_IMAGE_280_180: "https://res.cloudinary.com/dkzhvtsml/image/upload/c_limit,h_280,w_180/v1588030335/image_default_upload.jpg",
    DEFAULT_URL_AVATAR_IMAGE: "https://res.cloudinary.com/dkzhvtsml/image/upload/v1590241907/user_default_gre8n9.jpg",
    DEFAULT_URL_INSTITUTION_IMAGE: "https://res.cloudinary.com/dkzhvtsml/image/upload/v1590247481/institution_default_gzxvrn.jpg",
    //ORDER_URL: 'https://gaos-frontend.herokuapp.com/#/orders'
   SUCCESS_MESSAGE_SUBMIT_ES: 'Operación realizada com exito',
   SUCCESS_MESSAGE_SUBMIT_PT: 'Operacão bem sucedida',
   ERROR_UNEXPECTED_MESSAGE_SUBMIT: 'Problema inesperado',

    //COUNTRY
    AR: 'AR',
    BR: 'BR',
    //LANGUAGE
    ES: 'es',
    PT: 'pt',

    TITLE_ES: 'Los deportes de tu ciudad.',
    TITLE_PT: 'Os esportes da sua cidade.', 
    DO_LOGIN_ES: 'INICIAR SESIÓN CON SU USUARIO',
    DO_LOGIN_PT: 'FAÇA LOGIN COM SEU USUÁRIO',


    LOADING_ES: 'Cargando...',
    LOADING_PT: 'Carregando...',

    PROCESSING_ES: 'Procesando...',
    PROCESSING_PT: 'Em processamento...',

    
}