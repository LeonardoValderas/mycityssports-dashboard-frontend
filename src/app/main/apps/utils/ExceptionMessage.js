export function exceptionMessage(e) {
    if (e) {
        if (e.response) {
            if(e.response.data){
                return e.response.data.message
            }
            //console.log(error.response.data);
           // console.log(error.response.status);
           // console.log(error.response.headers);
        } else if (e.request) {
            return e.request;
        } else if(e.message){
             return e.message;
        } else {
            return null        
        }
    }

    return null
} 