export  function setToken(token) {
    localStorage.setItem('jwt_access_token', token);
};

export function getToken() {
    return window.localStorage.getItem('jwt_access_token');
};

export function removeToken() {
    localStorage.removeItem('jwt_access_token');
};

export  function setUserId(userId) {
    localStorage.setItem('local_id_user', userId);
};

export function getUserId() {
    return window.localStorage.getItem('local_id_user');
};

export function removeUserId() {
    localStorage.removeItem('local_id_user');
};

export  function setInstitutionId(institutionId) {
    localStorage.setItem('local_id_institution', institutionId);
};

export function getInstitutionId() {
    return window.localStorage.getItem('local_id_institution');
};

export function removeInstitutionId() {
    localStorage.removeItem('local_id_institution');
};

export  function setInstitutionName(name) {
    localStorage.setItem('local_name_institution', name);
};

export function getInstitutionName() {
    return window.localStorage.getItem('local_name_institution');
};

export function removeInstitutionName() {
    localStorage.removeItem('local_name_institution');
};

export  function setCityId(institutionId) {
    localStorage.setItem('local_id_city', institutionId);
};

export function getCityId() {
    return window.localStorage.getItem('local_id_city');
};

export function removeCityId() {
    localStorage.removeItem('local_id_city');
};

export  function setCityPosition(position) {
    localStorage.setItem('local_position_city', JSON.stringify(position));
};

export function getCityPosition() {
    const position = JSON.parse(window.localStorage.getItem('local_position_city'));
    return position
};

export function setSponsors(sponsor) {
    localStorage.setItem('local_sponsors_institution', sponsor);
};

export function getSponsors() {
    const sponsor = window.localStorage.getItem('local_sponsors_institution');
    return (sponsor == 'true');
};

export function removeSponsors() {
    localStorage.removeItem('local_sponsors_institution');
};

export function removeAll() {
    localStorage.removeItem('local_position_city');
    localStorage.removeItem('local_id_user');
    localStorage.removeItem('local_id_institution');
    localStorage.removeItem('local_id_city');
    removeSponsors()
};

export  function setCountry(country) {
    localStorage.setItem('local_country', country);
};

export function getCountry() {
    return window.localStorage.getItem('local_country');
};

export function removeCountry() {
    localStorage.removeItem('local_country');
};