import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FusePageCarded from '@fuse/core/FusePageCarded';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import moment from 'moment/moment';
import _ from '@lodash';
import clsx from 'clsx';
import constants from '../../utils/Constants'
import Button from '@material-ui/core/Button';
import { isMaiorOrSameDateNow } from '../../utils/DateValidator'
import { orange } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import jimp from 'jimp';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../player-i18n/es';
import pt from '../player-i18n/pt';
i18next.addResourceBundle('es', 'player', es);
i18next.addResourceBundle('pt', 'player', pt);

const useStyles = makeStyles(theme => ({
	productImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		color: orange[400],
		opacity: 0
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	productImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $productImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $productImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $productImageFeaturedStar': {
				opacity: 1
			}
		}
	}
}));

function Player(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('player');
	const player = useSelector(({ playerApp }) => playerApp.player.data);
	const teamModels = useSelector(({ playerApp }) => playerApp.player.teams);
	const savedModel = useSelector(({ playerApp }) => playerApp.player.savedModel);
	const sports = useSelector(({ playerApp }) => playerApp.player.sports);
	const divisions = useSelector(({ playerApp }) => playerApp.player.divisions);
	const loading = useSelector(({ playerApp }) => playerApp.player.loading);
	const progress = useSelector(({ playerApp }) => playerApp.player.progress);
	const theme = useTheme();
	const classes = useStyles(props);
	const [tabValue, setTabValue] = useState(0);
	const [teams, setTeams] = useState(teamModels);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();
	const [imageError, setImageError] = useState("");

	useDeepCompareEffect(() => {
		dispatch(Actions.getDivisions());
		dispatch(Actions.getSports());
		function updateDivisionState() {
			const { playerId } = routeParams;
			if (playerId === 'new') {
				dispatch(Actions.newPlayer());
			} else {
				dispatch(Actions.getPlayer(playerId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(player);
		}
	}, [savedModel]);

	useEffect(() => {
		if (teamModels) {
			setTeams(teamModels);
		}
	}, [teamModels]);

	useEffect(() => {
		if ((player && !form) || (player && form && player._id !== form._id)) {
			if (routeParams.playerId !== 'new') {
				getDataFromFormData(player);
			}
			setForm(player);
		}
	}, [form, player, setForm]);

	function getDataFromFormData(data) {
		dispatch(Actions.getTeamsForSport(data.sport && data.sport._id));
	}

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return form && form.name.length > 0
			&& form.member.length > 0
			&& form.memberId.length > 0
			&& form.birthday.length > 0
			&& !isMaiorOrSameDateNow(moment(form.birthday).format('MM/DD/YYYY'))
			&& form.team && form.division
			&& !_.isEqual(player, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function handleUploadChange(e) {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		form.nameImage = file.name
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = async () => {
			let r = `data:${file.type};base64,${btoa(reader.result)}`
			const image = await jimp.read(r);
			await image.resize(128, 128);
			let base = await image.getBase64Async(image.getMIME())
			let d = await image.bitmap.data
			let f = new File(d, file.name)
			let newSize = f.size / 1024 / 1024
			if (newSize > 1.0) {
				setImageError(t('IMAGE_BIG'))
				return
			} else {
				setImageError("")
				setForm(
					_.set(
						{ ...form },
						`photo`, base
					)
				);
			}
		};

		reader.onerror = () => {
			console.log('error on load image');
		};
	}

	function handleChipChange(value, name) {
		if (_.isEqual(name, 'sport') && (!form.sport || form.sport._id !== value._id)) {
			form.team = null
			dispatch(Actions.getTeamsForSport(value._id));
		}
		setForm(
			_.set(
				{ ...form },
				name, value,
			)
		);
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/player/players"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">{t('PLAYERS')}</span>
								</Typography>
							</FuseAnimate>
						</div>
						<CircularProgress color="secondary" style={{ display: progress ? '' : 'none' }} />
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.savePlayer(form, routeParams.playerId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.playerId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label={t('PLAYER')} />
					<Tab className="h-64 normal-case" label="Foto" />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<TextField
									className="mt-16 "
									error={form.name === ''}
									required
									label={t('NAME')}
									autoFocus
									id="name"
									name="name"
									value={form.name}
									onChange={handleChange}
									variant="outlined"
									disabled={progress}
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 100,
									}}
								/>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-16 md:mr-8 w-full md:w-2/5"
										error={form.memberId === ''}
										required
										label="No Documento"
										id="memberId"
										name="memberId"
										value={form.memberId}
										onChange={handleChange}
										variant="outlined"
										disabled={progress}
										inputProps={{
											maxLength: 20,
										}}
									/>

									<TextField
										className="mt-16 md:mr-8 w-full md:w-2/5"
										error={form.member === ''}
										required
										label="No Socio"
										id="member"
										name="member"
										value={form.member}
										onChange={handleChange}
										variant="outlined"
										disabled={progress}
										inputProps={{
											maxLength: 20,
										}}
									/>

									<TextField
										id="birthday"
										name="birthday"
										label={t('DATE_BIRTHDAY')}
										type="date"
										className="mt-16 w-full md:w-1/5"
										InputLabelProps={{
											shrink: true
										}}
										inputProps={{
											max: moment(new Date()).format('YYYY-MM-DD')
										}}
										disabled={progress}
										value={form.birthday}
										onChange={handleChange}
										variant="outlined"
									/>
								</div>

								<FuseChipSelect
									className="mt-16"
									value={form.sport}
									onChange={value => handleChipChange(value, 'sport')}
									placeholder={t('SPORT_SELECTION')}
									required={true}
									getOptionLabel={option => `${option.name} (${option.gender.name})`}
									getOptionValue={option => option._id}
									variant='fixed'
									textFieldProps={{
										label: t('SPORT'),
										InputLabelProps: {
											shrink: true
										},
										variant: 'outlined'
									}}
									options={sports || []}
									isDisabled={progress}
								/>
								<FuseChipSelect
									className="mt-16"
									value={form.team}
									onChange={value => handleChipChange(value, 'team')}
									placeholder={t('TEAM_SELECTION')}
									required={true}
									getOptionLabel={option => option.name}
									getOptionValue={option => option._id}
									variant='fixed'
									textFieldProps={{
										label: t('TEAM'),
										InputLabelProps: {
											shrink: true
										},
										variant: 'outlined'
									}}
									options={teams || []}
									isDisabled={progress}
								/>

								<FuseChipSelect
									className="mt-16"
									value={form.division}
									onChange={value => handleChipChange(value, 'division')}
									placeholder={t('DIVISION_SELECTION')}
									getOptionLabel={option => option.name}
									getOptionValue={option => option._id}
									required={false}
									variant='fixed'
									textFieldProps={{
										label: t('DIVISION'),
										InputLabelProps: {
											shrink: true,
										},
										variant: 'outlined'
									}}
									options={divisions || []}
									isDisabled={progress}
								/>

								<TextField
									className="mt-16"
									label={t('SOURCE')}
									id="source"
									name="source"
									value={form.source}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 50,
									}}
									disabled={progress}
								/>
							</div>
						)}	{tabValue === 1 && (
							<div>
								<div className="flex justify-center sm:justify-start flex-wrap -mx-8">
									<label
										htmlFor="button-file"
										className={clsx(
											classes.productImageUpload,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5'
										)}
									>
										<input
											accept="image/*"
											className="hidden"
											id="button-file"
											type="file"
											onChange={handleUploadChange}
										/>
										<Icon fontSize="large" color="action">
											cloud_upload
										</Icon>
									</label>
									<div
										role="button"
										tabIndex={0}
										className={clsx(
											classes.productImageItem,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5',
										)}
										key={form._id}
									>
										<img className="max-w-128 w-128 h-128" src={form.photo || form.urlImage || constants.DEFAULT_URL_IMAGE_128_128} alt="Escudo" />
									</div>
								</div>
								<div className="flex flex-col">
									<span className="mx-4">{t('IMAGE_RECOMMENTED')}</span>
									<span className="mx-4" style={{ color: 'red', fontStyle: 'bold' }}>{imageError}</span>
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('playerApp', reducer)(Player);
