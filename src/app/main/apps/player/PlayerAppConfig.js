import React from 'react';
import { Redirect } from 'react-router-dom';

const PlayerAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/player/players/:playerId',
			component: React.lazy(() => import('./player/Player'))
		},
		{
			path: '/apps/player/players',
			component: React.lazy(() => import('./players/Players'))
		},
		{
			path: '/apps/player',
			component: () => <Redirect to="/apps/player/players" />
		}
	]
};

export default PlayerAppConfig;
