	
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import moment from 'moment/moment';
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
import { getInstitutionId, getUserId, getInstitutionName } from '../../../utils/AuthUtils'
export const GET_PLAYER = '[PLAYER APP] GET PLAYER';
export const GET_PLAYER_DIVISIONS = '[PLAYER APP] GET PLAYER DIVISIONS';
export const GET_PLAYER_TEAMS = '[PLAYER APP] GET PLAYER TEAMS';
export const GET_PLAYER_SPORTS = '[PLAYER APP] GET PLAYER SPORTS';
export const SAVE_PLAYER = '[PLAYER APP] SAVE PLAYER';
export const SET_LOADING_PLAYER = '[PLAYER APP] SET LOADING';
export const SET_PROGRESS_PLAYER = '[PLAYER APP] SET PROGRESS PLAYER';

const lang = i18next.language
const translate = translateInfo(lang)

export function getDivisions() {
	return dispatch => {
		const institutionId = getInstitutionId()
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/divisions/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const divisions = resp.data.models
			dispatch(setDivisions(divisions))
		}).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
	}
}

export function getSports() {
	return dispatch => {
		const institutionId = getInstitutionId()
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/mySports/form/institutions/${institutionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const sports = resp.data.model.sports
			dispatch(setSports(sports))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function getTeamsForSport(sportId) {
	return dispatch => {
		dispatch(setProgress(true))
		const institutionId = getInstitutionId()
		//dispatch(setLoading({ show: true, label: 'Cargando...' }))
		axios.get(`${translate.baseUrl}/teams/institutions/${institutionId}/sports/${sportId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const teams = resp.data.models
			dispatch(setTeams(teams))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setProgress(false))
			//dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function getPlayer(playerId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/players/${playerId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function setModel(model) {
	return {
		type: GET_PLAYER,
		payload: model
	}
}

export function setDivisions(divisions) {
	return {
		type: GET_PLAYER_DIVISIONS,
		payload: divisions
	}
}

export function setSports(models) {
	return {
		type: GET_PLAYER_SPORTS,
		payload: models
	}
}

export function setTeams(teams) {
	return {
		type: GET_PLAYER_TEAMS,
		payload: teams
	}
}

export function savePlayer(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing}))
		let id = ''
		if (method === 'post') {
			delete data['_id']
		} else {
			id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/players/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant: 'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newPlayer() {
	const data = {
		name: '',
		member: '',
		memberId: '',
		birthday: moment(new Date()).format('YYYY-MM-DD'),
		sport: null,
		team: null,
		division: null,
		photo: null,
		nameImage: null,
		nameId: null,
		urlImage: null,
		source: '',
		folder: `players/${getInstitutionName()}`,
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_PLAYER,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_PLAYER,
		payload: model
	};
}

export function setLoading(loading) {
	return {
		type: SET_LOADING_PLAYER,
		payload: loading
	}
}

export function setProgress(loading) {
	return {
		type: SET_PROGRESS_PLAYER,
		payload: loading
	}
}
