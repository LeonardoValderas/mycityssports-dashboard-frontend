import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_PLAYERS = '[PLAYERS APP] GET PLAYERS';
export const SET_PLAYERS_SEARCH_TEXT = '[PLAYERS APP] SET PLAYERS SEARCH TEXT';
export const SET_PLAYERS_PAGE_NUMBER_ROW = '[PLAYERS APP] SET PLAYERS PAGE NUMBER ROW';
export const SET_LOADING = '[PLAYERS APP] SET LOADING';
export const CLEAN_SELELCTED_PLAYER = '[PLAYERS APP] CLEAN SELELCTED PLAYER';

const lang = i18next.language
const translate = translateInfo(lang)

export function getPlayers() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/players/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_PLAYERS,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setPlayersSearchText(event) {
    return {
        type: SET_PLAYERS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function setPageAndRow(page, row) {
    return {
        type: SET_PLAYERS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function deletePlayers(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/players/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getPlayers())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_PLAYER,
        payload: clean
    }
}
