import { combineReducers } from 'redux';
import player from './player.reducer';
import players from './players.reducer';

const reducer = combineReducers({
	players,
	player
});

export default reducer;
