import * as Actions from '../actions';

const initialState = {
	data: null,
	sports: [],
	teams: [],
	divisions: [],
	returnPage: false,
	loading: {
		show: false,
		label: ''
	},
	progress: false,
	savedModel: false
};

const playerReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_PLAYER: {
			return {
				...state,
				data: action.payload,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				returnPage: false,
				savedModel: false
			};
		}
		case Actions.GET_PLAYER_DIVISIONS: {
			return {
				...state,
				divisions: action.payload,
				progress: false,
				savedModel: false
			};
		}
		case Actions.GET_PLAYER_SPORTS: {
			return {
				...state,
				sports: action.payload,
				savedModel: false,
				progress: false,
				loading: {
					show: false,
					label: ''
				}
			};
		}
		case Actions.GET_PLAYER_TEAMS: {
			return {
				...state,
				teams: action.payload,
				savedModel: false,
				progress: false,
				loading: {
					show: false,
					label: ''
				}
			};
		}
		case Actions.SAVE_PLAYER: {
			return {
				...state,
				data: action.payload,
				returnPage: true,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				savedModel: true
			};
		}
		case Actions.SET_LOADING_PLAYER: {
			return {
				...state,
				loading: action.payload,
				savedModel: false
			};
		}
		case Actions.SET_PROGRESS_PLAYER: {
			return {
				...state,
				progress: action.payload,
				loading: {
					show: false,
					label: ''
				},
				savedModel: false
			};
		}
		default: {
			return state;
		}
	}
};

export default playerReducer;
