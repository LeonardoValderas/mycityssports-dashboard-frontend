const locale = {
	IMAGE_BIG: 'O tamanho da imagem excede o máximo suportado.',
	PLAYER: 'Jogador',
	PLAYERS: 'Jogadores',
	NAME: 'Nome',
	UPDATE: 'Atualizar',
	DATE_BIRTHDAY: 'Data de aniversário',
	SPORT: 'Esporte',
	SPORT_SELECTION: 'Selecione o esporte',
	TEAM: 'Time',
	TEAM_SELECTION: 'Seleccione o time',
	DIVISION: 'Divisão',
	DIVISION_SELECTION: 'Selecione a divisão',
	SOURCE: 'Fonte',
	IMAGE_RECOMMENTED: 'Use imagem quadrada. Tamanho recomendado 128x128',
	ADD_NEW_PLAYER: 'Adicionar novo jogador',
	NEW_M: 'Novo',
	ATTENTION: 'Atenção',
	SINGLE_NEWS: 'o jogador',
	PLURAL_NEWS: 'os jogadores',
	DELETE_ITEM: 'Deseja deletar',
	NAME: 'Nome'

};

export default locale;
