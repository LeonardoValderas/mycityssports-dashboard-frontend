const locale = {
	IMAGE_BIG: 'El tamaño de la imagen supera el maximo soportado.',
	PLAYER: 'Jugador',
	PLAYERS: 'Jugadores',
	NAME: 'Nombre',
	UPDATE: 'Actualizar',
	DATE_BIRTHDAY: 'Fecha de nacimiento',
	SPORT: 'Deporte',
	SPORT_SELECTION: 'Seleccione el deporte',
	TEAM: 'Equipo',
	TEAM_SELECTION: 'Seleccione el equipo',
	DIVISION: 'División',
	DIVISION_SELECTION: 'Seleccione la división',
	SOURCE: 'Fuente',
	IMAGE_RECOMMENTED: 'Usar imagen cuadrada. Tamaño recomendado 128x128',
	ADD_NEW_PLAYER: 'Adicionar nuevo jugador',
	NEW_M: 'Nuevo',
	ATTENTION: 'Atención',
	SINGLE_NEWS: 'el jugador',
	PLURAL_NEWS: 'los jugadores',
	DELETE_ITEM: 'Desea eliminar',
	NAME: 'Nombre'
};

export default locale;
