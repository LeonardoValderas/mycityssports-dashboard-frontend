import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import PlayersHeader from './PlayersHeader';
import PlayersTable from './PlayersTable';

function Players() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<PlayersHeader />}
			content={<PlayersTable />}
			innerScroll
		/>
	);
}

export default withReducer('playerApp', reducer)(Players);
