import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
//import FuseChipSelect from '@fuse/core/FuseChipSelect';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../../store/actions';
import reducer from '../../store/reducers';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../../ranking-i18n/es';
import pt from '../../ranking-i18n/pt';
i18next.addResourceBundle('es', 'ranking', es);
i18next.addResourceBundle('pt', 'ranking', pt);

function RankingType(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('ranking');
	const rankingType = useSelector(({ rankingTypeApp }) => rankingTypeApp.rankingType.data);
	//const sports = useSelector(({ rankingTypeApp }) => rankingTypeApp.rankingType.sports);
	const savedModel = useSelector(({ rankingTypeApp }) => rankingTypeApp.rankingType.savedModel);
	const theme = useTheme();
	const loading = useSelector(({ rankingTypeApp }) => rankingTypeApp.rankingType.loading);
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();

	useDeepCompareEffect(() => {
		//dispatch(Actions.getMySports());
		function updateRankingTypeState() {
			const { rankingTypeId } = routeParams;
			if (rankingTypeId === 'new') {
				dispatch(Actions.newRankingType());
			} else {
				dispatch(Actions.getRankingType(rankingTypeId));
			}
		}

		updateRankingTypeState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(rankingType);
		}
	}, [savedModel]);

	// function setFormValue(name, value) {
	// 	setForm(
	// 		_.set(
	// 			{ ...form },
	// 			name, value
	// 		)
	// 	);
	// }

	// function handleChipChangeSport(value) {
	// 	setFormValue('sports', value)
	// }

	useEffect(() => {
		if ((rankingType && !form) || (rankingType && form && rankingType._id !== form._id)) {
			setForm(rankingType);
		}		
	}, [form, rankingType, setForm]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		//return form.name.length > 0 && form.sports && !_.isEqual(rankingType, form);
		return form.name.length > 0 && !_.isEqual(rankingType, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/ranking/rankingType/rankingTypes"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">Tipos de Ranking</span>
								</Typography>
							</FuseAnimate>
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.saveRankingType(form, routeParams.rankingTypeId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.rankingTypeId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}
				>
					<Tab className="h-64 normal-case" label="Tipo de Ranking" />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div>
								<TextField
									className="mt-8 mb-16"
									error={form.name === ''}
									required
									label="Tipo de Ranking"
									autoFocus
									id="name"
									name="name"
									value={form.name}
									onChange={handleChange}
									variant="outlined"
									fullWidth
									inputProps={{
										maxLength: 80,
									}}
								/>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('rankingTypeApp', reducer)(RankingType);
