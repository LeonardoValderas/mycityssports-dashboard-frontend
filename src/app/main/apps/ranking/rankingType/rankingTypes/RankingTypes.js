import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../../store/reducers';
import RankingTypesHeader from './RankingTypesHeader';
import RankingTypesTable from './RankingTypesTable';

function RankingTypes() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<RankingTypesHeader />}
			content={<RankingTypesTable />}
			innerScroll
		/>
	);
}

export default withReducer('rankingTypeApp', reducer)(RankingTypes);
