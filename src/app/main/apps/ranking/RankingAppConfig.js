import React from 'react';
import { Redirect } from 'react-router-dom';

const RankingAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/ranking/ranking/rankings/:rankingId',
			component: React.lazy(() => import('./ranking/ranking/Ranking'))
		},
		{
			path: '/apps/ranking/ranking/rankings',
			component: React.lazy(() => import('./ranking/rankings/Rankings'))
		},
		{
			path: '/apps/ranking/rankingType/rankingTypes/:rankingTypeId',
			component: React.lazy(() => import('./rankingType/rankingType/RankingType'))
		},
		{
			path: '/apps/ranking/rankingType/rankingTypes',
			component: React.lazy(() => import('./rankingType/rankingTypes/RankingTypes'))
		},
		{
			path: '/apps/ranking',
			component: () => <Redirect to="/apps/ranking/ranking/rankings" />
		}
	]
};

export default RankingAppConfig;
