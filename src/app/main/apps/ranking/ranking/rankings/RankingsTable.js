import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import TablePagination from '@material-ui/core/TablePagination';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../../store/actions';
import FuseLoading from '@fuse/core/FuseLoading';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CollapsibleTable from './CollapsibleTable';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../../ranking-i18n/es';
import pt from '../../ranking-i18n/pt';
i18next.addResourceBundle('es', 'ranking', es);
i18next.addResourceBundle('pt', 'ranking', pt);

function RankingsTable(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('ranking');
	const rankings = useSelector(({ rankingsApp }) => rankingsApp.rankings.data);
	const searchText = useSelector(({ rankingsApp }) => rankingsApp.rankings.searchText);
	const loading = useSelector(({ rankingsApp }) => rankingsApp.rankings.loading);
	const cleanSelected = useSelector(({ rankingsApp }) => rankingsApp.rankings.cleanSelected);
	const [clean, setClean] = useState(cleanSelected);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(rankings);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	// const [order, setOrder] = useState({
	// 	direction: 'asc',
	// 	id: null
	// });
	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useEffect(() => {
		setClean(cleanSelected)
		if (cleanSelected) {
			setSelected([]);
			//setSelectedrankingsToDelete([]);
		}
	}, [cleanSelected]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(rankings, item => item.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(rankings);
		}
	}, [rankings, searchText]);

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function deleteRankings() {
		handleClose()
		const tournamentId = rankings[0].tournament._id
		dispatch(Actions.deleteRankings(tournamentId, { ids: selected }));
	}

	function deleteSelectedItems() {
		handleClickOpen()
	}

	function handleClick(item) {
		props.history.push(`/apps/ranking/ranking/rankings/${item._id}`);
	}

	function handleCheck(event, id) {
		setClean(false)
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			// dont exists, concatenate two arrays adding id in the end of selected array.
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			// item found, it is on first position. so delete first item slice(1) and add new list in newSelected.
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			// slice(0, -1) remove the last item
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			// slice(0, selectedIndex) it return the firsts items until item's index.
			// slice(selectedIndex + 1) it return the last items without item.
			//concat does a new list
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	return (
		<div className="w-full flex flex-col">
			<div>
				<Dialog
					open={open}
					onClose={handleClose}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title">{t('ATTENTION')}</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							{`${t('DELETE_ITEM')} ${selected.length === 1 ? t('SINGLE_RANKING') : t('PLURAL_RANKING')}?`}
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose} color="primary">
							Cancelar
          				</Button>
						<Button onClick={deleteRankings} color="primary" autoFocus>
							Eliminar
          				</Button>
					</DialogActions>
				</Dialog>
			</div>
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<CollapsibleTable
					rankings={data || []}
					selected={selected}
					handleCheck={handleCheck}
					handleClick={handleClick}
					deleteSelectedItems={deleteSelectedItems}
				/>
			</FuseScrollbars>
			<div>
				<TablePagination
					className="overflow-hidden"
					component="div"
					count={data.length}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Prev Pag'
					}}
					nextIconButtonProps={{
						'aria-label': 'Sig Pag'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
}

export default withRouter(RankingsTable);
