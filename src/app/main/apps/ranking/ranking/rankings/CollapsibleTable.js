import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import CollapsibleRankingTable from './CollapsibleRankingTable';
import RankingsTableHead from './RankingsTableHead';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

function Row(props) {
  const {
    row,
    handleClick,
    handleCheck,
    selected,
    deleteSelectedItems
  } = props;
  const [open, setOpen] = React.useState(false);
  return (
    <React.Fragment>
      <TableRow
        className="h-64 cursor-pointer"
        onClick={() => setOpen(!open)}>
        <TableCell align="left">
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell align="left">
          {row.tournament.name} {row.tournament.year}
        </TableCell>
        <TableCell align="left">
          {row.division.name}
        </TableCell>
        <TableCell align="left">
          {row.subDivision}
        </TableCell>
        <TableCell align="left">
          {row.sport.name} {row.sport.gender.name}
        </TableCell>
      </TableRow>

      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <FuseScrollbars className="flex-grow overflow-x-auto">
              <div >
                <React.Fragment>
                  <div className="mt-16">
                    {
                      row.rankings !== undefined &&
                      row.rankings &&
                      row.rankings.length > 0 && <CollapsibleRankingTable
                        rankings={row.rankings}
                        handleClick={handleClick}
                        handleCheck={handleCheck}
                        selected={selected}
                        deleteSelectedItems={deleteSelectedItems}
                      />
                    }
                  </div>
                </React.Fragment>
              </div>
            </FuseScrollbars>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

export default function CollapsibleTable(props) {
  const {
    handleClick,
    handleCheck,
    selected,
    deleteSelectedItems,
    rankings
  } = props
  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <RankingsTableHead
          rowCount={rankings.length}
        />
        <TableBody>
          {rankings !== undefined && rankings && rankings.length > 0 && rankings.map((row) => (
            <Row
              key={row._id}
              row={row}
              handleClick={handleClick}
              handleCheck={handleCheck}
              selected={selected}
              deleteSelectedItems={deleteSelectedItems}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}