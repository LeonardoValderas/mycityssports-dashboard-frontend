import React from 'react';
import _ from '@lodash';
import { makeStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';
import CollapsibleTableRankingHead from './CollapsibleTableRankingHead';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const useRowStyles = makeStyles({
	root: {
		'& > *': {
			borderBottom: 'unset',
		},
	},
});


function Row(props) {
	const {
		row,
		clean,
		deleteSelectedItems,
		selected,
		handleClick,
		handleCheck
	} = props;

	const [page, setPage] = React.useState(0);
	const [rowsPerPage, setRowsPerPage] = React.useState(10);
	const [order, setOrder] = React.useState({
		direction: 'asc',
		id: null
	});
	const [open, setOpen] = React.useState(false);
	const [rankings, setRankings] = React.useState(row.rankings);
	const classes = useRowStyles();

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	return (
		<React.Fragment>
			<TableRow className={classes.root} onClick={() => setOpen(!open)}>
				<TableCell style={{ width: 5 }} size='small' component="td" scope="row" align="left">
					<IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
						{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
					</IconButton>
				</TableCell>
				<TableCell align="left">
					{row.rankingType.name} {row.isDescendent ? '(Descendente)' : '(Ascendente)'}
				</TableCell>
			</TableRow>

			<TableRow>
				<TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
					<Collapse in={open} timeout="auto" unmountOnExit>
						<FuseScrollbars className="flex-grow overflow-x-auto">
							<Table className="min-w-xl" aria-labelledby="tableTitle">
								<CollapsibleTableRankingHead
									numSelected={selected && selected.length}
									order={order}
									cleanSelected={clean}
									onRequestSort={handleRequestSort}
									rowCount={rankings.length}
									deleteSelectedItems={deleteSelectedItems}
								/>

								<TableBody>
									{_.orderBy(
										rankings,
										[
											o => {
												switch (order.id) {
													case 'categories': {
														return o.categories[0];
													}
													default: {
														const orderId = order.id
														if (orderId && o !== null) {
															let filter = o
															const split = orderId.split('.')
															split.map(s => {
																const f = filter[s]
																if (f) {
																	filter = f
																}
															})
															return filter;
														}
														return o[order.id];
													}
												}
											}
										],
										[order && order.direction]
									)
										.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
										.map(n => {
											const isSelected = selected && selected.indexOf(n._id) !== -1;
											return (
												<TableRow
													className="h-64 cursor-pointer"
													hover
													role="checkbox"
													aria-checked={isSelected}
													tabIndex={-1}
													key={n._id}
													selected={isSelected}
													onClick={event => handleClick(n)}
												>
													<TableCell className="w-64 text-center" padding="none">
														<Checkbox
															checked={isSelected}
															onClick={event => event.stopPropagation()}
															onChange={event => handleCheck(event, n._id)}
														/>
													</TableCell>

													<TableCell component="th" scope="row">
														{n.player.name}
													</TableCell>
													<TableCell component="th" scope="row">
														{n.team.name}
													</TableCell>
													<TableCell component="th" scope="row">
														{n.point}
													</TableCell>
												</TableRow>
											);
										})}
								</TableBody>
							</Table>
						</FuseScrollbars>
						<div>
							<TablePagination
								className="overflow-hidden"
								component="div"
								count={rankings && rankings.length}
								rowsPerPage={rowsPerPage}
								page={page}
								backIconButtonProps={{
									'aria-label': 'Prev Pag'
								}}
								nextIconButtonProps={{
									'aria-label': 'Sig Pag'
								}}
								onChangePage={handleChangePage}
								onChangeRowsPerPage={handleChangeRowsPerPage}
							/>
						</div>
					</Collapse>
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
}

export default function CollapsibleRankingTable(props) {
	const {
		handleClick,
		handleCheck,
		selected,
		deleteSelectedItems,
		rankings,
	} = props
	return (
		<TableContainer component={Paper}>
			<Table aria-label="collapsible table">
				<TableHead>
					<TableRow>
						<TableCell align="left" colSpan={2}>Rankings</TableCell>
						<TableCell />
					</TableRow>
				</TableHead>

				<TableBody>
					{rankings !== undefined && rankings.length > 0 && rankings.map((row) => (
						<Row
							key={row._id}
							row={row}
							handleClick={handleClick}
							handleCheck={handleCheck}
							selected={selected}
							deleteSelectedItems={deleteSelectedItems}
						/>
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
}