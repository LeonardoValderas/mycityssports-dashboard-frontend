import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../../store/reducers';
import RankingsHeader from './RankingsHeader';
import RankingsTable from './RankingsTable';

function Rankings() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<RankingsHeader />}
			content={<RankingsTable />}
			innerScroll
		/>
	);
}

export default withReducer('rankingsApp', reducer)(Rankings);
