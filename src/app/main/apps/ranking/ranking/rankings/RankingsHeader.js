import FuseAnimate from '@fuse/core/FuseAnimate';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import Typography from '@material-ui/core/Typography';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import * as Actions from '../../store/actions';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../../ranking-i18n/es';
import pt from '../../ranking-i18n/pt';
i18next.addResourceBundle('es', 'ranking', es);
i18next.addResourceBundle('pt', 'ranking', pt);

function RankingsHeader(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('ranking');
	const tournaments = useSelector(({ rankingsApp }) => rankingsApp.rankings.tournaments);
	const [tournament, setTournament] = useState(null);

	useEffect(() => {
		dispatch(Actions.getTournaments());
	}, [dispatch]);

	useEffect(() => {
		if (tournaments && tournaments[0] && !tournament) {
			if(tournament === null){
				const t = tournaments[0]
				setTournament(t)
				dispatch(Actions.getRankingsByTournamentId(t._id));
			}
		}
	}, [tournaments]);


	const handleTournamentChange = (value) => {
		if (!tournament || tournament._id !== value._id) {
			setTournament(value)
			dispatch(Actions.getRankingsByTournamentId(value._id));
		}
	}

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex items-center">
				<FuseAnimate animation="transition.expandIn" delay={300}>
					<Icon className="text-32">looks_one</Icon>
				</FuseAnimate>
				<FuseAnimate animation="transition.slideLeftIn" delay={300}>
					<Typography className="hidden sm:flex mx-0 sm:mx-12" variant="h6">
						Rankings
					</Typography>
				</FuseAnimate>
			</div>

			<div className="flex flex-1 items-center justify-center px-12">
				<FuseChipSelect
					className="w-full max-w-512"
					value={tournament}
					onChange={value => handleTournamentChange(value)}
					placeholder={t('TOURNAMENT')}
					// required={true}
					variant='fixed'
					getOptionLabel={option => `${option.name} ${option.year}`}
					getOptionValue={option => option._id}
					textFieldProps={{
						label: t('TOURNAMENT'),
						InputLabelProps: {
							shrink: true
						},
						variant: 'outlined'
					}}
					options={tournaments || []}
				/>
			</div>
			<FuseAnimate animation="transition.slideRightIn" delay={300}>
				<Button
					component={Link}
					to="/apps/ranking/ranking/rankings/new"
					className="whitespace-no-wrap normal-case"
					variant="contained"
					color="secondary"
				>
					<span className="hidden sm:flex">{t('ADD_NEW_RANKING')}</span>
					<span className="flex sm:hidden">{t('NEW_M')}</span>
				</Button>
			</FuseAnimate>
		</div>
	);
}

export default RankingsHeader;
