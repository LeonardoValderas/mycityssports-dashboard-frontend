import * as Actions from '../actions';

const initialState = {
	data: [],
	tournaments: [],
	searchText: '',
	loading: {
	  show: false,
	  label: ''
	},
	cleanSelected: false
};

const rankingsReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_RANKINGS: {
			return {
				...state,
				data: action.payload, 
				loading: {
					show: false,
					label: ''
				  }
			};
		}
		case Actions.GET_TOURNAMENTS_RANKINGS: {
			return {
				...state,
				tournaments: action.payload,
				progress: false,
				savedModel: false
			};
		}
		case Actions.SET_LOADING: {
			return {
				...state,
				loading: action.payload
			};
		}
		case Actions.SET_RANKINGS_SEARCH_TEXT: {
			return {
				...state,
				searchText: action.searchText
			};
		}
		case Actions.CLEAN_SELELCTED_RANKINGS: {
			return {
				...state,
				cleanSelected: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default rankingsReducer;
