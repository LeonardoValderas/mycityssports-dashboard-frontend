import { combineReducers } from 'redux';
import rankingType from './rankingType.reducer';
import rankingTypes from './rankingTypes.reducer';
import ranking from './ranking.reducer';
import rankings from './rankings.reducer';

const reducer = combineReducers({
	rankingTypes,
	rankingType,
	ranking,
	rankings
});

export default reducer;
