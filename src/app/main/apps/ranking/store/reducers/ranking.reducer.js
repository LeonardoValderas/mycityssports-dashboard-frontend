import * as Actions from '../actions';

const initialState = {
	data: null,
	rankingTypes: [],
	teams: [],
	tournaments: [],
	players: [],
	returnPage: false,
	loading: {
		show: false,
		label: ''
	},
	progress: false,
	savedModel: false
};

const rankingReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_RANKING: {
			return {
				...state,
				data: action.payload,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				returnPage: false,
				savedModel: false
			};
		}
		case Actions.GET_RANKING_FORM: {
			return {
				...state,
				rankingTypes: action.payload.rankingTypes || [],
				tournaments: action.payload.tournaments || [],
				progress: false,
				savedModel: false
			};
		}
		// case Actions.GET_RANKING_SPORTS: {
		// 	return {
		// 		...state,
		// 		sports: action.payload,
		// 		progress: false,
		// 		savedModel: false
		// 	};
		// }
		case Actions.GET_RANKING_TEAMS: {
			return {
				...state,
				teams: action.payload,
				savedModel: false,
				loading: {
					show: false,
					label: '',
				},
				progress: false,
			};
		}
		// case Actions.GET_RANKING_DIVISIONS: {
		// 	return {
		// 		...state,
		// 		divisions: action.payload,
		// 		savedModel: false,
		// 		loading: {
		// 			show: false,
		// 			label: ''
		// 		},
		// 		progress: false,
		// 	};
		// }
		case Actions.GET_RANKING_TOURNAMENTS: {
			return {
				...state,
				tournaments: action.payload,
				savedModel: false,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
			};
		}
		case Actions.GET_RANKING_PLAYERS: {
			return {
				...state,
				players: action.payload,
				savedModel: false,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
			};
		}
		case Actions.SAVE_RANKING: {
			return {
				...state,
				data: action.payload,
				returnPage: true,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				savedModel: true
			};
		}
		case Actions.SET_LOADING_RANKING: {
			return {
				...state,
				loading: action.payload,
				savedModel: false,
				progress: false,
			};
		}
		case Actions.SET_PROGRESS_RANKING: {
			return {
				...state,
				progress: action.payload,
				loading: {
					show: false,
					label: ''
				},
				savedModel: false
			};
		}
		default: {
			return state;
		}
	}
};

export default rankingReducer;
