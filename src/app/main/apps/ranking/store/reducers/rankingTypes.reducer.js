import * as Actions from '../actions';

const initialState = {
	data: [],
	searchText: '',
	loading: {
	  show: false,
	  label: ''
	},
	cleanSelected: false
};

const rankingTypesReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_RANKING_TYPES: {
			return {
				...state,
				data: action.payload, 
				loading: {
					show: false,
					label: ''
				  }
			};
		}
		case Actions.SET_LOADING: {
			return {
				...state,
				loading: action.payload
			};
		}
		case Actions.SET_RANKING_TYPES_SEARCH_TEXT: {
			return {
				...state,
				searchText: action.searchText
			};
		}
		case Actions.CLEAN_SELELCTED_RANKING_TYPES: {
			return {
				...state,
				cleanSelected: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default rankingTypesReducer;
