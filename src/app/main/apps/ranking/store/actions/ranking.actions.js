
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_RANKING = '[RANKING APP] GET RANKING';
export const GET_RANKING_FORM = '[RANKING APP] GET RANKING FORM';
export const GET_RANKING_SPORTS = '[RANKING APP] GET RANKING SPORTS';
export const GET_RANKING_TEAMS = '[RANKING APP] GET RANKING TEAMS';
export const GET_RANKING_DIVISIONS = '[RANKING APP] GET RANKING DIVISIONS';
export const GET_RANKING_TOURNAMENTS = '[RANKING APP] GET RANKING TOURNAMENTS';
export const GET_RANKING_PLAYERS = '[RANKING APP] GET RANKING PLAYERS';
export const SAVE_RANKING = '[RANKING APP] SAVE RANKING';
export const SET_LOADING_RANKING = '[RANKING APP] SET LOADING';
export const SET_PROGRESS_RANKING = '[RANKING APP] SET PROGRESS RANKING';


const lang = i18next.language
const translate = translateInfo(lang)

export function getRankingForm() {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		const institutionId = getInstitutionId()
		axios.get(`${translate.baseUrl}/rankings/form/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
	        const models = resp.data.models
			dispatch(setModels(models || []))
		}).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
	}
}

export function getTeamsForSportId(sportId) {
	return dispatch => {
		dispatch(setProgress(true))
		//dispatch(setLoading({ show: true, label: 'Cargando...' }))
		const institutionId = getInstitutionId()
		axios.get(`${translate.baseUrl}/teams/institutions/${institutionId}/sports/${sportId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
			const models = resp.data.models
			dispatch(setTeams(models || []))
		}).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setProgress(false))
        })
	}
}

export function getPlayersForTeamIdAndDivisionId(teamId, divisionId) {
	return dispatch => {
		dispatch(setProgress(true))
		//dispatch(setLoading({ show: true, label: 'Cargando...' }))
		const institutionId = getInstitutionId()
		axios.get(`${translate.baseUrl}/players/institutions/${institutionId}/teams/${teamId}/divisions/${divisionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
			const models = resp.data.models
			dispatch(setPlayers(models || []))
		}).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
			dispatch(setProgress(false))
			//dispatch(setLoading({ show: false, label: '' }))
        })
	}
}

export function getRanking(rankingId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/rankings/${rankingId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function setModel(model) {
	return {
		type: GET_RANKING,
		payload: model
	}
}

export function setModels(models) {
	return {
		type: GET_RANKING_FORM,
		payload: models
	}
}

export function setTeams(teams) {
	return {
		type: GET_RANKING_TEAMS,
		payload: teams
	}
}

export function setPlayers(players) {
	return {
		type: GET_RANKING_PLAYERS,
		payload: players
	}
}

export function saveRanking(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/rankings/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newRanking() {
	const data = {
		rankingType: null,
		sport: null,
		team: null,
		division: null,
		subDivision: null,
		tournament: null,
		player: null,
		point: null,
		isDescendent: true,
		comment:'',
		source:'',
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_RANKING,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_RANKING,
		payload: model
	};
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_RANKING,
        payload: loading
    }
}

export function setProgress(loading) {
	return {
		type: SET_PROGRESS_RANKING,
		payload: loading
	}
}