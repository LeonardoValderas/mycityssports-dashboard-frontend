
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_RANKING_TYPE = '[RANKING_TYPE APP] GET RANKING_TYPE';
export const SAVE_RANKING_TYPE = '[RANKING_TYPE APP] SAVE RANKING_TYPE';
export const SET_LOADING_RANKING_TYPE = '[RANKING_TYPE APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getRankingType(rankingTypeId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/rankingTypes/${rankingTypeId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function setModel(model) {
	return {
		type: GET_RANKING_TYPE,
		payload: model
	}
}

export function saveRankingType(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing}))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/rankingTypes/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newRankingType() {
	const data = {
		name: '',
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_RANKING_TYPE,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_RANKING_TYPE,
		payload: model
	};
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_RANKING_TYPE,
        payload: loading
    }
}
