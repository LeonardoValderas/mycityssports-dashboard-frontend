import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_RANKING_TYPES = '[RANKING TYPES APP] GET RANKING TYPES';
export const SET_RANKING_TYPES_SEARCH_TEXT = '[RANKING TYPES APP] SET RANKING TYPES SEARCH TEXT';
export const SET_LOADING = '[RANKING TYPES APP] SET LOADING';
export const CLEAN_SELELCTED_RANKING_TYPES = '[RANKING TYPES APP] CLEAN SELELCTED RANKING TYPES';

const lang = i18next.language
const translate = translateInfo(lang)

export function getRankingTypes() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading}))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/rankingTypes/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_RANKING_TYPES,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setRankingTypesSearchText(event) {
    return {
        type: SET_RANKING_TYPES_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteRankingTypes(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/rankingTypes/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getRankingTypes())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_RANKING_TYPES,
        payload: clean
    }
}
