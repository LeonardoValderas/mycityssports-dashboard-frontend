import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_RANKINGS = '[RANKINGS APP] GET RANKINGS';
export const GET_TOURNAMENTS_RANKINGS = '[RANKINGS APP] GET TOURNAMENTS RANKINGS';
export const SET_RANKINGS_SEARCH_TEXT = '[RANKINGS APP] SET RANKINGS SEARCH TEXT';
export const SET_LOADING = '[RANKINGS APP] SET LOADING';
export const CLEAN_SELELCTED_RANKINGS = '[RANKINGS APP] CLEAN SELELCTED RANKINGS';


const lang = i18next.language
const translate = translateInfo(lang)

export function getTournaments() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading}))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/tournaments/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setTournaments(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function getRankingsByTournamentId(tournamentId) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/rankings/institutions/${institutionId}/tournaments/${tournamentId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setTournaments(tournaments) {
    return {
        type: GET_TOURNAMENTS_RANKINGS,
        payload: tournaments
    }
}

export function setModels(models) {
    return {
        type: GET_RANKINGS,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setRankingsSearchText(event) {
    return {
        type: SET_RANKINGS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteRankings(tournamentId, ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/rankings/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getRankingsByTournamentId(tournamentId))
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_RANKINGS,
        payload: clean
    }
}
