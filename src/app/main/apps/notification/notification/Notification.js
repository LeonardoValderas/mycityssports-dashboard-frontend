import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../notification-i18n/es';
import pt from '../notification-i18n/pt';
i18next.addResourceBundle('es', 'notification', es);
i18next.addResourceBundle('pt', 'notification', pt);


function Notification(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('notification');
	const notification = useSelector(({ notificationApp }) => notificationApp.notification.data);
	const savedModel = useSelector(({ notificationApp }) => notificationApp.notification.savedModel);
	const loading = useSelector(({ notificationApp }) => notificationApp.notification.loading);
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();

	useDeepCompareEffect(() => {
		function updateNotificationState() {
			dispatch(Actions.newNotification());
		}

		updateNotificationState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(notification);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((notification && !form) || (notification && form && notification._id !== form._id)) {
			setForm(notification);
		}
	}, [form, notification, setForm]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return form.message && form.message.length > 15 && !_.isEqual(notification, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.saveNotification(form, 'post'))}
							>
								Enviar
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}
				>
					<Tab className="h-64 normal-case" label={t('NOTIFICATION')} />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row mt-8">
									<TextField
										error={form.message === ''}
										required
										label={t('MESSAGE')}
										autoFocus
										id="message"
										name="message"
										inputProps={{
											maxLength: 150,
										}}
										value={form.message}
										onChange={handleChange}
										variant="outlined"
										fullWidth
									/>
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('notificationApp', reducer)(Notification);
