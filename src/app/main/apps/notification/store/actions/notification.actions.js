
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { getInstitutionId, getUserId, getInstitutionName } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_NOTIFICATION = '[NOTIFICATION APP] GET NOTIFICATION';
export const SAVE_NOTIFICATION = '[NOTIFICATION APP] SAVE NOTIFICATION';
export const SET_LOADING_NOTIFICATION = '[NOTIFICATION APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

// export function getNotification(notificationId) {
// 	return dispatch => {
// 		dispatch(setLoading({ show: true, label: translate.loading }))
// 		axios.get(`${translate.baseUrl}/notifications/${notificationId}`, {
// 			headers: {
// 				'Content-Type': 'application/json'
// 			}
// 		}).then(resp => {
// 			const model = resp.data.model
// 			dispatch(setModel(model))
// 		}).catch(e => {
// 				const message = exceptionMessage(e);
// 				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
// 				dispatch(setLoading({ show: false, label: '' }))
// 			})
// 	}
// }

export function setModel(model) {
	return {
		type: GET_NOTIFICATION,
		payload: model
	}
}

export function saveNotification(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		delete data['_id']
		axios[method](`${translate.baseUrl}/notifications`, data)
			.then(resp => {
				console.log('passooo')
				dispatch(save())
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newNotification() {
	const data = {
		message: '',
		user: getUserId(),
		institutionName: getInstitutionName(),
		institution: getInstitutionId()
	};

	return {
		type: GET_NOTIFICATION,
		payload: data
	};
}

export function save() {
	return {
		type: SAVE_NOTIFICATION,
		payload: {
			message: '',
			user: getUserId(),
			institutionName: getInstitutionName(),
			institution: getInstitutionId()
	    }
	}
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_NOTIFICATION,
        payload: loading
    }
}
