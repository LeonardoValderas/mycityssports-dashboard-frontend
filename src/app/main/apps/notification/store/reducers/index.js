import { combineReducers } from 'redux';
import notification from './notification.reducer';

const reducer = combineReducers({
	notification
});

export default reducer;
