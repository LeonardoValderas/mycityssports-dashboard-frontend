import * as Actions from '../actions';

const initialState = {
	data: null,
	returnPage: false,
	loading: {
		show: false,
		label: ''
	  },
	savedModel: false  
};

const notificationReducer = (state = initialState, action) => {
	console.log('type', action.type)
	switch (action.type) {
		case Actions.GET_NOTIFICATION: {
			return {
				...state,
				data: action.payload,
				loading: {
					show: false,
					label: ''
				  },
				  returnPage: false,
				  savedModel: false
			};
		}
		// case Actions.GET_NOTIFICATION_SPORTS: {
		// 	return {
		// 		...state,
		// 		sports: action.payload,
		// 		loading: {
		// 			show: false,
		// 			label: ''
		// 		  },
		// 		  returnPage: false,
		// 		  savedModel: false
		// 	};
		// }
		case Actions.SAVE_NOTIFICATION: {
			return {
				...state,
				data: action.payload, 
				returnPage: true,
				loading: {
					show: false,
					label: ''
				  },
				  savedModel: true
			};
		}
		case Actions.SET_LOADING_NOTIFICATION: {
			return {
				...state,
				loading: action.payload,
	        	savedModel: false
			};
		}
		default: {
			return state;
		}
	}
};

export default notificationReducer;
