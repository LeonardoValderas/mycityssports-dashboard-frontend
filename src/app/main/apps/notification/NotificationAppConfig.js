import React from 'react';
import { Redirect } from 'react-router-dom';

const NotificationAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/notification',
			component: React.lazy(() => import('./notification/Notification'))
		}
	]
};

export default NotificationAppConfig;
