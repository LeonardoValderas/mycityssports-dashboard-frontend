import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import TablePagination from '@material-ui/core/TablePagination';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../store/actions';
import FuseLoading from '@fuse/core/FuseLoading';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CollapsibleTable from '../CollapsibleTable';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../fixture-i18n/es';
import pt from '../fixture-i18n/pt';

i18next.addResourceBundle('es', 'fixture', es);
i18next.addResourceBundle('pt', 'fixture', pt);

function FixturesTable(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('fixture');
	const fixtures = useSelector(({ fixturesApp }) => fixturesApp.fixtures.data);
	const searchText = useSelector(({ fixturesApp }) => fixturesApp.fixtures.searchText);
	const loading = useSelector(({ fixturesApp }) => fixturesApp.fixtures.loading);
	const mainStateFixtures = useSelector(({ fixturesApp }) => fixturesApp.fixtures.mainStateFixtures);
	const tournament = useSelector(({ fixturesApp }) => fixturesApp.fixtures.tournament);
	const cleanSelected = useSelector(({ fixturesApp }) => fixturesApp.fixtures.cleanSelected);
	const [clean, setClean] = useState(cleanSelected);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(fixtures);
	const [page, setPage] = useState(mainStateFixtures.mainPageRow.page);
	const [rowsPerPage, setRowsPerPage] = useState(mainStateFixtures.mainPageRow.row);
	const [openRows, setOpenRows] = useState(mainStateFixtures.openMainRows);
	const [openDialog, setOpenDialog] = React.useState(false);
	const [tournamentId, setTournamentId] = useState(tournament);

	const handleClickOpen = () => {
		setOpenDialog(true);
	};
	const handleClose = () => {
		setOpenDialog(false);
	};

	useEffect(() => {
		setClean(cleanSelected)
		if (cleanSelected) {
			setSelected([]);
		}
	}, [cleanSelected]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(fixtures, item => item.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(fixtures);
		}
	}, [fixtures, searchText]);

	useEffect(() => {
		if (tournament !== tournamentId) {
			setOpenRows([])
			setTournamentId(tournament)
		}
	}, [tournament]);

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function deleteFixtures() {
		handleClose()
		const tournamentId = fixtures[0].tournament._id
		dispatch(Actions.deleteFixtures(tournamentId, { ids: selected }));
	}

	function deleteSelectedItems() {
		handleClickOpen()
	}

	function handleClick(item) {
		dispatch(Actions.setMainStatesFixtures(page, rowsPerPage, openRows))
		props.history.push(`/apps/fixture/fixtures/${item._id}`);
	}

	function handleCheck(event, id) {
		setClean(false)
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			// dont exists, concatenate two arrays adding id in the end of selected array.
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			// item found, it is on first position. so delete first item slice(1) and add new list in newSelected.
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			// slice(0, -1) remove the last item
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			// slice(0, selectedIndex) it return the firsts items until item's index.
			// slice(selectedIndex + 1) it return the last items without item.
			//concat does a new list
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleOpenMainRows(id) {
		const newRow = [{
			idRow: id,
			zones: [
			],
			instances: [

			],
			points: [

			]
		}]
		const rowIndex = openRows.findIndex((row) => row.idRow === id)

		let newOpenRows = [];

		if (rowIndex === -1) {
			// dont exists, concatenate two arrays adding id in the end of selected array.
			newOpenRows = newOpenRows.concat(openRows, newRow);
		} else if (rowIndex === 0) {
			// item found, it is on first position. so delete first item slice(1) and add new list in newSelected.
			newOpenRows = newOpenRows.concat(openRows.slice(1));
		} else if (rowIndex === openRows.length - 1) {
			// slice(0, -1) remove the last item
			newOpenRows = newOpenRows.concat(openRows.slice(0, -1));
		} else if (rowIndex > 0) {
			// slice(0, rowIndex) it return the firsts items until item's index.
			// slice(selectedIndex + 1) it return the last items without item.
			//concat does a new list
			newOpenRows = newOpenRows.concat(openRows.slice(0, rowIndex), openRows.slice(rowIndex + 1));
		}
		setOpenRows(newOpenRows)
	}

	function handleOpenMainRowZones(id, zoneId) {
		const copyRows = openRows
		const rowIndex = copyRows.findIndex((row) => row.idRow === id)
		if (rowIndex === -1) {
			return
		}

		const row = copyRows[rowIndex]
		const zones = row.zones
		const zoneIndex = zones.findIndex((z) => z.id === zoneId);
		let newOpenRowZones = [];

		if (zoneIndex === -1) {
			// dont exists, concatenate two arrays adding id in the end of selected array.
			newOpenRowZones = newOpenRowZones.concat(zones, {
				id: zoneId,
				page: 0,
				rowPage: 10
			});
		} else if (zoneIndex === 0) {
			// item found, it is on first position. so delete first item slice(1) and add new list in newSelected.
			newOpenRowZones = newOpenRowZones.concat(zones.slice(1));
		} else if (zoneIndex === zones.length - 1) {
			// slice(0, -1) remove the last item
			newOpenRowZones = newOpenRowZones.concat(zones.slice(0, -1));
		} else if (zoneIndex > 0) {
			// slice(0, rowIndex) it return the firsts items until item's index.
			// slice(selectedIndex + 1) it return the last items without item.
			//concat does a new list
			newOpenRowZones = newOpenRowZones.concat(zones.slice(0, zoneIndex), zones.slice(zoneIndex + 1));
		}

		row.zones = newOpenRowZones
		copyRows[rowIndex] = row
		setOpenRows(copyRows)
	}

	function handlePageRowPageZone(id, zoneId, page, rowPage) {
		const copyRows = openRows
		const rowIndex = copyRows.findIndex((row) => row.idRow === id)
		if (rowIndex === -1) {
			return
		}

		const row = copyRows[rowIndex]
		const zones = row.zones
		const zoneIndex = zones.findIndex((z) => z.id === zoneId);

		if (zoneIndex === -1) {
			return
		}
		const zone = zones[zoneIndex]
		zone.page = page
		zone.rowPage = rowPage
		zones[zoneIndex] = zone
		row.zones = zones
		copyRows[rowIndex] = row
		setOpenRows(copyRows)
	}

	function handleOpenMainRowInstances(id, instanceId) {
		const copyRows = openRows
		const rowIndex = copyRows.findIndex((row) => row.idRow === id)
		if (rowIndex === -1) {
			return
		}

		const row = copyRows[rowIndex]
		const instances = row.instances

		const instanceIndex = instances.findIndex((z) => z.id === instanceId);
		let newOpenRowInstances = [];

		if (instanceIndex === -1) {
			// dont exists, concatenate two arrays adding id in the end of selected array.
			newOpenRowInstances = newOpenRowInstances.concat(instances, {
				id: instanceId,
				page: 0,
				rowPage: 10
			});
		} else if (instanceIndex === 0) {
			// item found, it is on first position. so delete first item slice(1) and add new list in newSelected.
			newOpenRowInstances = newOpenRowInstances.concat(instances.slice(1));
		} else if (instanceIndex === instances.length - 1) {
			// slice(0, -1) remove the last item
			newOpenRowInstances = newOpenRowInstances.concat(instances.slice(0, -1));
		} else if (instanceIndex > 0) {
			// slice(0, rowIndex) it return the firsts items until item's index.
			// slice(selectedIndex + 1) it return the last items without item.
			//concat does a new list
			newOpenRowInstances = newOpenRowInstances.concat(instances.slice(0, instanceIndex), instances.slice(instanceIndex + 1));
		}

		row.instances = newOpenRowInstances
		copyRows[rowIndex] = row
		setOpenRows(copyRows)
	}

	function handlePageRowPageInstance(id, instanceId, page, rowPage) {
		const copyRows = openRows
		const rowIndex = copyRows.findIndex((row) => row.idRow === id)
		if (rowIndex === -1) {
			return
		}

		const row = copyRows[rowIndex]
		const instances = row.instances
		const instanceIndex = instances.findIndex((i) => i.id === instanceId);

		if (instanceIndex === -1) {
			return
		}
		const instance = instances[instanceIndex]
		instance.page = page
		instance.rowPage = rowPage
		instances[instanceIndex] = instance
		row.instances = instances
		copyRows[rowIndex] = row
		setOpenRows(copyRows)
	}

	function handleOpenMainRowPoints(id, round) {
		const copyRows = openRows
		const rowIndex = copyRows.findIndex((row) => row.idRow === id)
		if (rowIndex === -1) {
			return
		}

		const row = copyRows[rowIndex]
		const points = row.points
		const pointIndex = points.findIndex((p) => p.round === round);
		let newOpenRowPoints = [];

		if (pointIndex === -1) {
			// dont exists, concatenate two arrays adding id in the end of selected array.
			newOpenRowPoints = newOpenRowPoints.concat(points, {
				round: round,
				page: 0,
				rowPage: 10
			});
		} else if (pointIndex === 0) {
			// item found, it is on first position. so delete first item slice(1) and add new list in newSelected.
			newOpenRowPoints = newOpenRowPoints.concat(points.slice(1));
		} else if (pointIndex === points.length - 1) {
			// slice(0, -1) remove the last item
			newOpenRowPoints = newOpenRowPoints.concat(points.slice(0, -1));
		} else if (pointIndex > 0) {
			// slice(0, rowIndex) it return the firsts items until item's index.
			// slice(selectedIndex + 1) it return the last items without item.
			//concat does a new list
			newOpenRowPoints = newOpenRowPoints.concat(points.slice(0, pointIndex), points.slice(pointIndex + 1));
		}

		row.points = newOpenRowPoints
		copyRows[rowIndex] = row
		setOpenRows(copyRows)
	}

	function handlePageRowPagePoint(id, round, page, rowPage) {
		const copyRows = openRows
		const rowIndex = copyRows.findIndex((row) => row.idRow === id)
		if (rowIndex === -1) {
			return
		}

		const row = copyRows[rowIndex]
		const points = row.points
		const pointIndex = points.findIndex((p) => p.round === round);

		if (pointIndex === -1) {
			return
		}
		const point = points[pointIndex]
		point.page = page
		point.rowPage = rowPage
		points[pointIndex] = point
		row.points = points
		copyRows[rowIndex] = row
		setOpenRows(copyRows)
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	return (
		<div className="w-full flex flex-col">
			<div>
				<Dialog
					open={openDialog}
					onClose={handleClose}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title">{t('ATTENTION')}</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							{`${t('DELETE_FIXTURE')} ${selected.length === 1 ? t('SINGLE_FIXTURE') : t('PLURAL_FIXTURE')}?`}
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose} color="primary">
							Cancelar
          				</Button>
						<Button onClick={deleteFixtures} color="primary" autoFocus>
							Eliminar
          				</Button>
					</DialogActions>
				</Dialog>
			</div>
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<CollapsibleTable
					fixtures={data || []}
					selected={selected}
					handleCheck={handleCheck}
					handleClick={handleClick}
					openRows={openRows}
					handleOpenMainRows={handleOpenMainRows}
					handleOpenMainRowZones={handleOpenMainRowZones}
					handlePageRowPageZone={handlePageRowPageZone}
					handleOpenMainRowInstances={handleOpenMainRowInstances}
					handlePageRowPageInstance={handlePageRowPageInstance}
					handleOpenMainRowPoints={handleOpenMainRowPoints}
					handlePageRowPagePoint={handlePageRowPagePoint}
					deleteSelectedItems={deleteSelectedItems}
				/>
			</FuseScrollbars>
			<div>
				<TablePagination
					className="overflow-hidden"
					component="div"
					count={data.length}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Prev Pag'
					}}
					nextIconButtonProps={{
						'aria-label': 'Sig Pag'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
}

export default withRouter(FixturesTable);
