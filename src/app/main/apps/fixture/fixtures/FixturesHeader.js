import FuseAnimate from '@fuse/core/FuseAnimate';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import Typography from '@material-ui/core/Typography';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import * as Actions from '../store/actions';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../fixture-i18n/es';
import pt from '../fixture-i18n/pt';

i18next.addResourceBundle('es', 'fixture', es);
i18next.addResourceBundle('pt', 'fixture', pt);

function FixturesHeader(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('fixture');
	const tournaments = useSelector(({ fixturesApp }) => fixturesApp.fixtures.tournaments);
	const tournamentId = useSelector(({ fixturesApp }) => fixturesApp.fixtures.tournament);
	const [tournament, setTournament] = useState(null);

	useEffect(() => {
		dispatch(Actions.getTournaments());
	}, [dispatch]);

	useEffect(() => {
		if(tournaments && tournaments.length > 0) {
			var currenteTournament = tournaments[0]
			if(tournamentId !== null){
				const t = tournaments.find(t => 
					t._id === tournamentId
				)
				if(t){
					currenteTournament = t
				} 
			}

			setTournament(currenteTournament)
			dispatch(Actions.getFixturesByTournamentId(currenteTournament._id));
		} else {
			dispatch(Actions.setTournament(null));
			setTournament(null)
		}
	}, [tournaments]);


	const handleTournamentChange = (value) => {
		if (!tournament || tournament._id !== value._id) {
			setTournament(value)
			dispatch(Actions.getFixturesByTournamentId(value._id));
		}
	}

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex items-center">
				<FuseAnimate animation="transition.expandIn" delay={300}>
					<Icon className="text-32">list_alt</Icon>
				</FuseAnimate>
				<FuseAnimate animation="transition.slideLeftIn" delay={300}>
					<Typography className="hidden sm:flex mx-0 sm:mx-12" variant="h6">
						Fixtures
					</Typography>
				</FuseAnimate>
			</div>

			<div className="flex flex-1 items-center justify-center px-12">
				<FuseChipSelect
					className="w-full max-w-512"
					value={tournament}
					onChange={value => handleTournamentChange(value)}
					placeholder={t('TOURNAMENT')}
					// required={true}
					variant='fixed'
					getOptionLabel={option => `${option.name} ${option.year}`}
					getOptionValue={option => option._id}
					textFieldProps={{
						label: t('TOURNAMENT'),
						InputLabelProps: {
							shrink: true
						},
						variant: 'outlined'
					}}
					options={tournaments || []}
				/>
			</div>
			<FuseAnimate animation="transition.slideRightIn" delay={300}>
				<Button
					component={Link}
					to="/apps/fixture/fixtures/new"
					className="whitespace-no-wrap normal-case"
					variant="contained"
					color="secondary"
				>
					<span className="hidden sm:flex">{t('ADD_NEW_FIXTURE')}</span>
					<span className="flex sm:hidden">{t('NEW')}</span>
				</Button>
			</FuseAnimate>
		</div>
	);
}

export default FixturesHeader;
