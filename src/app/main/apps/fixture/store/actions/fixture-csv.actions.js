
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import moment from 'moment/moment';
import i18next from 'i18next';
export const GET_FIXTURE_CSV = '[FIXTURE CSV APP] GET FIXTURE CSV';
export const GET_FIXTURE_CSV_TOURNAMENT = '[FIXTURE CSV APP] GET FIXTURE CSV TOURNAMENT';
export const INIT_FIXTURE_CSV = '[FIXTURE CSV APP] INIT FIXTURE CSV';
export const SET_LOADING_FIXTURE_CSV = '[FIXTURE CSV APP] SET LOADING';
export const SET_PROGRESS_FIXTURE_CSV = '[FIXTURE CSV APP] SET PROGRESS';

const lang = i18next.language
const translate = translateInfo(lang)

export function getTournamentsCsv() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/tournaments/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setTournaments(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function getFixturesToCSV(form) {
	const tournamentId = form.tournament._id
	const sportId = form.sport._id
    const divisionId = form.division._id
    const start = new Date(form.dateTimeStart).getTime()
    const end = new Date(form.dateTimeEnd).getTime()
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/fixtures/institutions/${institutionId}/tournaments/${tournamentId}/sports/${sportId}/divisions/${divisionId}?start=${start}&end=${end}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModel(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setTournaments(models) {
	return {
		type: GET_FIXTURE_CSV_TOURNAMENT,
		payload: models
	}
}


export function setModel(model) {
	return {
		type: GET_FIXTURE_CSV,
		payload: model
	}
}

export function initFixtureCsv() {
	const data = {
        dateTimeStart: moment(new Date()).format('YYYY-MM-DD'),
        dateTimeEnd: moment(new Date()).format('YYYY-MM-DD'),
		sport: null,
		tournament: null,
		division: null,
		subDivision: null
	};

	return {
		type: INIT_FIXTURE_CSV,
		payload: data
	};
}

export function setLoading(loading) {
	return {
		type: SET_LOADING_FIXTURE_CSV,
		payload: loading
	}
}

export function setProgress(loading) {
	return {
		type: SET_PROGRESS_FIXTURE_CSV,
		payload: loading
	}
}

