import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_FIXTURES = '[FIXTURES APP] GET FIXTURES';
export const GET_TOURNAMENTS_FIXTURES = '[FIXTURES APP] GET TOURNAMENTS FIXTURES';
export const SET_TOURNAMENT_FIXTURES = '[FIXTURES APP] SET TOURNAMENT FIXTURES';
export const SET_FIXTURES_MAIN_STATES = '[FIXTURES APP] SET FIXTURES MAIN STATES';
export const SET_FIXTURES_SEARCH_TEXT = '[FIXTURES APP] SET FIXTURES SEARCH TEXT';
export const SET_LOADING = '[FIXTURES APP] SET LOADING';
export const CLEAN_SELELCTED_FIXTURE = '[FIXTURES APP] CLEAN SELELCTED FIXTURE';

const lang = i18next.language
const translate = translateInfo(lang)

export function getTournaments() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading}))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/tournaments/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setTournaments(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function getFixturesByTournamentId(tournamentId) {
    setTournament(tournamentId)
    return dispatch => {
        dispatch(setTournament(tournamentId))
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/fixtures/institutions/${institutionId}/tournaments/${tournamentId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setMainStatesFixtures(page, row, openMainRows) {
    return {
        type: SET_FIXTURES_MAIN_STATES,
        mainStateFixtures: {
            mainPageRow: {
                page: page,
                row: row
            },
            openMainRows: openMainRows
        }
    };
}

export function setTournaments(tournaments) {
    return {
        type: GET_TOURNAMENTS_FIXTURES,
        payload: tournaments
    }
}

export function setTournament(tournament) {
    return {
        type: SET_TOURNAMENT_FIXTURES,
        tournament: tournament
    }
}

export function setModels(models) {
    return {
        type: GET_FIXTURES,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setFixturesSearchText(event) {
    return {
        type: SET_FIXTURES_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteFixtures(tournamentId, ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/fixtures/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getFixturesByTournamentId(tournamentId))
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_FIXTURE,
        payload: clean
    }
}
