
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import moment from 'moment/moment';
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_FIXTURE = '[FIXTURE APP] GET FIXTURE';
export const GET_FIXTURE_FORM = '[FIXTURE APP] GET FIXTURE FORM';
export const GET_FIXTURE_HOME_PLAYERS = '[FIXTURE APP] GET FIXTURE HOME PLAYERS';
export const GET_FIXTURE_VISITORS_PLAYERS = '[FIXTURE APP] GET FIXTURE VISITORS PLAYERS';
export const GET_FIXTURE_TEAMS = '[FIXTURE APP] GET FIXTURE TEAMS';
export const SAVE_FIXTURE = '[FIXTURE APP] SAVE FIXTURE';
export const SET_LOADING_FIXTURE = '[FIXTURE APP] SET LOADING';
export const SET_PROGRESS_FIXTURE = '[FIXTURE APP] SET PROGRESS';

const lang = i18next.language
const translate = translateInfo(lang)

export function getDataForm() {
	return dispatch => {
		const institutionId = getInstitutionId()
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/fixtures/form/institutions/${institutionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const models = resp.data.models
			dispatch(setModels(models || []))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function getTeamsForSportId(sportId) {
	return dispatch => {
		const institutionId = getInstitutionId()
		dispatch(setProgress(true))
		axios.get(`${translate.baseUrl}/teams/institutions/${institutionId}/sports/${sportId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const models = resp.data.models
			dispatch(setTeams(models || []))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setProgress(false))
			//dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function getPlayersForTeamIdDivisionId(teamId, divisionId, isHomeTeam) {
	return dispatch => {
		const institutionId = getInstitutionId()
		dispatch(setProgress(true))
		axios.get(`${translate.baseUrl}/players/institutions/${institutionId}/teams/${teamId}/divisions/${divisionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const models = resp.data.models
			dispatch(setPlayers(models || [], isHomeTeam))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setProgress(true))
			//dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function getFixture(fixtureId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/fixtures/${fixtureId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
			// if(model){
			// 	const zone = model.tournamentZones.find(tz => String(tz.tournamentZone) === String(tournamentZone))
			// 	if(zone){
			// 		const item = zone.fixtures.find(f => String(f._id) === String(zoneId))
					
			// 		dispatch(setModel(item))
			// 	}
			// }
			
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function setModel(model) {
	return {
		type: GET_FIXTURE,
		payload: model
	}
}

export function setModels(models) {
	return {
		type: GET_FIXTURE_FORM,
		payload: models
	}
}

export function setPlayers(players, isHomeTeam) {
	return {
		type: isHomeTeam ? GET_FIXTURE_HOME_PLAYERS : GET_FIXTURE_VISITORS_PLAYERS,
		payload: players
	}
}

export function setTeams(teams) {
	return {
		type: GET_FIXTURE_TEAMS,
		payload: teams
	}
}

export function saveFixture(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		let id = ''
		if (method === 'post') {
			delete data['_id']
		} else {
			id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/fixtures/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant: 'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newFixture() {
	const data = {
		dateTime: moment(new Date()).format('YYYY-MM-DD HH:mm'),
		sport: null,
		tournament: null,
		division: null,
		subDivision: null,
		round: 1,
		tournamentInstance: null,
		tournamentZone: null,
		homeTeam: null,
		visitorsTeam: null,
		playingField: null,
		comment: '',
		source: '',
		
		referees: [],

		homeResult: '',
		homeScorers: [],
		homeCards: [],
		homeResultPenalty:'',
		
		visitorsResult: '',
		visitorsScorers: [],
		visitorsCards: [],
		visitorsResultPenalty: '',
		
		isFinish: false,
		user: getUserId(),
		institution: getInstitutionId()

	};

	return {
		type: GET_FIXTURE,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_FIXTURE,
		payload: model
	};
}

export function setLoading(loading) {
	return {
		type: SET_LOADING_FIXTURE,
		payload: loading
	}
}

export function setProgress(loading) {
	return {
		type: SET_PROGRESS_FIXTURE,
		payload: loading
	}
}

