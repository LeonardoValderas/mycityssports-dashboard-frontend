import * as Actions from '../actions';

const initialState = {
	data: [],
	searchText: '',
	tournaments:[],
	tournament: null,
	mainStateFixtures:{
		mainPageRow: {
			page: 0, 
			row: 10
		},
		openMainRows:[]
	},
	loading: {
	  show: false,
	  label: ''
	},
	cleanSelected: false
};

const fixturesReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_TOURNAMENTS_FIXTURES: {
			return {
				...state,
				tournaments: action.payload, 
				loading: {
					show: false,
					label: ''
				  }
			};
		}
		case Actions.SET_TOURNAMENT_FIXTURES: {
			return {
				...state,
				tournament: action.tournament, 
			};
		}
		case Actions.SET_FIXTURES_MAIN_STATES: {
			return {
				...state,
				mainStateFixtures: action.mainStateFixtures
			};
		}
		case Actions.GET_FIXTURES: {
			return {
				...state,
				data: action.payload, 
				loading: {
					show: false,
					label: ''
				  }
			};
		}
		case Actions.SET_LOADING: {
			return {
				...state,
				loading: action.payload
			};
		}
		case Actions.SET_FIXTURES_SEARCH_TEXT: {
			return {
				...state,
				searchText: action.searchText
			};
		}
		case Actions.CLEAN_SELELCTED_FIXTURE: {
			return {
				...state,
				cleanSelected: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default fixturesReducer;
