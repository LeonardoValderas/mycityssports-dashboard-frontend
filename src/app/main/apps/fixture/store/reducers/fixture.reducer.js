import * as Actions from '../actions';

const initialState = {
	data: null,
	groups: null,
	sports: [],
	teams: [],
	tournaments: [],
	divisions: [],
	playingFieds: [],
	homePlayers: [],
	visitorsPlayers: [],
	referees:[],
	refereeFunctions: [],
	cards: [],
	returnPage: false,
	loading: {
		show: false,
		label: ''
	},
	progress: false,
	savedModel: false
};

const fixtureReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_FIXTURE: {
			return {
				...state,
				data: action.payload,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				returnPage: false,
				savedModel: false
			};
		}

		// case Actions.GET_NEW_FIXTURE: {
		// 	return {
		// 		...state,
		// 		data: null,
		// 		newData: action.payload,
		// 		loading: {
		// 			show: false,
		// 			label: ''
		// 		},
		// 		progress: false,
		// 		returnPage: false,
		// 		savedModel: false
		// 	};
		// }
		case Actions.GET_FIXTURE_FORM: {
			return {
				...state,
				sports: action.payload && action.payload.sports && action.payload.sports.sports || [],
				divisions: action.payload && action.payload.divisions || [],
				tournaments: action.payload && action.payload.tournaments || [],
				playingFields: action.payload && action.payload.playingFields || [],
				referees: action.payload && action.payload.referees || [],
				refereeFunctions: action.payload && action.payload.refereeFunctions || [],
				cards: action.payload && action.payload.cards || [],
				savedModel: false
			};
		}
		case Actions.GET_FIXTURE_HOME_PLAYERS: {
			return {
				...state,
				homePlayers: action.payload,
				savedModel: false,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
			};
		}
		case Actions.GET_FIXTURE_VISITORS_PLAYERS: {
			return {
				...state,
				visitorsPlayers: action.payload,
				savedModel: false,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
			};
		}
		case Actions.GET_FIXTURE_TEAMS: {
			return {
				...state,
				teams: action.payload,
				savedModel: false,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
			};
		}
		case Actions.SAVE_FIXTURE: {
			return {
				...state,
				data: action.payload,
				returnPage: true,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				savedModel: true
			};
		}
		case Actions.SET_LOADING_FIXTURE: {
			return {
				...state,
				loading: action.payload,
				progress: false,
				savedModel: false
			};
		}
		case Actions.SET_PROGRESS_FIXTURE: {
			return {
				...state,
				loading: {
					show: false,
					label: ''
				},
				progress: action.payload,
				savedModel: false
			};
		}
		default: {
			return state;
		}
	}
};

export default fixtureReducer;
