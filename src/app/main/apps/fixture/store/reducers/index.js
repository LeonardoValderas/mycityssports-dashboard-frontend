import { combineReducers } from 'redux';
import fixture from './fixture.reducer';
import fixtures from './fixtures.reducer';
import fixtureCsv from './fixture-csv.reducer';

const reducer = combineReducers({
	fixtures,
	fixture,
	fixtureCsv
});

export default reducer;
