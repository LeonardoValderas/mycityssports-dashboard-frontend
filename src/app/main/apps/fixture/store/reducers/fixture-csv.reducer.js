import * as Actions from '../actions';

const initialState = {
	dataInit: null,
	data: null,
	tournaments: [],
	returnPage: false,
	
	loading: {
		show: false,
		label: ''
	},
	progress: false
};

const fixtureCsvReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.INIT_FIXTURE_CSV: {
			return {
				...state,
				dataInit: action.payload,
			};
		}

		case Actions.GET_FIXTURE_CSV: {
			return {
				...state,
				data: action.payload,
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				returnPage: false,
			};
		}

		case Actions.GET_FIXTURE_CSV_TOURNAMENT: {
			return {
				...state,
				tournaments: action.payload || [],
				loading: {
					show: false,
					label: ''
				},
				progress: false,
				returnPage: false
			};
		}
		case Actions.SET_LOADING_FIXTURE_CSV: {
			return {
				...state,
				loading: action.payload,
				progress: false
			};
		}
		case Actions.SET_PROGRESS_FIXTURE_CSV: {
			return {
				...state,
				loading: {
					show: false,
					label: ''
				},
				progress: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default fixtureCsvReducer;
