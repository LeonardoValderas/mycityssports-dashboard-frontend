import React from 'react';
import { Redirect } from 'react-router-dom';

const FixturesAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/fixture/fixtures/:fixtureId',
			component: React.lazy(() => import('./fixture/Fixture'))
		},
		{
			path: '/apps/fixture/fixtures',
			component: React.lazy(() => import('./fixtures/Fixtures'))
		},
		{
			path: '/apps/fixture/fixture-csv',
			component: React.lazy(() => import('./fixture-csv/FixtureCsv'))
		},
		{
			path: '/apps/fixture',
			component: () => <Redirect to="/apps/fixture/fixtures" />
		}
	]
};

export default FixturesAppConfig;
