import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth:400,
        display: 'block',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    formControl: {
        marginTop: theme.spacing(0),
        display: 'flex'
    },
    gridList: {
        flexWrap: 'nowrap',
        height: '50',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
    title: {
        minWidth:280,
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
}));

export default function FixtureRefereesGridList(props) {
    const classes = useStyles();
    const t = props.t
    if (!props.refereeModels || props.refereeModels === '' || props.refereeModels.lenght === 0) return null

    var refereeModels = props.refereeModels || []
    function handleChange(name, event, i) {
        if (name === 'referee') {
            const value = event.target.value
            const referee = getRefereeById(value)
            refereeModels[i].referee = referee
        } else {
            const value = event.target.value
            const refereeFunction = getFunctionById(value)
            refereeModels[i].refereeFunction = refereeFunction
        } 

        props.onHandleChange(refereeModels, i)
    }
    const getFunctionById = (functionId) => {
        const refereeFunction = props.refereeFunctions.find(f => f._id === functionId)
        return refereeFunction
    }
    const getRefereeById = (refereeId) => {
        const referee = props.referees.find(r => r._id === refereeId)
        return referee
    }

    return (
        <div >
            <GridList cellHeight={80} className="flex flex-col w-full flex-shrink-0 sm:flex-row"> {
                refereeModels.map((item, index) => (
                    <GridListTile key={index}  cols={1} className={classes.title}>
                        <div className="flex mx-0">
                            <div className="flex mt-8 w-5/6">
                                <div className="w-2/3">
                                    <FormControl variant="outlined" disabled={props.progress} className={classes.formControl}  >
                                        <InputLabel id="selectLabel">
                                          {t('REFEERE')}
                                        </InputLabel>
                                        <Select
                                            className="min-w-120"
                                            labelId="selectLabel"
                                            name="referee"
                                            
                                            variant={'outlined'}
                                            value={item.referee && item.referee._id || ''}
                                            onChange={(value) => handleChange('referee', value, index)}>
                                            {
                                                props.referees.map((item, i) =>

                                                    <MenuItem
                                                        value={item._id}
                                                        key={i}>
                                                        {item.name}
                                                    </MenuItem>)
                                            }
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="ml-6 w-2/3">
                                <FormControl variant="outlined" disabled={props.progress} className={classes.formControl}  >
                                        <InputLabel id="selectLabel">
                                          {t('FUNCTION')}
                                        </InputLabel>
                                        <Select
                                            labelId="selectLabel"
                                            name="function"
                                            autoWidth
                                            variant={'outlined'}
                                            value={item.refereeFunction && item.refereeFunction._id || ''}
                                            onChange={(value) => handleChange('refereeFunction', value, index)}>
                                            {
                                                props.refereeFunctions.map((item, i) =>
                                                    <MenuItem
                                                        value={item._id}
                                                        key={i}>
                                                        {item.name}
                                                    </MenuItem>)
                                            }
                                        </Select>
                                    </FormControl>
                                </div>
                            </div>
                            <IconButton
                                onClick={() =>
                                    props.removeReferees()
                                }
                                disabled={props.progress}
                                className="mt-6"
                            >
                                <Icon>remove</Icon>
                            </IconButton>
                        </div>
                    </GridListTile>
                )
                )
            }
            </GridList>
        </div>
    );
}