import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FusePageCarded from '@fuse/core/FusePageCarded';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import moment from 'moment/moment';
import _ from '@lodash';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import FixtureScorersGridList from './FixtureScorersGridList';
import FixtureRefereesGridList from './FixtureRefereesGridList';
import FixtureCardsGridList from './FixtureCardsGridList';
import { DateTimePicker } from '@material-ui/pickers';
import { isIntegerMaiorZeroAndMinorEqualsToOneHundred, isIntegerAndMinorEqualsToOneHundred } from '../../utils/NumberValidator'
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../fixture-i18n/es';
import pt from '../fixture-i18n/pt';

i18next.addResourceBundle('es', 'fixture', es);
i18next.addResourceBundle('pt', 'fixture', pt);

function Fixture(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('fixture');
	const data = useSelector(({ fixtureApp }) => fixtureApp.fixture.data);
	const savedModel = useSelector(({ fixtureApp }) => fixtureApp.fixture.savedModel);
	const loading = useSelector(({ fixtureApp }) => fixtureApp.fixture.loading);
	const progress = useSelector(({ fixtureApp }) => fixtureApp.fixture.progress);
	const tournaments = useSelector(({ fixtureApp }) => fixtureApp.fixture.tournaments);
	const teamModels = useSelector(({ fixtureApp }) => fixtureApp.fixture.teams);
	const homePlayersModels = useSelector(({ fixtureApp }) => fixtureApp.fixture.homePlayers);
	const visitorsPlayersModels = useSelector(({ fixtureApp }) => fixtureApp.fixture.visitorsPlayers);
	const playingFields = useSelector(({ fixtureApp }) => fixtureApp.fixture.playingFields);
	const referees = useSelector(({ fixtureApp }) => fixtureApp.fixture.referees);
	const refereeFunctions = useSelector(({ fixtureApp }) => fixtureApp.fixture.refereeFunctions);
	const cards = useSelector(({ fixtureApp }) => fixtureApp.fixture.cards);
	const [teams, setTeams] = useState(teamModels);
	const [homePlayers, setHomePlayers] = useState(homePlayersModels);
	const [visitorsPlayers, setVisitorsPlayers] = useState(visitorsPlayersModels);
	const [instances, setInstances] = useState([]);
	const [zones, setZones] = useState([]);
	const [sports, setSports] = useState([]);
	const [divisions, setDivisions] = useState([]);
	const [subDivisions, setSubDivisions] = useState([]);
	const [tabValue, setTabValue] = useState(0);
	const [teamEnable, setTeamEnable] = useState(false);
	const [codeEnable, setCodeEnable] = useState(false);
	const [finish, setFinish] = useState(true);
	const [penaltyEnable, setPenaltyEnable] = useState(false);
	const theme = useTheme();
	const { form, handleChange, setInForm, setForm } = useForm(null);
	const routeParams = useParams();

	useDeepCompareEffect(() => {
		dispatch(Actions.getDataForm());
		function updateDivisionState() {
			const { fixtureId } = routeParams;
			if (fixtureId === 'new') {
				dispatch(Actions.newFixture());
			} else {
				dispatch(Actions.getFixture(fixtureId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (teamModels) {
			setTeams(teamModels);
		}
	}, [teamModels]);

	useEffect(() => {
		if (homePlayersModels) {
			setHomePlayers(homePlayersModels);
		}
	}, [homePlayersModels]);

	useEffect(() => {
		if (visitorsPlayersModels) {
			setVisitorsPlayers(visitorsPlayersModels);
		}
	}, [visitorsPlayersModels]);

	useEffect(() => {
		if (form) {
			if (form.sport && form.division) {
				setTeamEnable(true);
			} else {
				setTeamEnable(false);
			}
			if (form.tournament && form.tournament.tournamentType) {
				const code = form.tournament.tournamentType.code
				if (_.isEqual(code, "1")) {
					setCodeEnable(true)
				} else {
					setCodeEnable(false)
				}
			}

			if (form.homeResult && form.visitorsResult && form.tournamentInstance) {
				const resultEquals = _.isEqual(form.homeResult, form.visitorsResult) &&
					(!_.isEqual(form.homeResult, '') && !_.isEqual(form.visitorsResult, ''))
				// if (!resultEquals) {
				// 	form.homeResultPenalty = ''
				// 	form.visitorsResultPenalty = ''
				// }
				//setPenaltyEnable(resultEquals && form.tournamentInstance) // I need change the logic because now I have go and back and the global result can be equals 
				setPenaltyEnable(form.tournamentInstance)
			}
		}
	}, [form]);

	useEffect(() => {
		if (SubmittedFormValidation() !== null && form !== null
			&& (!_.isEqual(form.homeResult, '') && !_.isEqual(form.visitorsResult, ''))
			&& (form.homeResult !== null && form.visitorsResult !== null)
		) {
			setFinish(false);
		} else {
			if(form !== null)
				form.isFinish = false
			setFinish(true);
		}
	}, [form]);

	useEffect(() => {
		if (savedModel) {
			setForm(data);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((data && !form) || (data && form && data._id !== form._id)) {
			if (routeParams.fixtureId !== 'new') {
				getDataFromFormData(data);
			}
			setForm(data);
		}
	}, [form, data, setForm]);

	function getDataFromFormData(data) {
		dispatch(Actions.getTeamsForSportId(data.sport && data.sport !== undefined && data.sport._id));
		if (data.tournament && data.tournament !== undefined && data.tournament._id) {
			const id = data.tournament._id
			setTournamentSports(id)
			setTournamentDivisions(id)
			setTournamentSubDivisions(id)
			setTournamentInstances(id)
			setTournamentZones(id)
		}

		dispatch(
			Actions.getPlayersForTeamIdDivisionId(
				data.homeTeam && data.homeTeam !== undefined && data.homeTeam._id,
				data.division && data.division !== undefined && data.division._id,
				true
			)
		);
		dispatch(Actions.getPlayersForTeamIdDivisionId(
			data.visitorsTeam && data.visitorsTeam !== undefined && data.visitorsTeam._id,
			data.division && data.division !== undefined && data.division._id,
			false));
	}

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return SubmittedFormValidation()
			&& SubmittedTournamentType()
			&& !_.isEqual(data, form);
	}

	function SubmittedTournamentType() {
		const tournament = form.tournament
		if (!tournament && !tournament.tournamentType) {
			return false
		}
		const code = tournament.tournamentType.code
		const result = _.isEqual(code, "1") || (_.isEqual(code, "2")
			&& (form.tournamentInstance || form.tournamentZone))

		return result
	}

	function resultEnable() {
		return (form.homeTeam && form.visitorsTeam)
	}

	function SubmittedFormValidation() {
		return form
			&& form.dateTime !== ''
			&& form.round
			&& form.sport
			&& form.tournament
			&& form.division
			&& subDivisionValidation()
			&& form.homeTeam
			&& form.visitorsTeam
			&& form.homeTeam._id !== form.visitorsTeam._id
			&& form.playingField
			&& ResultSubmitted()
	}

	function subDivisionValidation(){
	   return _.isEmpty(subDivisions) ? true : form.subDivision
	}

	function ResultSubmitted() {
		const result = (
			form !== null && (
				(_.isEqual(form.homeResult, '') && _.isEqual(form.visitorsResult, ''))
				||
				(form.homeResult === null && form.visitorsResult === null)
				||
				(!_.isEqual(form.homeResult, '') && !_.isEqual(form.visitorsResult, ''))
				||
				(form.homeResult !== null && form.visitorsResult !== null)
			)
		)
		return result
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function setTournamentZones(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setZones(tournament.tournamentZones)
			return
		}
		setZones([])
	}

	function setTournamentInstances(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setInstances(tournament.tournamentInstances)
			return
		}
		setInstances([])
	}

	function setTournamentDivisions(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setDivisions(tournament.divisions)
			return
		}
		setDivisions([])
	}

	function setTournamentSports(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setSports(tournament.sports)
			return
		}
		setSports([])
	}

	function setTournamentSubDivisions(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setSubDivisions(tournament.subDivisions)
			return
		}
		setSubDivisions([])
	}

	function handleChipChangeVisitorsResulResult(event) {
		const value = event.target.value
		if (isIntegerAndMinorEqualsToOneHundred(value) === true) {
			setFormValue('visitorsResult', value)
		}
	}

	function handleChipChangeVisitorsResulResultPenalty(event) {
		const value = event.target.value
		if (isIntegerAndMinorEqualsToOneHundred(value) === true) {
			setFormValue('visitorsResultPenalty', value)
		}
	}

	function handleChipChangeHomeResulResult(event) {
		const value = event.target.value
		if (isIntegerAndMinorEqualsToOneHundred(value) === true) {
			setFormValue('homeResult', value)
		}
	}

	function addHomeResultScorers() {
		const result = form.homeResult
		var scorers = form.homeScorers
		if (result && result !== '') {
			if (result > scorers.length) {
				scorers.push({
					player: null,
					minute: null,
					against: false
				})
			}
		}

		setFormValue('homeScorers', scorers)
	}

	function removeHomeResultScorers() {
		var scorers = []
		scorers = scorers.concat(form.homeScorers.slice(0, -1))
		setFormValue('homeScorers', scorers)
	}

	function addHomeCards() {
		var homeCards = form.homeCards
		homeCards.push({
			player: null,
			minute: null,
			card: _.head(cards)
		})

		setFormValue('homeCards', homeCards)
	}

	function removeHomeCards() {
		var cards = []
		cards = cards.concat(form.homeCards.slice(0, -1))
		setFormValue('homeCards', cards)
	}

	function addVisitorsResultScorers() {
		const result = form.visitorsResult
		var scorers = form.visitorsScorers
		if (result && result !== '') {
			if (result > scorers.length) {
				scorers.push({
					player: null,
					minute: null,
					against: false
				})
			}
		}

		setFormValue('visitorsScorers', scorers)
	}

	function removeVisitorsResultScorers() {
		var scorers = []
		scorers = scorers.concat(form.visitorsScorers.slice(0, -1))
		setFormValue('visitorsScorers', scorers)
	}

	function addVisitorsCards() {
		var visitorsCards = form.visitorsCards
		visitorsCards.push({
			player: null,
			minute: null,
			card: _.head(cards)
		})

		setFormValue('visitorsCards', visitorsCards)
	}

	function removeVisitorsCards() {
		var cards = []
		cards = cards.concat(form.visitorsCards.slice(0, -1))
		setFormValue('visitorsCards', cards)
	}

	function handleChipChangeHomeResulResultPenalty(event) {
		const value = event.target.value
		if (isIntegerAndMinorEqualsToOneHundred(value) === true) {
			setFormValue('homeResultPenalty', value)
		}
	}

	function handleHomeResulScorers(homeScorers, i) {
		setFormValue('homeScorers', homeScorers)
	}

	function handleVisitorsResulScorers(visitorsScorers, i) {
		setFormValue('visitorsScorers', visitorsScorers)
	}

	function handleReferees(referees, i) {
		setFormValue('referees', referees)
	}

	function removeReferees() {
		var referees = []
		referees = referees.concat(form.referees.slice(0, -1))
		setFormValue('referees', referees)
	}

	function addReferees() {
		var refereesList = form.referees
		refereesList.push({
			referee:  _.head(referees),
			refereeFunction:  _.head(refereeFunctions),
		})

		setFormValue('referees', refereesList)
	}

	function handleHomeCards(homeCards, i) {
		setFormValue('homeCards', homeCards)
	}

	function handleVisitorsCards(visitorsCards, i) {
		setFormValue('visitorsCards', visitorsCards)
	}

	function setFormValue(name, value) {
		setForm(
			_.set(
				{ ...form },
				name, value
			)
		);
	}

	function handleChipChangeSport(value) {
		if (!form.sport || form.sport._id !== value._id) {
			form.homeTeam = null
			form.visitorsTeam = null
			setTeams([])
			dispatch(Actions.getTeamsForSportId(value._id));
			setFormValue('sport', value)
		}
	}

	function handleChipChangeTournament(value) {
		if (!form.tournament || form.tournament._id !== value._id) {
			setSports(value.sports)
			setDivisions(value.divisions)
			setSubDivisions(value.subDivisions)
			setTournamentInstances(value._id)
			setTournamentZones(value._id)
			setFormValue('tournament', value)
		}
	}

	function handleChipChangeTournamentInstance(value) {
		form.tournamentZone = null
		setFormValue('tournamentInstance', value)
	}

	function handleChipChangeTournamentZone(value) {
		form.tournamentInstance = null
		setPenaltyEnable(false)
		setFormValue('tournamentZone', value)
	}

	function handleChipChangeHomeTeam(value) {
		if ((!form.homeTeam || form.homeTeam._id !== value._id) && form.division) {
			form.homeScorers = []
			dispatch(Actions.getPlayersForTeamIdDivisionId(value._id, form.division._id, true));
			setFormValue('homeTeam', value)
		}
	}

	function handleChipChangeVisitorsTeam(value) {
		if ((!form.visitorsTeam || form.visitorsTeam._id !== value._id) && form.division) {
			form.visitorsScorers = []
			dispatch(Actions.getPlayersForTeamIdDivisionId(value._id, form.division._id, false));
			setFormValue('visitorsTeam', value)
		}
	}

	function handleChipChangeRound(event) {
		const value = event.target.value
		if (isIntegerMaiorZeroAndMinorEqualsToOneHundred(value) === true) {
			setFormValue('round', value)
		}
	}

	function handleChipChange(value, name) {
		const isSubdivision = name === 'subDivision'
        const v  =  isSubdivision ? value.value : value
		setFormValue(name, v)		
	}

	function cleanTournamentZone() {
		setFormValue('tournamentZone', null)
	}

	function cleanTournamentInstance() {
		setFormValue('tournamentInstance', null)
	}

	function cleanTournamentSubDivision() {
		setFormValue('subDivision', '')
	}

	function cleanHomeResult() {
		setFormValue('homeResult', null)
	}

	function cleanVisitorsResult() {
		const result = form.visitorsResult
		setFormValue('visitorsResult', null)
	}

	function cleanHomeResultPenalty() {
		setFormValue('homeResultPenalty', null)
	}

	function cleanVisitorsResultPenalty() {
		setFormValue('visitorsResultPenalty', null)
	}

	function dateTimeFormat(dateTime){
		let newDateTime = moment.parseZone(dateTime).format('YYYY-MM-DD HH:mm')
		//let newDateTime = moment(new Date(dateTime)).format('YYYY-MM-DD HH:mm')
		return newDateTime
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/fixture/fixtures"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">Fixtures</span>
								</Typography>
							</FuseAnimate>
						</div>
						<CircularProgress color="secondary" style={{ display: progress ? '' : 'none' }} />
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => {
									const dataToSave = form
									dataToSave.homeScorers = form.homeScorers.filter(s => (s.player !== null && s.player !== ''))
									dataToSave.visitorsScorers = form.visitorsScorers.filter(s => (s.player !== null && s.player !== ''))
									dataToSave.referees = form.referees.filter(r => (r.referee !== null && r.referee !== undefined && r.refereeFunction !== null && r.refereeFunction !== undefined))
									dispatch(Actions.saveFixture(dataToSave, routeParams.fixtureId === 'new' ? 'post' : 'put'))
								}
								}

							>
								{routeParams.fixtureId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label='Fixture'/>
					<Tab className="h-64 normal-case" label={t('REFEERES')} />
					<Tab className="h-64 normal-case" label={t('RESULTS')} />
					<Tab className="h-64 normal-case" label={t('SCORERS')} />
					<Tab className="h-64 normal-case" label={t('CARDS')} />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<div className="flex mx-0">
									<TextField
										className="mt-8 w-1/5 "
										label={t('ROUND')}
										id="round"
										name="round"
										value={form.round || 1}
										onChange={value => handleChipChangeRound(value)}
										variant="outlined"
										cols={8}
										fullWidth
										inputProps={{
											maxLength: 200,
										}}
										type='number'
										disabled={progress}
									/>
									<DateTimePicker
										id='dateTime'
										className="mt-8 ml-16 md:w-2/6"
										label={t('DATA')}
										format="DD-MM-YYYY HH:mm"
										name='dateTime'
										disabled={progress}
										inputVariant="outlined"
										// value={form.dateTime}
										value={dateTimeFormat(form.dateTime)}
										//onChange={date => setInForm('dateTime', moment(date).format('YYYY-MM-DD HH:mm'))}
										onChange={date => setInForm('dateTime', moment.parseZone(date).format('YYYY-MM-DD HH:mm'))}
									/>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<FuseChipSelect
										className="mt-24 sm:w-2/6"
										value={form.tournament}
										onChange={value => handleChipChangeTournament(value)}
										placeholder={t('TOURNAMENT_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => `${option.name} ${option.year}`}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('TOURNAMENT'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										isDisabled={progress}
										options={tournaments || []}
									/>
									<FuseChipSelect
										className="mt-24 md:ml-24 sm:w-2/6"
										value={form.sport}
										onChange={value => handleChipChangeSport(value)}
										placeholder={t('SPORT_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => `${option.name} (${option.gender.name})`}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('SPORT'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										isDisabled={progress}
										options={sports || []}
									//	isMulti
									/>
									<FuseChipSelect
										className="mt-24 md:ml-24 sm:w-2/6"
										value={form.division}
										onChange={value => handleChipChange(value, 'division')}
										placeholder={t('DIVISION_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => option.name}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('DIVISION'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={divisions || []}
										isDisabled={progress}
									/>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<div className="flex mx-0 mt-24 sm:w-2/6">
										<FuseChipSelect
											className="w-full"
											value={form.tournamentZone}
											onChange={value => handleChipChangeTournamentZone(value)}
											placeholder={t('ZONE_GROUP_SELECTION')}
											variant='fixed'
											getOptionLabel={option => option.name}
											getOptionValue={option => option._id}
											textFieldProps={{
												label: 'Zona/Grupo',
												InputLabelProps: {
													shrink: true
												},
												variant: 'outlined'
											}}
											options={zones || []}
											isDisabled={progress || codeEnable}
										/>
										<IconButton
											onClick={() =>
												cleanTournamentZone()
											}
											disabled={progress}
											className="p-2 h-24 mx-2 sm:mx-8 mt-8"
										>
											<Icon>autorenew</Icon>
										</IconButton>
									</div>
									<div className="flex mx-0 mt-24 sm:w-2/6">
										<FuseChipSelect
											className="w-full"
											value={form.tournamentInstance}
											onChange={value => handleChipChangeTournamentInstance(value)}
											placeholder={t('INSTANCE_SELECTION')}
											variant='fixed'
											getOptionLabel={option => option.name}
											getOptionValue={option => option._id}
											textFieldProps={{
												label: t('INSTANCE'),
												InputLabelProps: {
													shrink: true
												},
												variant: 'outlined'
											}}
											options={instances || []}
											isDisabled={progress}
										/>
										<IconButton
											onClick={() =>
												cleanTournamentInstance()
											}
											disabled={progress}
											className="p-2 h-24 mx-2 sm:mx-8 mt-8"
										>
											<Icon>autorenew</Icon>
										</IconButton>
									</div>
									<div className="flex mx-0 mt-24 sm:w-2/6">
										<FuseChipSelect
											className="w-full"
											value={
												form.subDivision ? 
												{ 
													value: form.subDivision, 
													label: form.subDivision
												} : form.subDivision 	
											}
											onChange={value => handleChipChange(value, 'subDivision')}
											placeholder={t('SUB_DIVISION_SELECTION')}
											variant='fixed'
											required={!_.isEmpty(subDivisions)}
											//getOptionLabel={option => option}
											//getOptionValue={option => option}
											textFieldProps={{
												label: t('SUB_DIVISION'),
												InputLabelProps: {
													shrink: true
												},
												variant: 'outlined'
											}}
											options={subDivisions.map(t => ({ value: t, label: t}))  || []}
											//options={subDivisions || []}
											isDisabled={progress}
										/>
										<IconButton
											onClick={() =>
												cleanTournamentSubDivision()
											}
											disabled={progress}
											className="p-2 h-24 mx-2 sm:mx-8 mt-8"
										>
											<Icon>autorenew</Icon>
										</IconButton>
									</div>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<FuseChipSelect
										className="mt-24 sm:w-3/6"
										value={form.homeTeam}
										onChange={value => handleChipChangeHomeTeam(value)}
										placeholder={t('LOCAL_TEAM_SELECTION')}
										required={true}
										variant='fixed'
										isDisabled={!teamEnable || progress}
										getOptionLabel={option => option.name}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('LOCAL_TEAM'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={teams || []}
									/>
									<FuseChipSelect
										className="mt-24 md:ml-24 sm:w-3/6"
										value={form.visitorsTeam}
										onChange={value => handleChipChangeVisitorsTeam(value)}
										placeholder={t('VISITORS_TEAM_SELECTION')}
										required={true}
										isDisabled={!teamEnable || progress}
										variant='fixed'
										getOptionLabel={option => option.name}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('VISITORS_TEAM'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={teams || []}
									/>
								</div>
								<FuseChipSelect
									className="mt-24 w-full"
									value={form.playingField}
									onChange={value => handleChipChange(value, 'playingField')}
									placeholder={t('PLAYING_FIELD_SELECTION')}
									required={true}
									variant='fixed'
									getOptionLabel={option => option.name}
									getOptionValue={option => option._id}
									textFieldProps={{
										label: t('PLAYING_FIELD'),
										InputLabelProps: {
											shrink: true
										},
										variant: 'outlined'
									}}
									options={playingFields || []}
									isDisabled={progress}
								/>
								<TextField
									className="mt-24"
									label="Comentario"
									id="comment"
									name="comment"
									value={form.comment}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 100,
									}}
									disabled={progress}
								/>
								<TextField
									className="mt-24"
									label={t('SOURCE')}
									id="source"
									name="source"
									value={form.source}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 50,
									}}
									disabled={progress}
								/>
								<div className="flex">
									<FormControlLabel
										disabled={finish || progress}
										control={
											<Checkbox
												id='isFinish'
												name='isFinish'
												tabIndex={-1}
												checked={form.isFinish}
												onChange={handleChange}

											/>
										}
										label={t('IS_FINISH')}
									/>
								</div>
							</div>
						)}


						{tabValue === 1 && (
							<div >
								<div className="flex-grid">
									<div className="flex">
										<Typography component="div">
											<Box className="mt-12" textAlign="justify" m={1} fontWeight="fontWeightMedium">
											      {t('REFEERES')}
											</Box>
										</Typography>
										<IconButton
											onClick={() =>
												addReferees()
											}
											disabled={progress}
											className="mb-8"
										>
											<Icon>add</Icon>
										</IconButton>
									</div>
									<FixtureRefereesGridList
										t= { t }
										progress= { progress }
										referees= { referees || [] }
										refereeModels={form.referees || []}
										refereeFunctions= { refereeFunctions || []}
										onHandleChange={(value, i) => handleReferees(value, i)}
										removeReferees={removeReferees}
									/>
						    	</div>

								<div className="flex">
									<FormControlLabel
										disabled={finish || progress}
										control={
											<Checkbox
												id='isFinish'
												name='isFinish'
												tabIndex={-1}
												checked={form.isFinish}
												onChange={handleChange}

											/>
										}
										label={t('IS_FINISH')}
									/>
								</div>
							</div>
						)}

						{tabValue === 2 && (
							<div >
								<div className="flex mx-0">
									<div className="flex mx-0 mt-16 w-3/6 md:w-2/6">
										<TextField
											label="RL"
											id="homeResult"
											name="homeResult"
											value={form.homeResult === 0 || form.homeResult ? form.homeResult : ''}
											onChange={value => handleChipChangeHomeResulResult(value)}
											variant="outlined"
											cols={8}
											fullWidth
											inputProps={{
												maxLength: 200,
											}}
											type='number'
											disabled={progress || !resultEnable()}
										/>
										<IconButton
											onClick={() =>
												cleanHomeResult()
											}
											disabled={progress || !resultEnable()}
											className="p-2 h-24 mx-2 md:mx-8 mt-8"
										>
											<Icon>autorenew</Icon>
										</IconButton>
									</div>
									<div className="flex mx-0 mt-16 ml-8 w-3/6 md:w-2/6">
										<TextField
											label={t('RL_PENALTIES')}  
											id="homeResultPenalty"
											name="homeResultPenalty"
											value={form.homeResultPenalty === 0 || form.homeResultPenalty ? form.homeResultPenalty : ''}
											onChange={value => handleChipChangeHomeResulResultPenalty(value)}
											variant="outlined"
											cols={8}
											fullWidth
											inputProps={{
												maxLength: 200,
											}}
											type='number'
											disabled={progress || !penaltyEnable}
										/>
										<IconButton
											onClick={() =>
												cleanHomeResultPenalty()
											}
											disabled={progress || !penaltyEnable}
											className="p-2 h-24 mx-2 md:mx-8 mt-8"
										>
											<Icon>autorenew</Icon>
										</IconButton>
									</div>
								</div>
								<div className="flex mx-0">
									<div className="flex mx-0 mt-16 w-3/6 md:w-2/6">
										<TextField
											label="RV"
											id="visitorsResult"
											name="visitorsResult"
											value={form.visitorsResult === 0 || form.visitorsResult ? form.visitorsResult :  ''}
											onChange={value => handleChipChangeVisitorsResulResult(value)}
											variant="outlined"
											cols={8}
											fullWidth
											inputProps={{
												maxLength: 200
											}}
											type='number'
											disabled={progress || !resultEnable()}
										/>
										<IconButton
											onClick={() =>
												cleanVisitorsResult()
											}
											disabled={progress || !resultEnable()}
											className="p-2 h-24 mx-8 mt-8"
										>
											<Icon>autorenew</Icon>
										</IconButton>
									</div>
									<div className="flex mx-0 mt-16 ml-8 w-3/6 md:w-2/6">
										<TextField
								          	label={t('RV_PENALTIES')}  
											id="visitorsResultPenalty"
											name="visitorsResultPenalty"
											value={form.visitorsResultPenalty === 0  || form.visitorsResultPenalty ? form.visitorsResultPenalty : ''}
											onChange={value => handleChipChangeVisitorsResulResultPenalty(value)}
											variant="outlined"
											cols={8}
											fullWidth
											inputProps={{
												maxLength: 200,
											}}
											type='number'
											disabled={progress || !penaltyEnable}
										/>
										<IconButton
											onClick={() =>
												cleanVisitorsResultPenalty()
											}
											disabled={progress || !penaltyEnable}
											className="p-2 h-24 mx-8 mt-8"
										>
											<Icon>autorenew</Icon>
										</IconButton>
									</div>
								</div>
								<div className="flex">
									<FormControlLabel
										disabled={finish || progress}
										control={
											<Checkbox
												id='isFinish'
												name='isFinish'
												tabIndex={-1}
												checked={form.isFinish}
												onChange={handleChange}
											/>
										}
										label={t('IS_FINISH')}
									/>
								</div>
							</div>
						)}

						{tabValue === 3 && (
							<div >
								{(form.homeResult !== '' && form.homeResult !== null) ? (<Typography component="div">
									<div className="flex">
										<Box className="mt-12" textAlign="justify" m={1} fontWeight="fontWeightMedium">
											{(form.homeResult !== '' && form.homeResult !== null) ? `Resultado Local ${form.homeResult}` : ''}
										</Box>
										<IconButton
											onClick={() =>
												addHomeResultScorers()
											}
											disabled={progress}
											className="mb-8"
										>
											<Icon>add</Icon>
										</IconButton>
									</div>
								</Typography>) : null}
								<div >
									<FixtureScorersGridList
										t= { t }
										progress={progress}
										players={homePlayers}
										result={form.homeResult}
										scorers={form.homeScorers || []}
										onHandleChange={(value, i) => handleHomeResulScorers(value, i)}
										removeResultScorers={removeHomeResultScorers}
									/>
								</div>
								{(form.visitorsResult !== '' && form.visitorsResult !== null) ? (<Typography component="div">
									<div className="flex">
										<Box className="mt-12" textAlign="justify" m={1} fontWeight="fontWeightMedium">
											{(form.visitorsResult !== '' && form.visitorsResult !== null) ? `Resultado Visitante ${form.visitorsResult}` : ''}
										</Box>
										<IconButton
											onClick={() =>
												addVisitorsResultScorers()
											}
											disabled={progress}
											className="mb-8"
										>
											<Icon>add</Icon>
										</IconButton>
									</div>
								</Typography>) : null}

								<FixtureScorersGridList
									t= { t }
									progress={progress}
									players={visitorsPlayers}
									result={form.visitorsResult}
									scorers={form.visitorsScorers || []}
									onHandleChange={(value, i) => handleVisitorsResulScorers(value, i)}
									removeResultScorers={removeVisitorsResultScorers}
								/>

								<div className="flex">
									<FormControlLabel
										disabled={finish || progress}
										control={
											<Checkbox
												id='isFinish'
												name='isFinish'
												tabIndex={-1}
												checked={form.isFinish}
												onChange={handleChange}

											/>
										}
										label={t('IS_FINISH')}
									/>
								</div>
							</div>
						)} 
						
						{tabValue === 4 && (
							<div >
								<div className="flex-grid">
									<div className="flex">
										<Typography component="div">
											<Box className="mt-12" textAlign="justify" m={1} fontWeight="fontWeightMedium">
												Local
											</Box>
										</Typography>
										<IconButton
											onClick={() =>
												addHomeCards()
											}
											disabled={progress}
											className="mb-8"
										>
											<Icon>add</Icon>
										</IconButton>
									</div>
									<FixtureCardsGridList
										t= { t }
										progress={progress}
										players={homePlayers}
										cards={cards || []}
										//result={form.homeResult}
										cardModels={form.homeCards || []}
										onHandleChange={(value, i) => handleHomeCards(value, i)}
										removeCards={removeHomeCards}
									/>
									<div className="flex">
										<Typography component="div">
											<Box className="mt-12" textAlign="justify" m={1} fontWeight="fontWeightMedium">
												Visitante
											</Box>
										</Typography>
										<IconButton
											onClick={() =>
												addVisitorsCards()
											}
											disabled={progress}
											className="mb-8"
										>
											<Icon>add</Icon>
										</IconButton>
									</div>
									<FixtureCardsGridList
										t= { t }
										progress={progress}
										players={visitorsPlayers}
										cards={cards || []}
										//result={form.homeResult}
										cardModels={form.visitorsCards || []}
										onHandleChange={(value, i) => handleVisitorsCards(value, i)}
										removeCards={removeVisitorsCards}
									/>
								</div>

								<div className="flex">
									<FormControlLabel
										disabled={finish || progress}
										control={
											<Checkbox
												id='isFinish'
												name='isFinish'
												tabIndex={-1}
												checked={form.isFinish}
												onChange={handleChange}

											/>
										}
										label={t('IS_FINISH')}
									/>
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('fixtureApp', reducer)(Fixture);
