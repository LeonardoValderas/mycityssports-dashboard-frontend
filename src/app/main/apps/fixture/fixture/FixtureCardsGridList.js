import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import { isIntegerMaiorZeroAndMinorEqualsToOneFityHundred } from '../../utils/NumberValidator'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'block',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    formControl: {
        marginTop: theme.spacing(0),
        display: 'flex'
    },
    gridList: {
        flexWrap: 'nowrap',
        height: '50',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
    title: {
        minWidth:300,
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
}));

export default function FixtureCardsGridList(props) {
    const classes = useStyles();
    const t = props.t
    if (!props.cardModels || props.cardModels === '' || props.cardModels.lenght === 0) return null

    //const items = []
    var cardModels = props.cardModels || []

    function handleChange(name, event, i) {
        if (name === 'player') {
            const value = event.target.value
            const player = getPlayerById(value)
            cardModels[i].player = player
        } else if (name === 'minute') {
            const value = event.target.value
            if (isIntegerMaiorZeroAndMinorEqualsToOneFityHundred(value)) {
                cardModels[i].minute = parseInt(value)
            }
        } else {
            const value = event.target.value
            const card = getCardById(value)
            cardModels[i].card = card
        }

        props.onHandleChange(cardModels, i)
    }
    const getCardById = (cardId) => {
        const card = props.cards.find(p => p._id === cardId)
        return card
    }
    const getPlayerById = (playerId) => {
        const player = props.players.find(p => p._id === playerId)
        return player
    }
    return (
        <div >
            <GridList cellHeight={80} className="flex flex-col w-full flex-shrink-0 sm:flex-row"> {
                cardModels.map((item, index) => (
                    <GridListTile key={index} cols={1} className={classes.title}>
                        <div className="flex mx-0" >
                            <div className="flex mt-8 w-5/6" >
                                <div className="w-1/3" >
                                    <FormControl variant="outlined" disabled={props.progress} className={classes.formControl}  >
                                        <InputLabel id="selectLabel">
                                            {t('CARD')}
                                        </InputLabel>
                                        <Select
                                            labelId="selectLabel"
                                            name="card"
                                            autoWidth
                                            variant={'outlined'}
                                            value={item.card && item.card._id || ''}
                                            onChange={(value) => handleChange('card', value, index)}>
                                            {
                                                props.cards.map((item, i) =>

                                                    <MenuItem
                                                        value={item._id}
                                                        key={i}>
                                                        {item.name}
                                                    </MenuItem>)
                                            }
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="w-2/3 ml-6">
                                <FormControl variant="outlined" disabled={props.progress} className={classes.formControl}  >
                                        <InputLabel id="selectLabel">
                                            {t('PLAYER')}
                                        </InputLabel>
                                        <Select
                                            className="min-w-120"
                                            labelId="selectLabel"
                                            name="player"
                                            autoWidth
                                            variant={'outlined'}
                                            value={item.player && item.player._id || ''}
                                            onChange={(value) => handleChange('player', value, index)}>
                                            {
                                                props.players.map((item, i) =>
                                                    <MenuItem
                                                        value={item._id}
                                                        key={i}>
                                                        {item.name}
                                                    </MenuItem>)
                                            }
                                        </Select>
                                    </FormControl>
                                </div>
                            </div>
                            <div className="mt-8 ml-6 w-1/6">
                                <TextField
                                    className="w-full"
                                    label="Min"
                                    name={`${index}`}
                                    value={(cardModels && cardModels[index] && cardModels[index].minute) ? cardModels[index].minute : ''}
                                    onChange={(value) => handleChange('minute', value, index)}
                                    variant="outlined"
                                    cols={8}
                                    fullWidth
                                    inputProps={{
                                        maxLength: 3,
                                    }}
                                    type='number'
                                    disabled={props.progress || (!cardModels || !cardModels[index] || !cardModels[index].player || cardModels[index].player === '')}
                                />

                            </div>
                            <IconButton
                                onClick={() =>
                                    props.removeCards()
                                }
                                disabled={props.progress}
                                className="mt-6"
                            >
                                <Icon>remove</Icon>
                            </IconButton>
                        </div>
                    </GridListTile>
                )
                )
            }
            </GridList>
        </div>
    );
}