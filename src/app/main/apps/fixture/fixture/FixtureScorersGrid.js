import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Input from '@material-ui/core/Input';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        flexWrap: 'nowrap',
        height: '50',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
    title: {
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
}));

/**
 * The example data is structured as follows:
 *
 * import image from 'path/to/image.jpg';
 * [etc...]
 *
 * const tileData = [
 *   {
 *     img: image,
 *     title: 'Image',
 *     author: 'author',
 *   },
 *   {
 *     [etc...]
 *   },
 * ];
 */
export default function FixtureScorersGrid(props) {
    const classes = useStyles();

    if (!props.scorers || props.scorers.lenght === 0) {
        return null;
    }

    function handleChange(event, item) {
        const value = event.target.value
        if(parseInt(value) <= 0){
            return
        }
        let scorer = item
        scorer.scorer = event.target.value
        props.onChange(scorer)
    }

    var gridTileStyle = {
        position: 'relative',
        float: 'left',
        width: '20%',
        minHeight: '80px',
        minWidth: '100px',
        overflow: 'hidden',
        height: '10% !important'
    }
   
    return (
        (!props.scorers || props.scorers.length === 0) ? null :  <div >
            <GridList cellHeight={40} className={classes.gridList} cols={1}>
                {props.scorers.map((item) => (
                    <GridListTile key={item.player._id} >
                        <span>{item.player.name}</span>
                        <Input className="ml-6 max-w-36"
                            fullWidth={false}
                            name="scorer"
                            type={'number'}
                            value={item.scorer}
                            placeholder={'Cantidad de goles'}
                            onChange={(value) => handleChange(value, item)}
                            disabled = {props.progress}
				            // disableUnderline
                        />
                    </GridListTile>
                ))}
            </GridList>
        </div>
    );
}