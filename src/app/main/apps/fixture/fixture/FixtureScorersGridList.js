import React, { useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { isIntegerMaiorZeroAndMinorEqualsToOneFityHundred } from '../../utils/NumberValidator'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    formControl: {
        marginTop: theme.spacing(0),
        display: 'flex'
        // minWidth: 120,

    },
    // gridList: {
    //     flexWrap: 'nowrap',
    //     // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    //     transform: 'translateZ(0)',
    // },
    gridList: {
        "width": 1000,
        "height": 'auto',
        "overflowY": 'auto'
    },
    title: {
        minWidth: 280,
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
}));

export default function FixtureScorersGridList(props) {
    const classes = useStyles();
    const t = props.t
    if (!props.result || props.result === '' || props.result === 0) return null

    const items = []
    var scorers = props.scorers || []


    function handleChange(name, event, i) {
        if (name === 'player') {
            const value = event.target.value
            const player = getPlayerById(value)
            scorers[i].player = player
        } else if (name === 'against') {
            const value = event.target.checked
            scorers[i].against = value
        } else {
            const value = event.target.value
            if (isIntegerMaiorZeroAndMinorEqualsToOneFityHundred(value)) {
                scorers[i].minute = parseInt(value)
            }
        }

        props.onHandleChange(scorers, i)
    }


    const getPlayerById = (playerId) => {
        const player = props.players.find(p => p._id === playerId)
        return player
    }
    
    for (var i = 0; i < props.result; i++) {
        items.push(i)
    }
    
    return (
        <div >
            <GridList cellHeight={95} className="flex flex-col w-full flex-shrink-0 sm:flex-row"> {
                    scorers.map((item, index) => (
                        <GridListTile key={index} cols={1} className={classes.title}>
                            <div className="flex mx-0">
                                <div className="mt-8 w-5/6" >
                                    <FormControl variant="outlined" disabled={props.progress} className={classes.formControl}  >
                                        <InputLabel id="selectLabel">
                                            {t('SCORER')}
                                        </InputLabel>
                                        <Select
                                            className="min-w-120"
                                            labelId="selectLabel"
                                            name="player"
                                            autoWidth
                                            variant={'outlined'}
                                            value={item.player && item.player._id || ''}
                                            onChange={(value) => handleChange('player', value, index)}>
                                            {
                                               props.players.map((item, i) =>
                                                    <MenuItem
                                                        value={item._id}
                                                        key={i}>
                                                        {item.name}
                                                    </MenuItem>)
                                            }
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-8 ml-6 w-2/6" >
                                    <TextField
                                        label="Min"
                                        name={`${index}`}
                                        value={(scorers && scorers[index] && scorers[index].minute) ? scorers[index].minute : ''}
                                        onChange={(value) => handleChange('minute', value, index)}
                                        variant="outlined"
                                        cols={8}
                                        fullWidth
                                        inputProps={{
                                            maxLength: 3,
                                        }}
                                        type='number'
                                        disabled={props.progress || (!scorers || !scorers[index] || !scorers[index].player || scorers[index].player === '')}
                                    />
                                </div>
                                <IconButton
                                    onClick={() =>
                                        props.removeResultScorers()
                                    }
                                    disabled={props.progress}
                                    className="mt-6"
                                >
                                    <Icon>remove</Icon>
                                </IconButton>
                            </div>
                            <div className="flex" >
                                <FormControlLabel
                                    disabled={props.progress}
                                    control={
                                        <Checkbox
                                            id='against'
                                            name='against'
                                            tabIndex={-1}
                                            checked={scorers[index].against}
                                            onChange={(value) => handleChange('against', value, index)}
                                        />
                                    }
                                    label={t('SCORER_AGAINT')}
                                />
                            </div>
                        </GridListTile>
                    ))
                }
            </GridList>
        </div>
    );
}