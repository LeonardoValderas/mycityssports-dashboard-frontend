import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FusePageCarded from '@fuse/core/FusePageCarded';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import moment from 'moment/moment';
import _ from '@lodash';
import MomentUtils from "@date-io/moment";
import "moment/locale/es";
import Button from '@material-ui/core/Button';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { CSVLink } from "react-csv";
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import { isDateStartMinorOrSameDateEnd } from '../../utils/DateValidator'
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../fixture-i18n/es';
import pt from '../fixture-i18n/pt';

i18next.addResourceBundle('es', 'fixture', es);
i18next.addResourceBundle('pt', 'fixture', pt);

function FixtureCsv(props) {
	const { t } = useTranslation('fixture');
	const dispatch = useDispatch();
	const dataInit = useSelector(({ fixtureCsvApp }) => fixtureCsvApp.fixtureCsv.dataInit);
	const data = useSelector(({ fixtureCsvApp }) => fixtureCsvApp.fixtureCsv.data);
	const loading = useSelector(({ fixtureCsvApp }) => fixtureCsvApp.fixtureCsv.loading);
	const progress = useSelector(({ fixtureCsvApp }) => fixtureCsvApp.fixtureCsv.progress);
	const tournaments = useSelector(({ fixtureCsvApp }) => fixtureCsvApp.fixtureCsv.tournaments);
	const [sports, setSports] = useState([]);
	const [divisions, setDivisions] = useState([]);
	const [columns, setColumns] = useState([]);
	const [showCSV, setShowCSV] = useState(false);
	//const [subDivisions, setSubDivisions] = useState([]);
	const [tabValue, setTabValue] = useState(0);
	const { form, setInForm, setForm } = useForm(null);
	const routeParams = useParams();

	const [tournament, setTournament] = useState('');
	const [sport, setSport] = useState('');
	const [division, setDivision] = useState('');

	const headers = [
		{ label: t('TOURNAMENT'), key: 'tournament' },
		{ label: t('SPORT'), key: 'sport' },
		{ label: t('DIVISION'), key: 'division' },
		{ label: t('SUB_DIVISION'), key: 'subDivision' },
		{ label: t('FINAL_INSTANCE'), key: 'instance' },
		{ label: 'Zona', key: 'zone' },
		{ label: t('ROUND'), key: 'round' },
		{ label: t('DATA'), key: 'dateTime' },
		{ label: t('PLAYING_FIELD_2'), key: 'playingField' },
		{ label: t('LOCAL_TEAM'), key: 'homeTeam' },
		{ label: 'RL', key: 'homeResult' },
		{ label: t('RL_PENALTIES'), key: 'homeResultPenalty' },
		{ label: t('SCORERS_LOCAL'), key: 'homeScorers' },
		{ label: t('CARDS_LOCAL'), key: 'homeCards' },
		{ label: t('VISITORS_TEAM'), key: 'visitorsTeam' },
		{ label: 'RV', key: 'visitorsResult' },
		{ label: t('RV_PENALTIES'), key: 'visitorsResultPenalty' },
		{ label: t('SCORERS_VISITORS'), key: 'visitorsScorers' },
		{ label: t('CARDS_VISITORS'), key: 'visitorsCards' },
		{ label: t('REFEERES'), key: 'referees' },
		{ label: 'Status', key: 'isFinish' },
		{ label: 'Comentario', key: 'comment' }
	];

	useDeepCompareEffect(() => {
		dispatch(Actions.getTournamentsCsv());
		function initForm() {
			dispatch(Actions.initFixtureCsv());
		}

		initForm();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (dataInit && !form) {
			setForm(dataInit);
		}
	}, [form, dataInit, setForm]);

	useEffect(() => {
		if (data) {
			setShowCSV(false)
			setColumnsData()
		}
	}, [data]);

	function setColumnsData() {
		setColumns([])
		var newColumns = []
		data.map((item) => {
			const t = item.tournament.name + " - " + item.tournament.year
			setTournament(t)
			const s = item.sport.name + " - " + item.sport.gender.name
			setSport(s)
			const d = item.division.name
			setDivision(d)
			const subDivision = item.subDivision

			item.tournamentInstances.map((i) => {
				const instance = i.tournamentInstance.name
				i.fixtures.map((f) => {
					const row = getRowFromFixture(tournament, sport, division, subDivision, instance, '', f)
					newColumns.push(row)
				})
			})

			item.tournamentZones.map((i) => {
				const zone = i.tournamentZone.name
				i.fixtures.map((f) => {
					const row = getRowFromFixture(tournament, sport, division, subDivision, '', zone, f)
					newColumns.push(row)
				})
			})

			item.tournamentPoints.map((i) => {
				i.fixtures.map((f) => {
					const row = getRowFromFixture(tournament, sport, division, subDivision, '', '', f)
					newColumns.push(row)
				})
			})
		})
		setColumns(newColumns)
		setShowCSV(true)
	}

	function getRowFromFixture(tournament, sport, division, subDivision, instance, zone, f) {
		var homeScorers = ""
		f.homeScorers.map((hs) => {
			const player = (hs.player === null) ? t('WITHOUT_DATA') : hs.player.name
			const minute = (hs.minute === null) ? " " : " (" + hs.minute + "m) "
			const against = (hs.against === true) ? " (C) " : " "
			const text = player + minute + against
			homeScorers += text
		})
		var homeCards = ""
		f.homeCards.map((hc) => {
			const player = (hc.player === null) ? t('WITHOUT_DATA') : hc.player.name
			const minute = (hc.minute === null) ? " " : " (" + hc.minute + "m) "
			const text = player + minute
			homeCards += text
		})
		var visitorsScorers = ""
		f.visitorsScorers.map((vs) => {
			const player = (vs.player === null) ? t('WITHOUT_DATA') : vs.player.name
			const minute = (vs.minute === null) ? " " : " (" + vs.minute + "m) "
			const against = (vs.against === true) ? " (C) " : " "
			const text = player + minute + against
			visitorsScorers += text
		})
		var visitorsCards = ''
		f.visitorsCards.map((vc) => {
			const player = (vc.player === null) ? t('WITHOUT_DATA') : vc.player.name
			const minute = (vc.minute === null) ? " " : " (" + vc.minute + "m) "
			const text = player + minute
			visitorsCards += text
		})

		var referees = ''
		f.referees.map((r, index) => {
			const text = r.referee.name + " (" + r.refereeFunction.name + ") "
			referees += text
		})

		return {
			tournament: tournament,
			sport: sport,
			division: division,
			subDivision: subDivision,
			instance: instance,
			zone: zone,
			round: f.round,
			dateTime: dateTimeFormat(f.dateTime),
			playingField: f.playingField.name,
			homeTeam: f.homeTeam.name,
			homeResult: f.homeResult,
			homeResultPenalty: f.homeResultPenalty,
			homeScorers: homeScorers,
			homeCards: homeCards,
			visitorsTeam: f.visitorsTeam.name,
			visitorsResult: f.visitorsResult,
			visitorsResultPenalty: f.visitorsResultPenalty,
			visitorsScorers: visitorsScorers,
			visitorsCards: visitorsCards,
			referees: referees,
			isFinish: f.isFinish === true ? "Finalizado" : t('NO_FINISH'),
			comment: f.comment
		}
	}
	function dateTimeFormat(dateTime) {
		let newDateTime = moment(new Date(dateTime)).format('YYYY-MM-DD')
		return newDateTime
	}

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return SubmittedFormValidation()
			&& !_.isEqual(data, form);
	}

	function handleChipChange(value, name) {
		// const isSubdivision = name === 'subDivision'
		setForm(
			_.set(
				{ ...form },
				name, value
			)
		);
		// name, isSubdivision ? value.value : value
	}

	function SubmittedFormValidation() {
		return form
			&& form.dateTimeStart !== ''
			&& form.dateTimeEnd !== ''
			&& validateDate()
			&& form.sport
			&& form.tournament
			&& form.division
		//			&& subDivisionValidation()
	}

	function validateDate() {
		return !isDateStartMinorOrSameDateEnd(form.dateTimeStart, form.dateTimeEnd)
	}
	// function subDivisionValidation(){
	//    return _.isEmpty(subDivisions) ? true : form.subDivision
	// }

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	// function setTournamentSubDivisions(tournamentId) {
	// 	const tournament = tournaments.find(t => t._id === tournamentId)
	// 	if (tournament) {
	// 		setSubDivisions(tournament.subDivisions)
	// 		return
	// 	}
	// 	setSubDivisions([])
	// }
	function setFormValue(name, value) {
		setForm(
			_.set(
				{ ...form },
				name, value
			)
		);
	}

	function handleChipChangeSport(value) {
		setFormValue('sport', value)
	}

	function handleChipChangeTournament(value) {
		if (!form.tournament || form.tournament._id !== value._id) {
			setSports(value.sports)
			setDivisions(value.divisions)
			// setSubDivisions(value.subDivisions)
			setFormValue('tournament', value)
		}
	}

	// function cleanTournamentSubDivision() {
	// 	setForm(
	// 		_.set(
	// 			{ ...form },
	// 			'subDivision', ''
	// 		)
	// 	);
	// }

	function dateTimeFormat(dateTime) {
		let newDateTime = moment(new Date(dateTime)).format('YYYY-MM-DD')
		return newDateTime
	}

	function getCSVFileName() {
		return `${tournament}_${sport}_${division}_fixtures.csv`
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						{/* <div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/fixture/fixtures"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">Fixtures</span>
								</Typography>
							</FuseAnimate>
						</div> */}
						<CircularProgress color="secondary" style={{ display: progress ? '' : 'none' }} />
						{/* <FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => {
									dispatch(Actions.saveFixture(form, routeParams.fixtureId === 'new' ? 'post' : 'put'))
								}
								}

							>
								{routeParams.fixtureId === 'new' ? 'Salvar' : 'Actualizar'}
							</Button>
						</FuseAnimate> */}
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label="Fixture CSV" />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<div className="flex mx-0">
									<MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={"es"}>
										<DatePicker
											id='dateTimeStart'
											className="mt-8 w-3/6"
											label="Desde"
											format="DD-MM-YYYY"
											name='dateTimeStart'
											disabled={progress}
											inputVariant="outlined"
											value={form.dateTimeStart}
											//onChange={date => setInForm('dateTime', moment(date).format('YYYY-MM-DD HH:mm'))}
											onChange={date => setInForm('dateTimeStart', moment.parseZone(date).format('YYYY-MM-DD'))}
										/>

										<DatePicker
											id='dateTimeEnd'
											className="mt-8 ml-16 w-3/6"
											label={t('UNTIL')}
											format="DD-MM-YYYY"
											name='dateTimeEnd'
											disabled={progress}
											inputVariant="outlined"
											value={form.dateTimeEnd}
											//onChange={date => setInForm('dateTime', moment(date).format('YYYY-MM-DD HH:mm'))}
											onChange={date => setInForm('dateTimeEnd', moment.parseZone(date).format('YYYY-MM-DD'))}
										/>
									</MuiPickersUtilsProvider>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<FuseChipSelect
										className="mt-24 w-full md:w-2/6"
										value={form.tournament}
										onChange={value => handleChipChangeTournament(value)}
										placeholder={t('TOURNAMENT_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => `${option.name} ${option.year}`}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('TOURNAMENT'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										isDisabled={progress}
										options={tournaments || []}
									/>
									<FuseChipSelect
										className="mt-24 md:ml-24 w-full md:w-2/6"
										value={form.sport}
										onChange={value => handleChipChangeSport(value)}
										placeholder={t('SPORT_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => `${option.name} (${option.gender.name})`}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('SPORT'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										isDisabled={progress}
										options={sports || []}
									//	isMulti
									/>
									<FuseChipSelect
										className="mt-24 md:ml-24 w-full md:w-2/6"
										value={form.division}
										onChange={value => handleChipChange(value, 'division')}
										placeholder={t('DIVISION_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => option.name}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('DIVISION'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={divisions || []}
										isDisabled={progress}
									/>
								</div>
								<div className="w-full mt-48" style={{ textAlign: 'center' }}>

									<FuseAnimate animation="transition.slideRightIn" delay={300}>
										<Button
											className="whitespace-no-wrap normal-case"
											variant="contained"
											color="secondary"
											disabled={!canBeSubmitted()}
											onClick={() => {
												dispatch(Actions.getFixturesToCSV(form))
											}
											}
										>
											{t('PROCESS')}
										</Button>
									</FuseAnimate>
								</div>
								<div className="w-full mt-32" style={{ textAlign: 'center' }}>
									{showCSV && (
										<CSVLink
											data={columns}
											headers={headers}
											filename={getCSVFileName()}
											className="btn btn-primary"
											target="_blank">
											{t('DOWNLOAD')}
										</CSVLink>
										)
									}
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('fixtureCsvApp', reducer)(FixtureCsv);
