import React from 'react';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useDispatch, useSelector } from 'react-redux';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import CollapsibleInstanceTable from './CollapsibleInstanceTable';
import CollapsibleZoneTable from './CollapsibleZoneTable';
import CollapsiblePointsTable from './CollapsiblePointsTable';
import FixturesTableHead from './fixtures/FixturesTableHead';

function Row(props) {
  const {
    id,
    row,
    handleClick,
    handleCheck,
    selected,
    deleteSelectedItems,
    openRow,
    zonesOpen,
    instancesOpen,
    pointsOpen,
    handleOpenMainRows,
    handleOpenMainRowZones,
    handlePageRowPageZone,
    handleOpenMainRowInstances,
    handlePageRowPageInstance,
    handleOpenMainRowPoints,
    handlePageRowPagePoint
  } = props;
  const [open, setOpen] = React.useState(openRow);
  return (
    <React.Fragment>
      <TableRow
        className="h-64 cursor-pointer"
        onClick={() => {
          setOpen(!open)
          handleOpenMainRows(id)
        }
        }>
        <TableCell align="left">
          <IconButton aria-label="expand row" size="small" onClick={() => {
            handleOpenMainRows(id)
            setOpen(!open)
          }}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell align="left">
          {row.tournament.name} {row.tournament.year}
        </TableCell>
        <TableCell align="left">
          {row.division.name}
        </TableCell>
        <TableCell align="left">
          {row.subDivision}
        </TableCell>
        <TableCell align="left">
          {row.sport.name} {row.sport.gender.name}
        </TableCell>
      </TableRow>

      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <FuseScrollbars className="flex-grow overflow-x-auto">
              <div >
                <React.Fragment>
                  <div className="mt-16">
                    {
                      row.tournamentInstances !== undefined &&
                      row.tournamentInstances &&
                      row.tournamentInstances.length > 0 && <CollapsibleInstanceTable
                        id={id}
                        tournamentInstances={row.tournamentInstances}
                        handleClick={handleClick}
                        handleCheck={handleCheck}
                        selected={selected}
                        instancesOpen={instancesOpen}
                        handleOpenMainRowInstances={handleOpenMainRowInstances}
                        handlePageRowPageInstance={handlePageRowPageInstance}
                        deleteSelectedItems={deleteSelectedItems}
                      />
                    }
                  </div>
                  <div className="mt-16">
                    {
                      row.tournamentZones !== undefined &&
                      row.tournamentZones &&
                      row.tournamentZones.length > 0 && <CollapsibleZoneTable
                        id={id}
                        tournamentZones={row.tournamentZones}
                        handleClick={handleClick}
                        handleCheck={handleCheck}
                        selected={selected}
                        zonesOpen={zonesOpen}
                        handlePageRowPageZone={handlePageRowPageZone}
                        handleOpenMainRowZones={handleOpenMainRowZones}
                        deleteSelectedItems={deleteSelectedItems}
                      />
                    }
                  </div>
                  <div className="mt-16 mb-16">
                    {
                      row.tournamentPoints !== undefined &&
                      row.tournamentPoints &&
                      row.tournamentPoints.length > 0 && <CollapsiblePointsTable
                        id={id}  
                        tournamentPoints={row.tournamentPoints}
                        handleClick={handleClick}
                        handleCheck={handleCheck}
                        selected={selected}
                        pointsOpen={pointsOpen}
                        handleOpenMainRowPoints={handleOpenMainRowPoints}
                      	handlePageRowPagePoint={handlePageRowPagePoint}
                        deleteSelectedItems={deleteSelectedItems}
                      />
                    }
                  </div>
                </React.Fragment>
              </div>
            </FuseScrollbars>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

export default function CollapsibleTable(props) {
  const {
    handleClick,
    handleCheck,
    selected,
    fixtures,
    openRows,
    handleOpenMainRows,
    handleOpenMainRowZones,
    handlePageRowPageZone,
    handleOpenMainRowInstances,
    handlePageRowPageInstance,
    handleOpenMainRowPoints,
  	handlePageRowPagePoint,
    deleteSelectedItems
  } = props

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <FixturesTableHead
          rowCount={fixtures.length}
        />
        <TableBody>
          {
            fixtures !== undefined && fixtures && fixtures.length > 0 && fixtures.map((row) => {
              const id = row.tournament._id + row.division._id + row.subDivision + row.sport._id + row.sport.gender._id
              const openRow = openRows.find((row) => row.idRow === id)
              const zonesOpen = openRow && openRow.zones || []
              const instancesOpen = openRow && openRow.instances || []
              const pointsOpen = openRow && openRow.points || []
              return (
                      <Row
                        key={row._id}
                        id={id}
                        row={row}
                        handleClick={handleClick}
                        handleCheck={handleCheck}
                        selected={selected}
                        openRow={openRow !== null && openRow !== undefined}
                        zonesOpen= {zonesOpen}
                        instancesOpen={instancesOpen}
                        pointsOpen={pointsOpen}
                        handleOpenMainRows={handleOpenMainRows}
                        handleOpenMainRowZones={handleOpenMainRowZones}
                        handlePageRowPageZone={handlePageRowPageZone}
                        handleOpenMainRowInstances={handleOpenMainRowInstances}
                        handlePageRowPageInstance={handlePageRowPageInstance}
                        handleOpenMainRowPoints={handleOpenMainRowPoints}
                        handlePageRowPagePoint={handlePageRowPagePoint}
                        deleteSelectedItems={deleteSelectedItems}
                      />
              )
            })
          }
        </TableBody>
      </Table>
    </TableContainer>
  );
}