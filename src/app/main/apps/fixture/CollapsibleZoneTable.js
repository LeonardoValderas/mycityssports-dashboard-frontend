import React from 'react';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { uuidv4 } from '../utils/GeneralUtils'
import TablePagination from '@material-ui/core/TablePagination';
import moment from 'moment/moment';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import FixturesTableZoneHead from './fixtures/FixturesTableZoneHead';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from './fixture-i18n/es';
import pt from './fixture-i18n/pt';

i18next.addResourceBundle('es', 'fixture', es);
i18next.addResourceBundle('pt', 'fixture', pt);

const useRowStyles = makeStyles({
	root: {
		'& > *': {
			borderBottom: 'unset',
		},
	},
});

function Row(props) {
	const {
		id,
		row,
		t,
		clean,
		selected,
		handleClick,
		handleCheck,
		isOpen,
		pageZone,
		rowPageZone,
		handleOpenMainRowZones,
		handlePageRowPageZone,
		deleteSelectedItems
	} = props;
	const [page, setPage] = React.useState(pageZone)
	const [rowsPerPage, setRowsPerPage] = React.useState(rowPageZone)
	const [order, setOrder] = React.useState({
		direction: 'asc',
		id: null
	});

	const [open, setOpen] = React.useState(isOpen)
	const [fixtures, setFixtures] = React.useState(row.fixtures)
	const classes = useRowStyles()
	const zoneId = row.tournamentZone._id

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleChangePage(event, value) {
		setPage(value);
		handlePageRowPageZone(id, zoneId, value, rowsPerPage)
	}

	function handleChangeRowsPerPage(event) {
		const value = event.target.value
		setRowsPerPage(value);
		handlePageRowPageZone(id, zoneId, page, value)
	}

	function handleRowSelected() {
		handleOpenMainRowZones(id, zoneId)
		setOpen(!open)
	}

	return (
		<React.Fragment>
			<TableRow className={classes.root} onClick={() => {
				handleRowSelected()
			}}>
				<TableCell style={{ width: 5 }} size='small' component="td" scope="row" align="left">
					<IconButton aria-label="expand row" size="small" onClick={() => {
						//handleRowSelected()
					}}>
						{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
					</IconButton>
				</TableCell>
				<TableCell align="left">
					{row.tournamentZone.name}
				</TableCell>
			</TableRow>

			<TableRow>
				<TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
					<Collapse in={open} timeout="auto" unmountOnExit>
						<FuseScrollbars className="flex-grow overflow-x-auto">
							<Table className="min-w-xl" aria-labelledby="tableTitle">
								<FixturesTableZoneHead
									numSelected={selected && selected.length}
									order={order}
									cleanSelected={clean}
									onRequestSort={handleRequestSort}
									rowCount={fixtures.length}
									deleteSelectedItems={deleteSelectedItems}
								/>

								<TableBody>
									{_.orderBy(
										fixtures,
										[
											o => {
												switch (order.id) {
													case 'categories': {
														return o.categories[0];
													}
													default: {
														const orderId = order.id
														if (orderId && o !== null) {
															let filter = o
															const split = orderId.split('.')
															split.map(s => {
																const f = filter[s]
																if (f) {
																	filter = f
																}
															})
															return filter;
														}
														return o[order.id];
													}
												}
											}
										],
										[order && order.direction]
									)
										.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
										.map(n => {
											const isSelected = selected && selected.indexOf(n._id) !== -1;
											// const date = moment(n.dateTime).format('DD-MM-YYYY HH:mm')
											const date = moment.parseZone(n.dateTime).format('YYYY-MM-DD HH:mm')
											return (
												<TableRow
													className="h-64 cursor-pointer"
													hover
													role="checkbox"
													aria-checked={isSelected}
													tabIndex={-1}
													key={uuidv4()}
													selected={isSelected}
													onClick={event => handleClick(n)}
												>
													<TableCell className="w-64 text-center" padding="none">
														<Checkbox
															checked={isSelected}
															onClick={event => event.stopPropagation()}
															onChange={event => handleCheck(event, n._id)}
														/>
													</TableCell>

													<TableCell component="th" scope="row">
														<div className={clsx('inline text-12 p-4 rounded truncate', n.isFinish ? 'bg-green text-white' : 'bg-orange text-black')}>
															{n.isFinish ? 'Finalizado' : t('NO_FINISH')}
														</div>
													</TableCell>
													<TableCell colSpan={3} component="th" scope="row" >
														<Typography noWrap>
															{date}
														</Typography>
													</TableCell>
													<TableCell component="th" scope="row">
														{n.round}
													</TableCell>
													<TableCell component="th" scope="row">
														{n.homeTeam.name}
													</TableCell>
													<TableCell component="th" scope="row">
														{(n.homeResult !== null && n.homeResult !== '') ? n.homeResult : ''} {(n.homeResultPenalty && n.homeResultPenalty !== '') ? `(${n.homeResultPenalty})` : ''}
													</TableCell>
													<TableCell component="th" scope="row">
														{n.homeScorers && n.homeScorers.map(s => s.player.name).join(' - ')}
														{/* {n.homeScorers && n.homeScorers.map(s => `${s.player.name} ${parseInt(s.scorer) > 1 ? `(x${s.scorer})` : ''}`).join(' - ')} */}
													</TableCell>
													<TableCell component="th" scope="row">
														{n.visitorsTeam.name}
													</TableCell>
													<TableCell component="th" scope="row">
														{(n.visitorsResult !== null && n.visitorsResult !== '') ? n.visitorsResult : ''} {(n.visitorsResultPenalty && n.visitorsResultPenalty !== '') ? `(${n.visitorsResultPenalty})` : ''}
														{/* {n.visitorsResult && n.visitorsResult.value} {(n.visitorsResultPenalty && n.visitorsResultPenalty.value) ? `(${n.visitorsResultPenalty.value})` : ''} */}
													</TableCell>
													<TableCell component="th" scope="row">
														{n.visitorsScorers && n.visitorsScorers.map(s => s.player.name).join(' - ')}
														{/* {n.visitorsScorers && n.visitorsScorers.map(s => `${s.player.name} ${parseInt(s.scorer) > 1 ? `(x${s.scorer})` : ''}`).join(' - ')} */}
													</TableCell>
													<TableCell component="th" scope="row">
														{n.playingField.name}
													</TableCell>
													<TableCell component="th" scope="row">
														{n.comment}
													</TableCell>
												</TableRow>
											);
										})}
								</TableBody>
							</Table>
						</FuseScrollbars>
						<div>
							<TablePagination
								className="overflow-hidden"
								component="div"
								count={fixtures && fixtures.length}
								rowsPerPage={rowsPerPage}
								page={page}
								backIconButtonProps={{
									'aria-label': 'Prev Pag'
								}}
								nextIconButtonProps={{
									'aria-label': 'Sig Pag'
								}}
								onChangePage={handleChangePage}
								onChangeRowsPerPage={handleChangeRowsPerPage}
							/>
						</div>
					</Collapse>
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
}

export default function CollapsibleZoneTable(props) {
	const { t } = useTranslation('fixture');
	const {
		id,
		tournamentZones,
		handleClick,
		selected,
		handleCheck,
		zonesOpen,
		handleOpenMainRowZones,
		handlePageRowPageZone,
		deleteSelectedItems
	} = props
	return (
		<TableContainer component={Paper}>
			<Table aria-label="collapsible table">
				<TableHead>
					<TableRow>
						<TableCell align="left">Grupos</TableCell>
						<TableCell />
					</TableRow>
				</TableHead>

				<TableBody>
					{
						tournamentZones !== undefined &&
						tournamentZones &&
						tournamentZones.length > 0 &&
						tournamentZones.map((row) => {
							const zone = zonesOpen.find((z) => z.id === row.tournamentZone._id)
							const page = zone && zone.page || 0
							const rowPage = zone && zone.rowPage || 10
							return (
								<Row
									id={id}
									key={row.tournamentZone._id}
									t={t}
									row={row}
									handleClick={handleClick}
									selected={selected}
									handleCheck={handleCheck}
									isOpen={zone !== null && zone !== undefined}
									pageZone={page}
									rowPageZone={rowPage}
									handleOpenMainRowZones={handleOpenMainRowZones}
									handlePageRowPageZone={handlePageRowPageZone}
									deleteSelectedItems={deleteSelectedItems}
								/>
							)
						})
					}
				</TableBody>
			</Table>
		</TableContainer>
	);
}