import DivisionAppConfig from './division/DivisionAppConfig';
import PlayerAppConfig from './player/PlayerAppConfig';
import RefereeAppConfig from './referee/RefereeAppConfig';
import TeamAppConfig from './team/TeamAppConfig';
import TournamentAppConfig from './tournament/TournamentAppConfig';
import InstitutionalAppConfig from './institutional/InstitutionalAppConfig';
import NewsAppConfig from './news/NewsAppConfig';
import PlayingFieldAppConfig from './playingField/PlayingFieldAppConfig';
import SanctionAppConfig from './sanction/SanctionAppConfig';
import FixtureAppConfig from './fixture/FixtureAppConfig';
import TableAppConfig from './table/TableAppConfig';
import RankingAppConfig from './ranking/RankingAppConfig';
import NotificationAppConfig from './notification/NotificationAppConfig';

const appsConfigs = [
	DivisionAppConfig,
	PlayerAppConfig,
	RefereeAppConfig,
	TeamAppConfig,
	TournamentAppConfig,
	InstitutionalAppConfig,
	NewsAppConfig,
	PlayingFieldAppConfig,
	SanctionAppConfig,
	FixtureAppConfig,
	TableAppConfig,
	RankingAppConfig,
	NotificationAppConfig
];

export default appsConfigs;
