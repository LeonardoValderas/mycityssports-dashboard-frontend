import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../referee-i18n/es';
import pt from '../referee-i18n/pt';
i18next.addResourceBundle('es', 'referee', es);
i18next.addResourceBundle('pt', 'referee', pt);

function Referee(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('referee');
	const referee = useSelector(({ refereeApp }) => refereeApp.referee.data);
	const savedModel = useSelector(({ refereeApp }) => refereeApp.referee.savedModel);
	const theme = useTheme();
	const loading = useSelector(({ refereeApp }) => refereeApp.referee.loading);
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();

	useDeepCompareEffect(() => {
		//dispatch(Actions.getMySports());
		function updateRefereeState() {
			const { refereeId } = routeParams;
			if (refereeId === 'new') {
				dispatch(Actions.newReferee());
			} else {
				dispatch(Actions.getReferee(refereeId));
			}
		}

		updateRefereeState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(referee);
		}
	}, [savedModel]);

	function setFormValue(name, value) {
		setForm(
			_.set(
				{ ...form },
				name, value
			)
		);
	}

	// function handleChipChangeSport(value) {
	// 	setFormValue('sports', value)
	// }

	useEffect(() => {
		if ((referee && !form) || (referee && form && referee._id !== form._id)) {
			setForm(referee);
		}
	}, [form, referee, setForm]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return form.name.length > 0 && !_.isEqual(referee, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/referee/referees"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">{t('REFEERES')}</span>
								</Typography>
							</FuseAnimate>
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.saveReferee(form, routeParams.refereeId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.refereeId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}
				>
					<Tab className="h-64 normal-case" label={t('REFEERE')} />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div>
								<div className="flex mx-0 mt-8">
									<TextField
										className="w-5/6"
										error={form.name === ''}
										required
										label={t('NAME')}
										autoFocus
										id="name"
										name="name"
										value={form.name}
										onChange={handleChange}
										variant="outlined"
										inputProps={{
											maxLength: 80,
										}}
										fullWidth
									/>
									{/* <TextField
										className="w-1/6  ml-6"
										label="Orden en la lista"
										id="sort"
										name="sort"
										value={form.sort || ''}
										onChange={handleChange}
										variant="outlined"
										cols={8}
										fullWidth
										inputProps={{
											maxLength: 2,
										}}
										type='number'
									/> */}
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('refereeApp', reducer)(Referee);
