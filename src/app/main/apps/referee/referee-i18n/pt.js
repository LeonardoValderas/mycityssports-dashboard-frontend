const locale = {
	UPDATE: 'Atualizar',
	REFEERES: 'Juiz',
	REFEERE: 'Juiz',
	NAME: 'Nome',
	FUNCTIONS: 'Funções',
	FUNCTION: 'Função',
	ADD_NEW_FUNCTION: 'Adicionar nova função',
	SINGLE_FUNCTION: 'a função',
	PLURAL_FUNCTION: 'as funções',
	NEW_F: 'Nova',
	ATTENTION: 'Atenção',
	DELETE_ITEM: 'Deseja deletar',
	ADD_NEW_REFEREE: 'Adicionar novo juiz',
	SINGLE_REFEREE: 'o juiz',
	PLURAL_REFEREE: 'os juiz',
	NEW_M: 'Novo'	
};

export default locale;
