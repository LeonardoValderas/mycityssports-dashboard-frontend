const locale = {
	UPDATE: 'Actualizar',
	REFEERES: 'Arbitros',
	REFEERE: 'Arbitro',
	NAME: 'Nombre',
	FUNCTIONS: 'Funciones',
	FUNCTION: 'Función',
	ADD_NEW_FUNCTION: 'Adicionar nueva función',
	SINGLE_FUNCTION: 'la función',
	PLURAL_FUNCTION: 'las funciones',
	NEW_F: 'Nueva',
	ATTENTION: 'Atención',	
	DELETE_ITEM: 'Desea eliminar',
	ADD_NEW_REFEREE: 'Adicionar nuevo arbitro',
	SINGLE_REFEREE: 'el arbitro',
	PLURAL_REFEREE: 'los arbitros',
	NEW_M: 'Nuevo'
};

export default locale;
