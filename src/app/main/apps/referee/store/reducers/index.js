import { combineReducers } from 'redux';
import referee from './referee.reducer';
import referees from './referees.reducer';
import refereeFunction from './referee-function.reducer';
import refereeFunctions from './referee-functions.reducer';

const reducer = combineReducers({
	referees,
	referee,
	refereeFunction,
	refereeFunctions
});

export default reducer;
