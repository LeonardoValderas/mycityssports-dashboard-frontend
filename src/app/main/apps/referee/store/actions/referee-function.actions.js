
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_REFEREE_FUNCTION = '[REFEREE_FUNCTION APP] GET REFEREE_FUNCTION';
export const SAVE_REFEREE_FUNCTION = '[REFEREE_FUNCTION APP] SAVE REFEREE_FUNCTION';
export const SET_LOADING_REFEREE_FUNCTION = '[REFEREE_FUNCTION APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getRefereeFunction(refereeFunctionId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading}))
		axios.get(`${translate.baseUrl}/referee-functions/${refereeFunctionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function setModel(model) {
	return {
		type: GET_REFEREE_FUNCTION,
		payload: model
	}
}

export function saveRefereeFunction(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing}))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/referee-functions/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newRefereeFunction() {
	const data = {
		name: '',
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_REFEREE_FUNCTION,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_REFEREE_FUNCTION,
		payload: model
	};
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_REFEREE_FUNCTION,
        payload: loading
    }
}
