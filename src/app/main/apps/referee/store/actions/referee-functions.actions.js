import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_REFEREE_FUNCTIONS = '[REFEREE FUNCTIONS APP] GET REFEREE FUNCTIONS';
export const SET_REFEREE_FUNCTIONS_SEARCH_TEXT = '[REFEREE FUNCTIONS APP] SET REFEREE FUNCTIONS SEARCH TEXT';
export const SET_REFEREE_FUNCTIONS_PAGE_NUMBER_ROW = '[REFEREE FUNCTIONS APP] SET REFEREE FUNCTIONS PAGE NUMBER ROW';
export const SET_LOADING = '[REFEREE FUNCTIONS APP] SET LOADING';
export const CLEAN_SELELCTED_REFEREE_FUNCTION = '[REFEREE FUNCTIONS APP] CLEAN SELELCTED REFEREE FUNCTION';

const lang = i18next.language
const translate = translateInfo(lang)

export function getRefereeFunctions() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading}))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/referee-functions/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_REFEREE_FUNCTIONS,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setPageAndRowFunctions(page, row) {
    return {
        type: SET_REFEREE_FUNCTIONS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function setRefereeFunctionsSearchText(event) {
    return {
        type: SET_REFEREE_FUNCTIONS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteRefereeFunctions(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/referee-functions/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getRefereeFunctions())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_REFEREE_FUNCTION,
        payload: clean
    }
}
