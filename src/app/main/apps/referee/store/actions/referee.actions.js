
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_REFEREE = '[REFEREE APP] GET REFEREE';
export const SAVE_REFEREE = '[REFEREE APP] SAVE REFEREE';
export const SET_LOADING_REFEREE = '[REFEREE APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getReferee(refereeId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/referees/${refereeId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function setModel(model) {
	return {
		type: GET_REFEREE,
		payload: model
	}
}

export function saveReferee(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/referees/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newReferee() {
	const data = {
		name: '',
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_REFEREE,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_REFEREE,
		payload: model
	};
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_REFEREE,
        payload: loading
    }
}
