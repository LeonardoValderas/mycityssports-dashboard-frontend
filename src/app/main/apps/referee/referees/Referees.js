import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import RefereesHeader from './RefereesHeader';
import RefereesTable from './RefereesTable';

function Referees() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<RefereesHeader />}
			content={<RefereesTable />}
			innerScroll
		/>
	);
}

export default withReducer('refereesApp', reducer)(Referees);
