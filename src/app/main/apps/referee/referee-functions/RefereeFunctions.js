import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import RefereeFunctionsHeader from './RefereeFunctionsHeader';
import RefereeFunctionsTable from './RefereeFunctionsTable';

function RefereeFunctions() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<RefereeFunctionsHeader />}
			content={<RefereeFunctionsTable />}
			innerScroll
		/>
	);
}

export default withReducer('refereeFunctionsApp', reducer)(RefereeFunctions);
