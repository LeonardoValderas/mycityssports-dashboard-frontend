import React from 'react';
import { Redirect } from 'react-router-dom';

const RefereeAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/referee/referees/:refereeId',
			component: React.lazy(() => import('./referee/Referee'))
		},
		{
			path: '/apps/referee/referees',
			component: React.lazy(() => import('./referees/Referees'))
		},
		{
			path: '/apps/referee/referee-functions/:refereeFunctionId',
			component: React.lazy(() => import('./referee-function/RefereeFunction'))
		},
		{
			path: '/apps/referee/referee-functions',
			component: React.lazy(() => import('./referee-functions/RefereeFunctions'))
		},
		{
			path: '/apps/referee',
			component: () => <Redirect to="/apps/referee/Referees" />
		}
	]
};

export default RefereeAppConfig;
