import React from 'react';
import { Redirect } from 'react-router-dom';

const TeamAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/team/teams/:teamId',
			component: React.lazy(() => import('./team/Team'))
		},
		{
			path: '/apps/team/teams',
			component: React.lazy(() => import('./teams/Teams'))
		},
		{
			path: '/apps/team',
			component: () => <Redirect to="/apps/team/teams" />
		}
	]
};

export default TeamAppConfig;
