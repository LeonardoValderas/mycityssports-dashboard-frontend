
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId, getInstitutionName } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_TEAM = '[TEAM APP] GET TEAM';
export const GET_TEAM_FORM = '[TEAM APP] GET TEAM FORM';
export const SAVE_TEAM = '[TEAM APP] SAVE TEAM';
export const SET_LOADING_TEAM = '[TEAM APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getMySports() {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		const institutionId = getInstitutionId()
		axios.get(`${translate.baseUrl}/mySports/form/institutions/${institutionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const dataForm = resp.data.model
			dispatch(setModels(dataForm))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function getTeam(teamId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/teams/${teamId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function setModel(model) {
	return {
		type: GET_TEAM,
		payload: model
	}
}

export function setModels(models) {
	return {
		type: GET_TEAM_FORM,
		payload: models
	}
}

export function saveTeam(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/teams/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant: 'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newTeam() {
	const data = {
		name: '',
		sport: null,
		photo: null,
		nameImage: null,
		nameId: null,
		urlImage: null,
		source: '',
		folder: `teams/${getInstitutionName()}`,
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_TEAM,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_TEAM,
		payload: model
	};
}

export function setLoading(loading) {
	return {
		type: SET_LOADING_TEAM,
		payload: loading
	}
}
