import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_TEAMS = '[TEAMS APP] GET TEAMS';
export const SET_TEAMS_SEARCH_TEXT = '[TEAMS APP] SET TEAMS SEARCH TEXT';
export const SET_TEAMS_PAGE_NUMBER_ROW = '[TEAMS APP] SET TEAMS PAGE NUMBER ROW';
export const SET_LOADING = '[TEAMS APP] SET LOADING';
export const CLEAN_SELELCTED_TEAM = '[TEAMS APP] CLEAN SELELCTED TEAM';

const lang = i18next.language
const translate = translateInfo(lang)

export function getTeams() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/teams/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_TEAMS,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setTeamsSearchText(event) {
    return {
        type: SET_TEAMS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function setPageAndRow(page, row) {
    return {
        type: SET_TEAMS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function deleteTeams(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/teams/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getTeams())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_TEAM,
        payload: clean
    }
}
