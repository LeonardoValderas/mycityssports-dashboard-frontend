import * as Actions from '../actions';

const initialState = {
	data: null,
	dataForm: null,
	returnPage: false,
	loading: {
		show: false,
		label: ''
	  },
	savedModel: false  
};

const teamReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_TEAM: {
			return {
				...state,
				data: action.payload,
				loading: {
					show: false,
					label: ''
				  },
				  returnPage: false,
				  savedModel: false
			};
		}
		case Actions.GET_TEAM_FORM: {
			return {
				...state,
				dataForm: action.payload,
		        savedModel: false
			};
		}
		case Actions.SAVE_TEAM: {
			return {
				...state,
				data: action.payload, 
				returnPage: true,
				loading: {
					show: false,
					label: ''
				  },
				  savedModel: true
			};
		}
		case Actions.SET_LOADING_TEAM: {
			return {
				...state,
				loading: action.payload,
	        	savedModel: false
			};
		}
		default: {
			return state;
		}
	}
};

export default teamReducer;
