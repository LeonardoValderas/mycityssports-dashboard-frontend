import { combineReducers } from 'redux';
import team from './team.reducer';
import teams from './teams.reducer';

const reducer = combineReducers({
	teams,
	team
});

export default reducer;
