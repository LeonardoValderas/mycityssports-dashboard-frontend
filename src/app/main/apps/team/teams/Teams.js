import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import TeamsHeader from './TeamsHeader';
import TeamsTable from './TeamsTable';

function Teams() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<TeamsHeader />}
			content={<TeamsTable />}
			innerScroll
		/>
	);
}

export default withReducer('teamApp', reducer)(Teams);
