import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../store/actions';
import TeamsTableHead from './TeamsTableHead';
import constants from '../../utils/Constants'
import FuseLoading from '@fuse/core/FuseLoading';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../team-i18n/es';
import pt from '../team-i18n/pt';
i18next.addResourceBundle('es', 'team', es);
i18next.addResourceBundle('pt', 'team', pt);

function TeamsTable(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('team');
	const teams = useSelector(({ teamApp }) => teamApp.teams.data);
	const searchText = useSelector(({ teamApp }) => teamApp.teams.searchText);
	const pageRow = useSelector(({ teamApp }) => teamApp.teams.pageRow);
	const loading = useSelector(({ teamApp }) => teamApp.teams.loading);
	const cleanSelected = useSelector(({ teamApp }) => teamApp.teams.cleanSelected);
	const [clean, setClean] = useState(cleanSelected);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(teams);
	const [page, setPage] = useState(pageRow.page);
	const [rowsPerPage, setRowsPerPage] = useState(pageRow.row);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useEffect(() => {
		dispatch(Actions.getTeams());
	}, [dispatch]);

	useEffect(() => {
		setClean(cleanSelected)
	}, [cleanSelected]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(teams, item => item.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(teams);
		}
	}, [teams, searchText]);

	// function isPageValidated(){
	//     if(teams && pageRow.page !== 0){
	// 	  let pageRowNumber = pageRow.page * pageRow.row
	// 	  console.log("pageRowNumber", pageRowNumber)
	// 	  console.log("teams.length", teams.length)
	// 	  return teams.length > pageRowNumber 
	// 	}
	// 	return false
	// }

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setClean(false)
			setSelected(data.map(n => n._id));
			return;
		}
		setSelected([]);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function deleteTeams() {
		handleClose()
		dispatch(Actions.deleteTeams({ ids: selected }));
	}

	function deleteSelectedItems() {
		handleClickOpen()
	}

	function handleClick(item) {
		dispatch(Actions.setPageAndRow(page, rowsPerPage))
		props.history.push(`/apps/team/teams/${item._id}`);
	}

	function handleCheck(event, id) {
		setClean(false)
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	return (
		<div className="w-full flex flex-col">
			<div>
				<Dialog
					open={open}
					onClose={handleClose}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title">{t('ATTENTION')}</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							{`${t('DELETE_ITEM')} ${selected.length === 1 ? t('SINGLE_TEAM') : t('PLURAL_TEAM')}?`}
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose} color="primary">
							Cancelar
          				</Button>
						<Button onClick={deleteTeams} color="primary" autoFocus>
							Eliminar
          				</Button>
					</DialogActions>
				</Dialog>
			</div>
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table className="min-w-xl" aria-labelledby="tableTitle">
					<TeamsTableHead
						numSelected={selected.length}
						order={order}
						cleanSelected={clean}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={data.length}
						deleteSelectedItems={deleteSelectedItems}
					/>

					<TableBody>
						{_.orderBy(
							data,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											const orderId = order.id
											if (orderId && o !== null) {
												let filter = o
												const split = orderId.split('.')
												split.map(s => {
													const f = filter[s]
													if (f) {
														filter = f
													}
												})
												return filter;
											}
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n._id) !== -1;
								return (
									<TableRow
										className="h-64 cursor-pointer"
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n._id}
										selected={isSelected}
										onClick={event => handleClick(n)}
									>
										<TableCell className="w-64 text-center" padding="none">
											<Checkbox
												checked={isSelected}
												onClick={event => event.stopPropagation()}
												onChange={event => handleCheck(event, n._id)}
											/>
										</TableCell>

										<TableCell className="w-52" component="th" scope="row" padding="none">
											{(
												<img
													className="w-full block rounded"
													src={n.urlImage || constants.DEFAULT_URL_IMAGE}
													alt={'Escudo'}
												/>
											)}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.name}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.sport.name} ({n.sport.gender.name})
										</TableCell>

										{/* <TableCell component="th" scope="row">
											{n.member}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.memberId}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.birthday}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.team.label}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.division.label}
										</TableCell> */}

										{/* 			<TableCell className="truncate" component="th" scope="row">
											{
												n.tournamentZones.map(z =>
													z.label
												).join(' - ')
											}
										</TableCell>

										<TableCell className="truncate" component="th" scope="row">
											{
												n.tournamentInstances.map(i =>
													i.label
												).join(' - ')
											}
										</TableCell> */}
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>
			<div>
				<TablePagination
					className="overflow-hidden"
					component="div"
					count={data.length}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
}

export default withRouter(TeamsTable);
