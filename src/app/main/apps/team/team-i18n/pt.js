const locale = {
	IMAGE_BIG: 'O tamanho da imagem excede o máximo suportado.',
	UPDATE: 'Atualizar',
	TEAMS: 'Times',
	TEAM: 'Time',
	NAME: 'Nome',
	SPORT: 'Esporte',
	SPORT_SELECTION: 'Selecione o esporte',
	SOURCE: 'Fonte',
	IMAGE_RECOMMENTED: 'Use imagem quadrada. Tamanho recomendado 128x128',
	ADD_NEW_TEAM: 'Adicionar novo time',
	NEW: 'Novo',
	ATTENTION: 'Atenção',
	DELETE_TEAM: 'Deseja deletar',
	SINGLE_TEAM: 'o Time',
	PLURAL_TEAM: 'os TimeS'
};

export default locale;
