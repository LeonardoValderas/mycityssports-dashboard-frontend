const locale = {
	IMAGE_BIG: 'El tamaño de la imagen supera el maximo soportado.',
	UPDATE: 'Actualizar',
	TEAMS: 'Equipos',
	TEAM: 'Equipo',
	NAME: 'Nombre',
	SPORT: 'Deporte',
	SPORT_SELECTION: 'Seleccione el deporte',
	SOURCE: 'Fuente',
	IMAGE_RECOMMENTED: 'Usar imagen cuadrada. Tamaño recomendado 128x128',
	ADD_NEW_TEAM: 'Adicionar nuevo equipo',
	NEW: 'Nuevo',
	ATTENTION: 'Atención',	
	DELETE_ITEM: 'Desea eliminar',
	SINGLE_TEAM: 'el equipo',
	PLURAL_TEAM: 'los equipos'	
};

export default locale;
