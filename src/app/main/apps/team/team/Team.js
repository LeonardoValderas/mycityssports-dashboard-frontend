import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import _ from '@lodash';
import clsx from 'clsx';
import { orange } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import constants from '../../utils/Constants'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import jimp from 'jimp';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../team-i18n/es';
import pt from '../team-i18n/pt';
i18next.addResourceBundle('es', 'team', es);
i18next.addResourceBundle('pt', 'team', pt);

const useStyles = makeStyles(theme => ({
	productImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		color: orange[400],
		opacity: 0
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	productImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $productImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $productImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $productImageFeaturedStar': {
				opacity: 1
			}
		}
	}
}));

function Team(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('team');
	const team = useSelector(({ teamApp }) => teamApp.team);
	const savedModel = useSelector(({ teamApp }) => teamApp.team.savedModel);
	const dataForm = useSelector(({ teamApp }) => teamApp.team.dataForm);
	const loading = useSelector(({ teamApp }) => teamApp.team.loading);
	const theme = useTheme();
	const classes = useStyles(props);
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();
	const [imageError, setImageError] = useState("");
	
	useDeepCompareEffect(() => {
		dispatch(Actions.getMySports());
		function updateDivisionState() {
			const { teamId } = routeParams;
			if (teamId === 'new') {
				dispatch(Actions.newTeam());
			} else {
				dispatch(Actions.getTeam(teamId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(team.data);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((team.data && !form) || (team.data && form && team.data._id !== form._id)) {
			setForm(team.data);
		}
	}, [form, team.data, setForm]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return form && form.name.length > 0
			&& form.sport
			&& !_.isEqual(team.data, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function handleUploadChange(e) {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		form.nameImage = file.name
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = async () => {
			let r = `data:${file.type};base64,${btoa(reader.result)}`
			const image = await jimp.read(r);
			await image.resize(128, 128);
			let base = await image.getBase64Async(image.getMIME())
			let d = await image.bitmap.data
			let f = new File(d, file.name)
			let newSize = f.size / 1024 / 1024
			if(newSize > 1.0){
			   setImageError(t('IMAGE_BIG')) 
			   return
			} else {
				setImageError("") 
				setForm(
					_.set(
						{ ...form },
						`photo`, base
					)
				);
			}
		};

		reader.onerror = () => {
			console.log('error on load image');
		};
	}

	function handleChipChange(value, name) {
		setForm(
			_.set(
				{ ...form },
				name, value,
			)
		);
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/team/teams"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">{t('TEAMS')}</span>
								</Typography>
							</FuseAnimate>
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.saveTeam(form, routeParams.teamId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.teamId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label={t('TEAM')} />
					<Tab className="h-64 normal-case" label="Escudo" />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<TextField
									className="mt-16"
									error={form.name === ''}
									required
									label={t('NAME')}
									autoFocus
									id="name"
									name="name"
									value={form.name}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 60,
									}}
								/>

								<FuseChipSelect
									className="mt-16"
									value={form.sport}
									onChange={value => handleChipChange(value, 'sport')}
									placeholder={t('SPORT_SELECTION')}
									required={true}
									variant='fixed'
									getOptionLabel={option => `${option.name} (${option.gender.name})`}
									getOptionValue={option => option._id}
									textFieldProps={{
										label: t('SPORT'),
										InputLabelProps: {
											shrink: true
										},
										size:"medium",
										variant: 'outlined'
									}}
									options={dataForm && dataForm.sports || []}
								/>
								
								<TextField
									className="mt-16"
									label={t('SOURCE')}
									id="source"
									name="source"
									value={form.source}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 50,
									}}
								/>
							</div>
						)}	{tabValue === 1 && (
							<div>
								<div className="flex justify-center sm:justify-start flex-wrap -mx-8">
									<label
										htmlFor="button-file"
										className={clsx(
											classes.productImageUpload,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5'
										)}
									>
										<input
											accept="image/*"
											className="hidden"
											id="button-file"
											type="file"
											onChange={handleUploadChange}
										/>
										<Icon fontSize="large" color="action">
											cloud_upload
										</Icon>
									</label>
									<div
										role="button"
										tabIndex={0}
										className={clsx(
											classes.productImageItem,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5',
										)}
										key={form._id}
									>
										<img className="max-w-128 w-128 h-128" src={form.photo || form.urlImage || constants.DEFAULT_URL_IMAGE_128_128} alt="Escudo" />
									</div>
								</div>
								<div className = "flex flex-col">
									<span className="mx-4">{t('IMAGE_RECOMMENTED')}</span>
									<span className="mx-4" style={{color:'red', fontStyle: 'bold'}}>{imageError}</span>
								</div>
							</div>
						)}
					</div>
				)
			}

			innerScroll
		/>
	);
}

export default withReducer('teamApp', reducer)(Team);
