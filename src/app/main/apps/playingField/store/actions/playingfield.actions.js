
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_PLAYING_FIELD = '[PLAYING_FIELD APP] GET PLAYING_FIELD';
export const GET_PLAYING_FIELD_FORM = '[PLAYING_FIELD APP] GET PLAYING_FIELD FORM';
export const SAVE_PLAYING_FIELD = '[PLAYING_FIELD APP] SAVE PLAYING_FIELD';
export const SET_LOADING_PLAYING_FIELD = '[PLAYING_FIELD APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getPlayingField(playingFieldId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/playingFields/${playingFieldId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function setModel(model) {
	return {
		type: GET_PLAYING_FIELD,
		payload: model
	}
}

export function setModels(models) {
	return {
		type: GET_PLAYING_FIELD_FORM,
		payload: models
	}
}

export function savePlayingField(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/playingFields/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant: 'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newPlayingField() {
	const data = {
		name: '',
		street: '',
		number: '',
		source: '',
		neighborhood: '',
		position: {
			latitude: '',
			longitude: '',
		},
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_PLAYING_FIELD,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_PLAYING_FIELD,
		payload: model
	};
}

export function setLoading(loading) {
	return {
		type: SET_LOADING_PLAYING_FIELD,
		payload: loading
	}
}
