import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_PLAYING_FIELDS = '[PLAYING_FIELDS APP] GET PLAYING_FIELDS';
export const SET_PLAYING_FIELDS_SEARCH_TEXT = '[PLAYING_FIELDS APP] SET PLAYING_FIELDS SEARCH TEXT';
export const SET_PLAYING_FIELDS_PAGE_NUMBER_ROW = '[PLAYING_FIELDS APP] SET PLAYING_FIELDS PAGE NUMBER ROW';
export const SET_LOADING = '[PLAYING_FIELDS APP] SET LOADING';
export const CLEAN_SELELCTED_PLAYING_FIELD = '[PLAYING_FIELDS APP] CLEAN SELELCTED PLAYING_FIELD';

const lang = i18next.language
const translate = translateInfo(lang)

export function getPlayingFields() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/playingFields/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_PLAYING_FIELDS,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setPageAndRow(page, row) {
    return {
        type: SET_PLAYING_FIELDS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function setPlayingFieldsSearchText(event) {
    return {
        type: SET_PLAYING_FIELDS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deletePlayingFields(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/playingFields/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getPlayingFields())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_PLAYING_FIELD,
        payload: clean
    }
}
