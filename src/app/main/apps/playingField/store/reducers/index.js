import { combineReducers } from 'redux';
import playingField from './playingfield.reducer';
import playingFields from './playingfields.reducer';

const reducer = combineReducers({
	playingFields,
	playingField
});

export default reducer;
