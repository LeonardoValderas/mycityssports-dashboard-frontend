import * as Actions from '../actions';

const initialState = {
	data: [],
	searchText: '',
	pageRow: {
		page: 0, 
		row: 10
	},
	loading: {
	  show: false,
	  label: ''
	},
	cleanSelected: false
};

const playingFieldsReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_PLAYING_FIELDS: {
			return {
				...state,
				data: action.payload, 
				loading: {
					show: false,
					label: ''
				  }
			};
		}
		case Actions.SET_LOADING: {
			return {
				...state,
				loading: action.payload
			};
		}
		case Actions.SET_PLAYING_FIELDS_SEARCH_TEXT: {
			return {
				...state,
				searchText: action.searchText
			};
		}
		case Actions.SET_PLAYING_FIELDS_PAGE_NUMBER_ROW: {
			return {
				...state,
				pageRow: action.pageRow
			};
		}
		case Actions.CLEAN_SELELCTED_PLAYING_FIELD: {
			return {
				...state,
				cleanSelected: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default playingFieldsReducer;
