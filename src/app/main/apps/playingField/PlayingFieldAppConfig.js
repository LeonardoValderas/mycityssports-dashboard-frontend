import React from 'react';
import { Redirect } from 'react-router-dom';

const PlayingFieldAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/playingField/playingFields/:playingFieldId',
			component: React.lazy(() => import('./playingField/PlayingField'))
		},
		{
			path: '/apps/playingField/playingFields',
			component: React.lazy(() => import('./playingFields/PlayingFields'))
		},
		{
			path: '/apps/playingField',
			component: () => <Redirect to="/apps/playingField/playingFields" />
		}
	]
};

export default PlayingFieldAppConfig;
