const locale = {
	IMAGE_BIG: 'O tamanho da imagem excede o máximo suportado.',
	PLAYINGS_FIELD: 'Campos de jogo',
	PLAYING_FIELD: 'Campo de jogo',
	UPDATE: 'Atualizar',
	NAME: 'Nome',
	STREET: 'Rua',
	NEIGHBORHOOD: 'Bairro',
	SOURCE: 'Fonte',
	ADD_NEW_PLAYING_FIELD: 'Adicionar novo campo',
	NEW_M: 'Novo',
	ATTENTION: 'Atenção',
	SINGLE_PLAYING_FIELD: 'o campo de jogo',
	PLURAL_PLAYING_FIELD: 'os campos de jogos',
	DELETE_ITEM: 'Deseja deletar'
};

export default locale;
