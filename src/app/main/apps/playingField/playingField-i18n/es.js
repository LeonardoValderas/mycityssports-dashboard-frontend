const locale = {
	IMAGE_BIG: 'El tamaño de la imagen supera el maximo soportado.',
	PLAYINGS_FIELD: 'Campos de juego',
	PLAYING_FIELD: 'Campo de juego',
	UPDATE: 'Actualizar',
	NAME: 'Nombre',
	STREET: 'Calle',
	NEIGHBORHOOD: 'Barrio',
	SOURCE: 'Fuente',
	ADD_NEW_PLAYING_FIELD: 'Adicionar nuevo campo',
	NEW_M: 'Nuevo',
	ATTENTION: 'Atención',
	SINGLE_PLAYING_FIELD: 'el campo de juego',
	PLURAL_PLAYING_FIELD: 'los campos de juegos',
	DELETE_ITEM: 'Desea eliminar'
};

export default locale;
