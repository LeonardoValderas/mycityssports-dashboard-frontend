import Checkbox from '@material-ui/core/Checkbox';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';
import clsx from 'clsx';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../playingField-i18n/es';
import pt from '../playingField-i18n/pt';
i18next.addResourceBundle('es', 'playingField', es);
i18next.addResourceBundle('pt', 'playingField', pt);



const useStyles = makeStyles(theme => ({
	actionsButtonWrapper: {
		background: theme.palette.background.paper
	}
}));

function PlayingFieldsTableHead(props) {
	const classes = useStyles(props);
	const { t } = useTranslation('playingField');
	const [selectedDivisionsMenu, setSelectedDivisionsMenu] = useState(null);

	const rows = [
		{
			id: 'name',
			align: 'left',
			disablePadding: false,
			label: t('NAME'),
			sort: true
		},
		{
			id: 'street',
			align: 'left',
			disablePadding: false,
			label: t('STREET'),
			sort: true
		},
		{
			id: 'number',
			align: 'left',
			disablePadding: false,
			label: 'Número',
			sort: true
		},
		{
			id: 'neighborhood',
			align: 'left',
			disablePadding: false,
			label: t('NEIGHBORHOOD'),
			sort: true
		}
	];

	const createSortHandler = property => event => {
		props.onRequestSort(event, property);
	};

	function openSelectedDivisionsMenu(event) {
		setSelectedDivisionsMenu(event.currentTarget);
	}

	function closeSelectedDivisionsMenu() {
		setSelectedDivisionsMenu(null);
	}

	function deleteAndCloseMenu(){
		closeSelectedDivisionsMenu()
        props.deleteSelectedItems()
	}
	return (
		<TableHead>
			<TableRow className="h-64">
				<TableCell padding="none" className="relative w-64 text-center">
					<Checkbox
						indeterminate={props.numSelected > 0 && props.numSelected < props.rowCount}
						checked={props.numSelected === props.rowCount}
						onChange={props.onSelectAllClick}
					/>
					
					{(props.numSelected > 0  && !props.cleanSelected) && (
						<div
							className={clsx(
								'flex items-center justify-center absolute w-64 top-0 ltr:left-0 rtl:right-0 mx-56 h-64 z-10',
								classes.actionsButtonWrapper
							)}
						>
							<IconButton
								aria-owns={selectedDivisionsMenu ? 'selectedDivisionsMenu' : null}
								aria-haspopup="true"
								onClick={openSelectedDivisionsMenu}
							>
								<Icon>more_horiz</Icon>
							</IconButton>
							<Menu
								id="selectedDivisionsMenu"
								anchorEl={selectedDivisionsMenu}
								open={Boolean(selectedDivisionsMenu)}
								onClose={closeSelectedDivisionsMenu}
							>
								<MenuList>
									<MenuItem
										onClick={() => {
											deleteAndCloseMenu();
										}}
									>
										<ListItemIcon className="min-w-40">
											<Icon>delete</Icon>
										</ListItemIcon>
										<ListItemText primary="Eliminar" />
									</MenuItem>
								</MenuList>
							</Menu>
						</div>
					)}
				</TableCell>
				{rows.map(row => {
					return (
						<TableCell
							key={row.id}
							align={row.align}
							padding={row.disablePadding ? 'none' : 'default'}
							sortDirection={props.order.id === row.id ? props.order.direction : false}
						>
							{row.sort && (
								<Tooltip
									title="Sort"
									placement={row.align === 'right' ? 'bottom-end' : 'bottom-start'}
									enterDelay={300}
								>
									<TableSortLabel
										active={props.order.id === row.id}
										direction={props.order.direction}
										onClick={createSortHandler(row.id)}
									>
										{row.label}
									</TableSortLabel>
								</Tooltip>
							)}
						</TableCell>
					);
				}, this)}
			</TableRow>
		</TableHead>
	);
}

export default PlayingFieldsTableHead;
