import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import PlayingFieldsHeader from './PlayingFieldsHeader';
import PlayingFieldsTable from './PlayingFieldsTable';

function PlayingFields() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<PlayingFieldsHeader />}
			content={<PlayingFieldsTable />}
			innerScroll
		/>
	);
}

export default withReducer('playingFieldApp', reducer)(PlayingFields);
