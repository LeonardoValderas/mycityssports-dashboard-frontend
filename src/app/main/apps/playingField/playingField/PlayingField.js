import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import _ from '@lodash';
import Map from '../../utils/GoogleMaps';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../playingField-i18n/es';
import pt from '../playingField-i18n/pt';
i18next.addResourceBundle('es', 'playingField', es);
i18next.addResourceBundle('pt', 'playingField', pt);

function PlayingField(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('playingField');
	const data = useSelector(({ playingFieldApp }) => playingFieldApp.playingField.data);
	const savedModel = useSelector(({ playingFieldApp }) => playingFieldApp.playingField.savedModel);
	const loading = useSelector(({ playingFieldApp }) => playingFieldApp.playingField.loading);
	const theme = useTheme();
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();

	useDeepCompareEffect(() => {
		//dispatch(Actions.getDataForm());
		function updateDivisionState() {
			const { playingFieldId } = routeParams;
			if (playingFieldId === 'new') {
				dispatch(Actions.newPlayingField());
			} else {
				dispatch(Actions.getPlayingField(playingFieldId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(data);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((data && !form) || (data && form && data._id !== form._id)) {
			setForm(data);
		}
	}, [form, data, setForm]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function mapClicked(mapProps, map, e) {
		setForm(
			_.set({ ...form },
				`position`, { latitude: e.latLng.lat(), longitude: e.latLng.lng() }
			)
		);
	}

	function canBeSubmitted() {
		return form && form.name.length > 0
			&& form.street.length > 0
			&& form.number.length > 0
			&& form.neighborhood.length > 0
			&& form.position.latitude !== ''
			&& form.position.longitude !== ''
			&& !_.isEqual(data, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/playingField/playingFields"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">{t('PLAYINGS_FIELD')}</span>
								</Typography>
							</FuseAnimate>
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.savePlayingField(form, routeParams.playingFieldId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.playingFieldId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label={t('PLAYING_FIELD')} />
					{/* <Tab className="h-64 normal-case" label="Foto" /> */}
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<TextField
									className="mt-16"
									error={form.name === ''}
									required
									label={t('NAME')}
									autoFocus
									id="name"
									name="name"
									value={form.name}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 150,
									}}
								/>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-16 w-full md:mr-8 md:w-3/6"
										error={form.street === ''}
										required
										label={t('STREET')}
										//autoFocus
										id="street"
										name="street"
										value={form.street}
										onChange={handleChange}
										variant="outlined"
										cols={8}
										fullWidth
										inputProps={{
											maxLength: 200,
										}}
									/>

									<TextField
										className="mt-16  w-full md:mr-8 md:w-1/6"
										error={form.number === ''}
										required
										label="Número"
										//autoFocus
										id="number"
										name="number"
										value={form.number}
										onChange={handleChange}
										variant="outlined"
										cols={8}
										fullWidth
										inputProps={{
											maxLength: 10,
										}}
									/>
									<TextField
										className="mt-16 w-full md:w-2/6"
										error={form.neighborhood === ''}
										required
										label={t('NEIGHBORHOOD')}
										//	autoFocus
										id="neighborhood"
										name="neighborhood"
										value={form.neighborhood}
										onChange={handleChange}
										variant="outlined"
										cols={8}
										fullWidth
										inputProps={{
											maxLength: 200,
										}}
									/>
								</div>
								<TextField
									className="mt-16"
									label={t('SOURCE')}
									id="source"
									name="source"
									value={form.source}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 50,
									}}
								/>

								<div className="h-320 mt-8 mb-16 mr-24">
									<Map
										latitude={parseFloat(form.position.latitude)}
										longitude={parseFloat(form.position.longitude)}
										//readOnly={readOnly || loading}
										mapClicked={mapClicked}
									//errorLabel={markerRequired}
									//error={errorMarker} 
									/>
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('playingFieldApp', reducer)(PlayingField);
