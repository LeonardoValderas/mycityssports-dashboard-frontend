import React from 'react';
import { Redirect } from 'react-router-dom';

const DivisionAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/division/divisions/:divisionId',
			component: React.lazy(() => import('./division/Division'))
		},
		{
			path: '/apps/division/divisions',
			component: React.lazy(() => import('./divisions/Divisions'))
		},
		// {
		// 	path: '/apps/division/orders/:orderId',
		// 	component: React.lazy(() => import('./order/Order'))
		// },
		// {
		// 	path: '/apps/division/orders',
		// 	component: React.lazy(() => import('./orders/Orders'))
		// },
		{
			path: '/apps/division',
			component: () => <Redirect to="/apps/division/divisions" />
		}
	]
};

export default DivisionAppConfig;
