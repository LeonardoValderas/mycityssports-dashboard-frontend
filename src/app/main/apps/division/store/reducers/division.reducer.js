import * as Actions from '../actions';

const initialState = {
	data: null,
	//sports: null,
	returnPage: false,
	loading: {
		show: false,
		label: ''
	  },
	savedModel: false  
};

const divisionReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_DIVISION: {
			return {
				...state,
				data: action.payload,
				loading: {
					show: false,
					label: ''
				  },
				  returnPage: false,
				  savedModel: false
			};
		}
		// case Actions.GET_DIVISION_SPORTS: {
		// 	return {
		// 		...state,
		// 		sports: action.payload,
		// 		loading: {
		// 			show: false,
		// 			label: ''
		// 		  },
		// 		  returnPage: false,
		// 		  savedModel: false
		// 	};
		// }
		case Actions.SAVE_DIVISION: {
			return {
				...state,
				data: action.payload, 
				returnPage: true,
				loading: {
					show: false,
					label: ''
				  },
				  savedModel: true
			};
		}
		case Actions.SET_LOADING_DIVISION: {
			return {
				...state,
				loading: action.payload,
	        	savedModel: false
			};
		}
		default: {
			return state;
		}
	}
};

export default divisionReducer;
