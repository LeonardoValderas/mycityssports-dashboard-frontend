import * as Actions from '../actions';

const initialState = {
	data: [],
	searchText: '',
	pageRow: {
		page: 0, 
		row: 10
	},
	loading: {
	  show: false,
	  label: ''
	},
	cleanSelected: false
};

const divisionsReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_DIVISIONS: {
			return {
				...state,
				data: action.payload, 
				loading: {
					show: false,
					label: ''
				  }
			};
		}
		case Actions.SET_LOADING: {
			return {
				...state,
				loading: action.payload
			};
		}
		case Actions.SET_DIVISIONS_PAGE_NUMBER_ROW: {
			return {
				...state,
				pageRow: action.pageRow
			};
		}
		case Actions.SET_DIVISIONS_SEARCH_TEXT: {
			return {
				...state,
				searchText: action.searchText
			};
		}
		case Actions.CLEAN_SELELCTED_DIVISION: {
			return {
				...state,
				cleanSelected: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default divisionsReducer;
