import { combineReducers } from 'redux';
import division from './division.reducer';
import divisions from './divisions.reducer';

const reducer = combineReducers({
	divisions,
	division
});

export default reducer;
