import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_DIVISIONS = '[DIVISIONS APP] GET DIVISIONS';
export const SET_DIVISIONS_SEARCH_TEXT = '[DIVISIONS APP] SET DIVISIONS SEARCH TEXT';
export const SET_DIVISIONS_PAGE_NUMBER_ROW = '[DIVISIONS APP] SET DIVISIONS PAGE NUMBER ROW';
export const SET_LOADING = '[DIVISIONS APP] SET LOADING';
export const CLEAN_SELELCTED_DIVISION = '[DIVISIONS APP] CLEAN SELELCTED DIVISION';

const lang = i18next.language
const translate = translateInfo(lang)

export function getDivisions() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/divisions/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_DIVISIONS,
        payload: models
    }
}

export function setPageAndRow(page, row) {
    return {
        type: SET_DIVISIONS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setDivisionsSearchText(event) {
    return {
        type: SET_DIVISIONS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteDivisions(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/divisions/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getDivisions())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_DIVISION,
        payload: clean
    }
}
