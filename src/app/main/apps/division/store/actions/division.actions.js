
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_DIVISION = '[DIVISION APP] GET DIVISION';
export const SAVE_DIVISION = '[DIVISION APP] SAVE DIVISION';
export const SET_LOADING_DIVISION = '[DIVISION APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getDivision(divisionId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/divisions/${divisionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function setModel(model) {
	return {
		type: GET_DIVISION,
		payload: model
	}
}

export function saveDivision(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/divisions/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newDivision() {
	const data = {
		name: '',
		sort: 0,
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_DIVISION,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_DIVISION,
		payload: model
	};
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_DIVISION,
        payload: loading
    }
}
