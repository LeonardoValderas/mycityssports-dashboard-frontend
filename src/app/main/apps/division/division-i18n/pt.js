const locale = {
	DIVISIONS: 'Divisões',
	DIVISION: 'Divisão',
	UPDATE: 'Atualizar',
	SORT_ON_LIST: 'Posição na lista',
	SORT: 'Posição',
	ADD_NEW_DIVISION: 'Adicionar nova divisão',
	NEW: 'Nova',
	ATTENTION: 'Atenção',
	SINGLE_DIVISION: 'a divisão',
	PLURAL_DIVISION: 'as divisões',
	DELETE_DIVISION: 'Deseja deletar',
};

export default locale;
