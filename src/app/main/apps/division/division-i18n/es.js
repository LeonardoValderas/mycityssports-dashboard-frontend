const locale = {
	DIVISIONS: 'Divisiones',
	DIVISION: 'División',
	UPDATE: 'Actualizar',
	SORT_ON_LIST: 'Orden en la lista',
	SORT: 'Orden',
	ADD_NEW_DIVISION: 'Adicionar nueva división',
	NEW: 'Nueva',
	ATTENTION: 'Atención',
	SINGLE_DIVISION: 'la división',
	PLURAL_DIVISION: 'las divisiones',
	DELETE_DIVISION: 'Desea eliminar',
};

export default locale;
