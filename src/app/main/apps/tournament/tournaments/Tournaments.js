import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import TournamentsHeader from './TournamentsHeader';
import TournamentsTable from './TournamentsTable';

function Tournaments() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<TournamentsHeader />}
			content={<TournamentsTable />}
			innerScroll
		/>
	);
}

export default withReducer('tournamentApp', reducer)(Tournaments);
