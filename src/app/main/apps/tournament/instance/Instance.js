import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../tournament-i18n/es';
import pt from '../tournament-i18n/pt';
i18next.addResourceBundle('es', 'tournament', es);
i18next.addResourceBundle('pt', 'tournament', pt);

function Instance(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('tournament');
	const instance = useSelector(({ tournamentApp }) => tournamentApp.instance);
	const savedModel = useSelector(({ tournamentApp }) => tournamentApp.instance.savedModel);
	const theme = useTheme();
	const loading = useSelector(({ tournamentApp }) => tournamentApp.instance.loading);
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();

	useDeepCompareEffect(() => {
		function updateDivisionState() {
			const { instanceId } = routeParams;
			if (instanceId === 'new') {
				dispatch(Actions.newInstance());
			} else {
				dispatch(Actions.getInstance(instanceId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(instance.data);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((instance.data && !form) || (instance.data && form && instance.data._id !== form._id)) {
			setForm(instance.data);
		}
	}, [form, instance.data, setForm]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return form.name.length > 0 && !_.isEqual(instance.data, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/tournament/instances"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">{t('INSTANCES')}</span>
								</Typography>
							</FuseAnimate>
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.saveInstance(form, routeParams.instanceId === 'new' ? 'post' : 'put'))}
							>
									{routeParams.instanceId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label={t('INSTANCE')} />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div>
								<TextField
									className="mt-8 mb-16"
									error={form.name === ''}
									required
									label={t('INSTANCE')}
									autoFocus
									id="name"
									name="name"
									value={form.name}
									onChange={handleChange}
									variant="outlined"
									fullWidth
								/>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('tournamentApp', reducer)(Instance);
