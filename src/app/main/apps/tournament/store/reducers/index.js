import { combineReducers } from 'redux';
import tournament from './tournament.reducer';
import tournaments from './tournaments.reducer';
import zones from './zones.reducer';
import zone from './zone.reducer';
import instances from './instances.reducer';
import instance from './instance.reducer';

const reducer = combineReducers({
	tournaments,
	tournament,
	zones,
	zone,
	instances,
	instance

});

export default reducer;
