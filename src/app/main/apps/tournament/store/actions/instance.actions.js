
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_INSTANCE = '[INSTANCE APP] GET INSTANCE';
export const SAVE_INSTANCE = '[INSTANCE APP] SAVE INSTANCE';
export const SET_LOADING_INSTANCE = '[INSTANCE APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getInstance(instanceId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/tournamentInstances/${instanceId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function setModel(model) {
	return {
		type: GET_INSTANCE,
		payload: model
	}
}

export function saveInstance(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing}))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/tournamentInstances/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newInstance() {
	const data = {
		name: '',
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_INSTANCE,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_INSTANCE,
		payload: model
	};
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_INSTANCE,
        payload: loading
    }
}
