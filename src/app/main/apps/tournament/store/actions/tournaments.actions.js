import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_TOURNAMENTS = '[TOURNAMENTS APP] GET TOURNAMENTS';
export const SET_TOURNAMENTS_SEARCH_TEXT = '[TOURNAMENTS APP] SET TOURNAMENTS SEARCH TEXT';
export const SET_TOURNAMENTS_PAGE_NUMBER_ROW = '[TOURNAMENTS APP] SET TOURNAMENTS PAGE NUMBER ROW';
export const SET_LOADING = '[TOURNAMENTS APP] SET LOADING';
export const CLEAN_SELELCTED_TOURNAMENT = '[TOURNAMENTS APP] CLEAN SELELCTED TOURNAMENT';

const lang = i18next.language
const translate = translateInfo(lang)

export function getTournaments() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/tournaments/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_TOURNAMENTS,
        payload: models
    }
}

export function setPageAndRow(page, row) {
    return {
        type: SET_TOURNAMENTS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setTournamentsSearchText(event) {
    return {
        type: SET_TOURNAMENTS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteTournaments(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/tournaments/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getTournaments())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_TOURNAMENT,
        payload: clean
    }
}
