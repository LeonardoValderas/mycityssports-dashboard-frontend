
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_TOURNAMENT = '[TOURNAMENT APP] GET TOURNAMENT';
export const GET_TOURNAMENT_FORM = '[TOURNAMENT APP] GET TOURNAMENT FORM';
export const SAVE_TOURNAMENT = '[TOURNAMENT APP] SAVE TOURNAMENT';
export const SET_LOADING_TOURNAMENT = '[TOURNAMENT APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getDataForm() {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		const institutionId = getInstitutionId()
		axios.get(`${translate.baseUrl}/tournaments/form/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const dataForm = resp.data.models
			dispatch(setModels(dataForm))
		}).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
	}
}

export function getTournament(tournamentId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/tournaments/${tournamentId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function setModel(model) {
	return {
		type: GET_TOURNAMENT,
		payload: model
	}
}

export function setModels(models) {
	return {
		type: GET_TOURNAMENT_FORM,
		payload: models
	}
}

export function saveTournament(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing}))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/tournaments/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newTournament() {
	const data = {
		name: '',
		year:'',
		
		winPoint: 3,
		user: getUserId(),
		institution: getInstitutionId(),
		tournamentType: null,
		sports: [],
		divisions: [],
		subDivisions:[],
		tournamentZones: [],
		tournamentInstances: []
	};

	return {
		type: GET_TOURNAMENT,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_TOURNAMENT,
		payload: model
	};
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_TOURNAMENT,
        payload: loading
    }
}
