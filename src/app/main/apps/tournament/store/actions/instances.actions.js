import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_INSTANCES = '[INSTANCES APP] GET INSTANCES';
export const SET_INSTANCES_SEARCH_TEXT = '[INSTANCES APP] SET INSTANCES SEARCH TEXT';
export const SET_INSTANCES_PAGE_NUMBER_ROW = '[INSTANCES APP] SET INSTANCES PAGE NUMBER ROW';
export const SET_LOADING = '[INSTANCES APP] SET LOADING';
export const CLEAN_SELELCTED_INSTANCE = '[INSTANCES APP] CLEAN SELELCTED INSTANCE';

const lang = i18next.language
const translate = translateInfo(lang)

export function getInstances() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/tournamentInstances/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_INSTANCES,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setPageAndRow(page, row) {
    return {
        type: SET_INSTANCES_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function setInstancesSearchText(event) {
    return {
        type: SET_INSTANCES_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteInstances(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/tournamentInstances/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getInstances())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_INSTANCE,
        payload: clean
    }
}
