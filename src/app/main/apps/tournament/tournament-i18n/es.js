const locale = {
	INSTANCES: 'Instancias',
	INSTANCE: 'Instancia',
	UPDATE: 'Actualizar',
	ADD_NEW_INSTANCE: 'Adicionar nueva instancia',
	ADD_NEW_TOURNAMENT: 'Adicionar nuevo torneo',
	ADD_NEW_ZONE: 'Adicionar nueva zona',	
	NEW_F: 'Nueva',
	NEW_M: 'Nuevo',
	ATTENTION: 'Atención',	
	DELETE_ITEM: 'Desea eliminar',
	SINGLE_INSTANCE: 'la instancia',
	PLURAL_INSTANCE: 'las instancias',
	SINGLE_INSTANCE: 'la instancia',
	PLURAL_TOURNAMENT: 'los torneos',
	SINGLE_TOURNAMENT: 'el torneo',
	PLURAL_ZONE: 'las zonas/grupos',
	SINGLE_ZONE: 'la zona/grupo',
	TOURNAMENTS: 'Torneos',
	TOURNAMENT: 'Torneo',
	TOURNAMENT_TYPE_SELECTION: 'Seleccione el tipo de torneo',
	TYPE_TOURNAMENT: 'Tipo de toneo',
	YEAR: 'Año',
	POINTS_WINNER: 'Puntos para el ganador',
	SPORT: 'Deporte',
	SPORTS: 'Deportes',
	SPORTS_SELECTION: 'Seleccione los deportes',
	DIVISIONS: 'Divisiones',
	DIVISIONS_SELECTION: 'Seleccione las divisiones',
	SUB_DIVISIONS: 'Sub Divisiones',
	ZONE_SELECTION: 'Seleccione las zonas',
	ZONE_TOURNAMENT: 'Zonas del toneo',
	INSTANCES: 'Instancias Finales',
	INSTANCES_SELECTION: 'Seleccione las instancias finales',
	INSTANCE_TOURNAMENT: 'Instancias del toneo',
	SORT_ON_LIST: 'Orden en la lista',
	SORT: 'Orden',	
};

export default locale;
