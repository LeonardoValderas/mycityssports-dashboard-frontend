import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FusePageCarded from '@fuse/core/FusePageCarded';
import CreatableSelect from 'react-select/creatable';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../tournament-i18n/es';
import pt from '../tournament-i18n/pt';
i18next.addResourceBundle('es', 'tournament', es);
i18next.addResourceBundle('pt', 'tournament', pt);

function Tournament(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('tournament');
	const tournament = useSelector(({ tournamentApp }) => tournamentApp.tournament.data);
	const savedModel = useSelector(({ tournamentApp }) => tournamentApp.tournament.savedModel);
	const dataForm = useSelector(({ tournamentApp }) => tournamentApp.tournament.dataForm);
	const loading = useSelector(({ tournamentApp }) => tournamentApp.tournament.loading);
	const theme = useTheme();
	const [tabValue, setTabValue] = useState(0);
	const [disableZones, setDisableZones] = useState(true);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();
	const components = {
		DropdownIndicator: null,
	};
	//const [subDivisions, setSubDivisions] = useState([]);
	const [subDivision, setSubDivision] = useState('');
	useDeepCompareEffect(() => {
		dispatch(Actions.getDataForm());
		function updateDivisionState() {
			const { tournamentId } = routeParams;
			if (tournamentId === 'new') {
				dispatch(Actions.newTournament());
			} else {
				dispatch(Actions.getTournament(tournamentId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(tournament);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((tournament && !form) || (tournament && form && tournament._id !== form._id)) {
			setForm(tournament);
		}
		if (routeParams.tournamentId !== 'new') {
			if (form && form.tournamentType) {
				if (form.tournamentType.code !== '2') {
					disabledZones()
				} else {
					setDisableZones(false)
				}
			}
		}

	}, [form, tournament, setForm]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return form.tournamentType
			&& form.name.length > 0
			&& form.year > 0
			&& form.divisions
			&& form.divisions.length > 0
			&& form.sports
			&& form.sports.length > 0
			&& isZonesSelected()
			&& !_.isEqual(tournament, form);
	}

	function isZonesSelected() {
		if (form.tournamentType && form.tournamentType.code === '2') {
			return form.tournamentZones && form.tournamentZones.length > 0
		}
		return true
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function handleChangeInput(value, actionMeta) {
		setFormSubDivisions(value ? value : [])
	}

	function handleInputChange(value) {
		setSubDivision(value)
	}

	function handleChipChange(value, name) {
		if (_.isEqual('tournamentType', name)) {
			if (value.code !== '2') {
				disabledZones()
			} else {
				setDisableZones(false)
			}
		}

		setForm(
			_.set(
				{ ...form },
				name, value,
			)
		);
	}
	function handleWinPoint(value) {
		const point = value.target.value
		if (point > 0) {
			setForm(
				_.set(
					{ ...form },
					'winPoint', point,
				)
			);
		}
	}

	function disabledZones() {
		form.tournamentZones = []
		setDisableZones(true)
	}

	const handleKeyDown = (event) => {
		if (!subDivision) return;
		switch (event.key) {
		  case 'Enter':
		  case 'Tab':
			if(!form.subDivisions.includes(subDivision)){
				var newDivisions = form.subDivisions
				newDivisions.push(subDivision)
				setSubDivision('')
				setFormSubDivisions(newDivisions)
			}
			event.preventDefault();
		}
	}

	function setFormSubDivisions(subDivisions){
		setForm(
			_.set(
				{ ...form },
				'subDivisions', subDivisions,
			)
		);
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/tournament/tournaments"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">{t('TOURNAMENTS')}</span>
								</Typography>
							</FuseAnimate>
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.saveTournament(form, routeParams.tournamentId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.tournamentId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label={t('TOURNAMENT')} />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<FuseChipSelect
									className="mt-8"
									value={form.tournamentType}
									onChange={value => handleChipChange(value, 'tournamentType')}
									placeholder={t('TOURNAMENT_TYPE_SELECTION')}
									required={true}
									variant='fixed'
									getOptionLabel={option => option.name}
									getOptionValue={option => option._id}
									textFieldProps={{
										label: t('TYPE_TOURNAMENT'),
										InputLabelProps: {
											shrink: true
										},
										variant: 'outlined'
									}}
									options={dataForm && dataForm.types || []}
								/>
								
								<div className="flex flex-col flex-shrink-0 sm:flex-row mt-24">
								<div className="w-full dm:w-4/6" >
									<TextField
										className="w-full"
										error={form.name === ''}
										required
										label={t('TOURNAMENT')}
										//autoFocus
										id="name"
										name="name"
										value={form.name}
										onChange={handleChange}
										variant="outlined"
										cols={8}
										//fullWidth
										inputProps={{
											maxLength: 50,
										}}
									/>
									</div>
									<div className="flex mt-24 md:mt-0">
									<TextField
										className="md:ml-8 w-2/4"
										error={form.year === ''}
										required
										label={t('YEAR')}
										//autoFocus
										type={'number'}
										id="year"
										name="year"
										value={form.year}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 4, //dont work
										}}
									/>

									<TextField
										className="ml-8 w-1/4"
										//error={form.winPoint === ''}
										required
										label={t('POINTS_WINNER')}
										//autoFocus
										id="winPoint"
										name="winPoint"
										type="number"
										value={form.winPoint}
										onChange={handleWinPoint}
										variant="outlined"
										cols={8}
										//fullWidth
										inputProps={{
											maxLength: 2,
										}}
									/>
									<TextField
										className="ml-8 w-1/4"
										label={t('SORT_ON_LIST')}
										id="sort"
										name="sort"
										value={form.sort || ''}
										onChange={handleChange}
										variant="outlined"
										cols={8}
										fullWidth
										inputProps={{
											maxLength: 2,
										}}
										type='number'
									/>
								</div>
								</div>
								<FuseChipSelect
									className="mt-24"
									value={form.sports}
									onChange={value => handleChipChange(value, 'sports')}
									placeholder={t('SPORTS_SELECTION')}
									variant='fixed'
									getOptionLabel={option => `${option.name} (${option.gender.name})`}
									getOptionValue={option => option._id}
									textFieldProps={{
										label: t('SPORTS'),
										InputLabelProps: {
											shrink: true,
										},
										variant: 'outlined'
									}}
									options={dataForm && dataForm.sports || []}
									isMulti
								/>

								<FuseChipSelect
									className="mt-24"
									value={form.divisions}
									onChange={value => handleChipChange(value, 'divisions')}
									placeholder={t('DIVISIONS_SELECTION')}
									variant='fixed'
									getOptionLabel={option => option.name}
									getOptionValue={option => option._id}
									textFieldProps={{
										label: t('DIVISIONS'),
										InputLabelProps: {
											shrink: true,
										},
										variant: 'outlined'
									}}
									options={dataForm && dataForm.divisions || []}
									isMulti
								/>

								<CreatableSelect
									className="mt-24"
									components={components}
									inputValue={subDivision}
									isClearable
									isMulti
									menuIsOpen={false}
									onChange={(value, actionMeta) => handleChangeInput(value, actionMeta)}
									onInputChange={value => handleInputChange(value)}
									onKeyDown={event => handleKeyDown(event)}
									placeholder={t('SUB_DIVISIONS')}
									value={form && form.subDivisions || []}
									getOptionLabel={option => option}
									getOptionValue={option => option}
								/>

								<FuseChipSelect
									className="mt-24"
									value={form.tournamentZones}
									onChange={value => handleChipChange(value, 'tournamentZones')}
									placeholder={t('ZONE_SELECTION')}
									required={false}
									isDisabled={disableZones}
									variant='fixed'
									getOptionLabel={option => option.name}
									getOptionValue={option => option._id}
									textFieldProps={{
										label: t('ZONE_TOURNAMENT'),
										InputLabelProps: {
											shrink: true,
										},
										variant: 'outlined'
									}}
									options={dataForm && dataForm.zones || []}
									isMulti
								/>

								<FuseChipSelect
									className="mt-24"
									value={form.tournamentInstances}
									onChange={value => handleChipChange(value, 'tournamentInstances')}
									placeholder={t('INSTANCES_SELECTION')}
									required={false}
									variant='fixed'
									getOptionLabel={option => option.name}
									getOptionValue={option => option._id}
									textFieldProps={{
										label: t('INSTANCE_TOURNAMENT'),
										InputLabelProps: {
											shrink: true,
										},
										variant: 'outlined'
									}}
									options={dataForm && dataForm.instances || []}
									isMulti
								/>
								
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('tournamentApp', reducer)(Tournament);
