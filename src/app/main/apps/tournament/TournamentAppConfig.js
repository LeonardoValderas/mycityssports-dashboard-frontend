import React from 'react';
import { Redirect } from 'react-router-dom';

const TournamentAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/tournament/tournaments/:tournamentId',
			component: React.lazy(() => import('./tournament/Tournament'))
		},
		{
			path: '/apps/tournament/tournaments',
			component: React.lazy(() => import('./tournaments/Tournaments'))
		},
		{
			path: '/apps/tournament/zones/:zoneId',
			component: React.lazy(() => import('./zone/Zone'))
		},
		{
			path: '/apps/tournament/zones',
			component: React.lazy(() => import('./zones/Zones'))
		},
		// {
		// 	path: '/apps/tournament/instances/:instanceId',
		// 	component: React.lazy(() => import('./instance/Instance'))
		// },
		// {
		// 	path: '/apps/tournament/instances',
		// 	component: React.lazy(() => import('./instances/Instances'))
		// },
		{
			path: '/apps/tournament',
			component: () => <Redirect to="/apps/tournament/tournaments" />
		}
	]
};

export default TournamentAppConfig;
