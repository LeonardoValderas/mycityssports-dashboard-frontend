import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import ZonesHeader from './ZonesHeader';
import ZonesTable from './ZonesTable';

function Zones() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<ZonesHeader />}
			content={<ZonesTable />}
			innerScroll
		/>
	);
}

export default withReducer('tournamentApp', reducer)(Zones);
