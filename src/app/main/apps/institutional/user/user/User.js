import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FusePageCarded from '@fuse/core/FusePageCarded';
import IconButton from '@material-ui/core/IconButton';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import _ from '@lodash';
import clsx from 'clsx'
import constants from '../../../utils/Constants'
import Button from '@material-ui/core/Button';
import { orange } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../../store/actions';
import reducer from '../../store/reducers';
import jimp from 'jimp';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../../institutional-i18n/es';
import pt from '../../institutional-i18n/pt';
i18next.addResourceBundle('es', 'institutional', es);
i18next.addResourceBundle('pt', 'institutional', pt);


const useStyles = makeStyles(theme => ({
	productImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		color: orange[400],
		opacity: 0
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	productImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $productImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $productImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $productImageFeaturedStar': {
				opacity: 1
			}
		}
	}
}));

function User(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('institutional');
	const user = useSelector(({ userApp }) => userApp.user.data);
	const savedModel = useSelector(({ userApp }) => userApp.user.savedModel);
	const loading = useSelector(({ userApp }) => userApp.user.loading);
	const theme = useTheme();
	const classes = useStyles(props);
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const [passwordDisable, setPasswordDisable] = useState(true);
	const [passwordReset, setPasswordReset] = useState(false);
	const [isNew, setIsNew] = useState(true);
	const routeParams = useParams();
	const [imageError, setImageError] = useState("");
	
	useDeepCompareEffect(() => {
		function updateDivisionState() {
			const { userId } = routeParams;
			setIsNew(userId === 'new')
			if (userId === 'new') {
				setPasswordDisable(false)
				dispatch(Actions.newUser());
			} else {
				setPasswordDisable(true)
				dispatch(Actions.getUser(userId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(user);
			resetPasswordEnable(false)
		}
	}, [savedModel]);

	useEffect(() => {
		if ((user && !form) || (user && form && user._id !== form._id)) {
			// if (!isNew && user.city && user.city._id) {
			// 	dispatch(Actions.getInstitutionsByCity(user.city._id));
			// }
			setForm(user);
		}
	}, [form, user, setForm]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function resetPasswordEnable(isReset) {
		setPasswordReset(isReset)
		setPasswordDisable(!isReset)
		if (!isReset) {
			form.password = ''
			form.confirmPassword = ''
		}
    }
    
	function canBeSubmitted() {
		return form
		//	&& form.city
		//	&& form.institution
			&& form.name !== ''
			&& form.email !== ''
			&& passwordValidation()
			&& !_.isEqual(user, form);
	}

     function passwordValidation(){
        const validation = (
			((isNew || passwordReset) && (form.password !== '' && form.confirmPassword !== '')) 
			|| 
			(!isNew && (form.password === '' && form.confirmPassword === ''))
		)
			return validation
	 }

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function handleUploadChange(e) {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		form.nameImage = file.name
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = async () => {
			let r = `data:${file.type};base64,${btoa(reader.result)}`
			const image = await jimp.read(r);
			await image.resize(128, 128);
			let base = await image.getBase64Async(image.getMIME())
			let d = await image.bitmap.data
			let f = new File(d, file.name)
			let newSize = f.size / 1024 / 1024
			if(newSize > 1.0){
			   setImageError(t('IMAGE_BIG')) 
			   return
			} else {
				setImageError("") 
				setForm(
					_.set(
						{ ...form },
						`photo`, base
					)
				);
			}
		};

		reader.onerror = () => {
			console.log('error on load image');
		};
	}

	function summitForm() {
		if (passwordReset) {
			dispatch(Actions.resetPasswordUser(form))
		} else {
			dispatch(Actions.saveUser(form, isNew ? 'post' : 'put'))
		}
	}

	function handleChipChange(value, name) {
		// if (_.isEqual(name, 'city') && (!form.city || form.city._id !== value._id)) {
		// 	form.institution = null
		// 	dispatch(Actions.getInstitutionsByCity(value._id));
		// }
		setForm(
			_.set(
				{ ...form },
				name, value,
			)
		);
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/institutional/user/users"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">Usuarios</span>
								</Typography>
							</FuseAnimate>
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => summitForm()}
							>
								{isNew ? 'Salvar' : (!passwordReset) ? t('UPDATE') : t('RESET_PASS')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label="Usuario" />
					<Tab className="h-64 normal-case" label="Foto" />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<TextField
									className="mt-16"
									error={form.name === ''}
									required
									label={t('NAME')}
									autoFocus
									id="name"
									name="name"
									value={form.name}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 200,
									}}
								/>

								<TextField
									className="mt-8"
									error={form.email === ''}
									required
									label="Email"
									//autoFocus
									id="email"
									name="email"
									disabled={routeParams.userId !== 'new'}
									value={form.email}
									onChange={handleChange}
									variant="outlined"
									fullWidth
									inputProps={{
										maxLength: 200,
									}}
								/>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-8 md:w-3/6 w-full"
										error={form.password === '' && isNew}
										required
										label={t('PASS')}
										//autoFocus
										id="password"
										name="password"
										value={form.password}
										onChange={handleChange}
										variant="outlined"
										disabled={passwordDisable}
										//fullWidth
										inputProps={{
											maxLength: 20,
										}}
									/>

									<TextField
										className="mt-8 md:ml-8 md:w-3/6 w-full"
										error={form.confirmPassword === '' && isNew}
										required
										label={t('CONFIRM_PASS')}
										//autoFocus
										id="confirmPassword"
										name="confirmPassword"
										value={form.confirmPassword}
										onChange={handleChange}
										disabled={passwordDisable}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 20,
										}}
									/>
									<IconButton disabled={isNew}
										onClick={() =>
											resetPasswordEnable(true)
										}
									>
										<Icon>autorenew</Icon>
									</IconButton>
									<IconButton disabled={isNew}
										onClick={() =>
											resetPasswordEnable(false)
										}
									>
										<Icon>cancel</Icon>
									</IconButton>
								</div>
								<div className="flex">
									<FormControlLabel
										control={
											<Checkbox
												id='isActive'
												name='isActive'
												tabIndex={-1}
												checked={form.isActive}
												onChange={handleChange}

											/>
										}
										label={t('USER_ACTIVE')}
									/>
								</div>
							</div>
						)}	{tabValue === 1 && (
							<div>
								<div className="flex justify-center sm:justify-start flex-wrap -mx-8">
									<label
										htmlFor="button-file"
										className={clsx(
											classes.productImageUpload,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5'
										)}
									>
										<input
											accept="image/*"
											className="hidden"
											id="button-file"
											type="file"
											onChange={handleUploadChange}
										/>
										<Icon fontSize="large" color="action">
											cloud_upload
										</Icon>
									</label>
									<div
										role="button"
										tabIndex={0}
										className={clsx(
											classes.productImageItem,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5',
										)}
										key={form._id}
									>
										<img className="max-w-128 w-128 h-128" src={form.photo || form.urlImage || constants.DEFAULT_URL_IMAGE_128_128} alt="Escudo" />
									</div>
								</div>
								<div className = "flex flex-col">
									<span className="mx-4">{t('IMAGE_RECOMMENTED_2')}</span>
									<span className="mx-4" style={{color:'red', fontStyle: 'bold'}}>{imageError}</span>
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('userApp', reducer)(User);
