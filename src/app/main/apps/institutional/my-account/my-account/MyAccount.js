import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import Map from '../../../utils/GoogleMaps';
import _ from '@lodash';
import clsx from 'clsx';
import constants from '../../../utils/Constants'
import Button from '@material-ui/core/Button';
import { orange } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {  useParams } from 'react-router-dom';
import * as Actions from '../../store/actions';
import reducer from '../../store/reducers';
import jimp from 'jimp';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../../institutional-i18n/es';
import pt from '../../institutional-i18n/pt';
i18next.addResourceBundle('es', 'institutional', es);
i18next.addResourceBundle('pt', 'institutional', pt);

const useStyles = makeStyles(theme => ({
	productImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		color: orange[400],
		opacity: 0
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	productImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $productImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $productImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $productImageFeaturedStar': {
				opacity: 1
			}
		}
	}
}));

function MyAccount(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('institutional');
	const institution = useSelector(({ myAccountApp }) => myAccountApp.myaccount.data);
	const savedModel = useSelector(({ myAccountApp }) => myAccountApp.myaccount.savedModel);
  	const loading = useSelector(({ myAccountApp }) => myAccountApp.myaccount.loading);
	const classes = useStyles(props);
	const [tabValue, setTabValue] = useState(0);
	const [position, setPosition] = useState(null);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();
	const [imageError, setImageError] = useState("");
	
	useDeepCompareEffect(() => {
		function updateDivisionState() {
			const { institutionId } = routeParams;
				dispatch(Actions.getInstitution(institutionId));
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(institution);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((institution && !form) || (institution && form && institution._id !== form._id)) {
			setForm(institution);
		}
	}, [form, institution, setForm]);

	useEffect(() => {
		if (form && form.city && form.city.position) {
			setPosition(form.city.position);
		}
	}, [form, setPosition]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return form 
		&& form.name.length > 0 
			// && form.description.length > 0
			// && form.address.street.length > 0
			// && form.address.number.length > 0
			// && form.address.neighborhood.length > 0
			// && form.address.position.longitude.length > 0
			// && form.address.position.latitude.length > 0
		&& !_.isEqual(institution, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function handleUploadChange(e) {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		form.nameImage = file.name
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = async () => {
			let r = `data:${file.type};base64,${btoa(reader.result)}`
			const image = await jimp.read(r);
			await image.resize(280, 280);
			let base = await image.getBase64Async(image.getMIME())
			let d = await image.bitmap.data
			let f = new File(d, file.name)
			let newSize = f.size / 1024 / 1024
			if(newSize > 1.0){
			   setImageError(t('IMAGE_BIG')) 
			   return
			} else {
				setImageError("") 
				setForm(
					_.set(
						{ ...form },
						`photo`, base
					)
				);
			}
		};

		reader.onerror = () => {
			console.log('error on load image');
		};
	}

	function mapClicked(mapProps, map, e) {
		setForm(
			_.set({ ...form },
				`address.position`, { latitude: e.latLng.lat(), longitude: e.latLng.lng() }
			)
		);
	}

	// function handleChipChange(value, name) {
	// 	setPosition(value.position)
	// 	setForm(
	// 		_.set(
	// 			{ ...form },
	// 			name, value,
	// 		)
	// 	);
	// }

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.saveInstitution(form, routeParams.institutionId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.institutionId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label={t('INSTITUTION')} />
					<Tab className="h-64 normal-case" label="Logo" />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<TextField
									className="mt-8 mb-16"
									error={form.name === ''}
									required
									label={t('NAME')}
									autoFocus
									id="name"
									name="name"
									value={form.name}
									onChange={handleChange}
									variant="outlined"
									cols={8}
                                    fullWidth
                                    disabled = {true}
									inputProps={{
										maxLength: 200,
									}}
								/>
								<TextField
									className="mt-8 mb-16"
									//error={form.description === ''}
									//required
									label={t('DESCRIPTION')}
									//autoFocus
									id="description"
									name="description"
									value={form.description}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 400,
									}}
								/>
								<div className="h-360 mt-8 mb-16 mr-24" >
									<Map
										initPosition={position}
										latitude={parseFloat(form.address.position.latitude)}
										longitude={parseFloat(form.address.position.longitude)}
										mapClicked={mapClicked}
									/>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-16 md:w-3/6 w-full"
										//error={form.address.street === ''}
										//required
										label={t('STREET')}
										//autoFocus
										id="street"
										name="address.street"
										value={form.address.street}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 200,
										}}
									/>

									<TextField
										className="mt-16 md:ml-8 md:w-1/6 w-full"
										//error={form.address.number === ''}
										//required
										label="Número"
										//autoFocus
										id="number"
										name="address.number"
										value={form.address.number}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 10,
										}}
									/>

									<TextField
										className="mt-16 md:ml-8 md:w-2/6 w-full"
										//error={form.address.neighborhood === ''}
										//required
										label={t('NEIGHBORHOOD')}
										//autoFocus
										id="neighborhood"
										name="address.neighborhood"
										value={form.address.neighborhood}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 200,
										}}
									/>
								</div>

								<TextField
									className="w-full mt-16"
									//error={form.address.complement  === ''}
									//required
									label="Complemento"
									//autoFocus
									id="complement"
									name="address.complement"
									value={form.address.complement}
									onChange={handleChange}
									variant="outlined"
									//fullWidth
									inputProps={{
										maxLength: 200,
									}}
								/>

								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-16 md:w-2/6 w-full"
										//error={form.phone === ''}
										//required
										label={t('PHONE')}
										//autoFocus
										id="phone"
										name="phone"
										value={form.phone}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 20,
										}}
									/>
									<TextField
										className="mt-16 md:ml-8 md:w-2/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Whatsapp"
										//autoFocus
										id="mobile"
										name="mobile"
										value={form.mobile}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 20,
										}}
									/>

									<TextField
										className="mt-16 md:ml-8 md:w-2/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Email"
										//autoFocus
										id="email"
										name="email"
										value={form.email}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 100,
										}}
									/>
								</div>

								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-16 md:w-3/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Web"
										//autoFocus
										id="web"
										name="web"
										value={form.web}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 300,
										}}
									/>
									<TextField
										className="mt-16 md:ml-8 md:w-3/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Facebook"
										//autoFocus
										id="facebook"
										name="facebook"
										value={form.facebook}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 300,
										}}
									/>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-16 md:w-3/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Instagram"
										//autoFocus
										id="instagram"
										name="instagram"
										value={form.instagram}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 300,
										}}
									/>
									<TextField
										className="mt-16 md:ml-8 md:w-3/6 w-full"
										label={t('SOURCE')}
										id="source"
										name="source"
										value={form.source}
										onChange={handleChange}
										variant="outlined"
										cols={8}
										fullWidth
										inputProps={{
											maxLength: 40,
										}}
								/>
								</div>
								{/* <div className="flex">
									<FormControlLabel
										control={
											<Checkbox
												id='isActive'
												name='isActive'
												tabIndex={-1}
												checked={form.isActive}
												onChange={handleChange}

											/>
										}
										label='Institution Activo'
									/>
								</div> */}
							</div>
						)}	{tabValue === 1 && (
							<div>
								<div className="flex justify-center sm:justify-start flex-wrap -mx-8">
									<label
										htmlFor="button-file"
										className={clsx(
											classes.productImageUpload,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5'
										)}
									>
										<input
											accept="image/*"
											className="hidden"
											id="button-file"
											type="file"
											onChange={handleUploadChange}
										/>
										<Icon fontSize="large" color="action">
											cloud_upload
										</Icon>
									</label>
									<div
										role="button"
										tabIndex={0}
										className={clsx(
											classes.productImageItem,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5',
										)}
										key={form._id}
									>
										<img className="max-w-128 w-128 h-128" src={form.photo || form.urlImage || constants.DEFAULT_URL_IMAGE} alt="Escudo" />
									</div>
								</div>
								<div className = "flex flex-col">
									<span className="mx-4">{t('IMAGE_RECOMMENTED')}</span>
									<span className="mx-4" style={{color:'red', fontStyle: 'bold'}}>{imageError}</span>
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('myAccountApp', reducer)(MyAccount);
