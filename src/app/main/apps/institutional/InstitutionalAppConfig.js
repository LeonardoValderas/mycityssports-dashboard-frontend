import React from 'react';
import { authRoles } from 'app/auth';

import { Redirect } from 'react-router-dom';

const InstitutionalAppConfig = {
	settings: {
		layout: {}
	},
	auth: authRoles.admin,
	routes: [
		{
			path: '/apps/institutional/my-account',
			component: React.lazy(() => import('./my-account/my-account/MyAccount'))
		},
		{
			path: '/apps/institutional/my-sport',
			component: React.lazy(() => import('./my-sport/my-sports/MySports'))
		},
		{
			path: '/apps/institutional/sponsor/sponsors/:sponsorId',
			component: React.lazy(() => import('./sponsor/sponsor/Sponsor'))
		},
		{
			path: '/apps/institutional/sponsor/sponsors',
			component: React.lazy(() => import('./sponsor/sponsors/Sponsors'))
		},
		{
			path: '/apps/institutional/user/users/:userId',
			component: React.lazy(() => import('./user/user/User'))
		},
		{
			path: '/apps/institutional/user/users',
			component: React.lazy(() => import('./user/users/Users'))
		},
		{
			path: '/apps/institutional',
			component: () => <Redirect to="/apps/institutional/my-account/my-account" />
		}
	]
};

export default InstitutionalAppConfig;
