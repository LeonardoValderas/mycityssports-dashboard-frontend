import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../../store/actions';
import MySportsTableHead from './MySportsTableHead';
import FuseLoading from '@fuse/core/FuseLoading';

function MySportsTable(props) {
	const dispatch = useDispatch();
	const sports = useSelector(({ mySportsApp }) => mySportsApp.mysports.data);
	const mySports = useSelector(({ mySportsApp }) => mySportsApp.mysports.selectedItems);
	const searchText = useSelector(({ mySportsApp }) => mySportsApp.mysports.searchText);
	const canBeSummitted = useSelector(({ mySportsApp }) => mySportsApp.mysports.canBeSummitted);
	const save = useSelector(({ mySportsApp }) => mySportsApp.mysports.clickSave);
	const loading = useSelector(({ mySportsApp }) => mySportsApp.mysports.loading);
	const cleanSelected = useSelector(({ mySportsApp }) => mySportsApp.mysports.cleanSelected);
	const [clean, setClean] = useState(cleanSelected);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(sports);
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});

	useEffect(() => {
		dispatch(Actions.getMySports());
	}, [dispatch]);

	useEffect(() => {
		setClean(cleanSelected)
	}, [cleanSelected]);

	useEffect(() => {
		if(save){
			dispatch(Actions.saveMySports(selected));
		}
	}, [save]);

	useEffect(() => {
		if(mySports){
			setSelected(mySports)
		}
		
	}, [mySports]);

	useEffect(() => {
		if (searchText.length !== 0) {
			const filter = _.filter(sports, item => item.name.toLowerCase().includes(searchText.toLowerCase()))
			setData(filter);
			setPage(0);
		} else {
			setData(sports);
		}
	}, [sports, searchText]);

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function canSummitted(can) {
		if (!_.isEqual(canBeSummitted, can)) {
			dispatch(Actions.canBeSummitted(can));
		}
	}
	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setSelected(data.map(n => n._id));
			canSummitted(true)
			return;
		}
		setSelected([]);
		canSummitted(false)
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function handleClick(item) {
		//props.history.push(`/apps/team/teams/${item._id}`);
	}

	function handleCheck(event, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
		canSummitted(true)
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table className="min-w-xl" aria-labelledby="tableTitle">
					<MySportsTableHead
						numSelected={selected ? selected.length : 0}
						order={order}
						cleanSelected={clean}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={data ? data.length : 0}
						//deleteSelectedItems={deleteSelectedItems}
					/>

					<TableBody>
						{_.orderBy(
							data,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											const orderId = order.id
											if (orderId) {
												let filter = o
												const split = orderId.split('.')
												split.map(s => {
													filter = filter[s]
												})
												return filter;
											}
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n._id) !== -1;
								return (
									<TableRow
										className="h-64 cursor-pointer"
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n._id}
										selected={isSelected}
										onClick={event => handleClick(n)}
									>
										<TableCell className="w-64 text-center" padding="none">
											<Checkbox
												checked={isSelected}
												onClick={event => event.stopPropagation()}
												onChange={event => handleCheck(event, n._id)}
											/>
										</TableCell>

										{/* <TableCell className="w-52" component="th" scope="row" padding="none">
											{n.images.length > 0 && n.featuredImageId ? (
												<img
													className="w-full block rounded"
													src={_.find(n.images, { id: n.featuredImageId }).url}
													alt={n.name}
												/>
											) : (
												<img
													className="w-full block rounded"
													src="assets/images/ecommerce/product-image-placeholder.png"
													alt={n.name}
												/>
											)}
										</TableCell> */}

										<TableCell component="th" scope="row">
											{n.name} {n.gender.name}
										</TableCell>

										{/* <TableCell component="th" scope="row">
											{n.member}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.memberId}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.birthday}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.team.label}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.division.label}
										</TableCell> */}

										{/* 			<TableCell className="truncate" component="th" scope="row">
											{
												n.tournamentZones.map(z =>
													z.label
												).join(' - ')
											}
										</TableCell>

										<TableCell className="truncate" component="th" scope="row">
											{
												n.tournamentInstances.map(i =>
													i.label
												).join(' - ')
											}
										</TableCell> */}
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="overflow-hidden"
				component="div"
				count={data ? data.length : 0}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
}

export default withRouter(MySportsTable);
