import FuseAnimate from '@fuse/core/FuseAnimate';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Input from '@material-ui/core/Input';
import Paper from '@material-ui/core/Paper';
import { ThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../../institutional-i18n/es';
import pt from '../../institutional-i18n/pt';
i18next.addResourceBundle('es', 'institutional', es);
i18next.addResourceBundle('pt', 'institutional', pt);

function MySportsHeader(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('institutional');
	const searchText = useSelector(({ mySportsApp }) => mySportsApp.mysports.searchText);
	const canBeSummitted = useSelector(({ mySportsApp }) =>  mySportsApp.mysports.canBeSummitted);
	const mainTheme = useSelector(({ fuse }) => fuse.settings.mainTheme);

	return (
		<div className="flex flex-1 w-full items-center justify-between">
			<div className="flex items-center">
				<FuseAnimate animation="transition.expandIn" delay={300}>
					<Icon className="text-32">rowing</Icon>
				</FuseAnimate>
				<FuseAnimate animation="transition.slideLeftIn" delay={300}>
					<Typography className="hidden sm:flex mx-0 sm:mx-12" variant="h6">
						{t('MY_SPORTS')}
					</Typography>
				</FuseAnimate>
			</div>

			<div className="flex flex-1 items-center justify-center px-12">
				<ThemeProvider theme={mainTheme}>
					<FuseAnimate animation="transition.slideDownIn" delay={300}>
						<Paper className="flex items-center w-full max-w-512 px-8 py-4 rounded-8" elevation={1}>
							<Icon color="action">search</Icon>

							<Input
								placeholder="Buscar"
								className="flex flex-1 mx-8"
								disableUnderline
								fullWidth
								value={searchText}
								inputProps={{
									'aria-label': 'Search'
								}}
								onChange={ev => dispatch(Actions.setMySportsSearchText(ev))}
							/>
						</Paper>
					</FuseAnimate>
				</ThemeProvider>
			</div>
		 	<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSummitted}
							    onClick={() => dispatch(Actions.setSave(true))}
							>
								Salvar
							</Button>
						</FuseAnimate>
			{/* <FuseAnimate animation="transition.slideRightIn" delay={300}>
				<Button
					component={Link}
					to="/apps/team/mySports/new"
					className="whitespace-no-wrap normal-case"
					variant="contained"
					color="secondary"
				>
					<span className="hidden sm:flex">Adicionar nuevo equipo</span>
					<span className="flex sm:hidden">Nuevo</span>
				</Button>
			</FuseAnimate> */}
		</div>
	);
}

export default MySportsHeader;
