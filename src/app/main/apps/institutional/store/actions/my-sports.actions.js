import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import i18next from 'i18next';
import { translateInfo } from '../../../utils/TranslateUtils'
export const GET_MY_SPORTS = '[MY_SPORTS APP] GET MY_SPORTS';
export const SET_MY_SPORTS_SEARCH_TEXT = '[MY_SPORTS APP] SET MY_SPORTS SEARCH TEXT';
export const SET_LOADING = '[MY_SPORTS APP] SET LOADING';
export const CLEAN_SELELCTED_MY_SPORTS = '[MY_SPORTS APP] CLEAN SELELCTED MY_SPORTS';
export const CAN_SUMMITTED_MY_SPORTS = '[MY_SPORTS APP] CAN SUMMITTED MY_SPORTS';
export const CLICK_SAVE_MY_SPORTS = '[MY_SPORTS APP] CLICK SAVE MY_SPORTS';

const lang = i18next.language
const translate = translateInfo(lang)

export function getMySports() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/mySports/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
               // Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_MY_SPORTS,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setMySportsSearchText(event) {
    return {
        type: SET_MY_SPORTS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function saveClick(save) {
    return dispatch => {
          dispatch(setSave(save))
    }
}

export function setSave(save) {
    return {
        type: CLICK_SAVE_MY_SPORTS,
        payload: save
    }
}

export function canBeSummitted(can) {
    return dispatch => {
          if(!can){
            dispatch(setSave(false))
          } 
          dispatch(setCanBeSummitted(can))
    }
}

export function setCanBeSummitted(can) {
    return {
        type: CAN_SUMMITTED_MY_SPORTS,
        payload: can
    }
}

export function saveMySports(data) {
	return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing}))
        const institutionId = getInstitutionId()
        const user = getUserId()
        const model = {
            sports: data,
            institution: institutionId,
            user: user
        }
		axios['post'](`${translate.baseUrl}/mySports`, model)
			.then(resp => {
                dispatch(showMessage({ message: translate.success, variant:'success' }));
                dispatch(setLoading({ show: false, label: '' }))
                dispatch(setCanBeSummitted(false))
                dispatch(setSave(false))
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}
