import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId } from '../../../utils/AuthUtils'
import i18next from 'i18next';
import { translateInfo } from '../../../utils/TranslateUtils'
export const GET_SPONSORS = '[SPONSORS APP] GET SPONSORS';
export const SET_SPONSORS_SEARCH_TEXT = '[SPONSORS APP] SET SPONSORS SEARCH TEXT';
export const SET_SPONSORS_PAGE_NUMBER_ROW = '[SPONSORS APP] SET SPONSORS PAGE NUMBER ROW';
export const SET_LOADING = '[SPONSORS APP] SET LOADING';
export const CLEAN_SELELCTED_SPONSOR = '[SPONSORS APP] CLEAN SELELCTED SPONSOR';

const lang = i18next.language
const translate = translateInfo(lang)

export function getSponsors() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/sponsors/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_SPONSORS,
        payload: models
    }
}

export function setPageAndRow(page, row) {
    return {
        type: SET_SPONSORS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setSponsorsSearchText(event) {
    return {
        type: SET_SPONSORS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteSponsors(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing}))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/sponsors/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getSponsors())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_SPONSOR,
        payload: clean
    }
}
