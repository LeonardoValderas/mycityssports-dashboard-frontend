import axios from 'axios';
import constants from '../../../utils/Constants'
import { getInstitutionId } from '../../../utils/AuthUtils'
import { showMessage } from 'app/store/actions/fuse';
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import i18next from 'i18next';
import { translateInfo } from '../../../utils/TranslateUtils'
export const GET_USERS = '[USERS APP] GET USERS';
export const SET_USERS_SEARCH_TEXT = '[USERS APP] SET USERS SEARCH TEXT';
export const SET_USERS_PAGE_NUMBER_ROW = '[USERS APP] SET USERS PAGE NUMBER ROW';
export const SET_LOADING = '[USERS APP] SET LOADING';
export const CLEAN_SELELCTED_USER = '[USERS APP] CLEAN SELELCTED USER';

const lang = i18next.language
const translate = translateInfo(lang)

export function getUsers() {
    return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/users/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_USERS,
        payload: models
    }
}

export function setPageAndRowUser(page, row) {
    return {
        type: SET_USERS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setUsersSearchText(event) {
    return {
        type: SET_USERS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteUsers(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing }))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/users/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getUsers())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_USER,
        payload: clean
    }
}
