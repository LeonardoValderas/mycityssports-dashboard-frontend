
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import {getUserId, getInstitutionId, getCityId, getInstitutionName } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import i18next from 'i18next';
import { translateInfo } from '../../../utils/TranslateUtils'
export const GET_USER = '[USER APP] GET USER';
export const GET_USER_INSTITUTIONS = '[USER APP] GET USER INSTITUTIONS';
export const GET_USER_CITIES = '[USER APP] GET USER CITIES';
export const GET_USER_TEAMS = '[USER APP] GET USER TEAMS';
export const GET_USER_SPORTS = '[USER APP] GET USER SPORTS';
export const SAVE_USER = '[USER APP] SAVE USER';
export const SET_LOADING_USER = '[USER APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getUser(userId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/users/${userId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			model['password'] = ''
				model['confirmPassword'] = ''
			dispatch(setModel(model))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function setModel(model) {
	return {
		type: GET_USER,
		payload: model
	}
}

export function saveUser(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		let id = ''
		if (method === 'post') {
			delete data['_id']
		} else {
			id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/users/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				model['password'] = ''
				model['confirmPassword'] = ''
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant: 'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function resetPasswordUser(data) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		const id = data._id ? data._id : ''
		axios['put'](`${translate.baseUrl}/users/reset/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				model['password'] = ''
				model['confirmPassword'] = ''
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant: 'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newUser() {
	const data = {
		name: '',
		email: '',
		isOwner: false,
		password: '',
		confirmPassword: '',
		institution: getInstitutionId(),
		role: 'user',
		photo: null,
		nameImage: null,
		nameId: null,
		urlImage: null,
		folder: `users/${getInstitutionName()}`,
		isActive: true,
		city: getCityId(),
		user: getUserId(),
	};

	return {
		type: GET_USER,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_USER,
		payload: model
	};
}

export function setLoading(loading) {
	return {
		type: SET_LOADING_USER,
		payload: loading
	}
}
