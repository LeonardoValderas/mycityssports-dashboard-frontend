
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { getInstitutionId, getUserId, getInstitutionName } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import i18next from 'i18next';
import { translateInfo } from '../../../utils/TranslateUtils'
export const GET_INSTITUTION = '[INSTITUTION APP] GET INSTITUTION';
export const GET_INSTITUTION_CITIES = '[INSTITUTION APP] GET INSTITUTION CITIES';
export const SAVE_INSTITUTION = '[INSTITUTION APP] SAVE INSTITUTION';
export const SET_LOADING_INSTITUTION = '[INSTITUTION APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getInstitution(institutionId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		const institutionId = getInstitutionId()
		axios.get(`${translate.baseUrl}/institutions/${institutionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function setModel(model) {
	return {
		type: GET_INSTITUTION,
		payload: model
	}
}

export function saveInstitution(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		const id = data._id ? data._id : ''
		axios['put'](`${translate.baseUrl}/institutions/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant: 'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newInstitution() {
	const data = {
		name: '',
		description: '',
		address: {
			street: '',
			number: '',
			neighborhood: '',
			position:{
				latitude: '',
				longitude: ''
			},
			complement:''
		},
		phone:'',
        mobile:'',
		email:'',
		web:'',
		facebook:'',
		instagram:'',
		photo: null,
		nameImage: null,
		nameId: null,
		urlImage: null,
		source:'',
		folder: `institutions/${getInstitutionName()}`,
		user: getUserId(),
	};

	return {
		type: GET_INSTITUTION,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_INSTITUTION,
		payload: model
	};
}

export function setLoading(loading) {
	return {
		type: SET_LOADING_INSTITUTION,
		payload: loading
	}
}
