
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId, getUserId, getInstitutionName, getCityId } from '../../../utils/AuthUtils'
import i18next from 'i18next';
import { translateInfo } from '../../../utils/TranslateUtils'
export const GET_SPONSOR = '[SPONSOR APP] GET SPONSOR';
export const GET_SPONSOR_FORM = '[SPONSOR APP] GET SPONSOR FORM';
export const SAVE_SPONSOR = '[SPONSOR APP] SAVE SPONSOR';
export const SET_LOADING_SPONSOR = '[SPONSOR APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getSponsor(sponsorId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/sponsors/${sponsorId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function setModel(model) {
	return {
		type: GET_SPONSOR,
		payload: model
	}
}

export function saveSponsor(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing }))
		let id = ''
		if(method === 'post'){
			delete data['_id']
		} else{
			 id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/sponsors/${id}`, data)
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant:'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newSponsor() {
	const data = {
		name: '',
		address:{
			street:'',
			number:'',
			neighborhood:'',
			complement:'',
			position:{
				latitude:'',
				longitude:''
			}
		},
		phone:'',
        mobile:'',
		email:'',
		web:'',
		facebook:'',
		instagram:'',
		supportMessage:'',
		photo: null,
		nameImage: null,
		nameId: null,
		urlImage: null,
		city: getCityId(),
		folder: `sponsors/${getInstitutionName()}`,
		isActive: true,
		user: getUserId(),
		institution: getInstitutionId()
	};
  
	return {
		type: GET_SPONSOR,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_SPONSOR,
		payload: model
	};
}

export function setLoading(loading) {
    return {
        type: SET_LOADING_SPONSOR,
        payload: loading
    }
}
