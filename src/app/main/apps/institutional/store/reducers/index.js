import { combineReducers } from 'redux';
import myaccount from './my-account.reducer';
import mysports from './my-sports.reducer';
import sponsors from './sponsors.reducer';
import sponsor from './sponsor.reducer';
import users from './users.reducer';
import user from './user.reducer';

const reducer = combineReducers({
	myaccount,
	mysports,
	sponsor,
	sponsors,
	user,
	users
});

export default reducer;
