import * as Actions from '../actions';

const initialState = {
	data: [],
	selectedItems: [],
	searchText: '',
	loading: {
	  show: false,
	  label: ''
	},
	canBeSummitted: false,
	clickSave: false
};

const mySportsReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_MY_SPORTS: {
			return {
				...state,
				data: action.payload.sports, 
				selectedItems: action.payload.mySports ? action.payload.mySports.sports : [],
				loading: {
					show: false,
					label: ''
				  }
			};
		}
		case Actions.SET_LOADING: {
			return {
				...state,
				loading: action.payload
			};
		}
		case Actions.SET_MY_SPORTS_SEARCH_TEXT: {
			return {
				...state,
				searchText: action.searchText
			};
		}
		case Actions.CAN_SUMMITTED_MY_SPORTS: {
			return {
				...state,
				canBeSummitted: action.payload
			};
		}
		case Actions.CLICK_SAVE_MY_SPORTS: {
			return {
				...state,
				clickSave: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default mySportsReducer;
