import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../../store/actions';
import SponsorsTableHead from './SponsorsTableHead';
import FuseLoading from '@fuse/core/FuseLoading';
import clsx from 'clsx';
import constants from '../../../utils/Constants'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../../institutional-i18n/es';
import pt from '../../institutional-i18n/pt';
i18next.addResourceBundle('es', 'institutional', es);
i18next.addResourceBundle('pt', 'institutional', pt);

function SponsorsTable(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('institutional');
	const sponsors = useSelector(({ sponsorApp }) => sponsorApp.sponsors.data);
	const searchText = useSelector(({ sponsorApp }) => sponsorApp.sponsors.searchText);
	const pageRow = useSelector(({ sponsorApp }) => sponsorApp.sponsors.pageRow);
	const loading = useSelector(({ sponsorApp }) => sponsorApp.sponsors.loading);
	const cleanSelected = useSelector(({ sponsorApp }) => sponsorApp.sponsors.cleanSelected);
	const [clean, setClean] = useState(cleanSelected);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(sponsors);
	const [page, setPage] = useState(pageRow.page);
	const [rowsPerPage, setRowsPerPage] = useState(pageRow.row);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useEffect(() => {
		dispatch(Actions.getSponsors());
	}, [dispatch]);

	useEffect(() => {
		setClean(cleanSelected)
	}, [cleanSelected]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(sponsors, item => item.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(sponsors);
		}
	}, [sponsors, searchText]);

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setClean(false)
			setSelected(data.map(n => n._id));
			return;
		}
		setSelected([]);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function deleteSponsors() {
		handleClose()
		dispatch(Actions.deleteSponsors({ ids: selected }));
	}

	function deleteSelectedItems() {
		handleClickOpen()
	}

	function handleClick(item) {
		dispatch(Actions.setPageAndRow(page, rowsPerPage))
		props.history.push(`/apps/institutional/sponsor/sponsors/${item._id}`);
	}

	function handleCheck(event, id) {
		setClean(false)
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	return (
		<div className="w-full flex flex-col">
			<div>
				<Dialog
					open={open}
					onClose={handleClose}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title">{t('ATTENTION')}</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							{`${t('DELETE_ITEM')} ${selected.length === 1 ? t('SINGLE_SPONSOR') : t('PLURAL_SPONSOR')}?`}
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose} color="primary">
							Cancelar
          				</Button>
						<Button onClick={deleteSponsors} color="primary" autoFocus>
							Eliminar
          				</Button>
					</DialogActions>
				</Dialog>
			</div>
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table className="min-w-xl" aria-labelledby="tableTitle">
					<SponsorsTableHead
						numSelected={selected.length}
						order={order}
						cleanSelected={clean}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={data.length}
						deleteSelectedItems={deleteSelectedItems}
					/>

					<TableBody>
						{_.orderBy(
							data,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											const orderId = order.id
											if (orderId) {
												let filter = o
												const split = orderId.split('.')
												split.map(s => {
													filter = filter[s]
												})
												return filter;
											}
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n._id) !== -1;
								return (
									<TableRow
										className="h-64 cursor-pointer"
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n._id}
										selected={isSelected}
										onClick={event => handleClick(n)}
									>
										<TableCell className="w-64 text-center" padding="none">
											<Checkbox
												checked={isSelected}
												onClick={event => event.stopPropagation()}
												onChange={event => handleCheck(event, n._id)}
											/>
										</TableCell>

										<TableCell className="w-52" component="th" scope="row" padding="none">
											{(
												<img
													className="w-full block rounded"
													src={n.urlImage || constants.DEFAULT_URL_IMAGE}
													alt={'Imagen'}
												/>
											)}
										</TableCell>
										<TableCell component="th" scope="row">
											<div className={clsx('inline text-12 p-4 rounded truncate', n.isActive ? 'bg-green text-white' : 'bg-orange text-black')}>
												{n.isActive ? t('ACTIVE') : t('NO_ACTIVE')}
											</div>
										</TableCell>
										<TableCell component="th" scope="row">
											{n.name}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.address.street}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.address.number}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.address.neighborhood}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.address.complement}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.phone}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.mobile}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.email}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.web}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.facebook}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.instagram}
										</TableCell>
										<TableCell component="th" scope="row">
											{n.supportMessage}
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>
			<div>
				<TablePagination
					className="overflow-hidden"
					component="div"
					count={data.length}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Previous Page'
					}}
					nextIconButtonProps={{
						'aria-label': 'Next Page'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div>
		</div>
	);
}

export default withRouter(SponsorsTable);
