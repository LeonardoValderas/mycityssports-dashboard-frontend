import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../../store/reducers';
import SponsorsHeader from './SponsorsHeader';
import SponsorsTable from './SponsorsTable';

function Sponsors() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<SponsorsHeader />}
			content={<SponsorsTable />}
			innerScroll
		/>
	);
}

export default withReducer('sponsorApp', reducer)(Sponsors);
