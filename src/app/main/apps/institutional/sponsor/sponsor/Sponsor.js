import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import Map from '../../../utils/GoogleMaps';
import _ from '@lodash';
import clsx from 'clsx';
import constants from '../../../utils/Constants'
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { orange } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../../store/actions';
import reducer from '../../store/reducers';
import { getSponsors } from '../../../../../main/apps/utils/AuthUtils'
import jimp from 'jimp';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../../institutional-i18n/es';
import pt from '../../institutional-i18n/pt';
i18next.addResourceBundle('es', 'institutional', es);
i18next.addResourceBundle('pt', 'institutional', pt);

const useStyles = makeStyles(theme => ({
	productImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		color: orange[400],
		opacity: 0
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	productImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $productImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $productImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $productImageFeaturedStar': {
				opacity: 1
			}
		}
	}
}));

function Sponsor(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('institutional');
	const sponsor = useSelector(({ sponsorApp }) => sponsorApp.sponsor.data);
	const savedModel = useSelector(({ sponsorApp }) => sponsorApp.sponsor.savedModel);
	const loading = useSelector(({ sponsorApp }) => sponsorApp.sponsor.loading);
	const theme = useTheme();
	const classes = useStyles(props);
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();
	const [imageError, setImageError] = useState("");

	useDeepCompareEffect(() => {
		function updateDivisionState() {
			const { sponsorId } = routeParams;
			if (sponsorId === 'new') {
				dispatch(Actions.newSponsor());
			} else {
				dispatch(Actions.getSponsor(sponsorId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(sponsor);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((sponsor && !form) || (sponsor && form && sponsor._id !== form._id)) {
			setForm(sponsor);
		}
	}, [form, sponsor, setForm]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function canBeSubmitted() {
		return form && form.name.length > 0
			&& form.address.street.length > 0
			&& form.address.number.length > 0
			&& form.address.neighborhood.length > 0
			&& form.address.position.longitude !== ''
			&& form.address.position.latitude !== ''
			&& !_.isEqual(sponsor, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function handleUploadChange(e) {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		
		form.nameImage = file.name		
	
		const reader = new FileReader();
		reader.readAsBinaryString(file);
		reader.onload = async () => {
			let r = `data:${file.type};base64,${btoa(reader.result)}`
			const image = await jimp.read(r);
			await image.resize(280, 280);
			let base = await image.getBase64Async(image.getMIME())
			let d = await image.bitmap.data
			let f = new File(d, file.name)
			let newSize = f.size / 1024 / 1024
			if(newSize > 1.0){
			   setImageError(t('IMAGE_BIG')) 
			   return
			} else {
				setImageError("") 
				setForm(
					_.set(
						{ ...form },
						`photo`, base
					)
				);
			}
		};

		reader.onerror = () => {
			console.log('error on load image');
		};
	}

	function mapClicked(mapProps, map, e) {
		setForm(
			_.set({ ...form },
				`address.position`, { latitude: e.latLng.lat(), longitude: e.latLng.lng() }
			)
		);
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/institutional/sponsor/sponsors"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">Sponsors</span>
								</Typography>
							</FuseAnimate>
						</div>
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted() || !getSponsors()}
								onClick={() => dispatch(Actions.saveSponsor(form, routeParams.sponsorId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.sponsorId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label="Sponsor" />
					<Tab className="h-64 normal-case" label="Logo" />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<TextField
									className="mt-8 mb-16"
									error={form.name === ''}
									required
									label={t('NAME')}
									autoFocus
									id="name"
									name="name"
									value={form.name}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									disabled={!getSponsors()}
									inputProps={{
										maxLength: 200,
									}}
								/>
								<div className="h-360 mt-8 mb-16 mr-24" style={{textAlign:"center"}}>
									<Map
										latitude={parseFloat(form.address.position.latitude)}
										longitude={parseFloat(form.address.position.longitude)}
										mapClicked={mapClicked}
									/>
								</div>

								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-8 md:w-4/6 w-full"
										error={form.address.street === ''}
										required
										label={t('STREET')}
										//autoFocus
										id="street"
										name="address.street"
										value={form.address.street}
										onChange={handleChange}
										disabled={!getSponsors()}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 200,
										}}
									/>

									<TextField
										className="mt-8 md:ml-8 md:w-2/6 w-full"
										error={form.address.number === ''}
										required
										label="Número"
										//autoFocus
										id="number"
										name="address.number"
										value={form.address.number}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										disabled={!getSponsors()}
										inputProps={{
											maxLength: 10,
										}}
									/>
								</div>

								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-8 md:w-4/6 w-full"
										error={form.address.neighborhood === ''}
										required
										label={t('NEIGHBORHOOD')}
										//autoFocus
										id="neighborhood"
										name="address.neighborhood"
										value={form.address.neighborhood}
										onChange={handleChange}
										variant="outlined"
										//fullWidth
										disabled={!getSponsors()}
										inputProps={{
											maxLength: 200,
										}}
									/>
									<TextField
										className="mt-8 md:ml-8 md:w-2/6 w-full"
										//error={form.address.complement  === ''}
										//required
										label="Complemento"
										//autoFocus
										id="complement"
										name="address.complement"
										value={form.address.complement}
										onChange={handleChange}
										disabled={!getSponsors()}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 200,
										}}
									/>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-8 md:w-2/6 w-full"
										//error={form.phone === ''}
										//required
										label={t('PHONE')}
										//autoFocus
										id="phone"
										name="phone"
										value={form.phone}
										onChange={handleChange}
										disabled={!getSponsors()}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 20,
										}}
									/>
									<TextField
										className="mt-8 md:ml-8 md:w-2/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Whatsapp"
										//autoFocus
										id="mobile"
										name="mobile"
										value={form.mobile}
										onChange={handleChange}
										disabled={!getSponsors()}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 20,
										}}
									/>

									<TextField
										className="mt-8 md:ml-8 md:w-2/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Email"
										//autoFocus
										id="email"
										name="email"
										value={form.email}
										onChange={handleChange}
										disabled={!getSponsors()}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 100,
										}}
									/>
								</div>
								<div className="flex flex-col flex-shrink-0 sm:flex-row">
									<TextField
										className="mt-8 md:w-2/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Web"
										//autoFocus
										id="web"
										name="web"
										value={form.web}
										onChange={handleChange}
										disabled={!getSponsors()}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 300,
										}}
									/>
									<TextField
										className="mt-8 md:ml-8 md:w-2/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Facebook"
										//autoFocus
										id="facebook"
										name="facebook"
										value={form.facebook}
										onChange={handleChange}
										disabled={!getSponsors()}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 300,
										}}
									/>
									<TextField
										className="mt-8 md:ml-8 md:w-2/6 w-full"
										//error={form.mobile === ''}
										//required
										label="Instagram"
										//autoFocus
										id="instagram"
										name="instagram"
										value={form.instagram}
										onChange={handleChange}
										disabled={!getSponsors()}
										variant="outlined"
										//fullWidth
										inputProps={{
											maxLength: 300,
										}}
									/>
								</div>
								<TextField
									className="mt-8 mb-16"
									//error={form.mobile === ''}
									//required
									label={t('MESSAGE_SPONSOR')}
									//autoFocus
									id="supportMessage"
									name="supportMessage"
									value={form.supportMessage}
									disabled={!getSponsors()}
									onChange={handleChange}
									variant="outlined"
									fullWidth
									inputProps={{
										maxLength: 400,
									}}
								/>
								<div className="flex">
									<FormControlLabel
										control={
											<Checkbox
												id='isActive'
												name='isActive'
												tabIndex={-1}
												checked={form.isActive}
												onChange={handleChange}
												disabled={!getSponsors()}
											/>
										}
										label={t('SPONSOR_ATIVE')}
									/>
								</div>
							</div>
						)}	{tabValue === 1 && (
							<div>
								<div className="flex justify-center sm:justify-start flex-wrap -mx-8">
									<label
										htmlFor="button-file"
										className={clsx(
											classes.productImageUpload,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5'
										)}
									>
										<input
											accept="image/*"
											className="hidden"
											id="button-file"
											type="file"
											onChange={handleUploadChange}
										/>
										<Icon fontSize="large" color="action">
											cloud_upload
										</Icon>
									</label>
									<div
										role="button"
										tabIndex={0}
										className={clsx(
											classes.productImageItem,
											'flex items-center justify-center relative w-280 h-180 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5',
										)}
										key={form._id}
									>
										<img className="max-w-280 w-280 h-180" src={form.photo || form.urlImage || constants.DEFAULT_URL_IMAGE_280_180} alt="Escudo" />
									</div>
								</div>
								<div className = "flex flex-col">
									<span className="mx-4">{t('IMAGE_RECOMMENTED')}</span>
									<span className="mx-4" style={{color:'red', fontStyle: 'bold'}}>{imageError}</span>
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('sponsorApp', reducer)(Sponsor);
