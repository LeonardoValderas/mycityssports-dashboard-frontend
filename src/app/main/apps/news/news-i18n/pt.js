const locale = {
	IMAGE_BIG: 'O tamanho da imagem excede o máximo suportado.',
	UPDATE: 'Atualizar',
	DATE_START: 'Data de inicio',
	DATE_END: 'Data de expiração',
	END: 'Expiração',
	TOURNAMENT: 'Torneio',
	TOURNAMENT_SELECTION: 'Selecione o torneio',
	SPORTS: 'Esportes',
	SPORT_SELECTION: 'Selecione o esporte',
	DIVISIONS: 'Divisões',
	DIVISION_SELECTION: 'Selecione a divisão',
	SUB_DIVISIONS: 'Sub Divisões',
	SUB_DIVISION_SELECTION: 'Selecione a sub divisão',
	SOURCE: 'Fonte',
	IMAGE_RECOMMENTED: 'Use imagem quadrada. Tamanho recomendado 288x280',
	ADD_NEW_NEWS: 'Adicionar nova noticia',
	NEW_F: 'Nova',
	ATTENTION: 'Atenção',
	SINGLE_NEWS: 'a noticia',
	PLURAL_NEWS: 'as noticias',
	DELETE_ITEM: 'Deseja deletar'
};

export default locale;
