const locale = {
	IMAGE_BIG: 'El tamaño de la imagen supera el maximo soportado.',
	UPDATE: 'Actualizar',
	DATE_START: 'Fecha de inicio',
	DATE_END: 'Fecha de expiración',
	END: 'Expiración',
	TOURNAMENT: 'Torneo',
	TOURNAMENT_SELECTION: 'Seleccione el torneo',
	SPORTS: 'Deportes',
	SPORT_SELECTION: 'Seleccione el deporte',
	DIVISIONS: 'Divisiones',
	DIVISION_SELECTION: 'Seleccione la división',
	SUB_DIVISIONS: 'Sub Divisiones',
	SUB_DIVISION_SELECTION: 'Seleccione la sub división',
	SOURCE: 'Fuente',
	IMAGE_RECOMMENTED: 'Usar imagen cuadrada. Tamaño recomendado 288x280',
	ADD_NEW_NEWS: 'Adicionar nueva noticia',
	NEW_F: 'Nueva',
	ATTENTION: 'Atención',
	SINGLE_NEWS: 'la noticia',
	PLURAL_NEWS: 'las noticias',
	DELETE_ITEM: 'Desea eliminar'
};

export default locale;
