import { combineReducers } from 'redux';
import newR from './new.reducer';
import news from './news.reducer';

const reducer = combineReducers({
	news,
	newR
});

export default reducer;
