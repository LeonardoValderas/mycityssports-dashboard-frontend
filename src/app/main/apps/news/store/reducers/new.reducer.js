import * as Actions from '../actions';

const initialState = {
	data: null,
	tournaments: null,
	//sports: null,
	//divisions: null,
	returnPage: false,
	loading: {
		show: false,
		label: ''
	  },
	savedModel: false  
};

const newReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_NEW: {
			return {
				...state,
				data: action.payload,
				loading: {
					show: false,
					label: ''
				  },
				  returnPage: false,
				  savedModel: false
			};
		}
		// case Actions.GET_NEW_FORM: {
		// 	return {
		// 		...state,
		// 		sports: action.payload && action.payload.sports && action.payload.sports.sports || [],
		// 		divisions: action.payload && action.payload.divisions || [],
		//         savedModel: false
		// 	};
		// }
		case Actions.GET_NEW_TOURNAMENTS: {
			return {
				...state,
				tournaments: action.payload || [],
		        savedModel: false
			};
		}
		case Actions.SAVE_NEW: {
			return {
				...state,
				data: action.payload, 
				returnPage: true,
				loading: {
					show: false,
					label: ''
				  },
				  savedModel: true
			};
		}
		case Actions.SET_LOADING_NEW: {
			return {
				...state,
				loading: action.payload,
	        	savedModel: false
			};
		}
		default: {
			return state;
		}
	}
};

export default newReducer;
