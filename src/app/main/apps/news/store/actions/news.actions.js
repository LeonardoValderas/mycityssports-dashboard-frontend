import axios from 'axios';
import constants from '../../../utils/Constants'
import { showMessage } from 'app/store/actions/fuse';
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import { getInstitutionId } from '../../../utils/AuthUtils'
import { translateInfo } from '../../../utils/TranslateUtils'
import i18next from 'i18next';
export const GET_NEWS = '[NEWS APP] GET NEWS';
export const SET_NEWS_SEARCH_TEXT = '[NEWS APP] SET NEWS SEARCH TEXT';
export const SET_NEWS_PAGE_NUMBER_ROW = '[NEWS APP] SET NEWS PAGE NUMBER ROW';
export const SET_LOADING = '[NEWS APP] SET LOADING';
export const CLEAN_SELELCTED_NEW = '[NEWS APP] CLEAN SELELCTED NEW';

const lang = i18next.language
const translate = translateInfo(lang)

export function getNews() {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.loading }))
        const institutionId = getInstitutionId()
        axios.get(`${translate.baseUrl}/news/institutions/${institutionId}`, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            const models = resp.data.models
            dispatch(setModels(models))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function setModels(models) {
    return {
        type: GET_NEWS,
        payload: models
    }
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        payload: loading
    }
}

export function setPageAndRow(page, row) {
    return {
        type: SET_NEWS_PAGE_NUMBER_ROW,
        pageRow: {
            page: page, 
            row: row
        }
    };
}

export function setNewsSearchText(event) {
    return {
        type: SET_NEWS_SEARCH_TEXT,
        searchText: event.target.value
    };
}

export function deleteNews(ids) {
    return dispatch => {
        dispatch(setLoading({ show: true, label: translate.processing}))
        dispatch(cleanSelected(false))
        axios.put(`${translate.baseUrl}/news/multi/delete`, ids, {
            headers: {
                'Content-Type': 'application/json',
                //Authorization: `Bearer ${this.getAccessToken()}`
            }
        }).then(resp => {
            dispatch(getNews())
            dispatch(cleanSelected(true))
        }).catch(e => {
            const message = exceptionMessage(e);
            dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant:'error' }));
            dispatch(setLoading({ show: false, label: '' }))
        })
    }
}

export function cleanSelected(clean) {
    return {
        type: CLEAN_SELELCTED_NEW,
        payload: clean
    }
}
