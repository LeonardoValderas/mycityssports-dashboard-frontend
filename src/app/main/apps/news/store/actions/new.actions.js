
import { showMessage } from 'app/store/actions/fuse';
import axios from 'axios';
import constants from '../../../utils/Constants'
import { translateInfo } from '../../../utils/TranslateUtils'
import { getInstitutionId, getUserId, getInstitutionName } from '../../../utils/AuthUtils'
import { exceptionMessage } from '../../../utils/ExceptionMessage'
import i18next from 'i18next';
import moment from 'moment/moment';
export const GET_NEW = '[NEW APP] GET NEW';
export const GET_NEW_TOURNAMENTS = '[NEW APP] GET NEW TOURNAMENTS';
export const SAVE_NEW = '[NEW APP] SAVE NEW';
export const SET_LOADING_NEW = '[NEW APP] SET LOADING';

const lang = i18next.language
const translate = translateInfo(lang)

export function getTournaments() {
	return dispatch => {
		const institutionId = getInstitutionId()
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/tournaments/institutions/${institutionId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const dataForm = resp.data.models
			dispatch(setModels(dataForm))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function getNew(newId) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.loading }))
		axios.get(`${translate.baseUrl}/news/${newId}`, {
			headers: {
				'Content-Type': 'application/json',
				//Authorization: `Bearer ${this.getAccessToken()}`
			}
		}).then(resp => {
			const model = resp.data.model
			dispatch(setModel(model))
		}).catch(e => {
			const message = exceptionMessage(e);
			dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
			dispatch(setLoading({ show: false, label: '' }))
		})
	}
}

export function setModel(model) {
	return {
		type: GET_NEW,
		payload: model
	}
}

export function setModels(models) {
	return {
		type: GET_NEW_TOURNAMENTS,
		payload: models
	}
}

export function saveNew(data, method) {
	return dispatch => {
		dispatch(setLoading({ show: true, label: translate.processing}))
		let id = ''
		if (method === 'post') {
			delete data['_id']
		} else {
			id = data._id ? data._id : ''
		}
		axios[method](`${translate.baseUrl}/news/${id}`, data)
			// {
			// 	headers: {
			// 		'Content-Type': 'multipart/form-data',
					
			// 	}
			// })
			.then(resp => {
				const model = resp.data.model
				dispatch(save(model))
				dispatch(showMessage({ message: translate.success, variant: 'success' }));
			})
			.catch(e => {
				const message = exceptionMessage(e);
				dispatch(showMessage({ message: message || constants.ERROR_UNEXPECTED_MESSAGE_SUBMIT, variant: 'error' }));
				dispatch(setLoading({ show: false, label: '' }))
			})
	}
}

export function newNew() {
	const data = {
		title: '',
		body: '',
		tournament: null,
		sports: [],
		divisions: [],
		subDivisions: [],
		start: moment(new Date()).format('YYYY-MM-DD'),
		expiration: moment(new Date()).format('YYYY-MM-DD'),
		photo: null,
		nameImage: null,
		nameId: null,
		urlImage: null,
		source: '',
		folder: `news/${getInstitutionName()}`,
		user: getUserId(),
		institution: getInstitutionId()
	};

	return {
		type: GET_NEW,
		payload: data
	};
}

export function save(model) {
	return {
		type: SAVE_NEW,
		payload: model
	};
}

export function setLoading(loading) {
	return {
		type: SET_LOADING_NEW,
		payload: loading
	}
}
