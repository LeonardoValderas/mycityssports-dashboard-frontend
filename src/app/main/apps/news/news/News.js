import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store/reducers';
import NewsHeader from './NewsHeader';
import NewsTable from './NewsTable';

function News() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<NewsHeader />}
			content={<NewsTable />}
			innerScroll
		/>
	);
}

export default withReducer('newsApp', reducer)(News);
