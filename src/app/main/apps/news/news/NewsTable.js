import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../store/actions';
import NewsTableHead from './NewsTableHead';
import FuseLoading from '@fuse/core/FuseLoading';
import moment from 'moment/moment';
import constants from '../../utils/Constants'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../news-i18n/es';
import pt from '../news-i18n/pt';
i18next.addResourceBundle('es', 'news', es);
i18next.addResourceBundle('pt', 'news', pt);

function NewsTable(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('news');
	const news = useSelector(({ newsApp }) => newsApp.news.data);
	const searchText = useSelector(({ newsApp }) => newsApp.news.searchText);
	const pageRow = useSelector(({ newsApp }) => newsApp.news.pageRow);
	const loading = useSelector(({ newsApp }) => newsApp.news.loading);
	const cleanSelected = useSelector(({ newsApp }) => newsApp.news.cleanSelected);
	const [clean, setClean] = useState(cleanSelected);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState(news);
	const [page, setPage] = useState(pageRow.page);
	const [rowsPerPage, setRowsPerPage] = useState(pageRow.row);
	const [order, setOrder] = useState({
		direction: 'asc',
		id: null
	});
	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useEffect(() => {
		dispatch(Actions.getNews());
	}, [dispatch]);

	useEffect(() => {
		setClean(cleanSelected)
	}, [cleanSelected]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(news, item => item.title.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(news);
		}
	}, [news, searchText]);

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (order.id === property && order.direction === 'desc') {
			direction = 'asc';
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setClean(false)
			setSelected(data.map(n => n._id));
			return;
		}
		setSelected([]);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function deleteNews() {
		handleClose()
		dispatch(Actions.deleteNews({ ids: selected }));
	}

	function deleteSelectedItems() {
		handleClickOpen()
	}

	function handleClick(item) {
		dispatch(Actions.setPageAndRow(page, rowsPerPage))
		props.history.push(`/apps/new/news/${item._id}`);
	}

	function handleCheck(event, id) {
		setClean(false)
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	return (
		<div className="w-full flex flex-col">
			<div>
				<Dialog
					open={open}
					onClose={handleClose}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title">{t('ATTENTION')}</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							{`${t('DELETE_ITEM')} ${selected.length === 1 ? t('SINGLE_NEWS') : t('PLURAL_NEWS')}?`}
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose} color="primary">
							Cancelar
          				</Button>
						<Button onClick={deleteNews} color="primary" autoFocus>
							Eliminar
          				</Button>
					</DialogActions>
				</Dialog>
			</div>
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table className="min-w-xl" aria-labelledby="tableTitle">
					<NewsTableHead
						numSelected={selected.length}
						order={order}
						cleanSelected={clean}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={data.length}
						deleteSelectedItems={deleteSelectedItems}
					/>

					<TableBody>
						{_.orderBy(
							data,
							[
								o => {
									switch (order.id) {
										case 'categories': {
											return o.categories[0];
										}
										default: {
											const orderId = order.id
											if (orderId && o !== null) {
												let filter = o
												const split = orderId.split('.')
												split.map(s => {
													const f = filter[s]
													if (f) {
														filter = f
													}
												})
												return filter;
											}
											return o[order.id];
										}
									}
								}
							],
							[order.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n._id) !== -1;
								return (
									<TableRow
										className="h-64 cursor-pointer"
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n._id}
										selected={isSelected}
										onClick={event => handleClick(n)}
									>
										<TableCell className="w-64 text-center" padding="none">
											<Checkbox
												checked={isSelected}
												onClick={event => event.stopPropagation()}
												onChange={event => handleCheck(event, n._id)}
											/>
										</TableCell>

										<TableCell className="w-52" component="th" scope="row" padding="none">
											{(
												<img
													className="w-full block rounded"
													src={n.urlImage || constants.DEFAULT_URL_IMAGE}
													alt={'Imagen'}
												/>
											)}
										</TableCell>

										<TableCell component="th" scope="row">
											{n.title}
										</TableCell>

										<TableCell component="th" colSpan={3} scope="row">
											{n.body}
										</TableCell>

										<TableCell component="th" scope="row">
											{moment(n.start).format('DD-MM-YYYY')}
										</TableCell>

										<TableCell component="th" scope="row">
											{moment(n.expiration).format('DD-MM-YYYY')}
										</TableCell>
										<TableCell component="th" scope="row">
											{
												`${n.tournament.name} ${n.tournament.year}`
											}
										</TableCell>
										<TableCell component="th" scope="row">
											{
												n.sports.map(s =>
													`${s.name} ${s.gender.name}`
												).join(' - ')
											}
										</TableCell>

										<TableCell component="th" scope="row">
											{
												n.divisions.map(d =>
													d.name
												).join(' - ')
											}
										</TableCell>
										<TableCell component="th" scope="row">
											{
												n.subDivisions.map(s =>
													s
												).join(' - ')
											}
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>
			<div>
				<TablePagination
					className="overflow-hidden"
					component="div"
					count={data.length}
					rowsPerPage={rowsPerPage}
					page={page}
					backIconButtonProps={{
						'aria-label': 'Prev Pag'
					}}
					nextIconButtonProps={{
						'aria-label': 'Sig Pag'
					}}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</div >
		</div >
	);
}

export default withRouter(NewsTable);
