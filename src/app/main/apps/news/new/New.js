import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import moment from 'moment/moment';
import _ from '@lodash';
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import constants from '../../utils/Constants'
import Button from '@material-ui/core/Button';
import { isMinorOrSameDateNow, isMinorDateNow, isDateStartMinorOrSameDateEnd } from '../../utils/DateValidator'
import { orange, red } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import jimp from 'jimp';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import es from '../news-i18n/es';
import pt from '../news-i18n/pt';
i18next.addResourceBundle('es', 'news', es);
i18next.addResourceBundle('pt', 'news', pt);


const useStyles = makeStyles(theme => ({
	productImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		color: orange[400],
		opacity: 0
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	productImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $productImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $productImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $productImageFeaturedStar': {
				opacity: 1
			}
		}
	}
}));

function New(props) {
	const dispatch = useDispatch();
	const { t } = useTranslation('news');
	const data = useSelector(({ newApp }) => newApp.newR.data);
	const tournaments = useSelector(({ newApp }) => newApp.newR.tournaments);
	const savedModel = useSelector(({ newApp }) => newApp.newR.savedModel);
	//const divisions = useSelector(({ newApp }) => newApp.newR.divisions);
	//const sports = useSelector(({ newApp }) => newApp.newR.sports);
	const loading = useSelector(({ newApp }) => newApp.newR.loading);
	const progress = useSelector(({ newApp }) => newApp.newR.progress);
	const theme = useTheme();
	const classes = useStyles(props);
	const [tabValue, setTabValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);
	const routeParams = useParams();
	const [imageError, setImageError] = useState("");
	const [subDivisions, setSubDivisions] = useState([]);
	const [sports, setSports] = useState([]);
	const [divisions, setDivisions] = useState([]);
	var isNew = false

	useDeepCompareEffect(() => {
		dispatch(Actions.getTournaments());
		function updateDivisionState() {
			const { newId } = routeParams;
			isNew = (newId === 'new')
			if (newId === 'new') {
				dispatch(Actions.newNew());
			} else {
				dispatch(Actions.getNew(newId));
			}
		}

		updateDivisionState();
	}, [dispatch, routeParams]);

	useEffect(() => {
		if (savedModel) {
			setForm(data);
		}
	}, [savedModel]);

	useEffect(() => {
		if ((data && !form) || (data && form && data._id !== form._id)) {
			if (!isNew) {
				getData(data);
			}
			setForm(data);
		}
	}, [form, data, setForm]);

	function getData(data) {
		if (data.tournament && data.tournament !== undefined && data.tournament._id) {
			const id = data.tournament._id
			setTournamentSports(id)
			setTournamentDivisions(id)
			setTournamentSubDivisions(id)
		}
	}

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function setTournamentDivisions(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setDivisions(tournament.divisions)
			return
		}
		setDivisions([])
	}

	function setTournamentSports(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setSports(tournament.sports)
			return
		}
		setSports([])
	}

	function setTournamentSubDivisions(tournamentId) {
		const tournament = tournaments.find(t => t._id === tournamentId)
		if (tournament) {
			setSubDivisions(tournament.subDivisions)
			return
		}
		setSubDivisions([])
	}

	function handleChipChangeTournament(value) {
		if (!form.tournament || form.tournament._id !== value._id) {
			setSports(value.sports)
			form.sports = []
			setDivisions(value.divisions)
			form.divisions = []
			setSubDivisions(value.subDivisions)
			form.subDivisions = []
			setFormValue('tournament', value)
		}
	}

	function setFormValue(name, value) {
		setForm(
			_.set(
				{ ...form },
				name, value
			)
		);
	}
	
	function canBeSubmitted() {
		return form 
		    && form.title.length > 0
			&& form.body.length > 0
			&& form.tournament
			&& form.sports.length > 0
			&& form.divisions.length > 0
			&& subDivisionValidation()
			&& ((!isMinorDateNow(moment.parseZone(form.start).format('MM/DD/YYYY')) && isNew) || !isNew)
			&& !isMinorOrSameDateNow(moment.parseZone(form.expiration).format('MM/DD/YYYY'))
			&& !isDateStartMinorOrSameDateEnd(moment.parseZone(form.start).format('MM/DD/YYYY'), moment.parseZone(form.expiration).format('MM/DD/YYYY'))
			// && (!isMinorDateNow(moment.parseZone(form.start).format('MM/DD/YYYY')) && isNew || isMinorDateNow(moment.parseZone(form.start).format('MM/DD/YYYY')) && !isNew)
			// && !isMinorOrSameDateNow(moment.parseZone(form.expiration).format('MM/DD/YYYY'))
			// && !isDateStartMinorOrSameDateEnd(moment.parseZone(form.start).format('MM/DD/YYYY'), moment.parseZone(form.expiration).format('MM/DD/YYYY'))
			&& !_.isEqual(data, form);
	}

	if (loading.show) {
		return <FuseLoading label={loading.label} />
	}

	function subDivisionValidation(){
		return _.isEmpty(subDivisions) ? true : form.subDivisions.length > 0
	}

	async function handleUploadChange(e) {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		form.nameImage = file.name

		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = async () => {
			let r = `data:${file.type};base64,${btoa(reader.result)}`
			const image = await jimp.read(r);
			await image.resize(280, 280);
			let base = await image.getBase64Async(image.getMIME())
			let d = await image.bitmap.data
			let f = new File(d, file.name)
			let newSize = f.size / 1024 / 1024
			if (newSize > 1.0) {
				setImageError(t('IMAGE_BIG'))
				return
			} else {
				setImageError("")
				setForm(
					_.set(
						{ ...form },
						`photo`, base
					)
				);
			}
		};

		reader.onerror = () => {
			console.log('error on load image');
		};
	}

	function handleChipChange(value, name) {
		const isSubdivision = name === 'subDivisions'
		setForm(
			_.set(
				{ ...form },
				name, isSubdivision ? value.map(v => v.value) : value
			)
		);
	}

	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="normal-case flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/new/news"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">Noticias</span>
								</Typography>
							</FuseAnimate>
						</div>
						<CircularProgress color="secondary" style={{ display: progress ? '' : 'none' }} />
						<FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-no-wrap normal-case"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(Actions.saveNew(form, routeParams.newId === 'new' ? 'post' : 'put'))}
							>
								{routeParams.newId === 'new' ? 'Salvar' : t('UPDATE')}
							</Button>
						</FuseAnimate>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}>
					<Tab className="h-64 normal-case" label="Noticia" />
					<Tab className="h-64 normal-case" label="Imagen" />
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div >
								<TextField
									className="mt-8"
									error={form.title === ''}
									required
									label="Titulo"
									autoFocus
									id="title"
									name="title"
									value={form.title}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 50,
									}}
								/>

								<TextField
									className="mt-16"
									error={form.body === ''}
									required
									label="Noticia"
									//autoFocus
									id="body"
									name="body"
									value={form.body}
									onChange={handleChange}
									variant="outlined"
									fullWidth
									inputProps={{
										maxLength: 200,
									}}
								/>
								<div className="flex mx-0">
									<TextField
										id="start"
										name="start"
										label={t('DATE_START')}
										type="date"
										className="mt-16 w-3/6 md:w-2/6"
										InputLabelProps={{
											shrink: true
										}}
										inputProps={{
											//min: moment(new Date()).format('YYYY-MM-DD')
										}}
										value={moment.parseZone(form.start).format('YYYY-MM-DD')}
										//value={moment(form.start).format('YYYY-MM-DD')}
										onChange={handleChange}
										variant="outlined"
									/>
									<TextField
										id="expiration"
										name="expiration"
										label={t('DATE_END')}
										type="date"
										className="mt-16 ml-16 w-3/6 md:w-2/6"
										InputLabelProps={{
											shrink: true
										}}
										inputProps={{
											//min: moment(new Date()).format('YYYY-MM-DD')
										}}
										value={moment.parseZone(form.expiration).format('YYYY-MM-DD')}
										//value={moment(form.expiration).format('YYYY-MM-DD')}
										onChange={handleChange}
										variant="outlined"
									/>
								</div>
								<FuseChipSelect
										className="mt-24"
										value={form.tournament}
										onChange={value => handleChipChangeTournament(value)}
										placeholder={t('TOURNAMENT_SELECTION')}
										required={true}
										variant='fixed'
										getOptionLabel={option => `${option.name} ${option.year}`}
										getOptionValue={option => option._id}
										textFieldProps={{
											label: t('TOURNAMENT'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										isDisabled={progress}
										options={tournaments || []}
									/>
								<FuseChipSelect
									className="mt-16"
									value={form.sports}
									onChange={value => handleChipChange(value, 'sports')}
									placeholder={t('SPORT_SELECTION')}
									required={true}
									variant='fixed'
									getOptionLabel={option => `${option.name} (${option.gender.name})`}
									getOptionValue={option => option._id}
									textFieldProps={{
										label: t('SPORTS'),
										InputLabelProps: {
											shrink: true
										},
										variant: 'outlined'
									}}
									options={sports || []}
									isMulti
								/>

								<FuseChipSelect
									className="mt-16"
									value={form.divisions}
									onChange={value => handleChipChange(value, 'divisions')}
									placeholder={t('DIVISION_SELECTION')}
									required={false}
									variant='fixed'
									getOptionLabel={option => option.name}
									getOptionValue={option => option._id}
									textFieldProps={{
										label: t('DIVISIONS'),
										InputLabelProps: {
											shrink: true,
										},
										variant: 'outlined'
									}}
									options={divisions || []}
									isMulti
								/>
							
									<FuseChipSelect
										className="mt-16"
										value={
											form.subDivisions.map(t => ({ value: t, label: t })) || []
										}
										onChange={value => handleChipChange(value, 'subDivisions')}
										placeholder={t('SUB_DIVISION_SELECTION')}
										variant='fixed'
										required={!_.isEmpty(subDivisions)}
										//getOptionLabel={option => option}
										//getOptionValue={option => option}
										textFieldProps={{
											label: t('SUB_DIVISIONS'),
											InputLabelProps: {
												shrink: true
											},
											variant: 'outlined'
										}}
										options={subDivisions.map(t => ({ value: t, label: t })) || []}
										isDisabled={progress}
										isMulti
									/>
				
								<TextField
									className="mt-16"
									label={t('SOURCE')}
									id="source"
									name="source"
									value={form.source}
									onChange={handleChange}
									variant="outlined"
									cols={8}
									fullWidth
									inputProps={{
										maxLength: 50,
									}}
								/>
							</div>
						)}	{tabValue === 1 && (
							<div>
								<div className="flex justify-center sm:justify-start flex-wrap -mx-8">
									<label
										htmlFor="button-file"
										className={clsx(
											classes.productImageUpload,
											'flex items-center justify-center relative w-128 h-128 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5'
										)}
									>
										<input
											accept="image/*"
											className="hidden"
											id="button-file"
											type="file"
											onChange={handleUploadChange}
										/>
										<Icon fontSize="large" color="action">
											cloud_upload
										</Icon>
									</label>
									<div
										role="button"
										tabIndex={0}
										className={clsx(
											classes.productImageItem,
											'flex items-center justify-center relative w-280 h-180 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5',
										)}
										key={form._id}
									>
										<img className="max-w-280 w-280 h-180" src={form.photo || form.urlImage || constants.DEFAULT_URL_IMAGE_280_180} alt="Noticia" />
									</div>
								</div>
								<div className="flex flex-col">
									<span className="mx-4">{t('IMAGE_RECOMMENTED')}</span>
									<span className="mx-4" style={{ color: 'red', fontStyle: 'bold' }}>{imageError}</span>
								</div>
							</div>
						)}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('newApp', reducer)(New);
