import React from 'react';
import { Redirect } from 'react-router-dom';

const NewsAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/new/news/:newId',
			component: React.lazy(() => import('./new/New'))
		},
		{
			path: '/apps/new/news',
			component: React.lazy(() => import('./news/News'))
		},
		{
			path: '/apps/new',
			component: () => <Redirect to="/apps/new/news" />
		}
	]
};

export default NewsAppConfig;
