import FuseUtils from '@fuse/utils/FuseUtils';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import constants from '../../main/apps/utils/Constants'
import {
	setToken, 
	getToken, 
	removeToken, 
	removeAll, 
	setUserId, 
	setCityId, 
	setInstitutionId, 
	setCityPosition, 
	setSponsors, 
	setInstitutionName,
	getCountry
} from '../../main/apps/utils/AuthUtils'
import { exceptionMessage } from '../../main/apps/utils/ExceptionMessage';

class JwtService extends FuseUtils.EventEmitter {
	init() {
		this.setInterceptors();
		this.handleAuthentication();
	}

	setInterceptors = () => {
		axios.interceptors.response.use(
			response => {
				return response;
			},
			err => {
				return new Promise((resolve, reject) => {
					if (err.response.status === 403 || err.response.status === 401 && err.config && !err.config.__isRetryRequest) {
						this.emit('onAutoLogout', 'Session expirada');
						this.setSession(null);
					} 
		
					throw err;
				});
			}
		);
	};

	handleAuthentication = () => {
		const access_token = this.getAccessToken();

		if (!access_token) {
			this.emit('onNoAccessToken');
			return;
		}

		if (this.isAuthTokenValid(access_token)) {
			this.setSession(access_token);
			this.emit('onAutoLogin', true);
		} else {
			this.setSession(null);
			this.emit('onAutoLogout', 'Session expirada');
		}
	};

	signInWithEmailAndPassword = (email, password) => {
		const user = {
			email,
			password
		}
		const country = getCountry()
		const baseUrl = (country === constants.AR) ? constants.OAPI_URL : constants.OAPI_URL_BR
		return new Promise((resolve, reject) => {
			axios
				.post(`${baseUrl}/auth/login`, user)
				.then(response => {
					if (response.data && response.data.success) {
						const model = response.data.model
						//console.log('token', model.token)
						this.setSession(model.token);
						this.setUtils(model)
						const user = this.setUserModel(model)
						resolve(user);
					} else {
						const message = exceptionMessage(response)
						reject(message);
					}
				}).catch(error => {
					const message = exceptionMessage(error)
					reject(message);
				});;
		});
	};

	setUtils = model => {
		setCityId(model.city._id)
		setCityPosition(model.city.position)
		setInstitutionId(model.institution._id)
		setInstitutionName(model.institution.name)
		setSponsors(model.institution.plan.sponsor)
		setUserId(model._id)
	}

	signInWithToken = () => {
		const token = this.getAccessToken()
		const country = getCountry()
		const baseUrl = (country === constants.AR) ? constants.OAPI_URL : constants.OAPI_URL_BR	
		return new Promise((resolve, reject) => {
			axios
				.get(`${baseUrl}/auth/token/${token}`)
				.then(response => {
					
					if (response.data && response.data.success) {
						const model = response.data.model
						this.setUtils(model)
						this.setSession(model.token);
						const user = this.setUserModel(model)
						resolve(user);
					} else {
						this.logout();
					}
				})
				.catch(error => {
					this.logout();
					const message = exceptionMessage(error)
					reject(message);
				});
		});
	};

	setUserModel = model => {
		//console.log(model.institution)
		return {
			_id: model._id,
			name: model.name,
			email: model.email,
			institution: {
			   name: model.institution.name,
			   urlImage: model.institution.urlImage,
			},
			role: model.role,
			urlImage:model.urlImage
		}
	}

	setSession = access_token => {
		if (access_token) {
			//console.log(access_token)
			setToken(access_token);
			//localStorage.setItem('jwt_access_token', access_token);
			axios.defaults.headers.common.Authorization = `Bearer ${access_token}`;
		} else {
			removeToken()
			removeAll()
			delete axios.defaults.headers.common.Authorization;
		}
	};

	logout = () => {
		this.setSession(null);
	};

	isAuthTokenValid = access_token => {
		if (!access_token) {
			return false;
		}
		//console.log(access_token)
		const decoded = jwtDecode(access_token);
		const currentTime = Date.now() / 1000;
		if (decoded.exp < currentTime) {
			console.log('access token expired');
			return false;
		}

		return true;
	};

	getAccessToken = () => {
		return getToken()
	};
}

const instance = new JwtService();

export default instance;
