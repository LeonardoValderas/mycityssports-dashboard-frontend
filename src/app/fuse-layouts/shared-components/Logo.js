import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import React from 'react';
import { useSelector } from 'react-redux';
import constants from '../../main/apps/utils/Constants'

const useStyles = makeStyles(theme => ({
	root: {
		'& .logo-icon': {
			width: 24,
			height: 24,
			transition: theme.transitions.create(['width', 'height'], {
				duration: theme.transitions.duration.shortest,
				easing: theme.transitions.easing.easeInOut
			})
		},
		'& .react-badge, & .logo-text': {
			transition: theme.transitions.create('opacity', {
				duration: theme.transitions.duration.shortest,
				easing: theme.transitions.easing.easeInOut
			})
		}
	},
	reactBadge: {
		backgroundColor: '#121212',
		color: '#61DAFB'
	}
}));

function Logo() {
	const classes = useStyles();
	const user = useSelector(({ auth }) => auth.user);
	const url = (user && user.institution && user.institution.urlImage) ? user.institution.urlImage : constants.DEFAULT_URL_INSTITUTION_IMAGE
	const name = (user && user.institution && user.institution.name) ? user.institution.name : ''
	return (
		<div className={clsx(classes.root, 'flex items-center')}>
			<img className="logo-icon" src={url} alt="logo" />
			<div className={clsx(classes.reactBadge, 'react-badge flex items-center py-4 px-8 ml-8 rounded')}>
			<Typography className="text-16 mx-12 font-light logo-text" color="inherit">
				{name}
			</Typography>
			</div>
		</div>
	);
}

export default Logo;
