import { authRoles } from 'app/auth';
import { getSponsors } from '../main/apps/utils/AuthUtils'

import i18next from 'i18next';
import es from './navigation-i18n/es';
import pt from './navigation-i18n/pt';

i18next.addResourceBundle('es', 'navigation', es);
i18next.addResourceBundle('pt', 'navigation', pt);

const navigationConfig = [
	{
		id: 'applications',
		title: 'MENU',
		translate: 'MENU',
		type: 'group',
		icon: 'apps',
		children: [
			{
				id: 'fixture',
				title: 'Fixtures',
				translate: 'Fixtures',
				type: 'collapse',
				icon: 'format_list_numbered',
				url: '/apps/fixture',
				children: [
					{
						id: 'fixtureItem',
						title: 'Fixtures',
						type: 'item',
						icon: 'format_list_numbered',
						url: '/apps/fixture/fixtures',
						exact: true
					},
					{
						id: 'fixtureCsv',
						title: 'Fixtures CSV',
						type: 'item',
						icon: 'cloud_download',
						url: '/apps/fixture/fixture-csv',
						exact: true
					},
				]
			},
			{
				id: 'table',
				title: 'Tabla de Posiciones',
				translate: 'TABLES',
				type: 'collapse',
				icon: 'list_alt',
				url: '/apps/table',
				children: [
					{

						id: 'tableItem',
						title: 'Tabla de Posiciones',
						type: 'item',
						translate: 'TABLES',
						icon: 'list_alt',
						url: '/apps/table/tables',
						exact: true
					},
					{
						id: 'tableCsv',
						title: 'Table de Posiciones CSV',
						translate: 'TABLES_CSV',
						type: 'item',
						icon: 'cloud_download',
						url: '/apps/table/table-csv',
						exact: true
					},
				]
			},	
			{
				id: 'tournament',
				title: 'tournaments',
				translate: 'TOURNAMENTS',
				type: 'collapse',
				icon: 'device_hub',
				url: '/apps/tournament',
				children: [
					{
						id: 'tournament-item',
						title: 'Torneos',
						translate: 'TOURNAMENTS',
						type: 'item',
						url: '/apps/tournament/tournaments',
						exact: true
					},
					{
						id: 'tournament-zone-item',
						title: 'Zonas/Grupos',
						type: 'item',
						url: '/apps/tournament/zones',
						exact: true
					},
					// {
					// 	id: 'tournament-instance-item',
					// 	title: 'Instancias',
					// 	type: 'item',
					// 	url: '/apps/tournament/instances',
					// 	exact: true
					// },
				]
			},
			{
				id: 'team',
				title: 'Equipos',
				translate: 'TEAMS',
				type: 'item',
				icon: 'security',
				url: '/apps/team/teams',
				exact: true
			},
			{
				id: 'division',
				title: 'Divisiones',
				translate: 'DIVISIONS',
				type: 'item',
				icon: 'hourglass_empty',
				url: '/apps/division/divisions',
				exact: true
			},
			{
				id: 'player',
				title: 'Jugadores',
				translate: 'PLAYERS',
				type: 'item',
				icon: 'portrait',
				url: '/apps/player/players',
				exact: true
			},
			{
				id: 'referee',
				title: 'Arbitros',
				translate: 'REFEERES',
				type: 'collapse',
				icon: 'accessibility',
				url: '/apps/referee',
				children: [
					{
						id: 'refereeItem',
						title: 'Arbitros',
						translate: 'REFEERES',
						type: 'item',
						url: '/apps/referee/referees',
						exact: true
					},
					{
						id: 'refereeFunction',
						title: 'Funciones',
						translate: 'FUNCTIONS',
						type: 'item',
						url: '/apps/referee/referee-functions',
						exact: true
					}
				]
			},
			{
				id: 'news',
				title: 'Noticias',
				translate: 'NEWS',
				type: 'item',
				icon: 'local_library',
				url: '/apps/new/news',
				exact: true
			},
			{
				id: 'playingField',
				title: 'Campos de Juego',
				translate: 'PLAYING_FIELD',
				type: 'item',
				icon: 'map',
				url: '/apps/playingField/playingFields',
				exact: true
			},
			{
				id: 'sanction',
				title: 'Sanciones',
				type: 'item',
				translate: 'SANCTIONS',
				icon: 'event_busy',
				url: '/apps/sanction/sanctions',
				exact: true
			},
			{
				id: 'ranking',
				title: 'Ranking',
				translate: 'Ranking',
				type: 'collapse',
				icon: 'looks_one',
				url: '/apps/ranking',
				children: [
					{
						id: 'rankingItem',
						title: 'Ranking',
						type: 'item',
						url: '/apps/ranking/ranking/rankings',
						exact: true
					},
					{
						id: 'rankingType',
						title: 'Tipo de Ranking',
						type: 'item',
						url: '/apps/ranking/rankingType/rankingTypes',
						exact: true
					}
				]
			},
			{
				id: 'institution',
				title: 'institution',
				translate: 'Intitucional',
				auth: authRoles.admin,
				type: 'collapse',
				icon: 'business',
				url: '/apps/institutional',
				children: [
					{
						id: 'myAccount',
						title: 'Mis Datos',
						translate: 'INSTITUTIONAL_MY_DATA',
						auth: authRoles.admin,
						type: 'item',
						url: '/apps/institutional/my-account/my-account',
						exact: true
					},
					{
						id: 'mySports',
						title: 'Mis Deportes',
						translate: 'INSTITUTIONAL_MY_SPORTS',
						auth: authRoles.admin,
						type: 'item',
						url: '/apps/institutional/my-sport/my-sports',
						exact: true
					},
					{
						id: 'sponsor',
						title: 'Mis Sponsors',
						translate: 'INSTITUTIONAL_MY_SPONSORS',
						auth: authRoles.admin,
						type: 'item',
						disabled: !getSponsors(),
						url: '/apps/institutional/sponsor/sponsors',
						exact: true
					},
					{
						id: 'user',
						title: 'Mis Usuarios',
						translate: 'INSTITUTIONAL_MY_USERS',
						auth: authRoles.admin,
						type: 'item',
						url: '/apps/institutional/user/users',
						exact: true
					}
				]
			},
		]
	},
	{
		id: 'other',
		title: 'Otros',
		translate: 'OTHER',
		type: 'group',
		icon: 'pages',
		children: [
			{
				id: 'notification',
				title: 'Notificación',
				type: 'item',
				translate: 'NOTIFICATIONS',
				icon: 'chat',
				url: '/apps/notification',
				exact: true
			},
			{
				id: 'document',
				title: 'Documentación',
				translate: 'DOCUMENTATIONS',
				type: 'item',
				icon: 'import_contacts',
				url: '/pages/knowledge-base'
			}
		]
	}
];

export default navigationConfig;
