const locale = {
	TABLES: 'Tabela de posições',
	TABLES_CSV: 'Tabela de posições CSV',
	TOURNAMENTS: 'Torneios',
	TEAMS: 'Equipes',
	DIVISIONS: 'Divisões',
	NOTIFICATIONS: 'Notificação',
	PLAYERS: 'Jogadores',
	REFEERES: 'Juiz',
	FUNCTIONS: 'Funções',
	NEWS: 'Notícia',
	PLAYING_FIELD: 'Campos de Jogo',
	SANCTIONS: 'Sanções',
	OTHER: 'Outros',
	INSTITUTIONAL_MY_DATA: 'Meus Dados',
	INSTITUTIONAL_MY_SPORTS: 'Meus Esportes',
	INSTITUTIONAL_MY_SPONSORS: 'Meus Sponsors',
	INSTITUTIONAL_MY_USERS: 'Meus Usuarios',
	DOCUMENTATIONS: 'Documentação',
};

export default locale;
