const locale = {
	TABLES: 'Tabla de posiciones',
	TABLES_CSV: 'Tabla de posiciones CSV',
	TOURNAMENTS: 'Torneos',
	TEAMS: 'Equipos',
	DIVISIONS: 'Divisiones',
	NOTIFICATIONS: 'Notificación',
	PLAYERS: 'Jugadores',
	REFEERES: 'Arbitros',
	FUNCTIONS: 'Funciones',
	NEWS: 'Noticias',
	PLAYING_FIELD: 'Campos de Juegos',
	SANCTIONS: 'Sanciones',
	OTHER: 'Otros',
	INSTITUTIONAL_MY_DATA: 'Mis Datos',
	INSTITUTIONAL_MY_SPORTS: 'Mis Deportes',
	INSTITUTIONAL_MY_SPONSORS: 'Mis Sponsors',
	INSTITUTIONAL_MY_USERS: 'Mis Usuarios',
	DOCUMENTATIONS: 'Documentación',
};

export default locale;
