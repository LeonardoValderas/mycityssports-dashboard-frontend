import history from '@history';
import _ from '@lodash';
import jwtService from 'app/services/jwtService';
import * as MessageActions from 'app/store/actions/fuse/message.actions';
import * as FuseActions from 'app/store/actions/fuse';
//import {setUserId, setCityId, setInstitutionId } from '../../../main/apps/utils/AuthUtils'
export const SET_USER_DATA = '[USER] SET DATA';
export const REMOVE_USER_DATA = '[USER] REMOVE DATA';
export const USER_LOGGED_OUT = '[USER] LOGGED OUT';

/**
 * Set User Data
 */
export function setUserData(user) {
	return dispatch => {
		/*
        You can redirect the logged-in user to a specific route depending on his role
         */

		// history.location.state = {
		//     redirectUrl: user.redirectUrl // for example 'apps/academy'
		// }

		/*
        Set User Settings
         */
		// const data = {
		// 		displayName: user.name,
		// 		photoURL: 'assets/images/avatars/Velazquez.jpg',
		// 		email: user.email,
		// 		shortcuts: ['calendar', 'mail', 'contacts', 'todo']
		// }
		// const data = {

		// 	layout: {
		// 		style: 'layout1',
		// 		config: {
		// 			scroll: 'content',
		// 			navbar: {
		// 				display: true,
		// 				folded: true,
		// 				position: 'left'
		// 			},
		// 			toolbar: {
		// 				display: true,
		// 				style: 'fixed',
		// 				position: 'below'
		// 			},
		// 			footer: {
		// 				display: true,
		// 				style: 'fixed',
		// 				position: 'below'
		// 			},
		// 			mode: 'fullwidth'
		// 		}
		// 	}
		// }

		// const data =  {
		// 	displayName: 'Abbott Keitch',
		// 	photoURL: 'assets/images/avatars/Abbott.jpg',
		// 	email: 'admin',
		// 	settings: {
		// 		layout: {
		// 			style: 'layout1',
		// 			config: {
		// 				scroll: 'content',
		// 				navbar: {
		// 					display: true,
		// 					folded: true,
		// 					position: 'left'
		// 				},
		// 				toolbar: {
		// 					display: true,
		// 					style: 'fixed',
		// 					position: 'below'
		// 				},
		// 				footer: {
		// 					display: true,
		// 					style: 'fixed',
		// 					position: 'below'
		// 				},
		// 				mode: 'fullwidth'
		// 			}
		// 		},
		// 		customScrollbars: true,
		// 		theme: {
		// 			main: 'defaultDark',
		// 			navbar: 'defaultDark',
		// 			toolbar: 'defaultDark',
		// 			footer: 'defaultDark'
		// 		}
		// 	},
		// 	shortcuts: ['calendar', 'mail', 'contacts']
		// }

		//dispatch(FuseActions.setDefaultSettings(user.data.settings));

		/*
        Set User Data
         */
		dispatch({
			type: SET_USER_DATA,
			payload: user
		});
	};
}

/**
 * Update User Settings
 */
export function updateUserSettings(settings) {
	return (dispatch, getState) => {
		const oldUser = getState().auth.user;
		const user = _.merge({}, oldUser, { data: { settings } });

		updateUserData(user, dispatch);

		return dispatch(setUserData(user));
	};
}

/**
 * Update User Shortcuts
 */
export function updateUserShortcuts(shortcuts) {
	return (dispatch, getState) => {
		const { user } = getState().auth;
		const newUser = {
			...user,
			data: {
				...user.data,
				shortcuts
			}
		};

		updateUserData(newUser, dispatch);

		return dispatch(setUserData(newUser));
	};
}

/**
 * Remove User Data
 */
export function removeUserData() {
	return {
		type: REMOVE_USER_DATA
	};
}

/**
 * Logout
 */
export function logoutUser() {
	return (dispatch, getState) => {
		const { user } = getState().auth;

		if (!user.role || user.role.length === 0) {
			// is guest
			return null;
		}

		history.push({
			pathname: '/'
		});

		jwtService.logout();
	
		dispatch(FuseActions.setInitialSettings());

		return dispatch({
			type: USER_LOGGED_OUT
		});
	};
}

/**
 * Update User Data
 */
function updateUserData(user, dispatch) {
	if (!user.role || user.role.length === 0) {
		// is guest
		return;
	}
	jwtService
		.updateUserData(user)
		.then(() => {
			dispatch(MessageActions.showMessage({ message: 'User data saved with api' }));
		})
		.catch(error => {
			dispatch(MessageActions.showMessage({ message: error.message }));
		});
}
