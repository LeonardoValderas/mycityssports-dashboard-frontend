/**
 * Authorization Roles
 */
const authRoles = {
	admin: ['admin', 'staff'],
	//staff: ['admin', 'staff'],
//	user: ['admin', 'staff', 'user'],
	user: ['user'],
	onlyGuest: []
};

export default authRoles;
